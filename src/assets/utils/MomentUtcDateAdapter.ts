import { Injectable, Optional, Inject } from "@angular/core";
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';

@Injectable()
export class MomentUtcDateAdapter extends MomentDateAdapter {

    constructor(@Optional() @Inject(MAT_DATE_LOCALE) dateLocale: string) {
        super(dateLocale);
    }

    createDate(year: number, month: number, date: number): Moment {
        if (month < 0 || month > 11) {
            throw Error(`Índice de mes no válido "${month}". El índice de mes debe estar entre 0 y 11.`);
        }
        if (date < 1) {
            throw Error(`Fecha no válida "${date}". La fecha debe ser mayor a 0`);
        }
        let result = moment.utc({ year, month, date }).locale(this.locale);
        if (!result.isValid()) {
            throw Error(`Fecha no válida "${date}" para un mes con índice "${month}".`);
        }
        return result;
    }
}