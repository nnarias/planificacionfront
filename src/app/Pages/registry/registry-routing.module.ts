import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'annex', loadChildren: () => import('./annex/annex.module').then(m => m.AnnexModule) },
  { path: 'call', loadChildren: () => import('./call/call.module').then(m => m.CallModule) },
  { path: 'college', loadChildren: () => import('./college/college.module').then(m => m.CollegeModule) },
  { path: 'externalInstitution', loadChildren: () => import('./external-institution/external-institution.module').then(m => m.ExternalInstitutionModule) },
  { path: 'knowledgeArea', loadChildren: () => import('./knowledge-area/knowledge-area.module').then(m => m.KnowledgeAreaModule) },
  { path: 'location', loadChildren: () => import('./location/location.module').then(m => m.LocationModule) },
  { path: 'project', loadChildren: () => import('./project/project.module').then(m => m.ProjectModule) },
  { path: 'purpose', loadChildren: () => import('./purpose/purpose.module').then(m => m.PurposeModule) },
  { path: 'research', loadChildren: () => import('./research/research.module').then(m => m.ResearchModule) },
  { path: 'staff', loadChildren: () => import('./staff/staff.module').then(m => m.StaffModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistryRoutingModule { }