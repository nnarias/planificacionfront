import { CollegeCareerComponent } from './collegeCareer/college-career/college-career.component';
import { CollegeCareerFormComponent } from './collegeCareer/college-career-form/college-career-form.component';
import { CollegeDepartmentComponent } from './collegeDepartment/college-department/college-department.component';
import { CollegeDepartmentFormComponent } from './collegeDepartment/college-department-form/college-department-form.component';
import { CollegeRoutingModule } from './college-routing.module';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { PostgraduateProgramComponent } from './postgraduateProgram/postgraduate-program/postgraduate-program.component';
import { PostgraduateProgramFormComponent } from './postgraduateProgram/postgraduate-program-form/postgraduate-program-form.component';
import { ProgramComponent } from './program/program/program.component';
import { ProgramFormComponent } from './program/program-form/program-form.component';

@NgModule({
  declarations: [CollegeCareerComponent, CollegeCareerFormComponent, CollegeDepartmentComponent, CollegeDepartmentFormComponent, MenuComponent, PostgraduateProgramComponent,
    PostgraduateProgramFormComponent, ProgramComponent, ProgramFormComponent],
  imports: [CommonModule, CollegeRoutingModule, ImportsModule]
})
export class CollegeModule { }