import { ActivatedRoute, Router } from '@angular/router';
import { CollegeCareer } from '../../model/collegeCareer';
import { CollegeCareerService } from '../../service/college-career.service';
import { CollegeDepartment } from '../../model/collegeDepartment';
import { CollegeDepartmentService } from '../../service/college-department.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-college-career-form',
  templateUrl: './college-career-form.component.html',
  styleUrls: ['./college-career-form.component.scss']
})
export class CollegeCareerFormComponent implements OnInit {
  private collegeDepartmentList: CollegeDepartment[] = [];
  private collegeCareer: CollegeCareer = new CollegeCareer();
  private form: FormGroup = this.formBuilder.group({
    collegeDepartmentId: ['', Validators.required],
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private collegeCareerService: CollegeCareerService, private collegeDepartmentService: CollegeDepartmentService,
    private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadCollegeCareer();
    this.loadCollegeDepartmentList();
  }

  loadCollegeCareer(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.collegeCareerService.find(id).subscribe(collegeCareer => {
          this.form.patchValue({
            collegeDepartmentId: collegeCareer.collegeDepartment.id,
            id: collegeCareer.id,
            name: collegeCareer.name
          });
        });
        this.title = 'Modificar Carrera';
      } else {
        this.title = 'Registrar Carrera';
      }
    });
  }

  loadCollegeDepartmentList(): void {
    this.collegeDepartmentService.findAll().subscribe(collegeDepartmentList => {
      this.collegeDepartmentList = collegeDepartmentList;
    });
  }

  save(): void {
    this.collegeCareer = this.form.value;
    this.collegeCareerService.save(this.collegeCareer).subscribe(() => {
      this.router.navigate(['app/registry/college/collegeCareer']);
      swal.fire(this.collegeCareer.id ? 'Carrera Actualizada' : 'Carrera Registrada', `${this.collegeCareer.name}`, 'success');
    });
  }

  public get collegeDepartmentId() { return this.form.get('collegeDepartmentId'); }

  public get getCollegeDepartmentList(): CollegeDepartment[] {
    return this.collegeDepartmentList;
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}