import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeCareerFormComponent } from './college-career-form.component';

describe('CollegeCareerFormComponent', () => {
  let component: CollegeCareerFormComponent;
  let fixture: ComponentFixture<CollegeCareerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollegeCareerFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeCareerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
