import { CollegeCareer } from '../../model/collegeCareer';
import { CollegeCareerService } from '../../service/college-career.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-college-career',
  templateUrl: './college-career.component.html',
  styleUrls: ['./college-career.component.scss']
})
export class CollegeCareerComponent implements OnInit {
  private collegeCareerList: CollegeCareer[] = [];
  private dataSource = new MatTableDataSource<CollegeCareer>();
  private displayedColumns: string[] = ['name', 'collegeDepartmentName', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private collegeCareerService: CollegeCareerService) { }

  ngOnInit(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'collegeDepartmentName': return item.collegeDepartment.name;
        default: return item[property];
      }
    }
    this.loadCollegeCareerList();
  }

  delete(collegeCareer: CollegeCareer): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Carrera: ${collegeCareer.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.collegeCareerService.delete(collegeCareer.id).subscribe(() => {
          this.collegeCareerList = this.collegeCareerList.filter(result => result !== collegeCareer);
          this.dataSource.data = this.collegeCareerList;
          swal.fire('Carrera Eliminada', `${collegeCareer.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadCollegeCareerList(): void {
    this.collegeCareerService.findAll().subscribe(collegeCareerList => {
      this.collegeCareerList = collegeCareerList;
      this.dataSource.data = collegeCareerList;
    });
  }

  public get getDataSource(): MatTableDataSource<CollegeCareer> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getCollegeCareerList(): CollegeCareer[] {
    return this.collegeCareerList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}