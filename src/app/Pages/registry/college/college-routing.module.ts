import { AuthGuard } from '../../security/guard/auth.guard';
import { CollegeCareerComponent } from './collegeCareer/college-career/college-career.component';
import { CollegeCareerFormComponent } from './collegeCareer/college-career-form/college-career-form.component';
import { CollegeDepartmentComponent } from './collegeDepartment/college-department/college-department.component';
import { CollegeDepartmentFormComponent } from './collegeDepartment/college-department-form/college-department-form.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { PostgraduateProgramComponent } from './postgraduateProgram/postgraduate-program/postgraduate-program.component';
import { PostgraduateProgramFormComponent } from './postgraduateProgram/postgraduate-program-form/postgraduate-program-form.component';
import { ProgramComponent } from './program/program/program.component';
import { ProgramFormComponent } from './program/program-form/program-form.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'collegeCareer', component: CollegeCareerComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'collegeCareer/form', component: CollegeCareerFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'collegeCareer/form/:id', component: CollegeCareerFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'collegeDepartment', component: CollegeDepartmentComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'collegeDepartment/form', component: CollegeDepartmentFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'collegeDepartment/form/:id', component: CollegeDepartmentFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'postgraduateProgram', component: PostgraduateProgramComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'postgraduateProgram/form', component: PostgraduateProgramFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'postgraduateProgram/form/:id', component: PostgraduateProgramFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'program', component: ProgramComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'program/form', component: ProgramFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'program/form/:id', component: ProgramFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollegeRoutingModule { }