import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PostgraduateProgram } from '../../model/postgraduateProgram';
import { PostgraduateProgramService } from '../../service/postgraduate-program.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-postgraduate-program',
  templateUrl: './postgraduate-program.component.html',
  styleUrls: ['./postgraduate-program.component.scss']
})
export class PostgraduateProgramComponent implements OnInit {
  private dataSource = new MatTableDataSource<PostgraduateProgram>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private postgraduateProgramList: PostgraduateProgram[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private postgraduateProgramService: PostgraduateProgramService) { }

  ngOnInit(): void {
    this.loadPostgraduateProgramList();
  }

  delete(postgraduateProgram: PostgraduateProgram): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Programa de Postgrado: ${postgraduateProgram.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.postgraduateProgramService.delete(postgraduateProgram.id).subscribe(() => {
          this.postgraduateProgramList = this.postgraduateProgramList.filter(result => result !== postgraduateProgram);
          this.dataSource.data = this.postgraduateProgramList;
          swal.fire('Programa de Postgrado Eliminado', `${postgraduateProgram.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadPostgraduateProgramList(): void {
    this.postgraduateProgramService.findAll().subscribe(postgraduateProgramList => {
      this.dataSource.data = postgraduateProgramList;
      this.postgraduateProgramList = postgraduateProgramList;
    });
  }

  public get getDataSource(): MatTableDataSource<PostgraduateProgram> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getPostgraduateProgramList(): PostgraduateProgram[] {
    return this.postgraduateProgramList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}