import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostgraduateProgramFormComponent } from './postgraduate-program-form.component';

describe('PostgraduateProgramFormComponent', () => {
  let component: PostgraduateProgramFormComponent;
  let fixture: ComponentFixture<PostgraduateProgramFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostgraduateProgramFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostgraduateProgramFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
