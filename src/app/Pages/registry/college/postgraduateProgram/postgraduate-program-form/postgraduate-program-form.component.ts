import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostgraduateProgram } from '../../model/postgraduateProgram';
import { PostgraduateProgramService } from '../../service/postgraduate-program.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-postgraduate-program-form',
  templateUrl: './postgraduate-program-form.component.html',
  styleUrls: ['./postgraduate-program-form.component.scss']
})
export class PostgraduateProgramFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private postgraduateProgram: PostgraduateProgram = new PostgraduateProgram();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private postgraduateProgramService: PostgraduateProgramService, private router: Router) { }

  ngOnInit(): void {
    this.loadPostgraduate();
  }

  loadPostgraduate(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.postgraduateProgramService.find(id).subscribe(postgraduateProgram => {
          this.form.patchValue({
            id: postgraduateProgram.id,
            name: postgraduateProgram.name
          });
        });
        this.title = 'Modificar Programa Postgrado';
      } else {
        this.title = 'Registrar Programa Postgrado';
      }
    });
  }

  save(): void {
    this.postgraduateProgram = this.form.value;
    this.postgraduateProgramService.save(this.postgraduateProgram).subscribe(() => {
      this.router.navigate(['app/registry/college/postgraduateProgram']);
      swal.fire(this.postgraduateProgram.id ? 'Programa de Postgrado Actualizado' : 'Programa de Postgrado Registrado', `${this.postgraduateProgram.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}