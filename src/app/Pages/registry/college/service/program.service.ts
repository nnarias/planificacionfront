import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Program } from '../model/program';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  private urlEndPoint: string = `${environment.apiUrl}/program`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Program> {
    return this.http.patch<Program>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<Program> {
    return this.http.get<Program>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Program[]> {
    return this.http.get<Program[]>(this.urlEndPoint);
  }

  save(program: Program): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, program);
  }
}