import { CollegeCareer } from '../model/collegeCareer';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CollegeCareerService {

  private urlEndPoint: string = `${environment.apiUrl}/collegeCareer`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<CollegeCareer> {
    return this.http.patch<CollegeCareer>(`${this.urlEndPoint}/${id}`, null);
  }


  find(id: number): Observable<CollegeCareer> {
    return this.http.get<CollegeCareer>(`${this.urlEndPoint}/${id}`);
  }


  findAll(): Observable<CollegeCareer[]> {
    return this.http.get<CollegeCareer[]>(this.urlEndPoint);
  }

  save(collegeCareer: CollegeCareer): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, collegeCareer);
  }
}