import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostgraduateProgram } from '../model/postgraduateProgram';

@Injectable({
  providedIn: 'root'
})
export class PostgraduateProgramService {

  private urlEndPoint: string = `${environment.apiUrl}/postgraduateProgram`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<PostgraduateProgram> {
    return this.http.patch<PostgraduateProgram>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<PostgraduateProgram> {
    return this.http.get<PostgraduateProgram>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<PostgraduateProgram[]> {
    return this.http.get<PostgraduateProgram[]>(this.urlEndPoint);
  }

  save(postgraduateProgram: PostgraduateProgram): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, postgraduateProgram);
  }
}