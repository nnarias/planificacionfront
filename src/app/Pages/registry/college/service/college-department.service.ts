import { CollegeDepartment } from '../model/collegeDepartment';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CollegeDepartmentService {
  private urlEndPoint: string = `${environment.apiUrl}/collegeDepartment`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<CollegeDepartment> {
    return this.http.patch<CollegeDepartment>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<CollegeDepartment> {
    return this.http.get<CollegeDepartment>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<CollegeDepartment[]> {
    return this.http.get<CollegeDepartment[]>(this.urlEndPoint);
  }

  save(collegeDepartment: CollegeDepartment): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, collegeDepartment);
  }
}