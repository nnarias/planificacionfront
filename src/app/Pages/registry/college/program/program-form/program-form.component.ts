import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Program } from '../../model/program';
import { ProgramService } from '../../service/program.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-program-form',
  templateUrl: './program-form.component.html',
  styleUrls: ['./program-form.component.scss']
})
export class ProgramFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private program: Program = new Program();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private programService: ProgramService, private router: Router) { }

  ngOnInit(): void {
    this.loadProgram();
  }

  loadProgram(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.programService.find(id).subscribe(Program => {
          this.form.patchValue({
            id: Program.id,
            name: Program.name
          });
        });
        this.title = 'Modificar Programa';
      } else {
        this.title = 'Registrar Programa';
      }
    });
  }

  save(): void {
    this.program = this.form.value;
    this.programService.save(this.program).subscribe(() => {
      this.router.navigate(['app/registry/college/program']);
      swal.fire(this.program.id ? 'Programa de Postgrado Actualizado' : 'Programa de Postgrado Registrado', `${this.program.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}