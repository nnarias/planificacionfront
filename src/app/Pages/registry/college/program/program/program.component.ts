import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Program } from '../../model/program';
import { ProgramService } from '../../service/program.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {
  private dataSource = new MatTableDataSource<Program>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private programList: Program[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private programService: ProgramService) { }

  ngOnInit(): void {
    this.loadProgramDataList();
  }

  delete(program: Program): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Prograna: ${program.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.programService.delete(program.id).subscribe(() => {
          this.programList = this.programList.filter(result => result !== program);
          this.dataSource.data = this.programList;
          swal.fire('Programa Eliminado', `${program.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadProgramDataList(): void {
    this.programService.findAll().subscribe(programList => {
      this.dataSource.data = programList;
      this.programList = programList;
    });
  }

  public get getDataSource(): MatTableDataSource<Program> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getProgramList(): Program[] {
    return this.programList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}