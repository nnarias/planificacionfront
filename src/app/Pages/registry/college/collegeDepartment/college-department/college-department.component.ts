import { CollegeDepartment } from '../../model/collegeDepartment';
import { CollegeDepartmentService } from '../../service/college-department.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-college-department',
  templateUrl: './college-department.component.html',
  styleUrls: ['./college-department.component.scss']
})
export class CollegeDepartmentComponent implements OnInit {
  private CollegeDepartmentList: CollegeDepartment[] = [];
  private dataSource = new MatTableDataSource<CollegeDepartment>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private collegeDepartmentService: CollegeDepartmentService) { }

  ngOnInit(): void {
    this.loadCollegeDepartmentList();
  }

  delete(collegeDepartment: CollegeDepartment): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el departamento/centro: ${collegeDepartment.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.collegeDepartmentService.delete(collegeDepartment.id).subscribe(() => {
          this.CollegeDepartmentList = this.CollegeDepartmentList.filter(result => result !== collegeDepartment);
          this.dataSource.data = this.CollegeDepartmentList;
          swal.fire('Departamento/Centro Eliminado', `${collegeDepartment.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadCollegeDepartmentList(): void {
    this.collegeDepartmentService.findAll().subscribe(collegeDepartment => {
      this.CollegeDepartmentList = collegeDepartment;
      this.dataSource.data = collegeDepartment;
    });
  }

  public get getDataSource(): MatTableDataSource<CollegeDepartment> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getCollegeDepartmentList(): CollegeDepartment[] {
    return this.CollegeDepartmentList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}