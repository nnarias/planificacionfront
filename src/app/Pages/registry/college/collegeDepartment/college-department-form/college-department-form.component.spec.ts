import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeDepartmentFormComponent } from './college-department-form.component';

describe('CollegeDepartmentFormComponent', () => {
  let component: CollegeDepartmentFormComponent;
  let fixture: ComponentFixture<CollegeDepartmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollegeDepartmentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeDepartmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
