import { ActivatedRoute, Router } from '@angular/router';
import { CollegeDepartment } from '../../model/collegeDepartment';
import { CollegeDepartmentService } from '../../service/college-department.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-college-department-form',
  templateUrl: './college-department-form.component.html',
  styleUrls: ['./college-department-form.component.scss']
})
export class CollegeDepartmentFormComponent implements OnInit {
  private collegeDepartment: CollegeDepartment = new CollegeDepartment();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private collegeDepartmentService: CollegeDepartmentService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadCollegeDepartment();
  }

  loadCollegeDepartment(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.collegeDepartmentService.find(id).subscribe(collegeDepartment => {
          this.form.patchValue({
            id: collegeDepartment.id,
            name: collegeDepartment.name
          });
        });
        this.title = 'Modificar Departamento/Centro';
      } else {
        this.title = 'Registrar Departamento/Centro';
      }
    });
  }

  save(): void {
    this.collegeDepartment = this.form.value;
    this.collegeDepartmentService.save(this.collegeDepartment).subscribe(() => {
      this.router.navigate(['app/registry/college/collegeDepartment']);
      swal.fire(this.collegeDepartment.id ? 'Departamento/Centro Actualizado' : 'Departamento/Centro Registrado', `${this.collegeDepartment.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}