export class CollegeDepartment {
    id: number;
    name: string;
    state: string;
}