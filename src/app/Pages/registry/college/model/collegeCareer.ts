import { CollegeDepartment } from './collegeDepartment';

export class CollegeCareer {
    collegeDepartment: CollegeDepartment = new CollegeDepartment();
    collegeDepartmentId: number;
    id: number;
    name: string;
}