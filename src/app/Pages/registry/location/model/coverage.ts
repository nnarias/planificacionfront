import { CoverageType } from "./coverageType";

export class Coverage {
    colspan: number;
    coverageType: CoverageType;
    id: number;
    name: string;
}