import { Component, OnInit, ViewChild } from '@angular/core';
import { Coverage } from '../../model/coverage';
import { CoverageService } from '../../service/coverage.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-coverage',
  templateUrl: './coverage.component.html',
  styleUrls: ['./coverage.component.scss']
})
export class CoverageComponent implements OnInit {
  private coverageList: Coverage[] = [];
  private dataSource = new MatTableDataSource<Coverage>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private coverageService: CoverageService) { }

  ngOnInit(): void {
    this.loadCoverageList();
  }

  delete(coverage: Coverage): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Cobertura: ${coverage.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.coverageService.delete(coverage.id).subscribe(() => {
          this.coverageList = this.coverageList.filter(result => result !== coverage);
          this.dataSource.data = this.coverageList;
          swal.fire('Cobertura Eliminada', `${coverage.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadCoverageList(): void {
    this.coverageService.findAll().subscribe(coverageList => {
      this.coverageList = coverageList;
      this.dataSource.data = coverageList;
    });
  }

  public get getCoverageList(): Coverage[] {
    return this.coverageList;
  }

  public get getDataSource(): MatTableDataSource<Coverage> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}