import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverageFormComponent } from './coverage-form.component';

describe('CoverageFormComponent', () => {
  let component: CoverageFormComponent;
  let fixture: ComponentFixture<CoverageFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoverageFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
