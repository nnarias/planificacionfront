import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Coverage } from '../../model/coverage';
import { CoverageType } from '../../model/coverageType';
import { CoverageTypeService } from '../../service/coverage-type.service';
import { CoverageService } from '../../service/coverage.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-coverage-form',
  templateUrl: './coverage-form.component.html',
  styleUrls: ['./coverage-form.component.scss']
})
export class CoverageFormComponent implements OnInit {
  private coverage: Coverage = new Coverage();
  private coverageTypeList: CoverageType[] = [];
  private form: FormGroup = this.formBuilder.group({
    coverageTypeId: ['', Validators.required],
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private coverageService: CoverageService, private coverageTypeService: CoverageTypeService,
    private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadCoverageTypeList();
    this.loadCoverage();
  }

  loadCoverage(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.coverageService.find(id).subscribe(coverage => {
          this.form.patchValue({
            coverageTypeId: coverage.coverageType.id,
            id: coverage.id,
            name: coverage.name
          });
        });
        this.title = 'Modificar Cobertura';
      } else {
        this.title = 'Crear Cobertura';
      }
    });
  }

  loadCoverageTypeList(): void {
    this.coverageTypeService.findAll().subscribe(coverageTypeList => {
      this.coverageTypeList = coverageTypeList;
    });
  }

  save(): void {
    this.coverage = this.form.value;
    this.coverageService.save(this.coverage).subscribe(() => {
      this.router.navigate(['app/registry/location/coverage']);
      swal.fire(this.coverage.id ? 'Cobertura Actualizada' : 'Nueva Cobertura', `${this.coverage.name}`, 'success');
    });
  }

  public get coverageTypeId() { return this.form.get('coverageTypeId'); }

  public get getCoverageTypeList(): CoverageType[] {
    return this.coverageTypeList;
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}