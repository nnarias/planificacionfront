import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CoverageType } from '../model/coverageType';

@Injectable({
  providedIn: 'root'
})
export class CoverageTypeService {
  private urlEndPoint: string = `${environment.apiUrl}/coverageType`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<CoverageType> {
    return this.http.patch<CoverageType>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<CoverageType> {
    return this.http.get<CoverageType>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<CoverageType[]> {
    return this.http.get<CoverageType[]>(this.urlEndPoint);
  }

  save(coverageType: CoverageType): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, coverageType);
  }
}