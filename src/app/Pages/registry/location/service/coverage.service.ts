import { Coverage } from '../model/coverage';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoverageService {
  private urlEndPoint: string = `${environment.apiUrl}/coverage`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Coverage> {
    return this.http.patch<Coverage>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<Coverage> {
    return this.http.get<Coverage>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Coverage[]> {
    return this.http.get<Coverage[]>(this.urlEndPoint);
  }

  save(coverage: Coverage): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, coverage);
  }
}