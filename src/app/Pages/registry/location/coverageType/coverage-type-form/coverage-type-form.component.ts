import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CoverageType } from '../../model/coverageType';
import { CoverageTypeService } from '../../service/coverage-type.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-coverage-type-form',
  templateUrl: './coverage-type-form.component.html',
  styleUrls: ['./coverage-type-form.component.scss']
})
export class CoverageTypeFormComponent implements OnInit {
  private coverageType: CoverageType = new CoverageType();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private coverageTypeService: CoverageTypeService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadCoverageType();
  }

  loadCoverageType(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.coverageTypeService.find(id).subscribe(coverageType => {
          this.form.patchValue({
            id: coverageType.id,
            name: coverageType.name
          });
        });
        this.title = 'Modificar Tipo de Cobertura';
      } else {
        this.title = 'Crear Tipo de Cobertura';
      }
    });
  }

  save(): void {
    this.coverageType = this.form.value;
    this.coverageTypeService.save(this.coverageType).subscribe(() => {
      this.router.navigate(['app/registry/location/coverageType']);
      swal.fire(this.coverageType.id ? 'Tipo de Cobertura Actualizado' : 'Nuevo Tipo de Cobertura', `${this.coverageType.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}