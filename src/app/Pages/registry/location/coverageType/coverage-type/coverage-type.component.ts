import { Component, OnInit, ViewChild } from '@angular/core';
import { CoverageType } from '../../model/coverageType';
import { CoverageTypeService } from '../../service/coverage-type.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-coverage-type',
  templateUrl: './coverage-type.component.html',
  styleUrls: ['./coverage-type.component.scss']
})
export class CoverageTypeComponent implements OnInit {
  private coverageTypeList: CoverageType[] = [];
  private dataSource = new MatTableDataSource<CoverageType>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private coverageTypeService: CoverageTypeService) { }

  ngOnInit(): void {
    this.loadCoverageTypeList();
  }

  delete(coverageType: CoverageType): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el tipo de covertura: ${coverageType.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.coverageTypeService.delete(coverageType.id).subscribe(() => {
          this.coverageTypeList = this.coverageTypeList.filter(result => result !== coverageType);
          this.dataSource.data = this.coverageTypeList;
          swal.fire('Tipo de covertira Eliminado', `${coverageType.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadCoverageTypeList(): void {
    this.coverageTypeService.findAll().subscribe(coverageTypeList => {
      this.coverageTypeList = coverageTypeList;
      this.dataSource.data = coverageTypeList;
    });
  }

  public get getCoverageTypeList(): CoverageType[] {
    return this.coverageTypeList;
  }

  public get getDataSource(): MatTableDataSource<CoverageType> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}