import { CommonModule } from '@angular/common';
import { CoverageComponent } from './coverage/coverage/coverage.component';
import { CoverageFormComponent } from './coverage/coverage-form/coverage-form.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { LocationRoutingModule } from './location-routing.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { CoverageTypeComponent } from './coverageType/coverage-type/coverage-type.component';
import { CoverageTypeFormComponent } from './coverageType/coverage-type-form/coverage-type-form.component';

@NgModule({
  declarations: [CoverageComponent, CoverageFormComponent, MenuComponent, CoverageTypeComponent, CoverageTypeFormComponent],
  imports: [CommonModule, ImportsModule, LocationRoutingModule]
})
export class LocationModule { }