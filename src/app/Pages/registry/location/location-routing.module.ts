import { AuthGuard } from '../../security/guard/auth.guard';
import { CoverageComponent } from './coverage/coverage/coverage.component';
import { CoverageFormComponent } from './coverage/coverage-form/coverage-form.component';
import { CoverageTypeComponent } from './coverageType/coverage-type/coverage-type.component';
import { CoverageTypeFormComponent } from './coverageType/coverage-type-form/coverage-type-form.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'coverage', component: CoverageComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'coverage/form', component: CoverageFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'coverage/form/:id', component: CoverageFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'coverageType', component: CoverageTypeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'coverageType/form', component: CoverageTypeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'coverageType/form/:id', component: CoverageTypeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationRoutingModule { }