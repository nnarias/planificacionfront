import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EconomicPartnerGoalFormComponent } from './economic-partner-goal-form.component';

describe('EconomicPartnerGoalFormComponent', () => {
  let component: EconomicPartnerGoalFormComponent;
  let fixture: ComponentFixture<EconomicPartnerGoalFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EconomicPartnerGoalFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomicPartnerGoalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
