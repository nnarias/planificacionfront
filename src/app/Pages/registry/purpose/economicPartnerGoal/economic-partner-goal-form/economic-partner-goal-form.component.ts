import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EconomicPartnerGoal } from '../../model/economicPartnerGoal';
import { EconomicPartnerGoalService } from '../../service/economic-partner-goal.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-economic-partner-goal-form',
  templateUrl: './economic-partner-goal-form.component.html',
  styleUrls: ['./economic-partner-goal-form.component.scss']
})
export class EconomicPartnerGoalFormComponent implements OnInit {
  private economicPartnerGoal: EconomicPartnerGoal = new EconomicPartnerGoal();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required],
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private economicPartnerGoalService: EconomicPartnerGoalService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadEconomicPartnerGoal();
  }

  loadEconomicPartnerGoal(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.economicPartnerGoalService.find(id).subscribe(economicPartnerGoal => {
          this.form.patchValue({
            id: economicPartnerGoal.id,
            name: economicPartnerGoal.name
          });
        });
        this.title = 'Modificar Objetivo Socio Económico';
      } else {
        this.title = 'Crear Objetivo Socio Económico';
      }
    });
  }

  save(): void {
    this.economicPartnerGoal = this.form.value;
    this.economicPartnerGoalService.save(this.economicPartnerGoal).subscribe(() => {
      this.router.navigate(['app/registry/purpose/economicPartnerGoal']);
      swal.fire(this.economicPartnerGoal.id ? 'Objetivo Socio Económico Actualizado' : 'Nuevo Objetivo Socio Económico', `${this.economicPartnerGoal.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}