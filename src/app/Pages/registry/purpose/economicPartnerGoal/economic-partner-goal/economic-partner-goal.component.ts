import { Component, OnInit, ViewChild } from '@angular/core';
import { EconomicPartnerGoal } from '../../model/economicPartnerGoal';
import { EconomicPartnerGoalService } from '../../service/economic-partner-goal.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-economic-partner-goal',
  templateUrl: './economic-partner-goal.component.html',
  styleUrls: ['./economic-partner-goal.component.scss']
})
export class EconomicPartnerGoalComponent implements OnInit {
  private dataSource = new MatTableDataSource<EconomicPartnerGoal>();
  private displayedColumns: string[] = ['name', 'actions'];
  private economicPartnerGoalList: EconomicPartnerGoal[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private economicPartnerGoalService: EconomicPartnerGoalService) { }

  ngOnInit(): void {
    this.loadEconomicPartnerGoalList();
  }

  delete(economicPartnerGoal: EconomicPartnerGoal): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Objetivo Socio Económico: ${economicPartnerGoal.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.economicPartnerGoalService.delete(economicPartnerGoal.id).subscribe(() => {
          this.economicPartnerGoalList = this.economicPartnerGoalList.filter(result => result !== economicPartnerGoal);
          this.dataSource.data = this.economicPartnerGoalList;
          swal.fire('Objetivo Socio Económico Eliminado', `${economicPartnerGoal.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadEconomicPartnerGoalList(): void {
    this.economicPartnerGoalService.findAll().subscribe(economicPartnerGoalList => {
      this.dataSource.data = economicPartnerGoalList;
      this.economicPartnerGoalList = economicPartnerGoalList;
    });
  }

  public get getDataSource(): MatTableDataSource<EconomicPartnerGoal> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getEconomicPartnerGoalList(): EconomicPartnerGoal[] {
    return this.economicPartnerGoalList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}