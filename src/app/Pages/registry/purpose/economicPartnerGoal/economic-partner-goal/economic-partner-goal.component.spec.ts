import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EconomicPartnerGoalComponent } from './economic-partner-goal.component';

describe('EconomicPartnerGoalComponent', () => {
  let component: EconomicPartnerGoalComponent;
  let fixture: ComponentFixture<EconomicPartnerGoalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EconomicPartnerGoalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomicPartnerGoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
