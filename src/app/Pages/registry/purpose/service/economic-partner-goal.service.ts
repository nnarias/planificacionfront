import { EconomicPartnerGoal } from '../model/economicPartnerGoal';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EconomicPartnerGoalService {

  private urlEndPoint: string = `${environment.apiUrl}/economicPartnerGoal`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<EconomicPartnerGoal> {
    return this.http.patch<EconomicPartnerGoal>(`${this.urlEndPoint}/${id}`,null);
  }

  find(id: number): Observable<EconomicPartnerGoal> {
    return this.http.get<EconomicPartnerGoal>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<EconomicPartnerGoal[]> {
    return this.http.get<EconomicPartnerGoal[]>(this.urlEndPoint);
  }

  save(economicPartnerGoal: EconomicPartnerGoal): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, economicPartnerGoal);
  }
}