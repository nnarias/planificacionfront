import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PolicyPlan } from '../model/policyPlan';

@Injectable({
  providedIn: 'root'
})
export class PolicyPlanService {

  private urlEndPoint: string = `${environment.apiUrl}/policyPlan`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<PolicyPlan> {
    return this.http.patch<PolicyPlan>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<PolicyPlan> {
    return this.http.get<PolicyPlan>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<PolicyPlan[]> {
    return this.http.get<PolicyPlan[]>(this.urlEndPoint);
  }

  findByGoalPlan(goalPlanid: number): Observable<PolicyPlan[]> {
    return this.http.get<PolicyPlan[]>(`${this.urlEndPoint}/goalPlan/${goalPlanid}`);
  }

  save(policyPlan: PolicyPlan): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, policyPlan);
  }
}