import { environment } from 'src/environments/environment';
import { GoalPlan } from '../model/goalPlan';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GoalPlanService {

  private urlEndPoint: string = `${environment.apiUrl}/goalPlan`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<GoalPlan> {
    return this.http.patch<GoalPlan>(`${this.urlEndPoint}/${id}`,null);
  }

  find(id: number): Observable<GoalPlan> {
    return this.http.get<GoalPlan>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<GoalPlan[]> {
    return this.http.get<GoalPlan[]>(this.urlEndPoint);
  }

  save(goalPlan: GoalPlan): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, goalPlan);
  }
}