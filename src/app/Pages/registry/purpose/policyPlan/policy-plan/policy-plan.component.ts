import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PolicyPlan } from '../../model/policyPlan';
import { PolicyPlanService } from '../../service/policy-plan.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-policy-plan',
  templateUrl: './policy-plan.component.html',
  styleUrls: ['./policy-plan.component.scss']
})
export class PolicyPlanComponent implements OnInit {
  private dataSource = new MatTableDataSource<PolicyPlan>();
  private displayedColumns: string[] = ['name', 'goalPlanName', 'actions'];
  private policyPlanList: PolicyPlan[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private policyPlanService: PolicyPlanService) { }

  ngOnInit(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'goalPlanName': return item.goalPlan.name;
        default: return item[property];
      }
    }
    this.loadPolicyPlanList();
  }

  delete(policyPlan: PolicyPlan): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Política del Plan de Desarrollo: ${policyPlan.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.policyPlanService.delete(policyPlan.id).subscribe(() => {
          this.policyPlanList = this.policyPlanList.filter(result => result !== policyPlan);
          this.dataSource.data = this.policyPlanList;
          swal.fire('Política del Plan de Desarrollo eliminada', `${policyPlan.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadPolicyPlanList(): void {
    this.policyPlanService.findAll().subscribe(policyPlanList => {
      this.dataSource.data = policyPlanList;
      this.policyPlanList = policyPlanList;
    });
  }

  public get getDataSource(): MatTableDataSource<PolicyPlan> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getPolicyPlanList(): PolicyPlan[] {
    return this.policyPlanList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}