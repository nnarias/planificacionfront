import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyPlanComponent } from './policy-plan.component';

describe('PolicyPlanComponent', () => {
  let component: PolicyPlanComponent;
  let fixture: ComponentFixture<PolicyPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolicyPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
