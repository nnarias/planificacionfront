import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyPlanFormComponent } from './policy-plan-form.component';

describe('PolicyPlanFormComponent', () => {
  let component: PolicyPlanFormComponent;
  let fixture: ComponentFixture<PolicyPlanFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolicyPlanFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyPlanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
