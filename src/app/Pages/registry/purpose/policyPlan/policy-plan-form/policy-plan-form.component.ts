import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GoalPlan } from '../../model/goalPlan';
import { GoalPlanService } from '../../service/goal-plan.service';
import { PolicyPlan } from '../../model/policyPlan';
import { PolicyPlanService } from '../../service/policy-plan.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-policy-plan-form',
  templateUrl: './policy-plan-form.component.html',
  styleUrls: ['./policy-plan-form.component.scss']
})
export class PolicyPlanFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    goalPlanId: ['', Validators.required],
    id: [''],
    name: ['', Validators.required],
  });
  private goalPlanList: GoalPlan[] = [];
  private policyPlan: PolicyPlan = new PolicyPlan();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private goalPlanService: GoalPlanService, private policyPlanService: PolicyPlanService,
    private router: Router) { }

  ngOnInit(): void {
    this.loadGoalPlanList();
    this.loadPolicyPlan();
  }

  loadGoalPlanList(): void {
    this.goalPlanService.findAll().subscribe(goalPlanList => {
      this.goalPlanList = goalPlanList;
    });
  }

  loadPolicyPlan(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.policyPlanService.find(id).subscribe(policyPlan => {
          this.form.patchValue({
            goalPlanId: policyPlan.goalPlan.id,
            id: policyPlan.id,
            name: policyPlan.name
          });
        });
        this.title = 'Modificar Política del Plan de Desarrollo';
      } else {
        this.title = 'Registrar Política del Plan de Desarrollo';
      }
    });
  }

  save(): void {
    this.policyPlan = this.form.value;
    this.policyPlanService.save(this.policyPlan).subscribe(() => {
      this.router.navigate(['app/registry/purpose/policyPlan']);
      swal.fire(this.policyPlan.id ? 'Política del Plan de Desarrollo Actualizada' : 'Nueva Política del Plan de Desarrollo', `${this.policyPlan.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getGoalPlanList(): GoalPlan[] {
    return this.goalPlanList;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get goalPlanId() { return this.form.get('goalPlanId'); }
  public get name() { return this.form.get('name'); }
}