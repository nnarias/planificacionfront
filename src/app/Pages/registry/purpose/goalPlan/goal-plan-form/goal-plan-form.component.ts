import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GoalPlan } from '../../model/goalPlan';
import { GoalPlanService } from '../../service/goal-plan.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-goal-plan-form',
  templateUrl: './goal-plan-form.component.html',
  styleUrls: ['./goal-plan-form.component.scss']
})
export class GoalPlanFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required],
  });
  private goalPlan: GoalPlan = new GoalPlan();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private goalPlanService: GoalPlanService, private router: Router) { }

  ngOnInit(): void {
    this.loadGoalPlan();
  }

  loadGoalPlan(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.goalPlanService.find(id).subscribe(goalPlan => {
          this.form.patchValue({
            id: goalPlan.id,
            name: goalPlan.name
          });
        });
        this.title = 'Modificar Objetivo del Plan de Desarrollo Nacional Toda una vida';
      } else {
        this.title = 'Crear Objetivo del Plan de Desarrollo Nacional Toda una vida';
      }
    });
  }

  save(): void {
    this.goalPlan = this.form.value;
    this.goalPlanService.save(this.goalPlan).subscribe(() => {
      this.router.navigate(['app/registry/purpose/goalPlan']);
      swal.fire(this.goalPlan.id ? 'Objetivo del Plan de Desarrollo Nacional Toda una vida Actualizado' : 'Nuevo Objetivo del Plan de Desarrollo Nacional Toda una vida', 
      `${this.goalPlan.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}