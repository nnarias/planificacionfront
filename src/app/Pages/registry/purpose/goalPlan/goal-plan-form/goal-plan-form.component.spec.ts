import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalPlanFormComponent } from './goal-plan-form.component';

describe('GoalPlanFormComponent', () => {
  let component: GoalPlanFormComponent;
  let fixture: ComponentFixture<GoalPlanFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalPlanFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoalPlanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
