import { Component, OnInit, ViewChild } from '@angular/core';
import { GoalPlan } from '../../model/goalPlan';
import { GoalPlanService } from '../../service/goal-plan.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-goal-plan',
  templateUrl: './goal-plan.component.html',
  styleUrls: ['./goal-plan.component.scss']
})
export class GoalPlanComponent implements OnInit {
  private dataSource = new MatTableDataSource<GoalPlan>();
  private displayedColumns: string[] = ['name', 'actions'];
  private goalPlanList: GoalPlan[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private goalPlanService: GoalPlanService) { }

  ngOnInit(): void {
    this.loadGoalPlanList();
  }

  delete(goalPlan: GoalPlan): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Objetivo del Plan de Desarrollo: ${goalPlan.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.goalPlanService.delete(goalPlan.id).subscribe(() => {
          this.goalPlanList = this.goalPlanList.filter(result => result !== goalPlan);
          this.dataSource.data = this.goalPlanList;
          swal.fire('Objetivo del Plan de Desarrollo Eliminado', `${goalPlan.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadGoalPlanList(): void {
    this.goalPlanService.findAll().subscribe(goalPlanList => {
      this.dataSource.data = goalPlanList;
      this.goalPlanList = goalPlanList;
    });
  }


  public get getDataSource(): MatTableDataSource<GoalPlan> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getGoalPlanList(): GoalPlan[] {
    return this.goalPlanList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}