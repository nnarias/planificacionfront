import { GoalPlan } from './goalPlan';

export class PolicyPlan {
    id: number;
    goalPlan: GoalPlan = new GoalPlan();
    name: string;
    state: boolean;
}