export class EconomicPartnerGoal {
    id: number;
    name: string;
    state: boolean;
}  