export class GoalPlan {
    id: number;
    name: string;
    state: boolean;
}  