import { AuthGuard } from '../../security/guard/auth.guard';
import { EconomicPartnerGoalComponent } from './economicPartnerGoal/economic-partner-goal/economic-partner-goal.component';
import { EconomicPartnerGoalFormComponent } from './economicPartnerGoal/economic-partner-goal-form/economic-partner-goal-form.component';
import { GoalPlanComponent } from './goalPlan/goal-plan/goal-plan.component';
import { GoalPlanFormComponent } from './goalPlan/goal-plan-form/goal-plan-form.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { PolicyPlanComponent } from './policyPlan/policy-plan/policy-plan.component';
import { PolicyPlanFormComponent } from './policyPlan/policy-plan-form/policy-plan-form.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'economicPartnerGoal', component: EconomicPartnerGoalComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'economicPartnerGoal/form', component: EconomicPartnerGoalFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'economicPartnerGoal/form/:id', component: EconomicPartnerGoalFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'goalPlan', component: GoalPlanComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'goalPlan/form', component: GoalPlanFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'goalPlan/form/:id', component: GoalPlanFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'policyPlan', component: PolicyPlanComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'policyPlan/form', component: PolicyPlanFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'policyPlan/form/:id', component: PolicyPlanFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurposeRoutingModule { }