import { CommonModule } from '@angular/common';
import { EconomicPartnerGoalComponent } from './economicPartnerGoal/economic-partner-goal/economic-partner-goal.component';
import { EconomicPartnerGoalFormComponent } from './economicPartnerGoal/economic-partner-goal-form/economic-partner-goal-form.component';
import { GoalPlanComponent } from './goalPlan/goal-plan/goal-plan.component';
import { GoalPlanFormComponent } from './goalPlan/goal-plan-form/goal-plan-form.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { PolicyPlanComponent } from './policyPlan/policy-plan/policy-plan.component';
import { PolicyPlanFormComponent } from './policyPlan/policy-plan-form/policy-plan-form.component';
import { PurposeRoutingModule } from './purpose-routing.module';

@NgModule({
  declarations: [EconomicPartnerGoalComponent, EconomicPartnerGoalFormComponent, GoalPlanComponent, GoalPlanFormComponent, MenuComponent, PolicyPlanComponent, PolicyPlanFormComponent],
  imports: [CommonModule, ImportsModule, PurposeRoutingModule]
})
export class PurposeModule { }