import { AuthGuard } from '../../security/guard/auth.guard';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectComponent } from './project/project/project.component';
import { ProjectFormComponent } from './project/project-form/project-form.component';
import { ProjectViewComponent } from './project/project-view/project-view.component';
import { ProjectNotificationComponent } from './project/project-notification/project-notification.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_DIRECTOR'] } },
{ path: 'project', component: ProjectComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_DIRECTOR'] } },
{ path: 'project/form', component: ProjectFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_DIRECTOR'] } },
{ path: 'project/form/:id', component: ProjectFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectView/:id', component: ProjectViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectNotification/:id', component: ProjectNotificationComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_DIRECTOR'] } }]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }