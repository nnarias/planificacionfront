import { AnnexComponent } from './project/project-form/data/annex/annex.component';
import { BibliographyComponent } from './project/project-form/data/bibliography/bibliography.component';
import { CommonModule } from '@angular/common';
import { CoverageComponent } from './project/project-form/data/coverage/coverage.component';
import { DiagnosisAndProblemComponent } from './project/project-form/data/diagnosis-and-problem/diagnosis-and-problem.component';
import { DiagnosisAndProblemViewComponent } from './project/project-view/data-view/diagnosis-and-problem-view/diagnosis-and-problem-view.component';
import { GeneralDataComponent } from './project/project-form/data/general-data/general-data.component';
import { GeneralDataViewComponent } from './project/project-view/data-view/general-data-view/general-data-view.component';
import { GoalComponent } from './project/project-form/data/goal/goal.component';
import { GoalViewComponent } from './project/project-view/data-view/goal-view/goal-view.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { InstitutionInvolvedComponent } from './project/project-form/data/institution-involved/institution-involved.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectComponent } from './project/project/project.component';
import { ProjectFormComponent } from './project/project-form/project-form.component';
import { ProjectNotificationComponent } from './project/project-notification/project-notification.component';
import { ProjectRequirementComponent } from './project/project-form/data/project-requirement/project-requirement.component';
import { ProjectRequirementViewComponent } from './project/project-view/data-view/project-requirement-view/project-requirement-view.component';
import { ProjectResponsibleComponent } from './project/project-form/data/project-responsible/project-responsible.component';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectViewComponent } from './project/project-view/project-view.component';
import { ViabilityComponent } from './project/project-form/data/viability/viability.component';
import { ViabilityViewComponent } from './project/project-view/data-view/viability-view/viability-view.component';

@NgModule({
  declarations: [AnnexComponent, BibliographyComponent, CoverageComponent, DiagnosisAndProblemComponent, DiagnosisAndProblemViewComponent, GeneralDataComponent,
    GeneralDataViewComponent, GoalComponent, GoalViewComponent, InstitutionInvolvedComponent, MenuComponent, ProjectComponent, ProjectFormComponent,
    ProjectNotificationComponent, ProjectRequirementComponent, ProjectRequirementViewComponent, ProjectResponsibleComponent, ProjectViewComponent, ViabilityComponent,
    ViabilityViewComponent,],
  imports: [CommonModule, ImportsModule, ProjectRoutingModule]
})
export class ProjectModule { }