import { ActivatedRoute, Router } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ProjectDTO } from '../../model/projectDTO';
import { ProjectService } from '../../service/project.service';
import { StepperOrientation, STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { SubUnescoAreaKnowledge } from '../../../knowledge-area/model/subUnescoAreaKnowledge';
import swal from 'sweetalert2';
import * as textEditor from 'src/assets/ckeditor/ckEditor';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class ProjectFormComponent implements OnInit {
  private _editor = textEditor;

  private _form: FormGroup = this.formBuilder.group({
    ampleAreaKnowledgeId: ['', Validators.required],
    ancestralKnowledge: [''],
    bibliographyList: [[]],
    callId: ['', Validators.required],
    collegeCareerId: ['', Validators.required],
    collegeDepartmentId: ['', Validators.required],
    coverageList: [[]],
    criticalFactorSuccess: [''],
    currentSituation: [''],
    detailedAreaKnowledgeId: ['', Validators.required],
    diffusion: [''],
    economicPartnerGoalId: ['', Validators.required],
    endDate: ['', Validators.required],
    environmentalImpactAnalysis: [''],
    espeAreaKnowledgeId: ['', Validators.required],
    espeCurrentRiskBudget: [''],
    espeInvestmentRiskBudget: [''],
    id: [''],
    impactEconomicResult: [''],
    impactOtherResult: [''],
    impactPoliticalResult: [''],
    impactScientificResult: [''],
    impactSocialResult: [''],
    institutionInvolvedList: [[]],
    intellectualProperty: [''],
    lineResearchId: ['', Validators.required],
    nameEnglish: ['', Validators.required],
    nameSpanish: ['', Validators.required],
    numberParticipantOther: [''],
    numberParticipantProfessorFemale: [''],
    numberParticipantProfessorMale: [''],
    numberParticipantStudentFemale: [''],
    numberParticipantStudentMale: [''],
    numberPopulationDisabledPerson: [''],
    numberPopulationFemale: [''],
    numberPopulationIndirect: [''],
    numberPopulationMale: [''],
    policyPlanList: [[], Validators.required],
    postgraduateProgramId: ['', Validators.required],
    problemDiagnosis: [''],
    programId: ['', Validators.required],
    projectAnnexList: [[]],
    projectBase: [''],
    projectGoalList: [[]],
    projectRequirementList: [[], Validators.required],
    projectResponsibleList: [[]],
    projectStatusId: ['', Validators.required],
    prototype: [''],
    reasonProjectExecution: [''],
    researchGroupId: ['', Validators.required],
    researchMethodology: [''],
    researchTypeId: ['', Validators.required],
    restrictionAssumption: [''],
    scientificArticle: [''],
    scientificDisciplineId: ['', Validators.required],
    socialSustainability: [''],
    specificAreaKnowledgeId: ['', Validators.required],
    spinOffs: [''],
    sponsorCurrentRiskBudget: [''],
    sponsorInvestmentRiskBudget: [''],
    startDate: ['', Validators.required],
    state: [''],
    subUnescoAreaKnowledgeId: ['', Validators.required],
    technicalViability: [''],
    technologicalEquipment: [''],
    technologyTransfer: [''],
    unintentionalConsequences: ['']
  });

  private _fullForm: FormGroup = this.formBuilder.group({
    ampleAreaKnowledgeId: ['', Validators.required],
    ancestralKnowledge: ['', Validators.required],
    bibliographyList: [[], Validators.required],
    callId: [[], Validators.required],
    collegeCareerId: ['', Validators.required],
    collegeDepartmentId: ['', Validators.required],
    coverageList: ['', Validators.required],
    criticalFactorSuccess: ['', Validators.required],
    currentSituation: ['', Validators.required],
    detailedAreaKnowledgeId: ['', Validators.required],
    diffusion: ['', Validators.required],
    economicPartnerGoalId: ['', Validators.required],
    endDate: ['', Validators.required],
    environmentalImpactAnalysis: ['', Validators.required],
    espeAreaKnowledgeId: ['', Validators.required],
    espeCurrentRiskBudget: ['', Validators.required],
    espeInvestmentRiskBudget: ['', Validators.required],
    id: [''],
    impactEconomicResult: ['', Validators.required],
    impactOtherResult: ['', Validators.required],
    impactPoliticalResult: ['', Validators.required],
    impactScientificResult: ['', Validators.required],
    impactSocialResult: ['', Validators.required],
    institutionInvolvedList: [[], Validators.required],
    intellectualProperty: ['', Validators.required],
    lineResearchId: ['', Validators.required],
    nameEnglish: ['', Validators.required],
    nameSpanish: ['', Validators.required],
    numberParticipantOther: ['', Validators.required],
    numberParticipantProfessorFemale: ['', Validators.required],
    numberParticipantProfessorMale: ['', Validators.required],
    numberParticipantStudentFemale: ['', Validators.required],
    numberParticipantStudentMale: ['', Validators.required],
    numberPopulationDisabledPerson: ['', Validators.required],
    numberPopulationFemale: ['', Validators.required],
    numberPopulationIndirect: ['', Validators.required],
    numberPopulationMale: ['', Validators.required],
    policyPlanList: [[], Validators.required],
    postgraduateProgramId: ['', Validators.required],
    problemDiagnosis: ['', Validators.required],
    programId: ['', Validators.required],
    projectAnnexList: [[], Validators.required],
    projectBase: ['', Validators.required],
    projectGoalList: [[], Validators.required],
    projectRequirementList: [[], Validators.required],
    projectResponsibleList: [[], Validators.required],
    projectStatusId: ['', Validators.required],
    prototype: ['', Validators.required],
    reasonProjectExecution: ['', Validators.required],
    researchGroupId: ['', Validators.required],
    researchMethodology: ['', Validators.required],
    researchTypeId: ['', Validators.required],
    restrictionAssumption: ['', Validators.required],
    scientificArticle: ['', Validators.required],
    scientificDisciplineId: ['', Validators.required],
    socialSustainability: ['', Validators.required],
    specificAreaKnowledgeId: ['', Validators.required],
    spinOffs: ['', Validators.required],
    sponsorCurrentRiskBudget: ['', Validators.required],
    sponsorInvestmentRiskBudget: ['', Validators.required],
    startDate: ['', Validators.required],
    state: ['', Validators.required],
    subUnescoAreaKnowledgeId: ['', Validators.required],
    technicalViability: ['', Validators.required],
    technologicalEquipment: ['', Validators.required],
    technologyTransfer: ['', Validators.required],
    unintentionalConsequences: ['', Validators.required]
  });
  private project: ProjectDTO = new ProjectDTO();
  private _showSpiner: boolean = true;
  private _stepperOrientation: Observable<StepperOrientation>;
  private _subUnescoAreaKnowledge: SubUnescoAreaKnowledge;
  private _title: string;

  constructor(private activatedRoute: ActivatedRoute, private breakpointObserver: BreakpointObserver, private formBuilder: FormBuilder, private projectService: ProjectService, private router: Router) { }

  ngOnInit(): void {
    this._stepperOrientation = this.breakpointObserver.observe('(min-width: 900px)')
      .pipe(map(({ matches }) => matches ? 'horizontal' : 'vertical'));
    this.loadProject();
    this.updateFullForm();
  }

  loadProject(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.projectService.findByProjectDirector(id).subscribe(project => {
          if (project.state.toString() != 'REGISTERED' && project.state.toString() != 'MODIFICATION' && project.state.toString() != 'REJECTED') {
            this.router.navigate(['app/registry/project/project']);
            let state;
            switch (project.state.toString()) {
              case 'APPROVING':
                state = "Tu proyecto se encuentra en aprobación departamental, y ya no puede ser modificado";
                break;
              case 'EVALUATORASSIGNING':
                state = "Tu proyecto se encuentra en asignación de evaluador, no puede ser modificado";
                break;
              case 'EVALUATING':
                state = "Tu proyecto se encuentra en evaluación, espera a que termine el proceso y te habiliten para poder modificarlo";
                break;
              case 'COUNCILAPPROVAL':
                state = "Tu proyecto se encuentra en aprobación del concejo, no puede ser modificado";
                break;
              case 'DEVELOPING':
                state = "Tu proyecto se encuentra en desarrolloo, ya no puede ser modificado, pero puedes ingresar sus respectivos entregables y facturas";
                break;
              default:
                state = "Este proyecto está finalizado y no puede modificarse";
                break;
            }
            swal.fire('Proyecto no habilitado para edición', state, 'error');
          }
          this._form.patchValue(project);
          this._form.patchValue({
            ampleAreaKnowledgeId: project.ampleAreaKnowledge.id,
            callId: project.call.id,
            collegeCareerId: project.collegeCareer.id,
            collegeDepartmentId: project.collegeDepartment.id,
            detailedAreaKnowledgeId: project.detailedAreaKnowledge.id,
            economicPartnerGoalId: project.economicPartnerGoal.id,
            espeAreaKnowledgeId: project.espeAreaKnowledge.id,
            lineResearchId: project.lineResearch.id,
            postgraduateProgramId: project.postgraduateProgram.id,
            programId: project.program.id,
            projectStatusId: project.projectStatus.id,
            researchGroupId: project.researchGroup.id,
            researchTypeId: project.researchType.id,
            scientificDisciplineId: project.scientificDiscipline.id,
            specificAreaKnowledgeId: project.specificAreaKnowledge.id,
            state: project.state
          });
          this._fullForm.patchValue(this._form.value);
          this._subUnescoAreaKnowledge = project.subUnescoAreaKnowledge;
          this._showSpiner = false;
        });
        this._title = 'Modificar Proyecto';
      } else {
        this._title = 'Crear Proyecto';
        this._showSpiner = false;
      }
    });
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  save(): void {
    let formData = new FormData();
    let project = this._form.value;
    project.projectAnnexList.forEach(projectAnnex => {
      formData.append('annexList', projectAnnex.file);
      projectAnnex.description = this._form.get("annexFileDescription" + projectAnnex.annexId).value;
    });
    project.projectRequirementList.forEach(requirement => {
      formData.append('requirementList', requirement.file);
    });
    formData.append('project', new Blob([JSON.stringify(project)], { type: 'application/json' }));
    this.projectService.save(formData).subscribe(() => {
      this._showSpiner = false;
      this.router.navigate(['app/registry/project/project']);
      swal.fire(project.id ? 'Proyecto Actualizado' : 'Nuevo Proyecto', `${project.nameSpanish}`, 'success');
    });
  }

  send(): void {
    this.project = this._form.value;
    console.log(this.project);
    this._fullForm = this._form;
    console.log(this._fullForm);
    console.log(this._fullForm.valid);
  }

  updateFullForm(): void {
    this._form.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      this._fullForm.patchValue(value);
    })
  }

  public get callId() { return this._form.get('callId'); }

  public get editor(): textEditor {
    return this._editor;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get fullForm(): FormGroup {
    return this._fullForm;
  }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public get stepperOrientation(): Observable<StepperOrientation> {
    return this._stepperOrientation;
  }

  public get subUnescoAreaKnowledge(): SubUnescoAreaKnowledge {
    return this._subUnescoAreaKnowledge;
  }

  public get title(): string {
    return this._title;
  }
}