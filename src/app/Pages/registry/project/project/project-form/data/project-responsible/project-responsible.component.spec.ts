import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectResponsibleComponent } from './project-responsible.component';

describe('ProjectResponsibleComponent', () => {
  let component: ProjectResponsibleComponent;
  let fixture: ComponentFixture<ProjectResponsibleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectResponsibleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectResponsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
