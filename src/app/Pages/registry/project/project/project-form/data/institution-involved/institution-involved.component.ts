import { address, customEmail, fax, phone, url } from 'src/app/validators/validation.directive';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ControlContainer, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { InstitutionInvolved } from 'src/app/Pages/registry/external-institution/model/institutionInvolved';
import { InstitutionInvolvedService } from 'src/app/Pages/registry/external-institution/service/institution-involved.service';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatStepper } from '@angular/material/stepper';
import { Observable } from 'rxjs';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import swal from 'sweetalert2';

@Component({
  selector: 'app-institution-involved',
  templateUrl: './institution-involved.component.html',
  styleUrls: ['./institution-involved.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class InstitutionInvolvedComponent implements OnInit {
  private _form: FormGroup = this.formBuilder.group({
    address: ['', Validators.required],
    email: [''],
    executingAgency: [''],
    fax: [''],
    legalRepresentative: [''],
    legalRepresentativeId: [''],
    name: ['', Validators.required],
    phone: [''],
    url: ['']
  });
  private institution: InstitutionInvolved = new InstitutionInvolved();
  private institutionChipForm: FormControl = new FormControl();
  private institutionIdList: number[] = [];
  private institutionList: InstitutionInvolved[] = [];
  private institutionListFiltered: Observable<InstitutionInvolved[]>;
  private projectForm: FormGroup;
  private _selectedInstitutions: InstitutionInvolved[] = [];
  private separatorKeys: number[] = [ENTER, COMMA];


  @ViewChild('institutionInput') institutionInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private controlContainer: ControlContainer, private formBuilder: FormBuilder, private institutionInvolvedService: InstitutionInvolvedService) { }

  ngOnInit(): void {
    this.projectForm = <FormGroup>this.controlContainer.control;
    this.loadInstitutionList();
    this.validate();
  }

  addInstitution(event: MatChipInputEvent): void {
    const value = event.value;
    if ((value || '').trim()) {
      this.selectInstitutionByName(value.trim());
    }
    this.resetInstitutionInputs();
  }

  filterInstitution(institutionList: InstitutionInvolved[], institutionName: String): InstitutionInvolved[] {
    const filterValue = institutionName.toLowerCase();
    let institutionsMatchingName = institutionList.filter(institution => institution.name.toLowerCase().indexOf(filterValue) === 0);
    if (institutionsMatchingName.length) {
      return institutionsMatchingName;
    } else {
      return null;
    }
  }

  filterInstitutioneOnValueChange(institutionName: String): InstitutionInvolved[] {
    let institutionLessSelected = this.institutionList.filter(institution => this._selectedInstitutions.indexOf(institution) < 0);
    if (institutionName) {
      return this.filterInstitution(institutionLessSelected, institutionName);
    } else {
      return institutionLessSelected;
    }
  }

  institutionSelected(event: MatAutocompleteSelectedEvent): void {
    this.selectInstitutionByName(event.option.value);
    this.institutionChipForm.setValue(null);
  }

  loadInstitutionList(): void {
    this.institutionInvolvedService.findAll().subscribe(institutionInvolvedList => {
      this.institutionList = institutionInvolvedList;
      this.institutionListFiltered = this.institutionChipForm.valueChanges.pipe(startWith(this.institutionChipForm.value),
        map(institutionName => this.filterInstitutioneOnValueChange(institutionName)));
      let institutions = this.institutions.value;
      if (institutions.length > 0) {
        institutions.forEach(institution => {
          this.selectInstitutionByName(institution.name);
        });
      }
    });
  }

  removeInstitution(institution: InstitutionInvolved): void {
    const index = this._selectedInstitutions.indexOf(institution);
    if (index >= 0) {
      this._selectedInstitutions.splice(index, 1);
      this.institutionIdList.splice(index, 1);
      this.resetInstitutionInputs();
    }
  }

  private resetInstitutionInputs(): void {
    if (this.institutionInput) {
      this.institutionInput.nativeElement.value = '';
      this.institutionChipForm.setValue(null);
    }
  }

  save(stepper: MatStepper): void {
    this.institution = this._form.value;
    this.institutionInvolvedService.save(this.institution).subscribe(() => {
      swal.fire('Nueva Institución Involucrada', `${this.institution.name}`, 'success');
      this._form.patchValue({
        address: '',
        email: '',
        executingAgency: '',
        fax: '',
        legalRepresentative: '',
        legalRepresentativeId: '',
        name: '',
        phone: '',
        url: ''
      });
      this.loadInstitutionList();
      stepper.previous();
    });
  }

  private selectInstitutionByName(institutionName: String): void {
    let founded = this.institutionList.filter(institution => institution.name == institutionName);
    if (founded.length) {
      let institution = founded[0];
      this._selectedInstitutions.push(institution);
      this.institutionIdList.push(institution.id);
    }
    else {
      swal.fire('No existe la Institución', 'Intente registrar la institución antes de seleccionarla', 'error');
    }
    this.resetInstitutionInputs();
    this.institutions.setValue(this.institutionIdList);
  }

  validate(): void {
    this.address.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.address.setValidators(address());
      } else {
        this.address.clearValidators();
      }
      this.address.updateValueAndValidity();
    });
    this.fax.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.fax.setValidators(fax());
      } else {
        this.fax.clearValidators();
      }
      this.fax.updateValueAndValidity();
    });
    this.email.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.email.setValidators([Validators.email, customEmail()]);
      } else {
        this.email.clearValidators();
      }
      this.email.updateValueAndValidity();
    });
    this.phone.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.phone.setValidators(phone());
      } else {
        this.phone.clearValidators();
      }
      this.phone.updateValueAndValidity();
    });
    this.url.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.url.setValidators(url());
      } else {
        this.url.clearValidators();
      }
      this.url.updateValueAndValidity();
    });
  }

  public get address() { return this._form.get('address'); }
  public get email() { return this._form.get('email'); }
  public get executingAgency() { return this._form.get('executingAgency'); }
  public get fax() { return this._form.get('fax'); }

  public get filteredInstitutions(): Observable<InstitutionInvolved[]> {
    return this.institutionListFiltered;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get selectedInstitutions(): InstitutionInvolved[] {
    return this._selectedInstitutions;
  }

  public get institutionForm(): FormControl {
    return this.institutionChipForm;
  }

  public get institutions() { return this.projectForm.get('institutionInvolvedList'); }
  public get legalRepresentative() { return this._form.get('legalRepresentative'); }
  public get legalRepresentativeId() { return this._form.get('legalRepresentativeId'); }
  public get name() { return this._form.get('name'); }
  public get phone() { return this._form.get('phone'); }

  public get separatorKeysCodes(): number[] {
    return this.separatorKeys;
  }

  public get url() { return this._form.get('url'); }
}