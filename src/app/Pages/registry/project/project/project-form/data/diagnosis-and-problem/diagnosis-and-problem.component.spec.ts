import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosisAndProblemComponent } from './diagnosis-and-problem.component';

describe('DiagnosisAndProblemComponent', () => {
  let component: DiagnosisAndProblemComponent;
  let fixture: ComponentFixture<DiagnosisAndProblemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiagnosisAndProblemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosisAndProblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
