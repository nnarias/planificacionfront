import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-viability',
  templateUrl: './viability.component.html',
  styleUrls: ['./viability.component.scss']
})
export class ViabilityComponent implements OnInit {
  private _form: FormGroup;

  constructor(private controlContainer: ControlContainer) { }

  ngOnInit(): void {
    this._form = <FormGroup>this.controlContainer.control;
  }

  public get form(): FormGroup {
    return this._form;
  }
}