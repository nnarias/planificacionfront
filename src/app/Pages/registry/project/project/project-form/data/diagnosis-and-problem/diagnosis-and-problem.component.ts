import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormGroup } from '@angular/forms';
import * as textEditor from 'src/assets/ckeditor/ckEditor';

@Component({
  selector: 'app-diagnosis-and-problem',
  templateUrl: './diagnosis-and-problem.component.html',
  styleUrls: ['./diagnosis-and-problem.component.scss']
})
export class DiagnosisAndProblemComponent implements OnInit {
  private _editor = textEditor;
  private _form: FormGroup;

  constructor(private controlContainer: ControlContainer) { }

  ngOnInit(): void {
    this._form = <FormGroup>this.controlContainer.control;
  }

  onlyNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  public get editor(): textEditor {
    return this._editor;
  }

  public get form(): FormGroup {
    return this._form;
  }
}