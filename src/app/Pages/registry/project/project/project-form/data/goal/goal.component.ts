import { ActivityResource } from '../../../../model/activityResource';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as copy from 'lodash/CloneDeep';
import { GoalActivity } from '../../../../model/goalActivity';
import { MatSort } from '@angular/material/sort';
import { MatStepper } from '@angular/material/stepper';
import { MatTableDataSource } from '@angular/material/table';
import { ProjectGoal } from '../../../../model/projectGoal';
import { Resource } from '../../../../model/resource';
import { ResourceService } from '../../../../service/resource.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import swal from 'sweetalert2';

@Component({
  selector: 'app-goal',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class GoalComponent implements OnInit {
  private _activityDataSource = new MatTableDataSource<GoalActivity>();
  private _activityDisplayedColumns: string[] = ['name', 'actions'];
  private _activityForm: FormGroup = this.formBuilder.group({
    activityResourceList: [''],
    currentBudget: [''],
    deliverable: ['', Validators.required],
    endDate: ['', Validators.required],
    id: [''],
    investmentBudget: [''],
    name: ['', Validators.required],
    startDate: ['', Validators.required],
    state: ['']
  });
  private activityList: GoalActivity[] = [];
  private _activityResource: ActivityResource;
  private activityResourceList: ActivityResource[] = [];
  private _activityStep: boolean = false;
  private _filteredResourceList: Resource[] = [];
  private generalGoal: boolean = false;
  private _goalActivity: GoalActivity;
  private _goalDataSource = new MatTableDataSource<ProjectGoal>();
  private _goalDisplayedColumns: string[] = ['name', 'actions'];
  private _goalform: FormGroup = this.formBuilder.group({
    assumption: ['', Validators.required],
    deliverable: [''],
    general: [false],
    goalActivityList: [''],
    id: [''],
    indicator: ['', Validators.required],
    name: ['', Validators.required],
    state: ['']
  });
  private goalList: ProjectGoal[];
  private goalSort: MatSort;
  private _goalStep: boolean = true;
  private projectForm: FormGroup;
  private _projectGoal: ProjectGoal;
  private resource: Resource;
  private _resourceDataSource = new MatTableDataSource<ActivityResource>();
  private _resourceDisplayedColumns: string[] = ['name', 'actions'];
  private _resourceForm: FormGroup = this.formBuilder.group({
    currentBudget: ['', Validators.required],
    id: [''],
    investmentBudget: ['', Validators.required],
    resourceId: ['', Validators.required],
    state: ['']
  });
  private resourceList: Resource[] = [];
  private _resourceStep: boolean = false;
  private _showDeliverable: boolean = true;

  @ViewChild(MatSort, { static: false }) set goalMatSort(goalSort: MatSort) {
    this.goalSort = goalSort;
    if (this.goalSort) {
      this._goalDataSource.sort = this.goalSort;
    }
  };

  constructor(private controlContainer: ControlContainer, private formBuilder: FormBuilder, private resourceService: ResourceService) { }

  ngOnInit(): void {
    this.projectForm = <FormGroup>this.controlContainer.control;
    this.loadGoalList();
    this.loadResourceList();
  }

  addActivity(): void {
    this.activityList.push(this._activityForm.value);
    this._projectGoal.deliverable += this.deliverable.value;
    this._activityDataSource.data = this.activityList;
    this._projectGoal.goalActivityList = this.activityList;
    swal.fire('Actividad de objetivo agregada', `${this.activityName.value}`, 'success');
    this._activityForm.setValue(new GoalActivity());
    this.projectGoals.setValue(this.goalList);
  }

  addGoal(): void {
    this.goalList.push(this._goalform.value);
    this._goalDataSource.data = this.goalList;
    swal.fire('Objetivo de proyecto agregado', `${this.goalName.value}`, 'success');
    if (this.general.value) {
      this.generalGoal = true;
      this._showDeliverable = false;
    }
    this._goalform.setValue(new ProjectGoal());
    this.projectGoals.setValue(this.goalList);
  }

  addResource(): void {
    this._goalActivity.currentBudget += this.currentBudget.value;
    this._goalActivity.investmentBudget += this.investmentBudget.value;
    this.getResourceById(this.resourceId.value);
    this._activityResource = this.resourceForm.value;
    this._activityResource.resourceId = this.resource.id;
    this._activityResource.resourceName = this.resource.name;
    this._activityResource.resource = this.resource;
    this.activityResourceList.push(this._resourceForm.value);
    this.resourceDataSource.data = this.activityResourceList;
    swal.fire('Recurso de actividad agregado', `${this.resource.name}`, 'success');
    this._resourceForm.setValue(new ActivityResource());
    this._activityResource = null;
    this._goalActivity.activityResourceList = this.activityResourceList;
    this.filterResourceList();
    this.projectGoals.setValue(this.goalList);
  }

  backActivity(stepper: MatStepper): void {
    this._resourceStep = false;
    this._activityStep = true;
    this._goalActivity = null;
    this._activityForm.setValue(new GoalActivity());
    this._activityResource = null;
    this._resourceForm.setValue(new ActivityResource());
    stepper.previous();
  }

  backGoal(stepper: MatStepper): void {
    this._activityStep = false;
    this._goalStep = true;
    this._projectGoal = null;
    this._goalform.setValue(new ProjectGoal());
    this._goalActivity = null;
    this._activityForm.setValue(new GoalActivity());
    stepper.previous();
  }

  cancelActivity(): void {
    this._goalActivity = null;
    this._activityForm.setValue(new GoalActivity());
  }

  cancelGoal(): void {
    if (this._projectGoal.general) {
      this._showDeliverable = false;
    }
    this._projectGoal = null;
    this._goalform.setValue(new GoalActivity());
  }

  cancelResource(): void {
    this._activityResource = null;
    this._resourceForm.setValue(new ActivityResource());
    this.filterResourceList();
  }

  check(checked: boolean): void {
    this.generalGoal = checked;
  }

  deleteActivity(goalActivity: GoalActivity): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la actividad del objetivo: ${goalActivity.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.activityList = this.activityList.filter(result => result !== goalActivity);
        this._activityDataSource.data = this.activityList;
        let deliverable = "";
        this.activityList.forEach(activity => {
          deliverable += activity.deliverable;
        })
        this._projectGoal.deliverable = deliverable;
        this._projectGoal.goalActivityList = this.activityList;
        this.projectGoals.setValue(this.goalList);
        swal.fire('Actividad del objetivo eliminada', `${goalActivity.name}, eliminada con éxito`, 'success');
      }
    });
  }

  deleteGoal(projectGoal: ProjectGoal): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el objetivo del proyecto: ${projectGoal.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.goalList = this.goalList.filter(result => result !== projectGoal);
        this._goalDataSource.data = this.goalList;
        if (projectGoal.general) {
          this.generalGoal = false;
          this._showDeliverable = true;
        }
        this.projectGoals.setValue(this.goalList);
        swal.fire('Objetivo del proyecto eliminado', `${projectGoal.name}, eliminado con éxito`, 'success');
      }
    });
  }

  deleteResource(activityResource: ActivityResource): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el recurso ${activityResource.resourceName} de la actividad: ${this._goalActivity.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this._goalActivity.currentBudget -= activityResource.currentBudget;
        this._goalActivity.investmentBudget -= activityResource.investmentBudget;
        this.activityResourceList = this.activityResourceList.filter(result => result !== activityResource);
        this._resourceDataSource.data = this.activityResourceList;
        this._goalActivity.activityResourceList = this.activityResourceList;
        this.filterResourceList();
        swal.fire('Recurso de la actividad eliminado', `${activityResource.resourceName}, eliminado con éxito`, 'success');
        this.projectGoals.setValue(this.goalList);
      }
    });
  }

  filterResourceList(): void {
    this._filteredResourceList = this.resourceList;
    if (this.activityResourceList) {
      this.activityResourceList.forEach(activityResource => {
        if (this._activityResource != activityResource) {
          this._filteredResourceList = this._filteredResourceList.filter(resource => resource.id != activityResource.resourceId);
        }
      });
    }
    if (!this._activityResource) {
      this._filteredResourceList = this._filteredResourceList.filter(resource => resource.id != this.resourceId.value);
    }
  }

  getResourceById(id: number): void {
    this.resourceList.forEach(resource => {
      if (resource.id == id) {
        this.resource = resource;
      }
    });
  }

  loadGoalList(): void {
    this.goalList = this.projectGoals.value;
    if (this.goalList) {
      this.goalDataSource.data = this.goalList;
      this.goalList.forEach(goal => {
        if (goal.general) {
          this._showDeliverable = false;
          this.generalGoal = true;
        }
        else {
          if (goal.goalActivityList) {
            goal.goalActivityList.forEach(activity => {
              delete activity.deliverableDate;
              delete activity.deliverableDescription;
              delete activity.deliverableFileName;
              delete activity.deliverableFileUUID;
              if (activity.activityResourceList) {
                activity.activityResourceList.forEach(activityResource => {
                  activityResource.resourceId = activityResource.resource.id;
                  activityResource.resourceName = activityResource.resource.name;
                  delete activityResource.invoiceCurrentBudget;
                  delete activityResource.invoiceDate;
                  delete activityResource.invoiceDescription;
                  delete activityResource.invoiceFileName;
                  delete activityResource.invoiceFileUUID;
                  delete activityResource.invoiceInvestmentBudget;
                  delete activityResource.projectInvoice;
                });
              }
            });
          }
        }
      });
    } else {
      this.goalList = [];
    }
  }

  loadResourceList(): void {
    this.resourceService.findAll().subscribe(resourceList => {
      this.resourceList = resourceList;
    });
  }

  setActivity(goalActivity: GoalActivity): void {
    this._goalActivity = this.activityList[this.activityList.indexOf(goalActivity)];
    this._activityForm.setValue(this._goalActivity);
  }

  setActivityResource(goalActivity: GoalActivity, stepper: MatStepper): void {
    this._activityStep = false;
    this._resourceStep = true;
    this._goalActivity = goalActivity;
    if (goalActivity.activityResourceList) {
      this.activityResourceList = goalActivity.activityResourceList;
      this.filterResourceList();
    } else {
      this.activityResourceList = [];
      this._filteredResourceList = this.resourceList;
    }
    this._resourceDataSource.data = goalActivity.activityResourceList;
    stepper.next();
  }

  setGoal(projectGoal: ProjectGoal): void {
    if (projectGoal.general) {
      this._showDeliverable = true;
    }
    if (!projectGoal.general && this.generalGoal) {
      this._showDeliverable = false;
    }
    this._projectGoal = this.goalList[this.goalList.indexOf(projectGoal)];
    this._goalform.setValue(this._projectGoal);
  }

  setGoalActivity(goal: ProjectGoal, stepper: MatStepper): void {
    this._goalStep = false;
    this._activityStep = true;
    this._projectGoal = goal;
    if (goal.goalActivityList) {
      this.activityList = goal.goalActivityList;
    } else {
      this.activityList = [];
    }
    this._activityDataSource.data = this.activityList;
    stepper.next();
  }

  setResource(activityResource: ActivityResource): void {
    this._activityResource = this.activityResourceList[this.activityResourceList.indexOf(activityResource)];
    this.filterResourceList();
    activityResource = copy(this._activityResource);
    delete activityResource.resource;
    delete activityResource.resourceName;
    this._resourceForm.setValue(activityResource);
  }

  updateActivity(): void {
    this.activityList[this.activityList.indexOf(this._goalActivity)] = this._activityForm.value;
    this._activityDataSource.data = this.activityList;
    swal.fire('Actividad de objetivo actualizada', `${this.activityName.value}`, 'success');
    this._goalActivity = null;
    this._activityForm.setValue(new GoalActivity());
    this.projectGoals.setValue(this.goalList);
  }

  updateGoal(): void {
    let goal = this._goalform.value;
    this.goalList[this.goalList.indexOf(this._projectGoal)] = goal;
    this._goalDataSource.data = this.goalList;
    swal.fire('Objetivo de proyecto actualizado', `${this.goalName.value}`, 'success');
    if (this._projectGoal.general && !goal.general) {
      this.generalGoal = false;
      this._showDeliverable = true;
    }
    if (this.generalGoal) {
      this._showDeliverable = false;
    }
    this._projectGoal = null;
    this._goalform.setValue(new ProjectGoal());
    this.projectGoals.setValue(this.goalList);
  }

  updateResource(): void {
    this._goalActivity.currentBudget -= this._activityResource.currentBudget;
    this._goalActivity.currentBudget += this.currentBudget.value;
    this._goalActivity.investmentBudget -= this._activityResource.investmentBudget;
    this._goalActivity.investmentBudget += this.investmentBudget.value;
    let index = this.activityResourceList.indexOf(this._activityResource);
    this.getResourceById(this.resourceId.value);
    this._activityResource = this.resourceForm.value;
    this._activityResource.resourceId = this.resource.id;
    this._activityResource.resourceName = this.resource.name;
    this._activityResource.resource = this.resource;
    this.activityResourceList[index] = this._resourceForm.value;
    this.resourceDataSource.data = this.activityResourceList;
    swal.fire('Recurso de actividad actualizado', `${this.resource.name}`, 'success');
    this._resourceForm.setValue(new ActivityResource());
    this._activityResource = null;
    this._goalActivity.activityResourceList = this.activityResourceList;
    this.filterResourceList();
    this.projectGoals.setValue(this.goalList);
  }

  public get activityDataSource(): MatTableDataSource<GoalActivity> {
    return this._activityDataSource;
  }

  public get activityDisplayedColumns(): string[] {
    return this._activityDisplayedColumns;
  }

  public get activityForm(): FormGroup {
    return this._activityForm;
  }

  public get activityName() { return this._activityForm.get('name'); }

  public get activityResource(): ActivityResource {
    return this._activityResource;
  }

  public get activityStep(): boolean {
    return this._activityStep;
  }

  public get assumption() { return this._goalform.get('assumption'); }
  public get currentBudget() { return this._resourceForm.get('currentBudget'); }
  public get deliverable() { return this._activityForm.get('deliverable'); }
  public get endDate() { return this._activityForm.get('endDate') }

  public get filteredResourceList(): Resource[] {
    return this._filteredResourceList;
  }

  public get generalDeliverable() { return this._goalform.get('deliverable'); }

  public get goalForm(): FormGroup {
    return this._goalform;
  }

  public get general() { return this._goalform.get('general'); }

  public get goalActivity(): GoalActivity {
    return this._goalActivity;
  }

  public get goalDataSource(): MatTableDataSource<ProjectGoal> {
    return this._goalDataSource;
  }

  public get goalDisplayedColumns(): string[] {
    return this._goalDisplayedColumns;
  }

  public get goalName() { return this._goalform.get('name'); }

  public get goalStep(): boolean {
    return this._goalStep;
  }

  public get indicator() { return this._goalform.get('indicator'); }
  public get investmentBudget() { return this._resourceForm.get('investmentBudget'); }

  public get projectGoal(): ProjectGoal {
    return this._projectGoal;
  }
  public get projectGoals() { return this.projectForm.get('projectGoalList') }

  public get resourceDataSource(): MatTableDataSource<ActivityResource> {
    return this._resourceDataSource;
  }

  public get resourceDisplayedColumns(): string[] {
    return this._resourceDisplayedColumns;
  }

  public get resourceId() { return this._resourceForm.get('resourceId') }

  public get resourceForm(): FormGroup {
    return this._resourceForm;
  }

  public get resourceStep(): boolean {
    return this._resourceStep;
  }

  public get showDeliverable(): boolean {
    return this._showDeliverable;
  }

  public get startDate() { return this._activityForm.get('startDate') }
}