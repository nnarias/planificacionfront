import { Bibliography } from '../../../../model/bibliography';
import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';
import { url } from 'src/app/validators/validation.directive';

@Component({
  selector: 'app-bibliography',
  templateUrl: './bibliography.component.html',
  styleUrls: ['./bibliography.component.scss']
})
export class BibliographyComponent implements OnInit {
  private bibliographyList: Bibliography[] = [];
  private _dataSource = new MatTableDataSource<Bibliography>();
  private _displayedColumns: string[] = ['title', 'year', 'actions'];
  private _form: FormGroup = this.formBuilder.group({
    author: ['', Validators.required],
    city: [''],
    country: [''],
    editorial: ['', Validators.required],
    pageName: [''],
    title: ['', Validators.required],
    url: [''],
    website: [false],
    year: ['', Validators.required]
  });
  private projectForm: FormGroup;

  constructor(private controlContainer: ControlContainer, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.projectForm = <FormGroup>this.controlContainer.control;
    this.bibliographyList = this.bibliographies.value;
    this._dataSource.data = this.bibliographyList;
  }

  check(checked: boolean): void {
    if (checked) {
      this._form.patchValue({
        city: '',
        country: '',
        editorial: ''
      });
      this.editorial.clearValidators();
      this.editorial.updateValueAndValidity();
      this.url.setValidators(Validators.compose([Validators.required, url()]));
      this.url.updateValueAndValidity();
    }
    else {
      this._form.patchValue({
        pageName: '',
        url: ''
      });
      this.editorial.setValidators(Validators.required);
      this.editorial.updateValueAndValidity();
      this.url.clearValidators();
      this.url.updateValueAndValidity();
    }
  }

  delete(bibliography: Bibliography): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la referencia bibliográfica de la Bibliografía: ${bibliography.title}}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.bibliographyList = this.bibliographyList.filter(result => result !== bibliography);
        this._dataSource.data = this.bibliographyList;
        this.bibliographies.setValue(this.bibliographyList);
        swal.fire('Referencia bibliográfica eeliminada', `${bibliography.title}, eliminada con éxito`, 'success');
      }
    });
  }

  onlyNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  save(stepper: MatStepper): void {
    let bibliography = this.form.value;
    this.bibliographyList.push(bibliography);
    this._dataSource.data = this.bibliographyList;
    this.bibliographies.setValue(this.bibliographyList);
    swal.fire('Referencia bibliográfica agregada', `${bibliography.title}`, 'success');
    this._form.patchValue({
      author: '',
      city: '',
      country: '',
      editorial: '',
      pageName: '',
      title: '',
      url: '',
      website: false,
      year: ''
    });
    this.editorial.setValidators(Validators.required);
    this.editorial.updateValueAndValidity();
    this.url.clearValidators();
    this.url.updateValueAndValidity();
    stepper.previous();
  }

  public get author() { return this._form.get('author'); }
  public get bibliographies() { return this.projectForm.get('bibliographyList'); }

  public get dataSource(): MatTableDataSource<Bibliography> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get editorial() { return this._form.get('editorial'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get title() { return this._form.get('title'); }
  public get url() { return this._form.get('url'); }
  public get website() { return this._form.get('website'); }
  public get year() { return this._form.get('year'); }
}