import { Call } from 'src/app/Pages/registry/call/model/call';
import { CallRequirement } from 'src/app/Pages/registry/call/model/callRequirement';
import { CallService } from 'src/app/Pages/registry/call/service/call.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ControlContainer, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ProjectRequirement } from '../../../../model/projectRequirement';
import { ProjectRequirementService } from '../../../../service/project-requirement.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-project-requirement',
  templateUrl: './project-requirement.component.html',
  styleUrls: ['./project-requirement.component.scss']
})
export class ProjectRequirementComponent implements OnInit {
  private _callList: Call[] = [];
  private _callRequirementList: CallRequirement[] = [];
  private _dataSource = new MatTableDataSource<CallRequirement>();
  private _displayedColumns: string[] = ['name', 'file'];
  private _files = {};
  private _form: FormGroup;
  @Input() fullForm: FormGroup;
  private previousList: CallRequirement[];
  private _projectRequirementList: ProjectRequirement[] = [];
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private callService: CallService, private controlContainer: ControlContainer, private projectRequirementService: ProjectRequirementService, private router: Router) { }

  ngOnInit(): void {
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'name': return item.requirement.name;
        default: return item[property];
      }
    }
    this._form = <FormGroup>this.controlContainer.control;
    this.loadCallList();
  }

  loadCallList(): void {
    this.callService.findActive().subscribe(callList => {
      this._callList = callList;
      if (callList.length > 0) {
        if (this.callId.value) {
          let call = callList.find(call => call.id == this.callId.value);
          this._callRequirementList = call.callRequirementList;
          this._dataSource.data = this._callRequirementList;
          this._callRequirementList.forEach(callRequirement => {
            let fileId = 'requirementFile' + callRequirement.requirement.id;
            this._files[fileId] = 'Seleccionar archivo...';
            if (callRequirement.required) {
              this._form.addControl(fileId, new FormControl('', Validators.required));
              this.fullForm.addControl(fileId, new FormControl('', Validators.required));
            } else {
              this._form.addControl(fileId, new FormControl(''));
              this.fullForm.addControl(fileId, new FormControl(''));
            }
          });
          this.previousList = this._callRequirementList;
          if (this._form.get('projectRequirementList')) {
            this._projectRequirementList = this._form.get('projectRequirementList').value;
            this._projectRequirementList.forEach(projectRequirement => {
              projectRequirement.requirementId = projectRequirement.requirement.id;
              if (projectRequirement.fileName) {
                this.projectRequirementService.findFile(projectRequirement.id).subscribe(blob => {
                  let file = new File([blob], projectRequirement.fileName);
                  projectRequirement.file = file;
                  this._form.get('requirementFile' + projectRequirement.requirement.id).patchValue(file);
                });
                this._files['requirementFile' + projectRequirement.requirement.id] = projectRequirement.fileName;
              }
            });
          }
        }
        this._showSpiner = false;
      } else {
        this.router.navigate(['app/registry/project/project']);
        swal.fire('No hay convocatorias activas',
          'No se pueden crear o editar proyectos en esta fecha, intente cuando inicie el periodo de la convocatoria o cuando salga una nueva', 'error');
      }
    });
  }

  loadRequirements(): void {
    let call = this._callList.filter(call => call.id == this.callId.value)[0];
    this._callRequirementList = call.callRequirementList;
    this._dataSource.data = this._callRequirementList;
    if (this.previousList) {
      this.previousList.forEach(callRequirement => {
        let fileId = 'requirementFile' + callRequirement.requirement.id;
        this._form.removeControl(fileId);
        this.fullForm.removeControl(fileId);
        delete this._files[fileId];
      });
    }
    this._callRequirementList.forEach(callRequirement => {
      let fileId = 'requirementFile' + callRequirement.requirement.id;
      this._files[fileId] = 'Seleccionar archivo...';
      if (callRequirement.required) {
        this._form.addControl(fileId, new FormControl('', Validators.required));
        this.fullForm.addControl(fileId, new FormControl('', Validators.required));
      } else {
        this._form.addControl(fileId, new FormControl(''));
        this.fullForm.addControl(fileId, new FormControl(''));
      }
    });
    this.previousList = this._callRequirementList;
  }

  setFile(event, callRequirement: CallRequirement) {
    var fileElement = event.target;
    let fileId = 'requirementFile' + callRequirement.requirement.id;
    if (fileElement.value) {
      let file: File = fileElement.files[0];
      let checkFile = this._projectRequirementList.some(projectRequirement => projectRequirement.fileName == file.name);
      if (checkFile) {
        swal.fire('Error', 'No se puede agregar el mismo archivo!', 'error');
      } else {
        let projectRequirement: ProjectRequirement = new ProjectRequirement();
        projectRequirement.file = file;
        projectRequirement.fileName = file.name;
        this._files[fileId] = file.name;
        projectRequirement.requirement = callRequirement.requirement;
        projectRequirement.requirementId = callRequirement.requirement.id;
        let requirement = this._projectRequirementList.find(projectRequirement => projectRequirement.requirementId == callRequirement.requirement.id);
        if (requirement) {
          if (requirement.id) {
            projectRequirement.id = requirement.id
            projectRequirement.updated = true;
          }
          this._projectRequirementList[this._projectRequirementList.indexOf(requirement)] = projectRequirement;
        } else {
          this._projectRequirementList.push(projectRequirement);
        }
        this._form.get(fileId).patchValue(file);
        this._form.get('projectRequirementList').patchValue(this._projectRequirementList);
      }
    }
  }

  public get callId() { return this._form.get('callId'); }

  public get callList(): Call[] {
    return this._callList;
  }

  public get callRequirementList(): CallRequirement[] {
    return this._callRequirementList;
  }

  public get dataSource(): MatTableDataSource<CallRequirement> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get files(): {} {
    return this._files;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}