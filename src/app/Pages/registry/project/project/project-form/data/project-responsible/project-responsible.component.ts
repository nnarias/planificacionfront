import { CollegeDepartment } from 'src/app/Pages/registry/college/model/collegeDepartment';
import { CollegeDepartmentService } from 'src/app/Pages/registry/college/service/college-department.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { customEmail, phone } from 'src/app/validators/validation.directive';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatStepper } from '@angular/material/stepper';
import { MatTableDataSource } from '@angular/material/table';
import { ProjectResponsible } from 'src/app/Pages/registry/staff/model/projectResponsible';
import { ProjectResponsibleType } from 'src/app/Pages/registry/staff/model/projectResponsibleType';
import { ProjectResponsibleTypeService } from 'src/app/Pages/registry/staff/service/project-responsible-type.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import swal from 'sweetalert2';

@Component({
  selector: 'app-project-responsible',
  templateUrl: './project-responsible.component.html',
  styleUrls: ['./project-responsible.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class ProjectResponsibleComponent implements OnInit {
  private _collegeDepartmentList: CollegeDepartment[] = [];
  private _dataSource = new MatTableDataSource<ProjectResponsible>();
  private _displayedColumns: string[] = ['function', 'numberIdentification', 'completeName', 'externalResponsible', 'actions'];
  private _form: FormGroup = this.formBuilder.group({
    collegeDepartmentId: ['', Validators.required],
    email: ['', Validators.compose([Validators.email, customEmail(), Validators.required])],
    externalInstitution: [''],
    externalResponsible: [false],
    lastname: ['', Validators.required],
    name: ['', Validators.required],
    numberIdentification: ['', Validators.required],
    projectResponsibleTypeId: ['', Validators.required],
    telephone: ['', Validators.compose([phone(), Validators.required])]
  });
  private paginator: MatPaginator;
  private projectForm: FormGroup;
  private projectResponsibleType: ProjectResponsibleType;
  private projectResponsibleList: ProjectResponsible[] = [];
  private _projectResponsibleTypeList: ProjectResponsibleType[] = [];
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private collegeDepartmentService: CollegeDepartmentService, private controlContainer: ControlContainer, private formBuilder: FormBuilder, private projectResponsibleTypeService: ProjectResponsibleTypeService) { }

  ngOnInit(): void {
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'function': return item.projectResponsibleTypeName;
        case 'completeName': return item.name + item.lastName;
        default: return item[property];
      }
    }
    this.projectForm = <FormGroup>this.controlContainer.control;
    this.projectResponsibleList = this.projectResponsibles.value;
    if (this.projectResponsibleList) {
      this.projectResponsibleList.forEach(projectResponsible => {
        if (projectResponsible.collegeDepartment) {
          projectResponsible.collegeDepartmentId = projectResponsible.collegeDepartment.id;
        }
        projectResponsible.projectResponsibleTypeId = projectResponsible.projectResponsibleType.id;
        projectResponsible.projectResponsibleTypeName = projectResponsible.projectResponsibleType.name;
      });
    }
    this._dataSource.data = this.projectResponsibleList;
    this.loadCollegeDepartmentList();
    this.loadProjectResponsibleTypeList();
  }

  checkExternal(checked: boolean): void {
    if (checked) {
      this._form.patchValue({
        externalInstitution: ''
      });
      this.collegeDepartmentId.clearValidators();
      this.collegeDepartmentId.updateValueAndValidity();
      this.externalInstitution.setValidators(Validators.required);
      this.externalInstitution.updateValueAndValidity();
    }
    else {
      this._form.patchValue({
        collegeDepartmentId: ''
      });
      this.collegeDepartmentId.setValidators(Validators.required);
      this.collegeDepartmentId.updateValueAndValidity();
      this.externalInstitution.clearValidators();
      this.externalInstitution.updateValueAndValidity();
    }
  }

  delete(projectResponsible: ProjectResponsible): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Personal Responsable del Proyecto: ${projectResponsible.name} ${projectResponsible.lastName}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.projectResponsibleList = this.projectResponsibleList.filter(result => result !== projectResponsible);
        this._dataSource.data = this.projectResponsibleList;
        this.projectResponsibles.setValue(this.projectResponsibleList);
        swal.fire('Personal Responsable del Proyecto Eliminado', `${projectResponsible.name} ${projectResponsible.lastName}, eliminado con éxito`, 'success');
      }
    });
  }

  getProjectResponsibleTypeById(id: number): void {
    this.projectResponsibleTypeList.forEach(projectResponsibleType => {
      if (projectResponsibleType.id == id) {
        this.projectResponsibleType = projectResponsibleType;
      }
    });
  }

  loadCollegeDepartmentList(): void {
    this.collegeDepartmentService.findAll().subscribe(collegeDepartmentList => {
      this._collegeDepartmentList = collegeDepartmentList;
    });
  }

  loadProjectResponsibleTypeList(): void {
    this.projectResponsibleTypeService.findAll().subscribe(projectResponsibleTypeList => {
      this._projectResponsibleTypeList = projectResponsibleTypeList;
    });
  }

  save(stepper: MatStepper): void {
    let projectResponsible = this.form.value;
    this.getProjectResponsibleTypeById(this.projectResponsibleTypeId.value);
    projectResponsible.projectResponsibleTypeName = this.projectResponsibleType.name;
    this.projectResponsibleList.push(projectResponsible);
    this._dataSource.data = this.projectResponsibleList;
    this.projectResponsibles.setValue(this.projectResponsibleList);
    swal.fire('Responsable de proyecto agregado', `${projectResponsible.name} ${projectResponsible.lastname}`, 'success');
    this._form.patchValue({
      collegeDepartmentId: '',
      email: '',
      externalInstitution: '',
      externalResponsible: false,
      lastname: '',
      name: '',
      numberIdentification: '',
      projectResponsibleTypeId: '',
      telephone: ''
    });
    stepper.previous();
  }

  public get collegeDepartmentId() { return this._form.get('collegeDepartmentId'); }

  public get collegeDepartmentList(): CollegeDepartment[] {
    return this._collegeDepartmentList;
  }

  public get dataSource(): MatTableDataSource<ProjectResponsible> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get email() { return this._form.get('email'); }
  public get externalInstitution() { return this._form.get('externalInstitution'); }
  public get externalResponsible() { return this._form.get('externalResponsible'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get lastname() { return this._form.get('lastname'); }
  public get name() { return this._form.get('name'); }
  public get numberIdentification() { return this._form.get('numberIdentification'); }
  public get projectResponsibleTypeId() { return this._form.get('projectResponsibleTypeId'); }

  public get projectResponsibleTypeList(): ProjectResponsibleType[] {
    return this._projectResponsibleTypeList;
  }

  public get projectResponsibles() { return this.projectForm.get('projectResponsibleList'); }
  public get telephone() { return this._form.get('telephone'); }
}