import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ControlContainer, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Coverage } from 'src/app/Pages/registry/location/model/coverage';
import { CoverageService } from 'src/app/Pages/registry/location/service/coverage.service';
import { CoverageType } from 'src/app/Pages/registry/location/model/coverageType';
import { CoverageTypeService } from 'src/app/Pages/registry/location/service/coverage-type.service';
import { map, startWith } from 'rxjs/operators';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatStepper } from '@angular/material/stepper';
import { Observable } from 'rxjs';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import swal from 'sweetalert2';

@Component({
  selector: 'app-coverage',
  templateUrl: './coverage.component.html',
  styleUrls: ['./coverage.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class CoverageComponent implements OnInit {
  private coverage: Coverage = new Coverage();
  private coverageChipForm: FormControl = new FormControl();
  private coverageIdList: number[] = [];
  private coverageList: Coverage[] = [];
  private coverageListFiltered: Observable<Coverage[]>;
  private _coverageTypeList: CoverageType[] = [];
  private _form: FormGroup = this.formBuilder.group({
    coverageTypeId: ['', Validators.required],
    name: ['', Validators.required]
  });
  private projectForm: FormGroup;
  private _selectedCoverages: Coverage[] = [];
  private separatorKeys: number[] = [ENTER, COMMA];

  @ViewChild('coverageInput') coverageInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private controlContainer: ControlContainer, private coverageService: CoverageService, private coverageTypeService: CoverageTypeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.projectForm = <FormGroup>this.controlContainer.control;
    this.loadCoverageTypeList();
    this.loadCoverageList();
  }

  addCoverage(event: MatChipInputEvent): void {
    const value = event.value;
    if ((value || '').trim()) {
      this.selectCoverageByName(value.trim());
    }
    this.resetCoverageInputs();
  }

  coverageSelected(event: MatAutocompleteSelectedEvent): void {
    this.selectCoverageByName(event.option.value);
    this.coverageForm.setValue(null);
  }

  filterCoverage(coverageList: Coverage[], coverageName: String): Coverage[] {
    const filterValue = coverageName.toLowerCase();
    let coveragesMatchingName = coverageList.filter(coverage => coverage.name.toLowerCase().indexOf(filterValue) === 0);
    if (coveragesMatchingName.length) {
      return coveragesMatchingName;
    } else {
      return null;
    }
  }

  filterCoverageOnValueChange(coverageName: String): Coverage[] {
    //Removemos las coberturas que ya hemos seleccionado
    let coverageLessSelected = this.coverageList.filter(coverage => this._selectedCoverages.indexOf(coverage) < 0);
    if (coverageName) {
      return this.filterCoverage(coverageLessSelected, coverageName);
    } else {
      return coverageLessSelected;
    }
  }

  loadCoverageList(): void {
    this.coverageService.findAll().subscribe(coverageList => {
      this.coverageList = coverageList;
      this.coverageListFiltered = this.coverageForm.valueChanges.pipe(startWith(this.coverageForm.value), map(coverageName => this.filterCoverageOnValueChange(coverageName)));
      let coverages = this.coverages.value;
      if (coverages.length > 0) {
        coverages.forEach(coverage => {
          this.selectCoverageByName(coverage.name);
        });
      }
    });
  }

  loadCoverageTypeList(): void {
    this.coverageTypeService.findAll().subscribe(coverageTypeList => {
      this._coverageTypeList = coverageTypeList;
    });
  }

  removeCoverage(coverage: Coverage): void {
    const index = this._selectedCoverages.indexOf(coverage);
    if (index >= 0) {
      this._selectedCoverages.splice(index, 1);
      this.coverageIdList.splice(index, 1);
      this.resetCoverageInputs();
    }
  }

  private resetCoverageInputs(): void {
    if (this.coverageInput) {
      //Limpiar elementos de input
      this.coverageInput.nativeElement.value = '';
      //Limpiar valores de control y lanzamiento de evento control,valueChanges
      this.coverageForm.setValue(null);
    }
  }

  save(stepper: MatStepper): void {
    this.coverage = this._form.value;
    this.coverageService.save(this.coverage).subscribe(() => {
      this._form.patchValue({
        coverageTypeId: '',
        name: ''
      });
      this.loadCoverageList();
      swal.fire('Nueva Cobertura', `${this.coverage.name}`, 'success');
      stepper.previous();
    });
  }

  private selectCoverageByName(coverageName: String): void {
    let founded = this.coverageList.filter(coverage => coverage.name == coverageName);
    if (founded.length) {
      let coverage = founded[0];
      this._selectedCoverages.push(coverage);
      this.coverageIdList.push(coverage.id);
    }
    else {
      swal.fire('No existe la cobertura', 'Intente registrar la cobertura antes de seleccionarla', 'error');
    }
    this.resetCoverageInputs();
    this.coverages.setValue(this.coverageIdList);
  }

  public get coverageForm(): FormControl {
    return this.coverageChipForm;
  }

  public get coverages() { return this.projectForm.get('coverageList'); }
  public get coverageTypeId() { return this._form.get('coverageTypeId'); }

  public get coverageTypeList(): CoverageType[] {
    return this._coverageTypeList;
  }

  public get filteredCoverages(): Observable<Coverage[]> {
    return this.coverageListFiltered;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get name() { return this._form.get('name'); }

  public get selectedCoverages(): Coverage[] {
    return this._selectedCoverages;
  }

  public get separatorKeysCodes(): number[] {
    return this.separatorKeys;
  }
}