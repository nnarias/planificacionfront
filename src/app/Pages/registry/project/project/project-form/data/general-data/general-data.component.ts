import { AmpleAreaKnowledge } from 'src/app/Pages/registry/knowledge-area/model/ampleAreaKnowledge';
import { AmpleAreaKnowledgeService } from 'src/app/Pages/registry/knowledge-area/service/ample-area-knowledge.service';
import { CollegeCareer } from 'src/app/Pages/registry/college/model/collegeCareer';
import { CollegeCareerService } from 'src/app/Pages/registry/college/service/college-career.service';
import { CollegeDepartment } from 'src/app/Pages/registry/college/model/collegeDepartment';
import { CollegeDepartmentService } from 'src/app/Pages/registry/college/service/college-department.service';
import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, FormControl, FormGroup, Validators } from '@angular/forms';
import { DetailedAreaKnowledge } from 'src/app/Pages/registry/knowledge-area/model/detailedAreaKnowledge';
import { DetailedAreaKnowledgeService } from 'src/app/Pages/registry/knowledge-area/service/detailed-area-knowledge.service';
import { EconomicPartnerGoal } from 'src/app/Pages/registry/purpose/model/economicPartnerGoal';
import { EconomicPartnerGoalService } from 'src/app/Pages/registry/purpose/service/economic-partner-goal.service';
import { EspeAreaKnowledge } from 'src/app/Pages/registry/knowledge-area/model/espeAreaKnowledge';
import { EspeAreaKnowledgeService } from 'src/app/Pages/registry/knowledge-area/service/espe-area-knowledge.service';
import { GoalPlan } from 'src/app/Pages/registry/purpose/model/goalPlan';
import { GoalPlanService } from 'src/app/Pages/registry/purpose/service/goal-plan.service';
import { LineResearch } from 'src/app/Pages/registry/research/model/lineResearch';
import { LineResearchService } from 'src/app/Pages/registry/research/service/line-research.service';
import { PolicyPlan } from 'src/app/Pages/registry/purpose/model/policyPlan';
import { PolicyPlanService } from 'src/app/Pages/registry/purpose/service/policy-plan.service';
import { PostgraduateProgram } from 'src/app/Pages/registry/college/model/postgraduateProgram';
import { PostgraduateProgramService } from 'src/app/Pages/registry/college/service/postgraduate-program.service';
import { Program } from 'src/app/Pages/registry/college/model/program';
import { ProgramService } from 'src/app/Pages/registry/college/service/program.service';
import { ProjectStatus } from '../../../../model/projectStatus';
import { ProjectStatusService } from 'src/app/Pages/registry/project/service/project-status.service';
import { ResearchGroup } from 'src/app/Pages/registry/research/model/researchGroup';
import { ResearchGroupService } from 'src/app/Pages/registry/research/service/research-group.service';
import { ResearchType } from 'src/app/Pages/registry/research/model/researchType';
import { ResearchTypeService } from 'src/app/Pages/registry/research/service/research-type.service';
import { ScientificDiscipline } from 'src/app/Pages/registry/research/model/scientificDiscipline';
import { ScientificDisciplineService } from 'src/app/Pages/registry/research/service/scientific-discipline.service';
import { SpecificAreaKnowledge } from 'src/app/Pages/registry/knowledge-area/model/specificAreaKnowledge';
import { SpecificAreaKnowledgeService } from 'src/app/Pages/registry/knowledge-area/service/specific-area-knowledge.service';
import { SubUnescoAreaKnowledge } from 'src/app/Pages/registry/knowledge-area/model/subUnescoAreaKnowledge';
import { SubUnescoAreaKnowledgeService } from 'src/app/Pages/registry/knowledge-area/service/sub-unesco-area-knowledge.service';
import * as textEditor from 'src/assets/ckeditor/ckEditor';
import { UnescoAreaKnowledge } from 'src/app/Pages/registry/knowledge-area/model/unescoAreaKnowledge';
import { UnescoAreaKnowledgeService } from 'src/app/Pages/registry/knowledge-area/service/unesco-area-knowledge.service';

@Component({
  selector: 'app-general-data',
  templateUrl: './general-data.component.html',
  styleUrls: ['./general-data.component.scss']
})
export class GeneralDataComponent implements OnInit {
  private _ampleAreaKnowledgeList: AmpleAreaKnowledge[] = [];
  private _collegeCareerList: CollegeCareer[] = [];
  private _collegeDepartmentList: CollegeDepartment[] = [];
  private _economicPartnerGoalList: EconomicPartnerGoal[] = [];
  private _detailedAreaKnowledgeList: DetailedAreaKnowledge[] = [];
  private _editor = textEditor;
  private _espeAreaKnowledgeList: EspeAreaKnowledge[] = [];
  private _form: FormGroup;
  private _goalPlanForm: FormControl = new FormControl('', Validators.required);
  private _goalPlanList: GoalPlan[] = [];
  private _lineResearchList: LineResearch[] = [];
  private loading = {
    ampleAreaKnowledge: false,
    collegeCareer: false,
    collegeDepartment: false,
    detailedAreaKnowledge: false,
    economicPartnerGoal: false,
    espeAreaKnowledge: false,
    goalPlan: false,
    lineResearch: false,
    policies: false,
    postgraduateProgram: false,
    program: false,
    projectStatus: false,
    researchGroup: false,
    researchType: false,
    scientificDiscipline: false,
    specificAreaKnowledge: false,
    subUnescoAreaKnowledge: false,
    unescoAreaKnowledge: false
  };
  private _policyPlanList: PolicyPlan[] = [];
  private _postgraduateProgramList: PostgraduateProgram[] = [];
  private _programList: Program[] = [];
  private _projectStatusList: ProjectStatus[] = [];
  private _researchGroupList: ResearchGroup[] = [];
  private _researchTypeList: ResearchType[] = [];
  private _scientificDisciplineList: ScientificDiscipline[] = [];
  private _showSpiner: Boolean = true;
  private _specificAreaKnowledeList: SpecificAreaKnowledge[] = [];
  @Input() subUnescoAreaKnowledge: SubUnescoAreaKnowledge;
  private _subUnescoAreaKnowledgeList: SubUnescoAreaKnowledge[] = [];
  private _unescoAreaKnowledgeForm: FormControl = new FormControl('', Validators.required);
  private _unescoAreaKnowledgeList: UnescoAreaKnowledge[] = [];

  constructor(private ampleAreaKnowledgeService: AmpleAreaKnowledgeService, private collegeCareerService: CollegeCareerService,
    private collegeDepartmentService: CollegeDepartmentService, private controlContainer: ControlContainer, private detailedAreaKnowledgeService: DetailedAreaKnowledgeService,
    private economicPartnerGoalService: EconomicPartnerGoalService, private espeAreaKnowledgeService: EspeAreaKnowledgeService, private goalPlanService: GoalPlanService,
    private lineResearchService: LineResearchService, private policyPlanService: PolicyPlanService, private postgraduateProgramService: PostgraduateProgramService,
    private programService: ProgramService, private researchGroupService: ResearchGroupService, private researchTypeService: ResearchTypeService,
    private scientificDisciplineService: ScientificDisciplineService, private SpecificAreaKnowledgeService: SpecificAreaKnowledgeService,
    private projectStatusService: ProjectStatusService, private subUnescoAreaKnowledgeService: SubUnescoAreaKnowledgeService,
    private unescoAreaKnowledgeService: UnescoAreaKnowledgeService) { }

  ngOnInit(): void {
    this._form = <FormGroup>this.controlContainer.control;
    this.loadAmpleAreaKnowledgeList();
    this.loadCollegeCareerList();
    this.loadCollegeDepartmentList();
    this.loadDetailedAreaKnowledgeList();
    this.loadEconomicPartnerGoalList();
    this.loadEspeAreaKnowledgeList();
    this.loadGoalPlanList();
    this.loadLineResearchList();
    this.loadPostgraduateProgramList();
    this.loadProgramList();
    this.loadProjectStatus();
    this.loadResearchGroupList();
    this.loadResearchTypeList();
    this.loadScientificDisciplineList();
    this.loadSpecificAreaKnowledgeList();
    this.loadUnescoAreaKnowledgeList();
    this.setValues();
  }

  changePolicy(): void {
    this._showSpiner = true;
    this.policyPlanService.findByGoalPlan(this._goalPlanForm.value).subscribe(policyPlanList => {
      this.policies.setValue(null);
      this._policyPlanList = policyPlanList;
      this._showSpiner = false;
    });
  }

  changeUnesco(): void {
    this._showSpiner = true;
    this.subUnescoAreaKnowledgeService.findByUnesco(this._unescoAreaKnowledgeForm.value).subscribe(subUnescoAreaKnowledgeList => {
      this.subUnescoAreaKnowledgeId.setValue(null);
      this._subUnescoAreaKnowledgeList = subUnescoAreaKnowledgeList;
      this._showSpiner = false;
    });
  }

  check(): void {
    let charged = true;
    for (let value in this.loading) {
      if (!this.loading[value]) {
        charged = false;
        return;
      }
    }
    if (charged) {
      this._showSpiner = false;
    }
  }

  loadAmpleAreaKnowledgeList(): void {
    this.ampleAreaKnowledgeService.findAll().subscribe(ampleAreaKnowledgeList => {
      this._ampleAreaKnowledgeList = ampleAreaKnowledgeList;
      this.loading.ampleAreaKnowledge = true;
      this.check();
    });
  }

  loadCollegeCareerList(): void {
    this.collegeCareerService.findAll().subscribe(collegeCareerList => {
      this._collegeCareerList = collegeCareerList;
      this.loading.collegeCareer = true;
      this.check();
    });
  }

  loadCollegeDepartmentList(): void {
    this.collegeDepartmentService.findAll().subscribe(collegeDepartmentList => {
      this._collegeDepartmentList = collegeDepartmentList;
      this.loading.collegeDepartment = true;
      this.check();
    });
  }

  loadDetailedAreaKnowledgeList(): void {
    this.detailedAreaKnowledgeService.findAll().subscribe(detailedAreaKnowledgeList => {
      this._detailedAreaKnowledgeList = detailedAreaKnowledgeList;
      this.loading.detailedAreaKnowledge = true;
      this.check();
    });
  }

  loadEconomicPartnerGoalList(): void {
    this.economicPartnerGoalService.findAll().subscribe(economicPartnerGoalList => {
      this._economicPartnerGoalList = economicPartnerGoalList;
      this.loading.economicPartnerGoal = true;
      this.check();
    });
  }

  loadEspeAreaKnowledgeList(): void {
    this.espeAreaKnowledgeService.findAll().subscribe(espeAreaKnowledgeList => {
      this._espeAreaKnowledgeList = espeAreaKnowledgeList;
      this.loading.espeAreaKnowledge = true;
      this.check();
    });
  }

  loadGoalPlanList(): void {
    this.goalPlanService.findAll().subscribe(goalPlanList => {
      this._goalPlanList = goalPlanList;
      this.loading.goalPlan = true;
      this.check();
    })
  }

  loadLineResearchList(): void {
    this.lineResearchService.findAll().subscribe(lineResearchList => {
      this._lineResearchList = lineResearchList;
      this.loading.lineResearch = true;
      this.check();
    });
  }

  loadPostgraduateProgramList(): void {
    this.postgraduateProgramService.findAll().subscribe(postgraduateProgramList => {
      this._postgraduateProgramList = postgraduateProgramList;
      this.loading.postgraduateProgram = true;
      this.check();
    });
  }

  loadProgramList(): void {
    this.programService.findAll().subscribe(programList => {
      this._programList = programList;
      this.loading.program = true;
      this.check();
    });
  }

  loadProjectStatus(): void {
    this.projectStatusService.findAll().subscribe(projectStatusList => {
      this._projectStatusList = projectStatusList;
      this.loading.projectStatus = true;
      this.check();
    });
  }

  loadResearchGroupList(): void {
    this.researchGroupService.findAll().subscribe(researchGroupList => {
      this._researchGroupList = researchGroupList;
      this.loading.researchGroup = true;
      this.check();
    });
  }

  loadResearchTypeList(): void {
    this.researchTypeService.findAll().subscribe(researchTypeList => {
      this._researchTypeList = researchTypeList;
      this.loading.researchType = true;
      this.check();
    });
  }

  loadScientificDisciplineList(): void {
    this.scientificDisciplineService.findAll().subscribe(scientificDisciplineList => {
      this._scientificDisciplineList = scientificDisciplineList;
      this.loading.scientificDiscipline = true;
      this.check();
    });
  }

  loadSpecificAreaKnowledgeList(): void {
    this.SpecificAreaKnowledgeService.findAll().subscribe(specificAreaKnwoledgeList => {
      this._specificAreaKnowledeList = specificAreaKnwoledgeList;
      this.loading.specificAreaKnowledge = true;
      this.check();
    });
  }

  loadUnescoAreaKnowledgeList(): void {
    this.unescoAreaKnowledgeService.findAll().subscribe(unescoAreaKnowledgeList => {
      this._unescoAreaKnowledgeList = unescoAreaKnowledgeList;
      this.loading.unescoAreaKnowledge = true;
      this.check();
    });
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  setValues(): void {
    if (this.subUnescoAreaKnowledge) {
      let id = this.subUnescoAreaKnowledge.unescoAreaKnowledge.id;
      this._unescoAreaKnowledgeForm.setValue(id);
      this.subUnescoAreaKnowledgeService.findByUnesco(id).subscribe(subUnescoAreaKnowledgeList => {
        this._subUnescoAreaKnowledgeList = subUnescoAreaKnowledgeList;
        this.subUnescoAreaKnowledgeId.setValue(this.subUnescoAreaKnowledge.id);
        this.loading.subUnescoAreaKnowledge = true;
        this.check();
      });
    }
    else {
      this.loading.subUnescoAreaKnowledge = true;
    }
    let policies = this.policies.value;
    if (policies.length > 0) {
      let id = policies[0].goalPlan.id;
      this.goalPlanForm.setValue(id);
      this.policyPlanService.findByGoalPlan(id).subscribe(policyPlanList => {
        this._policyPlanList = policyPlanList;
        let policyPlanIdList: number[] = [];
        policies.forEach(policyPlan => {
          policyPlanIdList.push(policyPlan.id);
        });
        this.policies.setValue(policyPlanIdList);
        this.loading.policies = true;
        this.check();
      });
    }
    else {
      this.loading.policies = true;
    }
  }

  public get ampleAreaKnowledgeId() { return this._form.get('ampleAreaKnowledgeId'); }

  public get ampleAreaKnowledgeList(): AmpleAreaKnowledge[] {
    return this._ampleAreaKnowledgeList;
  }

  public get collegeCareerId() { return this._form.get('collegeCareerId'); }

  public get collegeCareerList(): CollegeCareer[] {
    return this._collegeCareerList;
  }

  public get collegeDepartmentId() { return this._form.get('collegeDepartmentId'); }

  public get collegeDepartmentList(): CollegeDepartment[] {
    return this._collegeDepartmentList;
  }

  public get detailedAreaKnowledgeId() { return this._form.get('detailedAreaKnowledgeId'); }

  public get detailedAreaKnowledgeList(): DetailedAreaKnowledge[] {
    return this._detailedAreaKnowledgeList;
  }

  public get economicPartnerGoalId() { return this._form.get('economicPartnerGoalId'); }

  public get economicPartnerGoalList(): EconomicPartnerGoal[] {
    return this._economicPartnerGoalList;
  }

  public get editor(): textEditor {
    return this._editor;
  }

  public get endDate() { return this._form.get('endDate'); }

  public get espeAreaKnowledgeId() { return this._form.get('espeAreaKnowledgeId'); }

  public get espeAreaKnowledgeList(): EspeAreaKnowledge[] {
    return this._espeAreaKnowledgeList;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get goalPlanForm(): FormControl {
    return this._goalPlanForm;
  }

  public get goalPlanList(): GoalPlan[] {
    return this._goalPlanList;
  }

  public get lineResearchId() { return this._form.get('lineResearchId'); }

  public get lineResearchList(): LineResearch[] {
    return this._lineResearchList;
  }

  public get nameEnglish() { return this._form.get('nameEnglish'); }
  public get nameSpanish() { return this._form.get('nameSpanish'); }
  public get policies() { return this._form.get('policyPlanList'); }

  public get policyPlanList(): PolicyPlan[] {
    return this._policyPlanList;
  }

  public get postgraduateProgramId() { return this._form.get('postgraduateProgramId'); }

  public get postgraduateProgramList(): PostgraduateProgram[] {
    return this._postgraduateProgramList;
  }

  public get programId() { return this._form.get('programId'); }

  public get programList(): Program[] {
    return this._programList;
  }

  public get projectStatusId() { return this._form.get('projectStatusId'); }

  public get projectStatusList(): ProjectStatus[] {
    return this._projectStatusList;
  }

  public get researchGroupId() { return this._form.get('researchGroupId'); }

  public get researchGroupList(): ResearchGroup[] {
    return this._researchGroupList;
  }

  public get researchTypeId() { return this._form.get('researchTypeId'); }

  public get researchTypeList(): ResearchType[] {
    return this._researchTypeList;
  }

  public get scientificDisciplineId() { return this._form.get('scientificDisciplineId'); }

  public get scientificDisciplineList(): ScientificDiscipline[] {
    return this._scientificDisciplineList;
  }

  public get specificAreaKnowledgeList(): SpecificAreaKnowledge[] {
    return this._specificAreaKnowledeList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get specificAreaKnowledgeId() { return this._form.get('specificAreaKnowledgeId'); }
  public get startDate() { return this._form.get('startDate'); }
  public get subUnescoAreaKnowledgeId() { return this._form.get('subUnescoAreaKnowledgeId'); }

  public get subUnescoAreaKnowledgeList(): SubUnescoAreaKnowledge[] {
    return this._subUnescoAreaKnowledgeList;
  }

  public get unescoAreaKnowledgeForm(): FormControl {
    return this._unescoAreaKnowledgeForm;
  }

  public get unescoAreaKnowledgeList(): UnescoAreaKnowledge[] {
    return this._unescoAreaKnowledgeList;
  }
}