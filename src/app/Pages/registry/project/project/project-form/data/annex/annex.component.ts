import { Annex } from 'src/app/Pages/registry/annex/model/annex';
import { AnnexService } from 'src/app/Pages/registry/annex/service/annex.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ControlContainer, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ProjectAnnex } from '../../../../model/projectAnnex';
import { ProjectAnnexService } from '../../../../service/project-annex.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-annex',
  templateUrl: './annex.component.html',
  styleUrls: ['./annex.component.scss']
})
export class AnnexComponent implements OnInit {
  private _annexList: Annex[] = [];
  private _dataSource = new MatTableDataSource<Annex>();
  private _displayedColumns: string[] = ['name', 'file', 'description'];
  private _files = {};
  private _form: FormGroup;
  @Input() fullForm: FormGroup;
  private _projectAnnexList: ProjectAnnex[] = [];
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private annexService: AnnexService, private controlContainer: ControlContainer, private projectAnnexService: ProjectAnnexService) { }

  ngOnInit(): void {
    this._form = <FormGroup>this.controlContainer.control;
    this.loadAnnexList();
  }

  loadAnnexList(): void {
    this.annexService.findAll().subscribe(annexList => {
      this._annexList = annexList;
      this._dataSource.data = this._annexList;
      this._annexList.forEach(annex => {
        let fileId = 'annexFile' + annex.id;
        this._files[fileId] = 'Seleccionar archivo...';
        if (annex.required) {
          this.fullForm.addControl(fileId, new FormControl('', Validators.required));
        } else {
          this.fullForm.addControl(fileId, new FormControl(''));
        }
        this._form.addControl(fileId, new FormControl(''));
        this._form.addControl("annexFileDescription" + annex.id, new FormControl(''));
        this.fullForm.addControl("annexFileDescription" + annex.id, new FormControl(''));
      });
      if (this._form.get('projectAnnexList')) {
        this._projectAnnexList = this._form.get('projectAnnexList').value;
        this._projectAnnexList.forEach(projectAnnex => {
          projectAnnex.annexId = projectAnnex.annex.id;
          if (projectAnnex.fileName) {
            this.projectAnnexService.findFile(projectAnnex.id).subscribe(blob => {
              let file = new File([blob], projectAnnex.fileName);
              projectAnnex.file = file;
              this._form.get('annexFile' + projectAnnex.annex.id).patchValue(file);
            });
            this._form.get("annexFileDescription" + projectAnnex.annex.id).patchValue(projectAnnex.description);
            this._files['annexFile' + projectAnnex.annex.id] = projectAnnex.fileName;
          }
        });
      }
      this._showSpiner = false;
    });
  }

  setFile(event, annex: Annex) {
    var fileElement = event.target;
    let fileId = 'annexFile' + annex.id;
    if (fileElement.value) {
      let file: File = fileElement.files[0];
      let checkFile = this._projectAnnexList.some(projectAnnex => projectAnnex.fileName == file.name);
      if (checkFile) {
        swal.fire('Error', 'No se puede agregar el mismo archivo!', 'error');
      } else {
        let projectAnnex: ProjectAnnex = new ProjectAnnex();
        projectAnnex.file = file;
        projectAnnex.fileName = file.name;
        this._files[fileId] = file.name;
        projectAnnex.annex = annex;
        projectAnnex.annexId = annex.id;
        let savedAnnex = this._projectAnnexList.find(projectAnnex => projectAnnex.annexId == annex.id);
        if (savedAnnex) {
          if (savedAnnex.id) {
            projectAnnex.id = savedAnnex.id;
            projectAnnex.updated = true;
          }
          this._projectAnnexList[this._projectAnnexList.indexOf(savedAnnex)] = projectAnnex;
        } else {
          this._projectAnnexList.push(projectAnnex);
        }
        this._form.get(fileId).patchValue(file);
        this._form.get('projectAnnexList').patchValue(this._projectAnnexList);
      }
    }
  }

  public get annexList(): Annex[] {
    return this._annexList;
  }

  public get dataSource(): MatTableDataSource<Annex> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get files(): {} {
    return this._files;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}