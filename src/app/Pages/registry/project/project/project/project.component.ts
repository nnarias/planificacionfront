import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from '../../model/project';
import { ProjectService } from '../../service/project.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Project>();
  private _displayedColumns: string[] = ['nameSpanish', 'callName', 'actions'];
  private paginator: MatPaginator;
  private _projectList: Project[] = [];
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'callName': return item.call.name;
        default: return item[property];
      }
    }
    this.loadProjectList();
  }

  delete(project: Project): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Si, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Proyecto: ${project.nameSpanish}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.projectService.delete(project.id).subscribe(() => {
          this._projectList = this._projectList.filter(result => result !== project);
          this._dataSource.data = this._projectList;
          swal.fire('Convocatoria Eliminada', `${project.nameSpanish}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadProjectList(): void {
    this.projectService.findAllByProjectDirector().subscribe(projectList => {
      this._projectList = projectList;
      this._dataSource.data = this._projectList;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<Project> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get projectList(): Project[] {
    return this._projectList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}