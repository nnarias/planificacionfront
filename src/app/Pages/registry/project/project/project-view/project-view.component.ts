import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Project } from '../../model/project';
import { ProjectService } from '../../service/project.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import * as textEditor from 'src/assets/ckeditor/ckEditor';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }]
})
export class ProjectViewComponent implements OnInit {
  private _bibliographyColumns: string[] = ['title', 'author', 'year'];
  private _editor = textEditor;
  private _showSpiner: boolean = true;
  private _project: Project;

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.projectService.findByProjectDirector(id).subscribe(project => {
          this._project = project;
          console.log(this._project);
          this._showSpiner = false;
        });
      }
    });
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    editor.isReadOnly = true;
  }

  public get bibliographyColumns(): string[] {
    return this._bibliographyColumns;
  }

  public get editor(): textEditor {
    return this._editor;
  }

  public get project(): textEditor {
    return this._project;
  }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }
}