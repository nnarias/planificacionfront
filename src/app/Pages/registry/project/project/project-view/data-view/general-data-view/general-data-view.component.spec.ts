import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralDataViewComponent } from './general-data-view.component';

describe('GeneralDataViewComponent', () => {
  let component: GeneralDataViewComponent;
  let fixture: ComponentFixture<GeneralDataViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralDataViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralDataViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
