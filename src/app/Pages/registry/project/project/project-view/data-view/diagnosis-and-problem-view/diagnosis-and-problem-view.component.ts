import { Component, Input, OnInit } from '@angular/core';
import * as textEditor from 'src/assets/ckeditor/ckEditor';
import { Project } from '../../../../model/project';

@Component({
  selector: 'app-diagnosis-and-problem-view',
  templateUrl: './diagnosis-and-problem-view.component.html',
  styleUrls: ['./diagnosis-and-problem-view.component.scss']
})
export class DiagnosisAndProblemViewComponent implements OnInit {
  private _editor = textEditor;
  @Input() project: Project;

  constructor() { }

  ngOnInit(): void {
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    editor.isReadOnly = true;
  }

  public get editor(): textEditor {
    return this._editor;
  }
}