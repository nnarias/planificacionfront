import { CallRequirement } from 'src/app/Pages/registry/call/model/callRequirement';
import { CallService } from 'src/app/Pages/registry/call/service/call.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from '../../../../model/project';

@Component({
  selector: 'app-project-requirement-view',
  templateUrl: './project-requirement-view.component.html',
  styleUrls: ['./project-requirement-view.component.scss']
})
export class ProjectRequirementViewComponent implements OnInit {
  private _dataSource = new MatTableDataSource<CallRequirement>();
  private _displayedColumns: string[] = ['name', "file"];
  private _files = {};
  @Input() project: Project;
  private _showSpiner: Boolean = true;

  constructor(private callService: CallService) { }

  ngOnInit(): void {
    this.loadCallList();
  }

  loadCallList(): void {
    this.callService.findActive().subscribe(callList => {
      let call = callList.find(call => call.id == this.project.call.id);
      this._dataSource.data = call.callRequirementList;
      call.callRequirementList.forEach(requirement => {
        let fileId = "file" + requirement.id;
        this._files[fileId] = "No se ha subido ningun archivo...";
      });
      this.project.projectRequirementList.forEach(projectRequirement => {
        this._files["file" + projectRequirement.requirement.id] = projectRequirement.file.name;
      });
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<CallRequirement> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get files(): {} {
    return this._files;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}