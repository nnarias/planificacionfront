import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../../model/project';

@Component({
  selector: 'app-viability-view',
  templateUrl: './viability-view.component.html',
  styleUrls: ['./viability-view.component.scss']
})
export class ViabilityViewComponent implements OnInit {
  @Input() project: Project;

  constructor() { }

  ngOnInit(): void {
  }
}