import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViabilityViewComponent } from './viability-view.component';

describe('ViabilityViewComponent', () => {
  let component: ViabilityViewComponent;
  let fixture: ComponentFixture<ViabilityViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViabilityViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViabilityViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
