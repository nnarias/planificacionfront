import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRequirementViewComponent } from './project-requirement-view.component';

describe('ProjectRequirementViewComponent', () => {
  let component: ProjectRequirementViewComponent;
  let fixture: ComponentFixture<ProjectRequirementViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectRequirementViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectRequirementViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
