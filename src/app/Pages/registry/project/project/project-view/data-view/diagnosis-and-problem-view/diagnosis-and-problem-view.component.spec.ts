import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosisAndProblemViewComponent } from './diagnosis-and-problem-view.component';

describe('DiagnosisAndProblemViewComponent', () => {
  let component: DiagnosisAndProblemViewComponent;
  let fixture: ComponentFixture<DiagnosisAndProblemViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiagnosisAndProblemViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosisAndProblemViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
