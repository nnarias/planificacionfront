import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from '../../../../model/project';
import { ProjectResponsible } from 'src/app/Pages/registry/staff/model/projectResponsible';
import * as textEditor from 'src/assets/ckeditor/ckEditor';

@Component({
  selector: 'app-general-data-view',
  templateUrl: './general-data-view.component.html',
  styleUrls: ['./general-data-view.component.scss']
})
export class GeneralDataViewComponent implements OnInit {
  private _dataSource = new MatTableDataSource<ProjectResponsible>();
  private _displayedColumns: string[] = ['function', 'numberIdentification', 'completeName', 'department/institution', 'telephone', 'email'];
  private _editor = textEditor;
  private paginator: MatPaginator;
  @Input() project: Project;
  private _showSpiner: boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor() { }

  ngOnInit(): void {
    this._dataSource.data = this.project.projectResponsibleList;
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'function': return item.projectResponsibleType.name;
        case 'completeName': return item.name + item.lastName;
        default: return item[property];
      }
    }
    this.project.coverageList.forEach(coverage => {
      coverage.colspan = (coverage.coverageType.name.length + coverage.name.length + 2) / 20;
      if (coverage.colspan < 0.5) {
        coverage.colspan = 1;
      }
    })
    this._showSpiner = false;
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    editor.isReadOnly = true;
  }

  public get dataSource(): MatTableDataSource<ProjectResponsible> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get editor(): textEditor {
    return this._editor;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}