import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../../model/project';

@Component({
  selector: 'app-goal-view',
  templateUrl: './goal-view.component.html',
  styleUrls: ['./goal-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class GoalViewComponent implements OnInit {
  private _activityColumns: string[] = ['startDate', 'endDate', 'name', 'deliverable', 'investmentBudget', 'currentBudget', 'total'];
  private _goalColumns: string[] = ['name', 'indicator', 'deliverable', 'assumption', 'general'];
  private _resourceColumns: string[] = ['name', 'investmentBudget', 'currentBudget', 'total'];
  @Input() project: Project;

  constructor() { }

  ngOnInit(): void {
  }

  public get activityColumns(): string[] {
    return this._activityColumns;
  }

  public get goalColumns(): string[] {
    return this._goalColumns;
  }

  public get resourceColumns(): string[] {
    return this._resourceColumns;
  }
}