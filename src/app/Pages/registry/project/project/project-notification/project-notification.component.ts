import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { ProjectNotificationService } from '../../../../admin/project-notification/service/project-notification.service';
import { ProjectNotification } from '../../../../admin/project-notification/model/projectNotification';

@Component({
  selector: 'app-project-notification',
  templateUrl: './project-notification.component.html',
  styleUrls: ['./project-notification.component.scss']
})
export class ProjectNotificationComponent implements OnInit {

  private dataSource = new MatTableDataSource<ProjectNotification>();
  private displayedColumns: string[] = ['title', 'user', 'commentary', 'creationDate'];
  private paginator: MatPaginator;
  private projectNotificationList: ProjectNotification[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private projectStatusChangeService: ProjectNotificationService) { }

  ngOnInit(): void {
    this.loadProjectStatusChangeList();
  }

  loadProjectStatusChangeList(): void {
    this.activatedRoute.params.subscribe(params => {
      let projectId = params['id'];
      this.projectStatusChangeService.find(projectId).subscribe(projectStatusChangeList => {
        this.dataSource.data = projectStatusChangeList;
        this.projectNotificationList = projectStatusChangeList;
      });
    });
  }


  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  public get getDataSource(): MatTableDataSource<ProjectNotification> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getProjectNotificationList(): ProjectNotification[] {
    return this.projectNotificationList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}
