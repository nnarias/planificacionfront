import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectStatusChangeComponent } from './project-status-change.component';

describe('ProjectStatusChangeComponent', () => {
  let component: ProjectStatusChangeComponent;
  let fixture: ComponentFixture<ProjectStatusChangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectStatusChangeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectStatusChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
