import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resource } from '../model/resource';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  private urlEndPoint: string = `${environment.apiUrl}/resource`;

  constructor(private http: HttpClient) { }

  find(id: number): Observable<Resource> {
    return this.http.get<Resource>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Resource[]> {
    return this.http.get<Resource[]>(this.urlEndPoint);
  }
}