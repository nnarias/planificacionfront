import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectStatus } from '../model/projectStatus';

@Injectable({
  providedIn: 'root'
})
export class ProjectStatusService {
  private urlEndPoint: string = `${environment.apiUrl}/projectStatus`;

  constructor(private http: HttpClient) { }

  find(id: number): Observable<ProjectStatus> {
    return this.http.get<ProjectStatus>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<ProjectStatus[]> {
    return this.http.get<ProjectStatus[]>(this.urlEndPoint);
  }
}