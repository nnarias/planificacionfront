/*import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProjectAnnexDocument } from '../model/projectAnnex';

@Injectable({
  providedIn: 'root'
})
export class ProjectAnnexDocumentService {

  private urlEndPoint: string = `${environment.apiUrl}/projectAnnexDocument`;

  constructor(private http: HttpClient) { }

  findAllProject(id: number): Observable<ProjectAnnexDocument[]> {
    return this.http.get<ProjectAnnexDocument[]>(`${this.urlEndPoint}/${id}`);
  }

  save(formData: FormData): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, formData);
  }
}*/
