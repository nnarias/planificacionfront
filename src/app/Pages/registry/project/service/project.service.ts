import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../model/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private urlEndPoint: string = `${environment.apiUrl}/project`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Project> {
    return this.http.patch<Project>(`${this.urlEndPoint}/${id}`, null);
  }

  findAllApproving(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/approving`);
  }

  findAllByDepartmentalEvaluator(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/departmentalEvaluator`);
  }

  findAllByEvaluatorAssigning(id: number): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/evaluatorAssigning/call/${id}`);
  }

  findAllByFinancialSupervisor(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/financialSupervisor`);
  }

  findAllByProjectDirector(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/projectDirector`);
  }

  findAllByTechnicalSupervisor(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/technicalSupervisor`);
  }

  findAllDeveloping(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/developing`);
  }

  findAllEvaluating(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.urlEndPoint}/evaluating`);
  }

  findByEvaluatorAssigning(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.urlEndPoint}/evaluatorAssigning/${id}`);
  }

  findByFinancialSupervisor(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.urlEndPoint}/financialSupervisor/${id}`);
  }

  findByProjectDirector(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.urlEndPoint}/projectDirector/${id}`);
  }

  findByProjectEvaluator(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.urlEndPoint}/projectEvaluator/${id}`);
  }

  findByTechnicalSupervisor(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.urlEndPoint}/technicalSupervisor/${id}`);
  }

  save(formData: FormData): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, formData);
  }
}