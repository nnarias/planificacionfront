import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectAnnexService {
  private urlEndPoint: string = `${environment.apiUrl}/projectAnnex`;

  constructor(private http: HttpClient) { }

  findFile(id: number): Observable<Blob> {
    return this.http.get<Blob>(`${this.urlEndPoint}/annex/${id}`, { responseType: 'blob' as 'json' });
  }
}