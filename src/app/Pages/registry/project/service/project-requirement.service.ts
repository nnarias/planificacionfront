import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectRequirementService {
  private urlEndPoint: string = `${environment.apiUrl}/projectRequirement`;

  constructor(private http: HttpClient) { }

  findFile(id: number): Observable<Blob> {
    return this.http.get<Blob>(`${this.urlEndPoint}/requirement/${id}`, { responseType: 'blob' as 'json' });
  }
}