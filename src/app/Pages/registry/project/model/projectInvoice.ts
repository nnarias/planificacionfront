export class ProjectInvoice {
    date: string;
    fileName: string;
    fileUUID: string;
    id: number;
}