export class Bibliography {
    author: string;
    city: string;
    country: string;
    editorial: string;
    id: number;
    pageName: string;
    title: string;
    url: string;
    website: boolean = false;
    year: number;
}