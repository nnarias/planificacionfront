import { Requirement } from "../../call/model/requirement";

export class ProjectRequirement {
  file: File;
  fileName: string;
  fileUUID: string;
  id: number;
  requirement: Requirement;
  requirementId: number;
  updated: boolean;
  constructor() {
    this.updated = false;
  }
}