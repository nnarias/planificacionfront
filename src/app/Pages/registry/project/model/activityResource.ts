import { GoalActivity } from './goalActivity';
import { ProjectInvoice } from './projectInvoice';
import { Resource } from './resource';

export class ActivityResource {
  currentBudget: number;
  goalActivity: GoalActivity;
  goalActivityId: number;
  id: number;
  investmentBudget: number;
  invoiceCurrentBudget: number;
  invoiceDate: string;
  invoiceDescription: string;
  invoiceFileName: string;
  invoiceFileUUID: string;
  invoiceInvestmentBudget: number;
  projectInvoice: ProjectInvoice;
  resource: Resource;
  resourceId: number;
  resourceName: string;
  state: boolean;
  constructor() {
    this.currentBudget = 0;
    this.id = 0;
    this.investmentBudget = 0;
    this.resourceId = 0;
    this.state = true;
  }
}