import { ActivityResource } from './activityResource';
import { ProjectGoal } from './projectGoal';

export class GoalActivity {
  activityResourceList: ActivityResource[];
  currentBudget: number;
  deliverable: string;
  deliverableDate: string;
  deliverableDescription: string;
  deliverableFileUUID: string;
  deliverableFileName: string;
  endDate: string;
  id: number;
  investmentBudget: number;
  name: string;
  projectGoal: ProjectGoal;
  startDate: string;
  state: boolean;
  constructor() {
    this.activityResourceList = [];
    this.currentBudget = 0;
    this.deliverable = "";
    this.deliverableDate = "";
    this.deliverableDescription = "";
    this.deliverableFileName = "";
    this.deliverableFileUUID = "";
    this.endDate = "";
    this.id = 0;
    this.investmentBudget = 0;
    this.name = "";
    this.startDate = "";
    this.state = true;
  }
}
