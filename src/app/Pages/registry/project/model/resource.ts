import { ActivityResource } from './activityResource';

export class Resource {
    activityResourceList: ActivityResource[];
    id: number;
    name: string;
    state: boolean;
}