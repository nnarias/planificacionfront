export class ProjectStatus {
    id: number;
    name: string;
    state: boolean;
}