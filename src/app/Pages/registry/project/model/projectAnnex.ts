import { Annex } from "../../annex/model/annex";

export class ProjectAnnex {
  annex: Annex;
  annexId: number;
  description: string;
  file: File;
  fileName: string;
  fileUUID: string;
  id: number;
  updated: boolean;
}