import { GoalActivity } from './goalActivity';

export class ProjectGoal {
    assumption: string;
    goalActivityList: GoalActivity[];
    deliverable: string;
    general: boolean;
    id: number;
    indicator: string;
    name: string;
    state: boolean;
    constructor() {
        this.assumption = "";
        this.deliverable = "";
        this.general = false;
        this.goalActivityList = [];
        this.id = 0;
        this.indicator = "";
        this.name = "";
        this.state = true;
    }
}