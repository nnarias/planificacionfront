import { AnnexComponent } from './anex/annex/annex.component';
import { AnnexFormComponent } from './anex/annex-form/annex-form.component';
import { AnnexRoutingModule } from './annex-routing.module';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [MenuComponent, AnnexComponent, AnnexFormComponent],
  imports: [AnnexRoutingModule, CommonModule, ImportsModule]
})
export class AnnexModule { }