import { AnnexComponent } from './anex/annex/annex.component';
import { AnnexFormComponent } from './anex/annex-form/annex-form.component';
import { AuthGuard } from '../../security/guard/auth.guard';
import { MenuComponent } from '../annex/menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'annex', component: AnnexComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'annex/form', component: AnnexFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'annex/form/:id', component: AnnexFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnexRoutingModule { }