import { Annex } from '../../model/annex';
import { AnnexService } from '../../service/annex.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-annex',
  templateUrl: './annex.component.html',
  styleUrls: ['./annex.component.scss']
})
export class AnnexComponent implements OnInit {
  private _annexList: Annex[] = [];
  private _dataSource = new MatTableDataSource<Annex>();
  private _displayedColumns: string[] = ['name', 'required', 'actions'];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private annexService: AnnexService) { }

  ngOnInit(): void {
    this.loadAnnexList();
  }

  delete(annex: Annex): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el anexo del documento: ${annex.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.annexService.delete(annex.id).subscribe(() => {
          this._annexList = this._annexList.filter(result => result !== annex);
          this._dataSource.data = this._annexList;
          swal.fire('Anexo del documento eliminado', `${annex.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadAnnexList(): void {
    this.annexService.findAll().subscribe(annexList => {
      this._annexList = annexList;
      this._dataSource.data = annexList;
      this._showSpiner = false;
    });
  }

  public get annexList(): Annex[] {
    return this._annexList;
  }

  public get dataSource(): MatTableDataSource<Annex> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}