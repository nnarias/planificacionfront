import { ActivatedRoute, Router } from '@angular/router';
import { Annex } from '../../model/annex';
import { AnnexService } from '../../service/annex.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-annex-form',
  templateUrl: './annex-form.component.html',
  styleUrls: ['./annex-form.component.scss']
})
export class AnnexFormComponent implements OnInit {
  private _annex: Annex = new Annex();
  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required],
    required: ['']
  });
  private _showSpiner: Boolean = true;
  private _title: string;

  constructor(private activatedRoute: ActivatedRoute, private annexService: AnnexService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadAnnex();
  }

  loadAnnex(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.annexService.find(id).subscribe(annex => {
          this._form.patchValue({
            id: annex.id,
            name: annex.name,
            required: annex.required
          });
          this._showSpiner = false
        });
        this._title = 'Modificar Anexoo';
      } else {
        this._title = 'Crear Anexo';
        this._showSpiner = false;
      }
    });
  }

  save(): void {
    this._annex = this._form.value;
    this.annexService.save(this._annex).subscribe(() => {
      this.router.navigate(['app/registry/annex/annex']);
      swal.fire(this._annex.id ? 'Anexo Actualizado' : 'Nuevo Anexo', `${this._annex.name}`, 'success');
    });
  }

  public get annex(): Annex {
    return this._annex;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get name() { return this.form.get('name'); }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get title(): string {
    return this._title;
  }
}