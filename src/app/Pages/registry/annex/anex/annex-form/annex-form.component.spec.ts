import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnexFormComponent } from './annex-form.component';

describe('AnnexFormComponent', () => {
  let component: AnnexFormComponent;
  let fixture: ComponentFixture<AnnexFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnnexFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnexFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
