export class Annex {
    id: number;
    name: string;
    required: boolean;
}