import { Annex } from '../model/annex';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnnexService {

  private urlEndPoint: string = `${environment.apiUrl}/annex`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Annex> {
    return this.http.patch<Annex>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<Annex> {
    return this.http.get<Annex>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Annex[]> {
    return this.http.get<Annex[]>(this.urlEndPoint);
  }

  save(annex: Annex): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, annex);
  }
}
