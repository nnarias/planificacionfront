import { AuthGuard } from '../../security/guard/auth.guard';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectResponsibleTypeComponent } from './projectResponsibleType/project-responsible-type/project-responsible-type.component';
import { ProjectResponsibleTypeFormComponent } from './projectResponsibleType/project-responsible-type-form/project-responsible-type-form.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'projectResponsibleType', component: ProjectResponsibleTypeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'projectResponsibleType/form', component: ProjectResponsibleTypeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'projectResponsibleType/form/:id', component: ProjectResponsibleTypeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffRoutingModule { }