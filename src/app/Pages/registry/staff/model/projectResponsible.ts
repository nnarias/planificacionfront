import { CollegeDepartment } from '../../college/model/collegeDepartment';
import { ProjectResponsibleType } from './projectResponsibleType';

export class ProjectResponsible {
    collegeDepartment: CollegeDepartment = new CollegeDepartment();
    collegeDepartmentId: number;
    email: string;
    externalInstitution: string;
    externalResponsible: boolean;
    id: number;
    lastName: string;
    name: string;
    numberIdentification: string;
    projectResponsibleType: ProjectResponsibleType = new ProjectResponsibleType();
    projectResponsibleTypeId: number;
    projectResponsibleTypeName: String;
    telephone: string;
}