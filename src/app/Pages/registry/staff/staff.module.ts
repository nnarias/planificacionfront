import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectResponsibleTypeComponent } from './projectResponsibleType/project-responsible-type/project-responsible-type.component';
import { ProjectResponsibleTypeFormComponent } from './projectResponsibleType/project-responsible-type-form/project-responsible-type-form.component';
import { StaffRoutingModule } from './staff-routing.module';


@NgModule({
  declarations: [MenuComponent, ProjectResponsibleTypeComponent, ProjectResponsibleTypeFormComponent],
  imports: [CommonModule, ImportsModule, StaffRoutingModule]
})
export class StaffModule { }