import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectResponsibleType } from '../model/projectResponsibleType';

@Injectable({
  providedIn: 'root'
})
export class ProjectResponsibleTypeService {
  private urlEndPoint: string = `${environment.apiUrl}/projectResponsibleType`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<ProjectResponsibleType> {
    return this.http.patch<ProjectResponsibleType>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<ProjectResponsibleType> {
    return this.http.get<ProjectResponsibleType>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<ProjectResponsibleType[]> {
    return this.http.get<ProjectResponsibleType[]>(this.urlEndPoint);
  }

  save(projectResponsibleType: ProjectResponsibleType): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, projectResponsibleType);
  }
}