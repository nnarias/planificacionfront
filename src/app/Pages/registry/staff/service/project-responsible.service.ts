import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectResponsible } from '../model/projectResponsible';

@Injectable({
  providedIn: 'root'
})
export class ProjectResponsibleService {
  private urlEndPoint: string = `${environment.apiUrl}/projectResponsible`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<ProjectResponsible> {
    return this.http.patch<ProjectResponsible>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<ProjectResponsible> {
    return this.http.get<ProjectResponsible>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<ProjectResponsible[]> {
    return this.http.get<ProjectResponsible[]>(this.urlEndPoint);
  }

  save(projectResponsible: ProjectResponsible): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, projectResponsible);
  }
}