import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectResponsibleTypeComponent } from './project-responsible-type.component';

describe('ProjectResponsibleTypeComponent', () => {
  let component: ProjectResponsibleTypeComponent;
  let fixture: ComponentFixture<ProjectResponsibleTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectResponsibleTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectResponsibleTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
