import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ProjectResponsibleType } from '../../model/projectResponsibleType';
import { ProjectResponsibleTypeService } from '../../service/project-responsible-type.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-project-responsible-type',
  templateUrl: './project-responsible-type.component.html',
  styleUrls: ['./project-responsible-type.component.scss']
})
export class ProjectResponsibleTypeComponent implements OnInit {
  private dataSource = new MatTableDataSource<ProjectResponsibleType>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private projectResponsibleTypeList: ProjectResponsibleType[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private projectResponsibleTypeService: ProjectResponsibleTypeService) { }

  ngOnInit(): void {
    this.loadProjectResponsibleTypeList();
  }

  delete(projectResponsibleType: ProjectResponsibleType): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Tipo de Responsable de Proyecto: ${projectResponsibleType.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.projectResponsibleTypeService.delete(projectResponsibleType.id).subscribe(() => {
          this.projectResponsibleTypeList = this.projectResponsibleTypeList.filter(result => result !== projectResponsibleType);
          this.dataSource.data = this.projectResponsibleTypeList;
          swal.fire('Tipo de Responsable de Proyecto Eliminado', `${projectResponsibleType.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }


  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadProjectResponsibleTypeList(): void {
    this.projectResponsibleTypeService.findAll().subscribe(projectResponsibleTypeList => {
      this.dataSource.data = projectResponsibleTypeList;
      this.projectResponsibleTypeList = projectResponsibleTypeList;
    });
  }

  public get getProjectResponsibleTypeList(): ProjectResponsibleType[] {
    return this.projectResponsibleTypeList;
  }

  public get getDataSource(): MatTableDataSource<ProjectResponsibleType> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}