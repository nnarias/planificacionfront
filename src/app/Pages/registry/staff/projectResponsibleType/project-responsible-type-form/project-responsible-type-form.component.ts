import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectResponsibleType } from '../../model/projectResponsibleType';
import { ProjectResponsibleTypeService } from '../../service/project-responsible-type.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-project-responsible-type-form',
  templateUrl: './project-responsible-type-form.component.html',
  styleUrls: ['./project-responsible-type-form.component.scss']
})
export class ProjectResponsibleTypeFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private projectResponsibleType: ProjectResponsibleType = new ProjectResponsibleType();;
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private projectResponsibleTypeService: ProjectResponsibleTypeService, private router: Router) { }

  ngOnInit(): void {
    this.loadProjectResponsibleType();
  }

  loadProjectResponsibleType(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.projectResponsibleTypeService.find(id).subscribe(projectResponsibleType => {
          this.form.patchValue({
            id: projectResponsibleType.id,
            name: projectResponsibleType.name
          });
        });
        this.title = 'Modificar Tipo de Responsable de Proyecto';
      } else {
        this.title = 'Crear Tipo de Responsable de Proyecto';
      }
    });
  }

  save(): void {
    this.projectResponsibleType = this.form.value;
    this.projectResponsibleTypeService.save(this.projectResponsibleType).subscribe(() => {
      this.router.navigate(['app/registry/staff/projectResponsibleType']);
      swal.fire(this.projectResponsibleType.id ? 'Tipo de Responsable de Proyecto Actualizado' : 'Nuevo Tipo de Responsable de Proyecto',
        `${this.projectResponsibleType.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}