import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectResponsibleTypeFormComponent } from './project-responsible-type-form.component';

describe('ProjectResponsibleTypeFormComponent', () => {
  let component: ProjectResponsibleTypeFormComponent;
  let fixture: ComponentFixture<ProjectResponsibleTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectResponsibleTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectResponsibleTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
