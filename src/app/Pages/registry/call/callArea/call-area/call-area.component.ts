import { CallArea } from '../../model/callArea';
import { CallAreaService } from '../../service/call-area.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-call-area',
  templateUrl: './call-area.component.html',
  styleUrls: ['./call-area.component.scss']
})
export class CallAreaComponent implements OnInit {
  private callAreaList: CallArea[] = [];
  private dataSource = new MatTableDataSource<CallArea>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private callAreaService: CallAreaService) { }

  ngOnInit(): void {
    this.loadCallAreaDataList();
  }

  delete(callArea: CallArea): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Área de Convocatoria: ${callArea.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.callAreaService.delete(callArea.id).subscribe(() => {
          this.callAreaList = this.callAreaList.filter(result => result !== callArea);
          this.dataSource.data = this.callAreaList;
          swal.fire('Área de Convocatoria Eliminada', `${callArea.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadCallAreaDataList(): void {
    this.callAreaService.findAll().subscribe(callAreaList => {
      this.callAreaList = callAreaList;
      this.dataSource.data = callAreaList;
    });
  }

  public get getCallAreaList(): CallArea[] {
    return this.callAreaList;
  }

  public get getDataSource(): MatTableDataSource<CallArea> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}