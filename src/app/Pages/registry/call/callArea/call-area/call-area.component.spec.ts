import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallAreaComponent } from './call-area.component';

describe('CallAreaComponent', () => {
  let component: CallAreaComponent;
  let fixture: ComponentFixture<CallAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
