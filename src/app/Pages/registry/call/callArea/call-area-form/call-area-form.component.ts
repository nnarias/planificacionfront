import { ActivatedRoute, Router } from '@angular/router';
import { CallArea } from '../../model/callArea';
import { CallAreaService } from '../../service/call-area.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-call-area-form',
  templateUrl: './call-area-form.component.html',
  styleUrls: ['./call-area-form.component.scss']
})
export class CallAreaFormComponent implements OnInit {
  private callArea: CallArea = new CallArea();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private callAreaService: CallAreaService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadCallArea();
  }

  loadCallArea(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.callAreaService.find(id).subscribe(callArea => {
          this.form.patchValue({
            id: callArea.id,
            name: callArea.name
          });
        });
        this.title = 'Modificar Área de Convocatoria';
      } else {
        this.title = 'Crear Área de Convocatoria';
      }
    });
  }

  save(): void {
    this.callArea = this.form.value;
    this.callAreaService.save(this.callArea).subscribe(() => {
      this.router.navigate(['app/registry/call/callArea']);
      swal.fire(this.callArea.id ? 'Área de Convocatoria Actualizada' : 'Nueva Área de Convocatoria',
        `El área de convocatoria ${this.callArea.name}, se ha guardado con éxito`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}