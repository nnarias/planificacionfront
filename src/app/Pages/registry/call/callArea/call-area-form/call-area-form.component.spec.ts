import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallAreaFormComponent } from './call-area-form.component';

describe('CallAreaFormComponent', () => {
  let component: CallAreaFormComponent;
  let fixture: ComponentFixture<CallAreaFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallAreaFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallAreaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
