
import { CallComponent } from './call/call/call.component';
import { CallAreaComponent } from './callArea/call-area/call-area.component';
import { CallAreaFormComponent } from './callArea/call-area-form/call-area-form.component';
import { CallExtensionComponent } from './call/call-extension/call-extension.component';
import { CallFormComponent } from './call/call-form/call-form.component';
import { CallRoutingModule } from './call-routing.module';
import { CallViewComponent } from './call/call-view/call-view.component';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RequirementComponent } from './requirement/requirement/requirement.component';
import { RequirementFormComponent } from './requirement/requirement-form/requirement-form.component';


@NgModule({
  declarations: [CallAreaComponent, CallAreaFormComponent, CallComponent, CallExtensionComponent, CallFormComponent, CallViewComponent, MenuComponent, RequirementComponent, RequirementFormComponent],
  imports: [CommonModule, ImportsModule, CallRoutingModule]
})
export class CallModule { }