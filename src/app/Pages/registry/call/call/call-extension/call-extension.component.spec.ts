import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallExtensionComponent } from './call-extension.component';

describe('CallExtensionComponent', () => {
  let component: CallExtensionComponent;
  let fixture: ComponentFixture<CallExtensionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallExtensionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
