import { ActivatedRoute, Router } from '@angular/router';
import { Call } from '../../model/call';
import { CallExtension } from '../../model/callExtension';
import { CallService } from '../../service/call.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-call-extension',
  templateUrl: './call-extension.component.html',
  styleUrls: ['./call-extension.component.scss']
})
export class CallExtensionComponent implements OnInit {
  private _call: Call = new Call();
  private callExtension: CallExtension = new CallExtension();
  private _form: FormGroup = this.formBuilder.group({
    callExtension: ['', Validators.required],
    callId: [''],
    support: ['']
  });
  private id: number;
  private _minDate: Date;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private callService: CallService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadCall();
  }

  extend(): void {
    this.callExtension = this._form.value;
    this.callService.extension(this.callExtension).subscribe(() => {
      this.router.navigate(['app/registry/call/call']);
      swal.fire('Extensión Actualizada', `${this.call.name}`, 'success');
    });
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadCallData();
      }
    });
  }

  loadCallData(): void {
    this.callService.find(this.id).subscribe(call => {
      this._call = call;
      this._form.patchValue({
        callExtension: call.callExtension,
        callId: call.id,
        support: call.support
      });
      this._minDate = new Date(call.endDate);
      this._minDate.setDate(this._minDate.getDate() + 2);
      this._showSpiner = false;
    });
  }

  public get call(): Call {
    return this._call;
  }

  public get extension() { return this._form.get('callExtension'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get minDate(): Date {
    return this._minDate;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}