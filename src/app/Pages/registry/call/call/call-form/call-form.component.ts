import { ActivatedRoute, Router } from '@angular/router';
import { Area } from '../../model/area';
import { AreaService } from '../../service/area.service';
import { CallArea } from '../../model/callArea';
import { CallAreaService } from '../../service/call-area.service';
import { CallRequirement } from '../../model/callRequirement';
import { CallService } from '../../service/call.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Evaluation } from 'src/app/Pages/evaluation/model/evaluation';
import { EvaluationService } from 'src/app/Pages/evaluation/service/evaluation.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Requirement } from '../../model/requirement';
import { RequirementService } from '../../service/requirement.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-call-form',
  templateUrl: './call-form.component.html',
  styleUrls: ['./call-form.component.scss']
})
export class CallFormComponent implements OnInit {
  private _areaList: Area[] = [];
  private _callAreaList: CallArea[] = [];
  private callRequirementList: CallRequirement[] = [];
  private _dataSource = new MatTableDataSource<Requirement>();
  private _displayedColumns: string[] = ['name', 'documentation'];
  private _evaluationList: Evaluation[];
  private _form: FormGroup = this.formBuilder.group({
    areaList: ['', Validators.required],
    callAreaId: ['', Validators.required],
    description: ['', Validators.required],
    endDate: ['', Validators.required],
    evaluationId: ['', Validators.required],
    extension: [false],
    finacingAmount: ['', Validators.required],
    id: [''],
    name: ['', Validators.required],
    startDate: ['', Validators.required]
  });
  private id: number;
  private paginator: MatPaginator;
  private _requirementList: Requirement[] = [];
  private _showSpiner: Boolean = true;
  private sort: MatSort;
  private _title: string;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private areaService: AreaService, private callAreaService: CallAreaService, private callService: CallService,
    private evaluationService: EvaluationService, private formBuilder: FormBuilder, private requirementService: RequirementService, private router: Router) { }

  ngOnInit(): void {
    this.loadAreaList();
    this.loadEvaluationList();
    this.loadRequirementList();
    this.loadCall();
  }

  changeDocumentation(checked: boolean, requirement: Requirement): void {
    this.callRequirementList.forEach(callRequirement => {
      if (callRequirement.requirement.id == requirement.id) {
        callRequirement.required = checked;
      }
    });
  }

  changeRequirement(checked: boolean, requirement: Requirement): void {
    requirement.checked = checked;
    if (checked) {
      let callRequirement: CallRequirement = new CallRequirement();
      callRequirement.callId = this._form.get('id').value;
      callRequirement.requirement = requirement;
      callRequirement.requirementId = requirement.id;
      this.callRequirementList.push(callRequirement);
    }
    else {
      this.callRequirementList = this.callRequirementList.filter(callRequirement => callRequirement.requirement.id != requirement.id);
    }
  }

  loadAreaList(): void {
    this.areaService.findAll().subscribe(areaList => {
      this._areaList = areaList;
    });
  }

  loadCall(): void {
    this.loadCallAreaList();
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadCallData();
        this._title = 'Modificar Convocatoria';
      } else {
        this._title = 'Crear Convocatoria';
        this._showSpiner = false;
      }
    });
  }

  loadCallAreaList(): void {
    this.callAreaService.findAll().subscribe(callAreaList => {
      this._callAreaList = callAreaList;
    });
  }

  loadCallData(): void {
    let mySelectedArea = new Array();
    this.callService.find(this.id).subscribe(call => {
      call.areaList.forEach(area => {
        mySelectedArea.push(area.id);
      });
      this._form.patchValue({
        areaList: mySelectedArea,
        callAreaId: call.callArea.id,
        description: call.description,
        endDate: call.endDate,
        evaluationId: call.evaluation.id,
        extension: call.extension,
        finacingAmount: call.finacingAmount,
        id: call.id,
        name: call.name,
        startDate: call.startDate
      });
      this.callRequirementList = call.callRequirementList;
      call.callRequirementList.forEach(callRequirement => {
        this._requirementList.forEach(requirement => {
          if (callRequirement.requirement.id == requirement.id) {
            callRequirement.requirementId = requirement.id;
            requirement.checked = true;
            requirement.documentation = callRequirement.required;
          }
        });
      });
      this._showSpiner = false;
    });
  }

  loadEvaluationList(): void {
    this.evaluationService.findAll().subscribe(evaluationList => {
      this._evaluationList = evaluationList;
    });
  }

  loadRequirementList(): void {
    this.requirementService.findAll().subscribe(requirementList => {
      requirementList.forEach(requirement => {
        requirement.checked = false;
      });
      this._requirementList = requirementList;
      this._dataSource.data = requirementList;
    });
  }

  save(): void {
    let call = this._form.value;
    call.callRequirementList = this.callRequirementList;
    this.callService.save(call).subscribe(() => {
      this.router.navigate(['app/registry/call/call']);
      swal.fire(call.id ? 'Convocatoria Actualizada' : 'Nueva Convocatoria', `${call.name}`, 'success');
    });
  }

  public get areaList(): Area[] {
    return this._areaList;
  }

  public get areas() { return this._form.get('areaList'); }
  public get callAreaId() { return this._form.get('callAreaId'); }

  public get callAreaList(): CallArea[] {
    return this._callAreaList;
  }

  public get dataSource(): MatTableDataSource<Requirement> {
    return this._dataSource;
  }

  public get description() { return this._form.get('description'); }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get endDate() { return this._form.get('endDate'); }
  public get evaluationId() { return this._form.get('evaluationId'); }

  public get evaluationList(): Evaluation[] {
    return this._evaluationList;
  }

  public get finacingAmount() { return this._form.get('finacingAmount'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get name() { return this._form.get('name'); }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get startDate() { return this._form.get('startDate'); }

  public get title(): string {
    return this._title;
  }
}