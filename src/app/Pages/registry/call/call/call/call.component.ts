import { Call } from '../../model/call';
import { CallService } from '../../service/call.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})
export class CallComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Call>();
  private _displayedColumns: string[] = ['name', 'startDate', 'endDate', 'extension', 'actions'];
  private _callList: Call[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private callService: CallService) { }

  ngOnInit(): void {
    this.loadCallDataList();
  }

  delete(call: Call): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar La Convocatoria: ${call.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.callService.delete(call.id).subscribe(() => {
          this._callList = this._callList.filter(result => result !== call);
          this._dataSource.data = this._callList;
          swal.fire('Convocatoria Eliminada', `${call.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadCallDataList(): void {
    this.callService.findAll().subscribe(callList => {
      this._callList = callList;
      this._dataSource.data = callList;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<Call> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get callList(): Call[] {
    return this._callList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}