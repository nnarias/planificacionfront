import { ActivatedRoute } from '@angular/router';
import { Call } from '../../model/call';
import { CallService } from '../../service/call.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-call-view',
  templateUrl: './call-view.component.html',
  styleUrls: ['./call-view.component.scss']
})
export class CallViewComponent implements OnInit {
  private _call: Call = new Call();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private callService: CallService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.callService.find(id).subscribe(call => {
          this._call = call;
          this._showSpiner = false;
        });
      }
    });
  }

  public get call(): Call {
    return this._call;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}