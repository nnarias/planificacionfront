import { Area } from './area';
import { CallArea } from './callArea';
import { CallRequirement } from './callRequirement';
import { Evaluation } from 'src/app/Pages/evaluation/model/evaluation';

export class Call {
    areaList: Area[] = [];
    callArea: CallArea = new CallArea();
    callExtension: string;
    callRequirementList: CallRequirement[] = [];
    description: string;
    endDate: string;
    evaluation: Evaluation;
    extension: boolean;
    finacingAmount: number;
    id: number;
    name: string;
    startDate: string;
    support: string;
}