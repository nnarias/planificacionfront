import { Requirement } from './requirement';

export class CallRequirement {
    callId: number;
    id: number;
    required: boolean;
    requirement: Requirement;
    requirementId: number;

    constructor() {
        this.required = false;
    }
}