export class Requirement {
    checked: boolean;
    documentation: boolean;
    id: number;
    name: string;
}