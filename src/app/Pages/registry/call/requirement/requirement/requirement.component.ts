import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Requirement } from '../../model/requirement';
import { RequirementService } from '../../service/requirement.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-requirement',
  templateUrl: './requirement.component.html',
  styleUrls: ['./requirement.component.scss']
})
export class RequirementComponent implements OnInit {
  private dataSource = new MatTableDataSource<Requirement>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private requirementList: Requirement[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private requirementService: RequirementService) { }

  ngOnInit(): void {
    this.loadRequirementDataList();
  }

  delete(requirement: Requirement): void {
    const swallBootstrap = swal.mixin({
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      },
      buttonsStyling: false
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      text: `Seguro que desea eliminar el Requisito de Convocatoria: ${requirement.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.requirementService.delete(requirement.id).subscribe(() => {
          this.requirementList = this.requirementList.filter(result => result !== requirement);
          this.dataSource.data = this.requirementList;
          swal.fire('Requisito de Convocatoria Eliminado', `${requirement.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadRequirementDataList(): void {
    this.requirementService.findAll().subscribe(requirementList => {
      this.dataSource.data = requirementList;
      this.requirementList = requirementList;
    });
  }

  public get getDataSource(): MatTableDataSource<Requirement> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getRequirementList(): Requirement[] {
    return this.requirementList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}