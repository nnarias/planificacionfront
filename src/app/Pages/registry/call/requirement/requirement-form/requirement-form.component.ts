import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Requirement } from '../../model/requirement';
import { RequirementService } from '../../service/requirement.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-requirement-form',
  templateUrl: './requirement-form.component.html',
  styleUrls: ['./requirement-form.component.scss']
})
export class RequirementFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private requirement: Requirement = new Requirement();;
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private requirementServie: RequirementService, private router: Router) { }

  ngOnInit(): void {
    this.loadRequirement();
  }

  loadRequirement(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.requirementServie.find(id).subscribe(requirement => {
          this.form.patchValue({
            id: requirement.id,
            name: requirement.name
          });
        });
        this.title = 'Modificar Requisito de Convocatoria';
      } else {
        this.title = 'Crear Requisito de Convocatoria';
      }
    });
  }

  save(): void {
    this.requirement = this.form.value;
    this.requirementServie.save(this.requirement).subscribe(() => {
      this.router.navigate(['app/registry/call/requirement']);
      swal.fire(this.requirement.id ? 'Requisito de Convocatoria Actualizado' : 'Nuevo Requisito de Convocatoria', `${this.requirement.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}