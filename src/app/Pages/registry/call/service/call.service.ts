import { Call } from '../model/call';
import { CallExtension } from '../model/callExtension';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallService {

  private urlEndPoint: string = `${environment.apiUrl}/call`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  extension(callExtension: CallExtension): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/extension/${callExtension.callId}`, callExtension);
  }

  find(id: number): Observable<Call> {
    return this.http.get<Call>(`${this.urlEndPoint}/${id}`);
  }

  findActive(): Observable<Call[]> {
    return this.http.get<Call[]>(`${this.urlEndPoint}/active`);
  }

  findAll(): Observable<Call[]> {
    return this.http.get<Call[]>(this.urlEndPoint);
  }

  findAllByEvaluationActive(): Observable<Call[]> {
    return this.http.get<Call[]>(`${this.urlEndPoint}/evaluationActive`);
  }

  findByEvaluationActive(id: number): Observable<Call> {
    return this.http.get<Call>(`${this.urlEndPoint}/evaluationActive/${id}`);
  }

  save(call: Call): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, call);
  }
}
