import { CallArea } from '../model/callArea';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallAreaService {
  private urlEndPoint: string = `${environment.apiUrl}/callArea`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<CallArea> {
    return this.http.patch<CallArea>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<CallArea> {
    return this.http.get<CallArea>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<CallArea[]> {
    return this.http.get<CallArea[]>(this.urlEndPoint);
  }

  save(callArea: CallArea): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, callArea);
  }
}