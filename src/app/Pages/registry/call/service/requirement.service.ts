import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Requirement } from '../model/requirement';

@Injectable({
  providedIn: 'root'
})
export class RequirementService {
  private urlEndPoint: string = `${environment.apiUrl}/requirement`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Requirement> {
    return this.http.patch<Requirement>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<Requirement> {
    return this.http.get<Requirement>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Requirement[]> {
    return this.http.get<Requirement[]>(this.urlEndPoint);
  }

  findAllByCall(id: number): Observable<Requirement[]> {
    return this.http.get<Requirement[]>(`${this.urlEndPoint}/call/${id}`);
  }

  save(requirement: Requirement): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, requirement);
  }
}