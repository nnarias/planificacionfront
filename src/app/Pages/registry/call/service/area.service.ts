import { Area } from '../model/area';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AreaService {
  private urlEndPoint: string = `${environment.apiUrl}/area`;

  constructor(private http: HttpClient) { }

  find(id: number): Observable<Area> {
    return this.http.get<Area>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Area[]> {
    return this.http.get<Area[]>(this.urlEndPoint);
  }
}