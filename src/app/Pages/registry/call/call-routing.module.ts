import { AuthGuard } from '../../security/guard/auth.guard';
import { CallAreaComponent } from './callArea/call-area/call-area.component';
import { CallAreaFormComponent } from './callArea/call-area-form/call-area-form.component';
import { CallComponent } from './call/call/call.component';
import { CallExtensionComponent } from './call/call-extension/call-extension.component';
import { CallFormComponent } from './call/call-form/call-form.component';
import { CallViewComponent } from './call/call-view/call-view.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RequirementComponent } from './requirement/requirement/requirement.component';
import { RequirementFormComponent } from './requirement/requirement-form/requirement-form.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'call', component: CallComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'call/form', component: CallFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'call/form/:id', component: CallFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'callArea', component: CallAreaComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'callArea/form', component: CallAreaFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'callArea/form/:id', component: CallAreaFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'callExtension/:id', component: CallExtensionComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'callView/:id', component: CallViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'requirement', component: RequirementComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'requirement/form', component: RequirementFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'requirement/form/:id', component: RequirementFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallRoutingModule { }