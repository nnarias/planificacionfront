import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { InstitutionInvolved } from '../../model/institutionInvolved';
import { InstitutionInvolvedService } from '../../service/institution-involved.service';

@Component({
  selector: 'app-institution-involved-view',
  templateUrl: './institution-involved-view.component.html',
  styleUrls: ['./institution-involved-view.component.scss']
})
export class InstitutionInvolvedViewComponent implements OnInit {
  private institutionInvolved: InstitutionInvolved = new InstitutionInvolved();

  constructor(private activatedRoute: ActivatedRoute, private institutionInvolvedService: InstitutionInvolvedService) { }

  ngOnInit(): void {
    this.loadExecutiveAgency();
  }

  loadExecutiveAgency(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.institutionInvolvedService.find(id).subscribe(institutionInvolved => {
          this.institutionInvolved = institutionInvolved;
        });
      }
    });
  }

  public get getInstitutionInvolved(): InstitutionInvolved {
    return this.institutionInvolved;
  }
}