import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionInvolvedViewComponent } from './institution-involved-view.component';

describe('InstitutionInvolvedViewComponent', () => {
  let component: InstitutionInvolvedViewComponent;
  let fixture: ComponentFixture<InstitutionInvolvedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionInvolvedViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionInvolvedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
