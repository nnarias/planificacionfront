import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionInvolvedFormComponent } from './institution-involved-form.component';

describe('InstitutionInvolvedFormComponent', () => {
  let component: InstitutionInvolvedFormComponent;
  let fixture: ComponentFixture<InstitutionInvolvedFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionInvolvedFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionInvolvedFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
