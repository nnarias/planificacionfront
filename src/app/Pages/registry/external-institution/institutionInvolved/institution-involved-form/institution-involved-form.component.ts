import { ActivatedRoute, Router } from '@angular/router';
import { address, customEmail, fax, phone, url } from 'src/app/validators/validation.directive';
import { Component, OnInit } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InstitutionInvolved } from '../../model/institutionInvolved';
import { InstitutionInvolvedService } from '../../service/institution-involved.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-institution-involved-form',
  templateUrl: './institution-involved-form.component.html',
  styleUrls: ['./institution-involved-form.component.scss']
})
export class InstitutionInvolvedFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    address: ['', Validators.required],
    email: [''],
    executingAgency: [''],
    fax: [''],
    id: [''],
    legalRepresentative: [''],
    legalRepresentativeId: [''],
    name: ['', Validators.required],
    phone: [''],
    url: ['']
  });
  private institutionInvolved: InstitutionInvolved = new InstitutionInvolved();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private institutionInvolvedService: InstitutionInvolvedService, private router: Router) { }

  ngOnInit(): void {
    this.loadExecutiveAgency();
    this.validate();
  }

  loadExecutiveAgency(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.institutionInvolvedService.find(id).subscribe(institutionInvolved => {
          this.form.patchValue({
            address: institutionInvolved.address,
            email: institutionInvolved.email,
            executingAgency: institutionInvolved.executingAgency,
            fax: institutionInvolved.fax,
            id: institutionInvolved.id,
            legalRepresentative: institutionInvolved.legalRepresentative,
            legalRepresentativeId: institutionInvolved.legalRepresentativeId,
            name: institutionInvolved.name,
            phone: institutionInvolved.phone,
            url: institutionInvolved.url
          });
          this.institutionInvolved = institutionInvolved;
        })
        this.title = 'Modificar Institución Involucrada';
      } else {
        this.title = 'Crear Institución Involucrada';
      }
    });
  }

  save(): void {
    this.institutionInvolved = this.form.value;
    this.institutionInvolvedService.save(this.institutionInvolved).subscribe(() => {
      this.router.navigate(['app/registry/call/call']);
      swal.fire(this.institutionInvolved.id ? 'Institución Involucrada Actualizada' : 'Nueva Institución Involucrada', `${this.institutionInvolved.name}`, 'success');
    });
  }

  validate(): void {
    this.address.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.address.setValidators(address());
      } else {
        this.address.clearValidators();
      }
      this.address.updateValueAndValidity();
    });
    this.fax.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.fax.setValidators(fax());
      } else {
        this.fax.clearValidators();
      }
      this.fax.updateValueAndValidity();
    });
    this.email.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.email.setValidators([Validators.email, customEmail()]);
      } else {
        this.email.clearValidators();
      }
      this.email.updateValueAndValidity();
    });
    this.phone.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.phone.setValidators(phone());
      } else {
        this.phone.clearValidators();
      }
      this.phone.updateValueAndValidity();
    });
    this.url.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      if (value) {
        this.url.setValidators(url());
      } else {
        this.url.clearValidators();
      }
      this.url.updateValueAndValidity();
    });
  }

  public get address() { return this.form.get('address'); }
  public get email() { return this.form.get('email'); }
  public get executingAgency() { return this.form.get('executingAgency'); }
  public get fax() { return this.form.get('fax'); }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get legalRepresentative() { return this.form.get('legalRepresentative'); }
  public get legalRepresentativeId() { return this.form.get('legalRepresentativeId'); }
  public get name() { return this.form.get('name'); }
  public get phone() { return this.form.get('phone'); }
  public get url() { return this.form.get('url'); }
}