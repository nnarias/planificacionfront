import { Component, OnInit, ViewChild } from '@angular/core';
import { InstitutionInvolved } from '../../model/institutionInvolved';
import { InstitutionInvolvedService } from '../../service/institution-involved.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-institution-involved',
  templateUrl: './institution-involved.component.html',
  styleUrls: ['./institution-involved.component.scss']
})
export class InstitutionInvolvedComponent implements OnInit {
  private dataSource = new MatTableDataSource<InstitutionInvolved>();
  private displayedColumns: string[] = ['name', 'email', 'phone', 'executingAgency', 'actions'];
  private institutionInvolvedList: InstitutionInvolved[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private institutionInvolvedService: InstitutionInvolvedService) { }

  ngOnInit(): void {
    this.loadExecutiveAgencyDataList();
  }

  delete(institutionInvolved: InstitutionInvolved): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la institución involucrada: ${institutionInvolved.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.institutionInvolvedService.delete(institutionInvolved.id).subscribe(() => {
          this.institutionInvolvedList = this.institutionInvolvedList.filter(result => result !== institutionInvolved);
          this.dataSource.data = this.institutionInvolvedList;
          swal.fire('Institución Involucrada Eliminada', `${institutionInvolved.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadExecutiveAgencyDataList(): void {
    this.institutionInvolvedService.findAll().subscribe(institutionInvolvedList => {
      this.dataSource.data = institutionInvolvedList;
      this.institutionInvolvedList = institutionInvolvedList;
    });
  }

  public get getDataSource(): MatTableDataSource<InstitutionInvolved> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getInstitutionInvolvedList(): InstitutionInvolved[] {
    return this.institutionInvolvedList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}