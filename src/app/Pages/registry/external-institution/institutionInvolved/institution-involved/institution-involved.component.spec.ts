import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionInvolvedComponent } from './institution-involved.component';

describe('InstitutionInvolvedComponent', () => {
  let component: InstitutionInvolvedComponent;
  let fixture: ComponentFixture<InstitutionInvolvedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionInvolvedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionInvolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
