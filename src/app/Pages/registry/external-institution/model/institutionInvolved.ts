export class InstitutionInvolved {
    address: string;
    email: string;
    executingAgency: String;
    fax: string;
    id: number;
    legalRepresentative: String;
    legalRepresentativeId: String;
    name: string;
    phone: string;
    url: string;
}  