import { CommonModule } from '@angular/common';
import { ExternalInstitutionRoutingModule } from './external-institution-routing.module';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { InstitutionInvolvedComponent } from './institutionInvolved/institution-involved/institution-involved.component';
import { InstitutionInvolvedFormComponent } from './institutionInvolved/institution-involved-form/institution-involved-form.component';
import { InstitutionInvolvedViewComponent } from './institutionInvolved/institution-involved-view/institution-involved-view.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [InstitutionInvolvedComponent, InstitutionInvolvedFormComponent, InstitutionInvolvedViewComponent, MenuComponent],
  imports: [CommonModule, ExternalInstitutionRoutingModule, ImportsModule]
})
export class ExternalInstitutionModule { }