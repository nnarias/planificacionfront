import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InstitutionInvolved } from '../model/institutionInvolved';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InstitutionInvolvedService {

  private urlEndPoint: string = `${environment.apiUrl}/institutionInvolved`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<InstitutionInvolved> {
    return this.http.patch<InstitutionInvolved>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<InstitutionInvolved> {
    return this.http.get<InstitutionInvolved>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<InstitutionInvolved[]> {
    return this.http.get<InstitutionInvolved[]>(this.urlEndPoint);
  }

  save(institutionInvolved: InstitutionInvolved): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, institutionInvolved);
  }
}