import { AuthGuard } from '../../security/guard/auth.guard';
import { InstitutionInvolvedComponent } from './institutionInvolved/institution-involved/institution-involved.component';
import { InstitutionInvolvedFormComponent } from './institutionInvolved/institution-involved-form/institution-involved-form.component';
import { InstitutionInvolvedViewComponent } from './institutionInvolved/institution-involved-view/institution-involved-view.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] }  },
{ path: 'institutionInvolved', component: InstitutionInvolvedComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'institutionInvolved/form', component: InstitutionInvolvedFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'institutionInvolved/form/:id', component: InstitutionInvolvedFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'institutionInvolvedView/form/:id', component: InstitutionInvolvedViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExternalInstitutionRoutingModule { }