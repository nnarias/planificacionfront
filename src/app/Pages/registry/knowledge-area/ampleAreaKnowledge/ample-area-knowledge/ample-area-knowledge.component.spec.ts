import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmpleAreaKnowledgeComponent } from './ample-area-knowledge.component';

describe('AmpleAreaKnowledgeComponent', () => {
  let component: AmpleAreaKnowledgeComponent;
  let fixture: ComponentFixture<AmpleAreaKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmpleAreaKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmpleAreaKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
