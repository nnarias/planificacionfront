import { AmpleAreaKnowledge } from '../../model/ampleAreaKnowledge';
import { AmpleAreaKnowledgeService } from '../../service/ample-area-knowledge.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-ample-area-knowledge',
  templateUrl: './ample-area-knowledge.component.html',
  styleUrls: ['./ample-area-knowledge.component.scss']
})
export class AmpleAreaKnowledgeComponent implements OnInit {
  private ampleAreaKnowledgeList: AmpleAreaKnowledge[] = [];
  private dataSource = new MatTableDataSource<AmpleAreaKnowledge>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private AmpleAreaKnowledgeService: AmpleAreaKnowledgeService) { }

  ngOnInit(): void {
    this.loadAmpleAreaKnowledgeList();
  }

  delete(ampleAreaKnowledge: AmpleAreaKnowledge): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Campo Amplio: ${ampleAreaKnowledge.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.AmpleAreaKnowledgeService.delete(ampleAreaKnowledge.id).subscribe(() => {
          this.ampleAreaKnowledgeList = this.ampleAreaKnowledgeList.filter(result => result !== ampleAreaKnowledge);
          this.dataSource.data = this.ampleAreaKnowledgeList;
          swal.fire('Campo Amplio Eliminado', `${ampleAreaKnowledge.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadAmpleAreaKnowledgeList(): void {
    this.AmpleAreaKnowledgeService.findAll().subscribe(ampleAreaKnowledgeList => {
      this.ampleAreaKnowledgeList = ampleAreaKnowledgeList;
      this.dataSource.data = ampleAreaKnowledgeList;
    });
  }

  public get getAmpleAreaKnowledgeList(): AmpleAreaKnowledge[] {
    return this.ampleAreaKnowledgeList;
  }

  public get getDataSource(): MatTableDataSource<AmpleAreaKnowledge> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}