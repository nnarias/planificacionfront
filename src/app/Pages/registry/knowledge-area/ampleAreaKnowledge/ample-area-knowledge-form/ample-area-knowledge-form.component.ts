import { ActivatedRoute, Router } from '@angular/router';
import { AmpleAreaKnowledge } from '../../model/ampleAreaKnowledge';
import { AmpleAreaKnowledgeService } from '../../service/ample-area-knowledge.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-ample-area-knowledge-form',
  templateUrl: './ample-area-knowledge-form.component.html',
  styleUrls: ['./ample-area-knowledge-form.component.scss']
})
export class AmpleAreaKnowledgeFormComponent implements OnInit {
  private ampleAreaKnowledge: AmpleAreaKnowledge = new AmpleAreaKnowledge();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private ampleAreaKnowledgeService: AmpleAreaKnowledgeService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadAmpleAreaKnowledge();
  }

  loadAmpleAreaKnowledge(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.ampleAreaKnowledgeService.find(id).subscribe(ampleAreaKnowledge => {
          this.form.patchValue({
            id: ampleAreaKnowledge.id,
            name: ampleAreaKnowledge.name
          });
        });
        this.title = 'Modificar Campo Amplio';
      } else {
        this.title = 'Crear Campo Amplio';
      }
    });
  }

  save(): void {
    this.ampleAreaKnowledge = this.form.value;
    this.ampleAreaKnowledgeService.save(this.ampleAreaKnowledge).subscribe(() => {
      this.router.navigate(['app/registry/knowledgeArea/ampleAreaKnowledge']);
      swal.fire(this.ampleAreaKnowledge.id ? 'Campo Amplio Actualizado' : 'Nuevo Campo Amplio', `${this.ampleAreaKnowledge.name}`, 'success');
    });
  }

  public get getAmpleAreaKnowledge(): AmpleAreaKnowledge {
    return this.ampleAreaKnowledge;
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}