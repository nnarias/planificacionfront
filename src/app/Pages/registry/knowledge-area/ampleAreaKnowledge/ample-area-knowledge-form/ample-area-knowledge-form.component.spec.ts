import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmpleAreaKnowledgeFormComponent } from './ample-area-knowledge-form.component';

describe('AmpleAreaKnowledgeFormComponent', () => {
  let component: AmpleAreaKnowledgeFormComponent;
  let fixture: ComponentFixture<AmpleAreaKnowledgeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmpleAreaKnowledgeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmpleAreaKnowledgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
