import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SpecificAreaKnowledge } from '../../model/specificAreaKnowledge';
import swal from 'sweetalert2';
import { SpecificAreaKnowledgeService } from '../../service/specific-area-knowledge.service';

@Component({
  selector: 'app-specific-area-knowledge',
  templateUrl: './specific-area-knowledge.component.html',
  styleUrls: ['./specific-area-knowledge.component.scss']
})
export class SpecificAreaKnowledgeComponent implements OnInit {

  private _dataSource = new MatTableDataSource<SpecificAreaKnowledge>();
  private _displayedColumns: string[] = ['name', 'actions'];
  private _specificAreaKnowledgeList: SpecificAreaKnowledge[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private specificAreaKnowledgeService: SpecificAreaKnowledgeService) { }

  ngOnInit(): void {
    this.loadSpecificAreaKnowledgeList();
  }

  delete(specificAreaKnowledge: SpecificAreaKnowledge): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el área de conocimiento específico: ${specificAreaKnowledge.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.specificAreaKnowledgeService.delete(specificAreaKnowledge.id).subscribe(() => {
          this._specificAreaKnowledgeList = this._specificAreaKnowledgeList.filter(result => result !== specificAreaKnowledge);
          this._dataSource.data = this._specificAreaKnowledgeList;
          swal.fire('Área de conocimiento específico eliminada', `${specificAreaKnowledge.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadSpecificAreaKnowledgeList(): void {
    this.specificAreaKnowledgeService.findAll().subscribe(specificAreaKnowledgeList => {
      this._specificAreaKnowledgeList = specificAreaKnowledgeList;
      this._dataSource.data = specificAreaKnowledgeList;
      this._showSpiner = false;
    });
  }

  public get specificAreaKnowledgeList(): SpecificAreaKnowledge[] {
    return this._specificAreaKnowledgeList;
  }

  public get dataSource(): MatTableDataSource<SpecificAreaKnowledge> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }

}
