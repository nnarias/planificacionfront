import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificAreaKnowledgeComponent } from './specific-area-knowledge.component';

describe('SpecificAreaKnowledgeComponent', () => {
  let component: SpecificAreaKnowledgeComponent;
  let fixture: ComponentFixture<SpecificAreaKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecificAreaKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificAreaKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
