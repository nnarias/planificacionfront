import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificAreaKnowledgeFormComponent } from './specific-area-knowledge-form.component';

describe('SpecificAreaKnowledgeFormComponent', () => {
  let component: SpecificAreaKnowledgeFormComponent;
  let fixture: ComponentFixture<SpecificAreaKnowledgeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecificAreaKnowledgeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificAreaKnowledgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
