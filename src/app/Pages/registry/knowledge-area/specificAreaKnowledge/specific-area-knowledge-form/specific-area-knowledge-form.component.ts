import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SpecificAreaKnowledgeService } from '../../service/specific-area-knowledge.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-specific-area-knowledge-form',
  templateUrl: './specific-area-knowledge-form.component.html',
  styleUrls: ['./specific-area-knowledge-form.component.scss']
})
export class SpecificAreaKnowledgeFormComponent implements OnInit {
  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required],
    state: ['']
  });
  private id: number;
  private _showSpiner: Boolean = true;
  private _title: string;

  constructor(private activatedRoute: ActivatedRoute, private specificAreaKnowledgeService: SpecificAreaKnowledgeService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadSpecificAreaKnowledge();
  }

  loadSpecificAreaKnowledge(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadCallData();
        this._title = 'Modificar área de conocimiento específico';
      } else {
        this._title = 'Crear área de conocimiento específico';
        this._showSpiner = false;
      }
    });
  }

  loadCallData(): void {
    this.specificAreaKnowledgeService.find(this.id).subscribe(specificAreaKnowledge => {
      this._form.patchValue({
        id: specificAreaKnowledge.id,
        name: specificAreaKnowledge.name,
      });
      this._showSpiner = false;
    });
  }

  save(): void {
    let specificAreaKnowledge = this._form.value;
    this.specificAreaKnowledgeService.save(specificAreaKnowledge).subscribe(() => {
      this.router.navigate(['app/registry/knowledgeArea/specificAreaKnowledge']);
      swal.fire(specificAreaKnowledge.id ? 'Área de conocimiento específico actualizada' : 'Nueva  área de conocimiento específico', `${specificAreaKnowledge.name}`, 'success');
    });
  }

  public get name() { return this._form.get('name'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}
