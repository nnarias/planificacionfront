import { UnescoAreaKnowledge } from './unescoAreaKnowledge';

export class SubUnescoAreaKnowledge {
    id: number;
    name: string;
    state: boolean;
    unescoAreaKnowledge: UnescoAreaKnowledge = new UnescoAreaKnowledge();
}