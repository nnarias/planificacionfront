export class DetailedAreaKnowledge {
  id: number;
  name: string;
  state: boolean;
}