import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubUnescoAreaKnowledge } from '../../model/subUnescoAreaKnowledge';
import { SubUnescoAreaKnowledgeService } from '../../service/sub-unesco-area-knowledge.service';
import swal from 'sweetalert2';
import { UnescoAreaKnowledge } from '../../model/unescoAreaKnowledge';
import { UnescoAreaKnowledgeService } from '../../service/unesco-area-knowledge.service';

@Component({
  selector: 'app-sub-unesco-area-knowledge-form',
  templateUrl: './sub-unesco-area-knowledge-form.component.html',
  styleUrls: ['./sub-unesco-area-knowledge-form.component.scss']
})
export class SubUnescoAreaKnowledgeFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required],
    unescoAreaKnowledgeId: ['', Validators.required]
  });
  private subUnescoAreaKnowledge: SubUnescoAreaKnowledge = new SubUnescoAreaKnowledge();
  private title: string;
  private unescoAreaKnowledgeList: UnescoAreaKnowledge[] = [];

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private subUnescoAreaKnowledgeService: SubUnescoAreaKnowledgeService,
    private unescoAreaKnowledgeService: UnescoAreaKnowledgeService, private router: Router) { }

  ngOnInit(): void {
    this.loadSubUnescoAreaKnowledge();
    this.loadUnescoAreaKnowledgeList();
  }

  loadSubUnescoAreaKnowledge(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.subUnescoAreaKnowledgeService.find(id).subscribe(subUnescoAreaKnowledge => {
          this.form.patchValue({
            id: subUnescoAreaKnowledge.id,
            name: subUnescoAreaKnowledge.name,
            unescoAreaKnowledgeId: subUnescoAreaKnowledge.unescoAreaKnowledge.id
          });
        });
        this.title = 'Modificar Sub área de Conocimiento Unesco';
      } else {
        this.title = 'Crear Sub área de Conocimiento Unesco';
      }
    });
  }

  loadUnescoAreaKnowledgeList(): void {
    this.unescoAreaKnowledgeService.findAll().subscribe(unescoAreaKnowledgeList => {
      this.unescoAreaKnowledgeList = unescoAreaKnowledgeList;
    });
  }

  save(): void {
    this.subUnescoAreaKnowledge = this.form.value;
    this.subUnescoAreaKnowledgeService.save(this.subUnescoAreaKnowledge).subscribe(() => {
      this.router.navigate(['app/registry/knowledgeArea/subUnescoAreaKnowledge']);
      swal.fire(this.subUnescoAreaKnowledge.id ? 'Sub área de Conocimiento Unesco Actualizada' : 'Nueva Sub área de Conocimiento Unesco',
        `${this.subUnescoAreaKnowledge.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get getUnescoAreaKnowledgeList(): UnescoAreaKnowledge[] {
    return this.unescoAreaKnowledgeList;
  }

  public get name() { return this.form.get('name'); }
  public get unescoAreaKnowledgeId() { return this.form.get('unescoAreaKnowledgeId'); }
}