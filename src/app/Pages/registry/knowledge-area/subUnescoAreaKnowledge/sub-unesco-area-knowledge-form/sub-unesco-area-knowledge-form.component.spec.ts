import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubUnescoAreaKnowledgeFormComponent } from './sub-unesco-area-knowledge-form.component';

describe('SubUnescoAreaKnowledgeFormComponent', () => {
  let component: SubUnescoAreaKnowledgeFormComponent;
  let fixture: ComponentFixture<SubUnescoAreaKnowledgeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubUnescoAreaKnowledgeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUnescoAreaKnowledgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
