import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubUnescoAreaKnowledgeComponent } from './sub-unesco-area-knowledge.component';

describe('SubUnescoAreaKnowledgeComponent', () => {
  let component: SubUnescoAreaKnowledgeComponent;
  let fixture: ComponentFixture<SubUnescoAreaKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubUnescoAreaKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUnescoAreaKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
