import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SubUnescoAreaKnowledge } from '../../model/subUnescoAreaKnowledge';
import { SubUnescoAreaKnowledgeService } from '../../service/sub-unesco-area-knowledge.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-sub-unesco-area-knowledge',
  templateUrl: './sub-unesco-area-knowledge.component.html',
  styleUrls: ['./sub-unesco-area-knowledge.component.scss']
})
export class SubUnescoAreaKnowledgeComponent implements OnInit {
  private dataSource = new MatTableDataSource<SubUnescoAreaKnowledge>();
  private displayedColumns: string[] = ['name', 'unescoAreaKnowledgeName', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;
  private subUnescoAreaKnowledgeList: SubUnescoAreaKnowledge[] = [];

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private subUnescoAreaKnowledgeService: SubUnescoAreaKnowledgeService) { }

  ngOnInit(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'unescoAreaKnowledgeName': return item.unescoAreaKnowledge.name;
        default: return item[property];
      }
    }
    this.loadSubUnescoAreaKnowledgeList();
  }

  delete(subUnescoAreaKnowledge: SubUnescoAreaKnowledge): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Si, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Sub área de Conocimiento Unesco: ${subUnescoAreaKnowledge.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.subUnescoAreaKnowledgeService.delete(subUnescoAreaKnowledge.id).subscribe(() => {
          this.subUnescoAreaKnowledgeList = this.subUnescoAreaKnowledgeList.filter(result => result !== subUnescoAreaKnowledge);
          this.dataSource.data = this.subUnescoAreaKnowledgeList;
          swal.fire('Sub área de Conocimiento Unesco Eliminado', `${subUnescoAreaKnowledge.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadSubUnescoAreaKnowledgeList(): void {
    this.subUnescoAreaKnowledgeService.findAll().subscribe(subUnescoAreaKnowledgeList => {
      this.dataSource.data = subUnescoAreaKnowledgeList;
      this.subUnescoAreaKnowledgeList = subUnescoAreaKnowledgeList;
    });
  }

  public get getDataSource(): MatTableDataSource<SubUnescoAreaKnowledge> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getSubUnescoAreaKnowledgeList(): SubUnescoAreaKnowledge[] {
    return this.subUnescoAreaKnowledgeList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}