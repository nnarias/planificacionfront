import { AmpleAreaKnowledgeComponent } from './ampleAreaKnowledge/ample-area-knowledge/ample-area-knowledge.component';
import { AmpleAreaKnowledgeFormComponent } from './ampleAreaKnowledge/ample-area-knowledge-form/ample-area-knowledge-form.component';
import { CommonModule } from '@angular/common';
import { DetailedAreaKnowledgeComponent } from './detailedAreaKnowledge/detailed-area-knowledge/detailed-area-knowledge.component';
import { DetailedAreaKnowledgeFormComponent } from './detailedAreaKnowledge/detailed-area-knowledge-form/detailed-area-knowledge-form.component';
import { EspeAreaKnowledgeComponent } from './espeAreaKnowledge/espe-area-knowledge/espe-area-knowledge.component';
import { EspeAreaKnowledgeFormComponent } from './espeAreaKnowledge/espe-area-knowledge-form/espe-area-knowledge-form.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { KnowledgeAreaRoutingModule } from './knowledge-area-routing.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { SubUnescoAreaKnowledgeComponent } from './subUnescoAreaKnowledge/sub-unesco-area-knowledge/sub-unesco-area-knowledge.component';
import { SubUnescoAreaKnowledgeFormComponent } from './subUnescoAreaKnowledge/sub-unesco-area-knowledge-form/sub-unesco-area-knowledge-form.component';
import { UnescoAreaKnowledgeComponent } from './unescoAreaKnowledge/unesco-area-knowledge/unesco-area-knowledge.component';
import { UnescoAreaKnowledgeFormComponent } from './unescoAreaKnowledge/unesco-area-knowledge-form/unesco-area-knowledge-form.component';
import { SpecificAreaKnowledgeComponent } from './specificAreaKnowledge/specific-area-knowledge/specific-area-knowledge.component';
import { SpecificAreaKnowledgeFormComponent } from './specificAreaKnowledge/specific-area-knowledge-form/specific-area-knowledge-form.component';

@NgModule({
  declarations: [AmpleAreaKnowledgeComponent, AmpleAreaKnowledgeFormComponent, DetailedAreaKnowledgeComponent, DetailedAreaKnowledgeFormComponent, EspeAreaKnowledgeComponent,
    EspeAreaKnowledgeFormComponent, MenuComponent, SubUnescoAreaKnowledgeComponent, SubUnescoAreaKnowledgeFormComponent, UnescoAreaKnowledgeComponent,
    UnescoAreaKnowledgeFormComponent,
    SpecificAreaKnowledgeComponent,
    SpecificAreaKnowledgeFormComponent],
  imports: [CommonModule, ImportsModule, KnowledgeAreaRoutingModule]
})
export class KnowledgeAreaModule { }