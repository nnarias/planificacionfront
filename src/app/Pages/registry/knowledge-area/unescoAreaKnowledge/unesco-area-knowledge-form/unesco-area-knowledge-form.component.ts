import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { UnescoAreaKnowledge } from '../../model/unescoAreaKnowledge';
import { UnescoAreaKnowledgeService } from '../../service/unesco-area-knowledge.service';

@Component({
  selector: 'app-unesco-area-knowledge-form',
  templateUrl: './unesco-area-knowledge-form.component.html',
  styleUrls: ['./unesco-area-knowledge-form.component.scss']
})
export class UnescoAreaKnowledgeFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;
  private unescoAreaKnowledge: UnescoAreaKnowledge = new UnescoAreaKnowledge();

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private router: Router, private unescoAreaKnowledgeService: UnescoAreaKnowledgeService) { }

  ngOnInit(): void {
    this.loadUnescoAreaKnowledge();
  }

  loadUnescoAreaKnowledge(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.unescoAreaKnowledgeService.find(id).subscribe(unescoAreaKnowledge => {
          this.form.patchValue({
            id: unescoAreaKnowledge.id,
            name: unescoAreaKnowledge.name
          });
        });
        this.title = "Modificar Área de Conocimiento Unesco";
      } else {
        this.title = "Crear Área de Conocimiento Unesco";
      }
    });
  }

  save(): void {
    this.unescoAreaKnowledge = this.form.value;
    this.unescoAreaKnowledgeService.save(this.unescoAreaKnowledge).subscribe(() => {
      this.router.navigate(['app/registry/knowledgeArea/unescoAreaKnowledge']);
      swal.fire('Área de Conocimiento Unesco creado', `${this.unescoAreaKnowledge.name}`, 'success');
    });
  }


  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}