import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnescoAreaKnowledgeFormComponent } from './unesco-area-knowledge-form.component';

describe('UnescoAreaKnowledgeFormComponent', () => {
  let component: UnescoAreaKnowledgeFormComponent;
  let fixture: ComponentFixture<UnescoAreaKnowledgeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnescoAreaKnowledgeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnescoAreaKnowledgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
