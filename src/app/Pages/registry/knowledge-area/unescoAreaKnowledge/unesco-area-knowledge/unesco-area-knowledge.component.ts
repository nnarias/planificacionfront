import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';
import { UnescoAreaKnowledge } from '../../model/unescoAreaKnowledge';
import { UnescoAreaKnowledgeService } from '../../service/unesco-area-knowledge.service';

@Component({
  selector: 'app-unesco-area-knowledge',
  templateUrl: './unesco-area-knowledge.component.html',
  styleUrls: ['./unesco-area-knowledge.component.scss']
})
export class UnescoAreaKnowledgeComponent implements OnInit {
  private dataSource = new MatTableDataSource<UnescoAreaKnowledge>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;
  private unescoAreaKnowledgeList: UnescoAreaKnowledge[] = [];

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private unescoAreaKnowledgeService: UnescoAreaKnowledgeService) { }

  ngOnInit(): void {
    this.loadUnescoAreaKnowledgeList();
  }

  delete(unescoAreaKnowledge: UnescoAreaKnowledge): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Área de Conocimiento Unesco: ${unescoAreaKnowledge.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.unescoAreaKnowledgeService.delete(unescoAreaKnowledge.id).subscribe(() => {
          this.unescoAreaKnowledgeList = this.unescoAreaKnowledgeList.filter(result => result !== unescoAreaKnowledge);
          this.dataSource.data = this.unescoAreaKnowledgeList;
          swal.fire('Área de Conocimiento Unesco Eliminada', `${unescoAreaKnowledge.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadUnescoAreaKnowledgeList(): void {
    this.unescoAreaKnowledgeService.findAll().subscribe(unescoAreaKnowledgeList => {
      this.dataSource.data = unescoAreaKnowledgeList;
      this.unescoAreaKnowledgeList = unescoAreaKnowledgeList;
    });
  }

  public get getDataSource(): MatTableDataSource<UnescoAreaKnowledge> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getUnescoAreaKnowledgeList(): UnescoAreaKnowledge[] {
    return this.unescoAreaKnowledgeList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}