import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnescoAreaKnowledgeComponent } from './unesco-area-knowledge.component';

describe('UnescoAreaKnowledgeComponent', () => {
  let component: UnescoAreaKnowledgeComponent;
  let fixture: ComponentFixture<UnescoAreaKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnescoAreaKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnescoAreaKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
