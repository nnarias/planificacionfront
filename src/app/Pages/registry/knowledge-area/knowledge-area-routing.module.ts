import { AmpleAreaKnowledgeComponent } from './ampleAreaKnowledge/ample-area-knowledge/ample-area-knowledge.component';
import { AmpleAreaKnowledgeFormComponent } from './ampleAreaKnowledge/ample-area-knowledge-form/ample-area-knowledge-form.component';
import { AuthGuard } from '../../security/guard/auth.guard';
import { DetailedAreaKnowledgeComponent } from './detailedAreaKnowledge/detailed-area-knowledge/detailed-area-knowledge.component';
import { DetailedAreaKnowledgeFormComponent } from './detailedAreaKnowledge/detailed-area-knowledge-form/detailed-area-knowledge-form.component';
import { EspeAreaKnowledgeComponent } from './espeAreaKnowledge/espe-area-knowledge/espe-area-knowledge.component';
import { EspeAreaKnowledgeFormComponent } from './espeAreaKnowledge/espe-area-knowledge-form/espe-area-knowledge-form.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';
import { SubUnescoAreaKnowledgeComponent } from './subUnescoAreaKnowledge/sub-unesco-area-knowledge/sub-unesco-area-knowledge.component';
import { SubUnescoAreaKnowledgeFormComponent } from './subUnescoAreaKnowledge/sub-unesco-area-knowledge-form/sub-unesco-area-knowledge-form.component';
import { UnescoAreaKnowledgeComponent } from './unescoAreaKnowledge/unesco-area-knowledge/unesco-area-knowledge.component';
import { UnescoAreaKnowledgeFormComponent } from './unescoAreaKnowledge/unesco-area-knowledge-form/unesco-area-knowledge-form.component';
import { SpecificAreaKnowledgeFormComponent } from './specificAreaKnowledge/specific-area-knowledge-form/specific-area-knowledge-form.component';
import { SpecificAreaKnowledgeComponent } from './specificAreaKnowledge/specific-area-knowledge/specific-area-knowledge.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'ampleAreaKnowledge', component: AmpleAreaKnowledgeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'ampleAreaKnowledge/form', component: AmpleAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'ampleAreaKnowledge/form/:id', component: AmpleAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'detailedAreaKnowledge', component: DetailedAreaKnowledgeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'detailedAreaKnowledge/form', component: DetailedAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'detailedAreaKnowledge/form/:id', component: DetailedAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'espeAreaKnowledge', component: EspeAreaKnowledgeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'espeAreaKnowledge/form', component: EspeAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'espeAreaKnowledge/form/:id', component: EspeAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'subUnescoAreaKnowledge', component: SubUnescoAreaKnowledgeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'subUnescoAreaKnowledge/form', component: SubUnescoAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'subUnescoAreaKnowledge/form/:id', component: SubUnescoAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'unescoAreaKnowledge', component: UnescoAreaKnowledgeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'unescoAreaKnowledge/form', component: UnescoAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'unescoAreaKnowledge/form/:id', component: UnescoAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'specificAreaKnowledge', component: SpecificAreaKnowledgeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'specificAreaKnowledge/form', component: SpecificAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'specificAreaKnowledge/form/:id', component: SpecificAreaKnowledgeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KnowledgeAreaRoutingModule { }
