import { Component, OnInit, ViewChild } from '@angular/core';
import { DetailedAreaKnowledge } from '../../model/detailedAreaKnowledge';
import { DetailedAreaKnowledgeService } from '../../service/detailed-area-knowledge.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-detailed-area-knowledge',
  templateUrl: './detailed-area-knowledge.component.html',
  styleUrls: ['./detailed-area-knowledge.component.scss']
})
export class DetailedAreaKnowledgeComponent implements OnInit {
  private dataSource = new MatTableDataSource<DetailedAreaKnowledge>();
  private detailedAreaKnowledgeList: DetailedAreaKnowledge[] = [];
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private detailedAreaKnowledgeService: DetailedAreaKnowledgeService) { }

  ngOnInit(): void {
    this.loadDetailedAreaKnowledgeList();
  }

  delete(detailedAreaKnowledge: DetailedAreaKnowledge): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Campo Detallado: ${detailedAreaKnowledge.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.detailedAreaKnowledgeService.delete(detailedAreaKnowledge.id).subscribe(() => {
          this.detailedAreaKnowledgeList = this.detailedAreaKnowledgeList.filter(result => result !== detailedAreaKnowledge);
          this.dataSource.data = this.detailedAreaKnowledgeList;
          swal.fire('Campo Detallado Eliminado', `${detailedAreaKnowledge.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadDetailedAreaKnowledgeList(): void {
    this.detailedAreaKnowledgeService.findAll().subscribe(detailedAreaKnowledgeList => {
      this.dataSource.data = detailedAreaKnowledgeList;
      this.detailedAreaKnowledgeList = detailedAreaKnowledgeList;
    });
  }

  public get getDataSource(): MatTableDataSource<DetailedAreaKnowledge> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getDetailedAreaKnowledgeList(): DetailedAreaKnowledge[] {
    return this.detailedAreaKnowledgeList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}