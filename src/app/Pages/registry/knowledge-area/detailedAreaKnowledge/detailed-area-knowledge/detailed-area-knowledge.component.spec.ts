import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedAreaKnowledgeComponent } from './detailed-area-knowledge.component';

describe('DetailedAreaKnowledgeComponent', () => {
  let component: DetailedAreaKnowledgeComponent;
  let fixture: ComponentFixture<DetailedAreaKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailedAreaKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedAreaKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
