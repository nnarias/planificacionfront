import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DetailedAreaKnowledge } from '../../model/detailedAreaKnowledge';
import { DetailedAreaKnowledgeService } from '../../service/detailed-area-knowledge.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-detailed-area-knowledge-form',
  templateUrl: './detailed-area-knowledge-form.component.html',
  styleUrls: ['./detailed-area-knowledge-form.component.scss']
})
export class DetailedAreaKnowledgeFormComponent implements OnInit {
  private detailedAreaKnowledge: DetailedAreaKnowledge = new DetailedAreaKnowledge();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private detailedAreaKnowledgeService: DetailedAreaKnowledgeService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadDetailedAreaKnowledge();
  }

  loadDetailedAreaKnowledge(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.detailedAreaKnowledgeService.find(id).subscribe(detailedAreaKnowledge => {
          this.form.patchValue({
            id: detailedAreaKnowledge.id,
            name: detailedAreaKnowledge.name
          });
        });
        this.title = 'Modificar Campo Detallado';
      } else {
        this.title = 'Crear Campo Detallado';
      }
    });
  }

  save(): void {
    this.detailedAreaKnowledge = this.form.value;
    this.detailedAreaKnowledgeService.save(this.detailedAreaKnowledge).subscribe(() => {
      this.router.navigate(['app/registry/knowledgeArea/detailedAreaKnowledge']);
      swal.fire(this.detailedAreaKnowledge.id ? 'Campo Detallado Actualizado' : 'Nuevo Detallado Amplio', `${this.detailedAreaKnowledge.name}`, 'success');
    });
  }

  public get getDetailedAreaKnowledge(): DetailedAreaKnowledge {
    return this.detailedAreaKnowledge;
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}