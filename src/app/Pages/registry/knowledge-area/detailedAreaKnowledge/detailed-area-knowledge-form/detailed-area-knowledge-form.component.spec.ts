import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedAreaKnowledgeFormComponent } from './detailed-area-knowledge-form.component';

describe('DetailedAreaKnowledgeFormComponent', () => {
  let component: DetailedAreaKnowledgeFormComponent;
  let fixture: ComponentFixture<DetailedAreaKnowledgeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailedAreaKnowledgeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedAreaKnowledgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
