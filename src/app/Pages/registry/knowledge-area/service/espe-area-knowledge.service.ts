import { environment } from 'src/environments/environment';
import { EspeAreaKnowledge } from '../model/espeAreaKnowledge';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EspeAreaKnowledgeService {

  private urlEndPoint: string = `${environment.apiUrl}/espeAreaKnowledge`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<EspeAreaKnowledge> {
    return this.http.patch<EspeAreaKnowledge>(`${this.urlEndPoint}/${id}`,null);
  }

  find(id: number): Observable<EspeAreaKnowledge> {
    return this.http.get<EspeAreaKnowledge>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<EspeAreaKnowledge[]> {
    return this.http.get<EspeAreaKnowledge[]>(this.urlEndPoint);
  }

  save(espeAreaKnowledge: EspeAreaKnowledge): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, espeAreaKnowledge);
  }
}