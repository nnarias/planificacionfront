import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SubUnescoAreaKnowledge } from '../model/subUnescoAreaKnowledge';

@Injectable({
  providedIn: 'root'
})
export class SubUnescoAreaKnowledgeService {

  private urlEndPoint: string = `${environment.apiUrl}/subUnescoAreaKnowledge`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<SubUnescoAreaKnowledge> {
    return this.http.patch<SubUnescoAreaKnowledge>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<SubUnescoAreaKnowledge> {
    return this.http.get<SubUnescoAreaKnowledge>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<SubUnescoAreaKnowledge[]> {
    return this.http.get<SubUnescoAreaKnowledge[]>(this.urlEndPoint);
  }

  findByUnesco(unescoAreaKnowledgeId: number): Observable<SubUnescoAreaKnowledge[]> {
    return this.http.get<SubUnescoAreaKnowledge[]>(`${this.urlEndPoint}/unescoAreaKnowledge/${unescoAreaKnowledgeId}`);
  }

  save(subUnescoAreaKnowledge: SubUnescoAreaKnowledge): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, subUnescoAreaKnowledge);
  }
}