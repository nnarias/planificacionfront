
import { DetailedAreaKnowledge } from '../model/detailedAreaKnowledge';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailedAreaKnowledgeService {
  private urlEndPoint: string = `${environment.apiUrl}/detailedAreaKnowledge`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<DetailedAreaKnowledge> {
    return this.http.patch<DetailedAreaKnowledge>(`${this.urlEndPoint}/${id}`,null);
  }

  find(id: number): Observable<DetailedAreaKnowledge> {
    return this.http.get<DetailedAreaKnowledge>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<DetailedAreaKnowledge[]> {
    return this.http.get<DetailedAreaKnowledge[]>(this.urlEndPoint);
  }

  save(detailedAreaKnowledge: DetailedAreaKnowledge): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, detailedAreaKnowledge);
  }
}