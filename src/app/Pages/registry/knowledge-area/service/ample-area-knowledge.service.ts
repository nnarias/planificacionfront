import { AmpleAreaKnowledge } from '../model/ampleAreaKnowledge';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AmpleAreaKnowledgeService {
  private urlEndPoint: string = `${environment.apiUrl}/ampleAreaKnowledge`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<AmpleAreaKnowledge> {
    return this.http.patch<AmpleAreaKnowledge>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<AmpleAreaKnowledge> {
    return this.http.get<AmpleAreaKnowledge>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<AmpleAreaKnowledge[]> {
    return this.http.get<AmpleAreaKnowledge[]>(this.urlEndPoint);
  }

  save(ampleAreaKnowledge: AmpleAreaKnowledge): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, ampleAreaKnowledge);
  }
}