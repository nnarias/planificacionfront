import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UnescoAreaKnowledge } from '../model/unescoAreaKnowledge';

@Injectable({
  providedIn: 'root'
})
export class UnescoAreaKnowledgeService {

  private urlEndPoint: string = `${environment.apiUrl}/unescoAreaKnowledge`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<UnescoAreaKnowledge> {
    return this.http.patch<UnescoAreaKnowledge>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<UnescoAreaKnowledge> {
    return this.http.get<UnescoAreaKnowledge>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<UnescoAreaKnowledge[]> {
    return this.http.get<UnescoAreaKnowledge[]>(this.urlEndPoint);
  }

  save(unescoAreaKnowledge: UnescoAreaKnowledge): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, unescoAreaKnowledge);
  }
}