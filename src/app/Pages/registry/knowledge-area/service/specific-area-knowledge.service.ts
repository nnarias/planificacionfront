import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SpecificAreaKnowledge } from '../model/specificAreaKnowledge';

@Injectable({
  providedIn: 'root'
})
export class SpecificAreaKnowledgeService {

  private urlEndPoint: string = `${environment.apiUrl}/specificAreaKnowledge`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<SpecificAreaKnowledge> {
    return this.http.patch<SpecificAreaKnowledge>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<SpecificAreaKnowledge> {
    return this.http.get<SpecificAreaKnowledge>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<SpecificAreaKnowledge[]> {
    return this.http.get<SpecificAreaKnowledge[]>(this.urlEndPoint);
  }

  save(detailedAreaKnowledge: SpecificAreaKnowledge): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, detailedAreaKnowledge);
  }
}