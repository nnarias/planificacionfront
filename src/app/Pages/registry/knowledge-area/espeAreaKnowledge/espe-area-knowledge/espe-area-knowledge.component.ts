import { Component, OnInit, ViewChild } from '@angular/core';
import { EspeAreaKnowledge } from '../../model/espeAreaKnowledge';
import { EspeAreaKnowledgeService } from '../../service/espe-area-knowledge.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-espe-area-knowledge',
  templateUrl: './espe-area-knowledge.component.html',
  styleUrls: ['./espe-area-knowledge.component.scss']
})
export class EspeAreaKnowledgeComponent implements OnInit {
  private dataSource = new MatTableDataSource<EspeAreaKnowledge>();
  private displayedColumns: string[] = ['name', 'actions'];
  private espeAreaKnowledgeList: EspeAreaKnowledge[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private espeAreaKnowledgeService: EspeAreaKnowledgeService) { }

  ngOnInit(): void {
    this.loadEspeAreaKnowledgeList();
  }

  delete(espeAreaKnowledge: EspeAreaKnowledge): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Área de Conocimiento Espe: ${espeAreaKnowledge.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.espeAreaKnowledgeService.delete(espeAreaKnowledge.id).subscribe(() => {
          this.espeAreaKnowledgeList = this.espeAreaKnowledgeList.filter(result => result !== espeAreaKnowledge);
          this.dataSource.data = this.espeAreaKnowledgeList;
          swal.fire('Área de Conocimiento Espe eliminada', `${espeAreaKnowledge.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadEspeAreaKnowledgeList(): void {
    this.espeAreaKnowledgeService.findAll().subscribe(espeAreaKnowledgeList => {
      this.dataSource.data = espeAreaKnowledgeList;
      this.espeAreaKnowledgeList = espeAreaKnowledgeList;
    });
  }

  public get getDataSource(): MatTableDataSource<EspeAreaKnowledge> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getEspeAreaKnowledgeList(): EspeAreaKnowledge[] {
    return this.espeAreaKnowledgeList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}