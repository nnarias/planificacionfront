import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EspeAreaKnowledgeComponent } from './espe-area-knowledge.component';

describe('EspeAreaKnowledgeComponent', () => {
  let component: EspeAreaKnowledgeComponent;
  let fixture: ComponentFixture<EspeAreaKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EspeAreaKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EspeAreaKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
