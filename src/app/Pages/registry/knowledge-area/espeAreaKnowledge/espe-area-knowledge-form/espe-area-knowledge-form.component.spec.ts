import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EspeAreaKnowledgeFormComponent } from './espe-area-knowledge-form.component';

describe('EspeAreaKnowledgeFormComponent', () => {
  let component: EspeAreaKnowledgeFormComponent;
  let fixture: ComponentFixture<EspeAreaKnowledgeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EspeAreaKnowledgeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EspeAreaKnowledgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
