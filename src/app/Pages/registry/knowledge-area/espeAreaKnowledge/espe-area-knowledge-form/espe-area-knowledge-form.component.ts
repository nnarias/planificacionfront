import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EspeAreaKnowledge } from '../../model/espeAreaKnowledge';
import { EspeAreaKnowledgeService } from '../../service/espe-area-knowledge.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-espe-area-knowledge-form',
  templateUrl: './espe-area-knowledge-form.component.html',
  styleUrls: ['./espe-area-knowledge-form.component.scss']
})
export class EspeAreaKnowledgeFormComponent implements OnInit {
  private espeAreaKnowledge: EspeAreaKnowledge = new EspeAreaKnowledge();
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private espeAreaKnowledgeService: EspeAreaKnowledgeService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loadEspeAreaKnowledge();
  }

  loadEspeAreaKnowledge(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.espeAreaKnowledgeService.find(id).subscribe(espeAreaKnowledge => {
          this.form.patchValue({
            id: espeAreaKnowledge.id,
            name: espeAreaKnowledge.name
          });
        });
        this.title = 'Modificar Área de Conocimiento Espe';
      } else {
        this.title = 'Crear Área de Conocimiento Espe';
      }
    });
  }

  save(): void {
    this.espeAreaKnowledge = this.form.value;
    console.log(this.espeAreaKnowledge);
    this.espeAreaKnowledgeService.save(this.espeAreaKnowledge).subscribe(() => {
      this.router.navigate(['app/registry/knowledgeArea/espeAreaKnowledge']);
      swal.fire(this.espeAreaKnowledge.id ? 'Área de Conocimiento ESPE Actualizada' : 'Nueva Área de Conocimiento ESPE', `${this.espeAreaKnowledge.name}`,
        'success');
    });
  }

  public get getEspeAreaKnowledge(): EspeAreaKnowledge {
    return this.espeAreaKnowledge;
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}