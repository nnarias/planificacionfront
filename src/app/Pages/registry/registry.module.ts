import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RegistryRoutingModule } from './registry-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, RegistryRoutingModule]
})
export class RegistryModule { }