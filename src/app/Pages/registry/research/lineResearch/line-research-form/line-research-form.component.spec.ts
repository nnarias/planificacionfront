import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LineResearchFormComponent } from './line-research-form.component';

describe('LineResearchFormComponent', () => {
  let component: LineResearchFormComponent;
  let fixture: ComponentFixture<LineResearchFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LineResearchFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LineResearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
