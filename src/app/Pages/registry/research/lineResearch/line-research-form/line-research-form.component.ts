import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LineResearch } from '../../model/lineResearch';
import { LineResearchService } from '../../service/line-research.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-line-research-form',
  templateUrl: './line-research-form.component.html',
  styleUrls: ['./line-research-form.component.scss']
})
export class LineResearchFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private lineResearch: LineResearch = new LineResearch();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private lineResearchService: LineResearchService, private router: Router) { }

  ngOnInit(): void {
    this.loadLineResearch();
  }

  loadLineResearch(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.lineResearchService.find(id).subscribe(lineResearch => {
          this.form.patchValue({
            id: lineResearch.id,
            name: lineResearch.name
          });
        });
        this.title = 'Modificar Línea de Investigación';
      } else {
        this.title = 'Registrar Línea de Investigación';
      }
    });
  }

  save(): void {
    this.lineResearch = this.form.value;
    this.lineResearchService.save(this.lineResearch).subscribe(() => {
      this.router.navigate(['app/registry/research/lineResearch']);
      swal.fire(this.lineResearch.id ? 'Línea de Investigación Actualizada' : 'Nueva Línea de Investigación', `${this.lineResearch.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}