import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LineResearchComponent } from './line-research.component';

describe('LineResearchComponent', () => {
  let component: LineResearchComponent;
  let fixture: ComponentFixture<LineResearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LineResearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LineResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
