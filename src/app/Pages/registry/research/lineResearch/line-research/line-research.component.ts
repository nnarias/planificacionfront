import { Component, OnInit, ViewChild } from '@angular/core';
import { LineResearch } from '../../model/lineResearch';
import { LineResearchService } from '../../service/line-research.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-line-research',
  templateUrl: './line-research.component.html',
  styleUrls: ['./line-research.component.scss']
})
export class LineResearchComponent implements OnInit {
  private dataSource = new MatTableDataSource<LineResearch>();
  private displayedColumns: string[] = ['name', 'actions'];
  private lineResearchList: LineResearch[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private lineResearchService: LineResearchService) { }

  ngOnInit(): void {
    this.loadLineResearchList();
  }

  delete(lineResearch: LineResearch): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Línea de Investigación: ${lineResearch.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.lineResearchService.delete(lineResearch.id).subscribe(() => {
          this.lineResearchList = this.lineResearchList.filter(result => result !== lineResearch);
          this.dataSource.data = this.lineResearchList;
          swal.fire('Línea de Investigación Eliminada', `${lineResearch.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadLineResearchList(): void {
    this.lineResearchService.findAll().subscribe(lineResearchList => {
      this.dataSource.data = lineResearchList;
      this.lineResearchList = lineResearchList;
    });
  }

  public get getDataSource(): MatTableDataSource<LineResearch> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getLineResearchList(): LineResearch[] {
    return this.lineResearchList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}