import { AuthGuard } from '../../security/guard/auth.guard';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';
import { LineResearchComponent } from './lineResearch/line-research/line-research.component';
import { LineResearchFormComponent } from './lineResearch/line-research-form/line-research-form.component';
import { ResearchGroupComponent } from './researchGroup/research-group/research-group.component';
import { ResearchGroupFormComponent } from './researchGroup/research-group-form/research-group-form.component';
import { ResearchTypeComponent } from './researchType/research-type/research-type.component';
import { ResearchTypeFormComponent } from './researchType/research-type-form/research-type-form.component';
import { ScientificDisciplineComponent } from './scientificDiscipline/scientific-discipline/scientific-discipline.component';
import { ScientificDisciplineFormComponent } from './scientificDiscipline/scientific-discipline-form/scientific-discipline-form.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'lineResearch', component: LineResearchComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'lineResearch/form', component: LineResearchFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'lineResearch/form/:id', component: LineResearchFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'researchGroup', component: ResearchGroupComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'researchGroup/form', component: ResearchGroupFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'researchGroup/form/:id', component: ResearchGroupFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'researchType', component: ResearchTypeComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'researchType/form', component: ResearchTypeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'researchType/form/:id', component: ResearchTypeFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'scientificDiscipline', component: ScientificDisciplineComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'scientificDiscipline/form', component: ScientificDisciplineFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'scientificDiscipline/form/:id', component: ScientificDisciplineFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResearchRoutingModule { }