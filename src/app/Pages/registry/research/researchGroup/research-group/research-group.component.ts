import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ResearchGroup } from '../../model/researchGroup';
import { ResearchGroupService } from '../../service/research-group.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-research-group',
  templateUrl: './research-group.component.html',
  styleUrls: ['./research-group.component.scss']
})
export class ResearchGroupComponent implements OnInit {
  private dataSource = new MatTableDataSource<ResearchGroup>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private researchGroupList: ResearchGroup[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private researchGroupService: ResearchGroupService) { }

  ngOnInit(): void {
    this.loadResearchGroupList();
  }

  delete(researchGroup: ResearchGroup): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Grupo de Investigación: ${researchGroup.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.researchGroupService.delete(researchGroup.id).subscribe(() => {
          this.researchGroupList = this.researchGroupList.filter(result => result !== researchGroup);
          this.dataSource.data = this.researchGroupList;
          swal.fire('Grupo de Investigación Eliminado', `${researchGroup.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadResearchGroupList(): void {
    this.researchGroupService.findAll().subscribe(researchGroupList => {
      this.researchGroupList = researchGroupList;
      this.dataSource.data = researchGroupList;
    });
  }

  public get getDataSource(): MatTableDataSource<ResearchGroup> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getResearchGroupList(): ResearchGroup[] {
    return this.researchGroupList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}