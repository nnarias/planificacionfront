import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ResearchGroup } from '../../model/researchGroup';
import { ResearchGroupService } from '../../service/research-group.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-research-group-form',
  templateUrl: './research-group-form.component.html',
  styleUrls: ['./research-group-form.component.scss']
})
export class ResearchGroupFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private researchGroup: ResearchGroup = new ResearchGroup();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private researchGroupService: ResearchGroupService, private router: Router) { }

  ngOnInit(): void {
    this.loadResearchGroup();
  }

  loadResearchGroup(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.researchGroupService.find(id).subscribe(researchGroup => {
          this.form.patchValue({
            id: researchGroup.id,
            name: researchGroup.name
          });
        });
        this.title = 'Modificar Grupo de Investigación';
      } else {
        this.title = 'Registrar Grupo de Investigación';
      }
    });
  }

  save(): void {
    this.researchGroup = this.form.value;
    this.researchGroupService.save(this.researchGroup).subscribe(() => {
      this.router.navigate(['app/registry/research/researchGroup']);
      swal.fire(this.researchGroup.id ? 'Grupo de Investigación Actualizado' : 'Nuevo Grupo de Investigación', `${this.researchGroup.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}