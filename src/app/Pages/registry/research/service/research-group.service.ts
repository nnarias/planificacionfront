import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResearchGroup } from '../model/researchGroup';

@Injectable({
  providedIn: 'root'
})
export class ResearchGroupService {
  private urlEndPoint: string = `${environment.apiUrl}/researchGroup`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<ResearchGroup> {
    return this.http.patch<ResearchGroup>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<ResearchGroup> {
    return this.http.get<ResearchGroup>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<ResearchGroup[]> {
    return this.http.get<ResearchGroup[]>(this.urlEndPoint);
  }

  save(researchGroup: ResearchGroup): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, researchGroup);
  }
}