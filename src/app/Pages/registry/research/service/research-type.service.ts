import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResearchType } from '../model/researchType';

@Injectable({
  providedIn: 'root'
})
export class ResearchTypeService {
  private urlEndPoint: string = `${environment.apiUrl}/researchType`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<ResearchType> {
    return this.http.patch<ResearchType>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<ResearchType> {
    return this.http.get<ResearchType>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<ResearchType[]> {
    return this.http.get<ResearchType[]>(this.urlEndPoint);
  }

  save(researchType: ResearchType): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, researchType);
  }
}