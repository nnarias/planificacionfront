import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LineResearch } from '../model/lineResearch';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LineResearchService {
  private urlEndPoint: string = `${environment.apiUrl}/lineResearch`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<LineResearch> {
    return this.http.patch<LineResearch>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<LineResearch> {
    return this.http.get<LineResearch>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<LineResearch[]> {
    return this.http.get<LineResearch[]>(this.urlEndPoint);
  }

  save(lineResearch: LineResearch): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, lineResearch);
  }
}