import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ScientificDiscipline } from '../model/scientificDiscipline';

@Injectable({
  providedIn: 'root'
})
export class ScientificDisciplineService {
  private urlEndPoint: string = `${environment.apiUrl}/scientificDiscipline`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<ScientificDiscipline> {
    return this.http.patch<ScientificDiscipline>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<ScientificDiscipline> {
    return this.http.get<ScientificDiscipline>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<ScientificDiscipline[]> {
    return this.http.get<ScientificDiscipline[]>(this.urlEndPoint);
  }

  save(scientificDiscipline: ScientificDiscipline): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, scientificDiscipline);
  }
}