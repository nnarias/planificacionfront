import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ResearchType } from '../../model/researchType';
import { ResearchTypeService } from '../../service/research-type.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-research-type',
  templateUrl: './research-type.component.html',
  styleUrls: ['./research-type.component.scss']
})
export class ResearchTypeComponent implements OnInit {
  private dataSource = new MatTableDataSource<ResearchType>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private researchTypeList: ResearchType[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private researchTypeService: ResearchTypeService) { }

  ngOnInit(): void {
    this.loadResearchTypeList();
  }

  delete(researchType: ResearchType): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Tipo de Investigación: ${researchType.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.researchTypeService.delete(researchType.id).subscribe(() => {
          this.researchTypeList = this.researchTypeList.filter(result => result !== researchType);
          this.dataSource.data = this.researchTypeList;
          swal.fire('Tipo de Investigación Eliminado', `${researchType.name}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadResearchTypeList(): void {
    this.researchTypeService.findAll().subscribe(researchTypeList => {
      this.dataSource.data = researchTypeList;
      this.researchTypeList = researchTypeList;
    });
  }

  public get getDataSource(): MatTableDataSource<ResearchType> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getResearchTypeList(): ResearchType[] {
    return this.researchTypeList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}