import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ResearchType } from '../../model/researchType';
import { ResearchTypeService } from '../../service/research-type.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-research-type-form',
  templateUrl: './research-type-form.component.html',
  styleUrls: ['./research-type-form.component.scss']
})
export class ResearchTypeFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private researchType: ResearchType = new ResearchType();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private researchTypeService: ResearchTypeService, private router: Router) { }

  ngOnInit(): void {
    this.loadResearchType();
  }

  loadResearchType(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.researchTypeService.find(id).subscribe(researchType => {
          this.form.patchValue({
            id: researchType.id,
            name: researchType.name
          });
        });
        this.title = 'Modificar Tipo de Investigación';
      } else {
        this.title = 'Registrar Tipo de Investigación';
      }
    });
  }

  save(): void {
    this.researchType = this.form.value;
    this.researchTypeService.save(this.researchType).subscribe(() => {
      this.router.navigate(['app/registry/research/researchType']);
      swal.fire(this.researchType.id ? 'Tipo de Investigación Actualizado' : 'Nuevo Tipo de Investigación', `${this.researchType.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}