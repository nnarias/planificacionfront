import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchTypeFormComponent } from './research-type-form.component';

describe('ResearchTypeFormComponent', () => {
  let component: ResearchTypeFormComponent;
  let fixture: ComponentFixture<ResearchTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResearchTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
