import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ScientificDiscipline } from '../../model/scientificDiscipline';
import { ScientificDisciplineService } from '../../service/scientific-discipline.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-scientific-discipline',
  templateUrl: './scientific-discipline.component.html',
  styleUrls: ['./scientific-discipline.component.scss']
})
export class ScientificDisciplineComponent implements OnInit {
  private dataSource = new MatTableDataSource<ScientificDiscipline>();
  private displayedColumns: string[] = ['name', 'actions'];
  private paginator: MatPaginator;
  private scientificDisciplineList: ScientificDiscipline[] = [];
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private scientificDisciplineService: ScientificDisciplineService) { }

  ngOnInit(): void {
    this.loadScientificDisciplineList();
  }

  delete(scientificDiscipline: ScientificDiscipline): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Disciplina Científica: ${scientificDiscipline.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.scientificDisciplineService.delete(scientificDiscipline.id).subscribe(() => {
          this.scientificDisciplineList = this.scientificDisciplineList.filter(result => result !== scientificDiscipline);
          this.dataSource.data = this.scientificDisciplineList;
          swal.fire('Disciplina Científica Eliminada', `${scientificDiscipline.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadScientificDisciplineList(): void {
    this.scientificDisciplineService.findAll().subscribe(scientificDisciplineList => {
      this.dataSource.data = scientificDisciplineList;
      this.scientificDisciplineList = scientificDisciplineList;
    });
  }

  public get getDataSource(): MatTableDataSource<ScientificDiscipline> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getScientificDisciplineList(): ScientificDiscipline[] {
    return this.scientificDisciplineList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}