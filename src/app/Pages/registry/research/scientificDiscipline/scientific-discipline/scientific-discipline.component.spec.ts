import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScientificDisciplineComponent } from './scientific-discipline.component';

describe('ScientificDisciplineComponent', () => {
  let component: ScientificDisciplineComponent;
  let fixture: ComponentFixture<ScientificDisciplineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScientificDisciplineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScientificDisciplineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
