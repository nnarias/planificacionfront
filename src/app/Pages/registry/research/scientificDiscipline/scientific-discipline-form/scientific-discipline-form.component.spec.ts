import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScientificDisciplineFormComponent } from './scientific-discipline-form.component';

describe('ScientificDisciplineFormComponent', () => {
  let component: ScientificDisciplineFormComponent;
  let fixture: ComponentFixture<ScientificDisciplineFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScientificDisciplineFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScientificDisciplineFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
