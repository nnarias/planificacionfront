import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ScientificDiscipline } from '../../model/scientificDiscipline';
import { ScientificDisciplineService } from '../../service/scientific-discipline.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-scientific-discipline-form',
  templateUrl: './scientific-discipline-form.component.html',
  styleUrls: ['./scientific-discipline-form.component.scss']
})
export class ScientificDisciplineFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', Validators.required]
  });
  private scientificDiscipline: ScientificDiscipline = new ScientificDiscipline();
  private title: string;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private scientificDisciplineService: ScientificDisciplineService, private router: Router) { }

  ngOnInit(): void {
    this.loadScientificDiscipline();
  }

  loadScientificDiscipline(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.scientificDisciplineService.find(id).subscribe(scientificDiscipline => {
          this.form.patchValue({
            id: scientificDiscipline.id,
            name: scientificDiscipline.name
          });
        });
        this.title = 'Modificar Disciplina Científica';
      } else {
        this.title = 'Registrar Disciplina Científica';
      }
    });
  }

  save(): void {
    this.scientificDiscipline = this.form.value;
    this.scientificDisciplineService.save(this.scientificDiscipline).subscribe(() => {
      this.router.navigate(['app/registry/research/scientificDiscipline']);
      swal.fire(this.scientificDiscipline.id ? 'Disciplina Científica Actualizada' : 'Nueva Disciplina Científica', `${this.scientificDiscipline.name}`, 'success');
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): string {
    return this.title;
  }

  public get name() { return this.form.get('name'); }
}