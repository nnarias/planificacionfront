import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { LineResearchComponent } from './lineResearch/line-research/line-research.component';
import { LineResearchFormComponent } from './lineResearch/line-research-form/line-research-form.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ResearchRoutingModule } from './research-routing.module';
import { ResearchGroupComponent } from './researchGroup/research-group/research-group.component';
import { ResearchGroupFormComponent } from './researchGroup/research-group-form/research-group-form.component';
import { ResearchTypeComponent } from './researchType/research-type/research-type.component';
import { ResearchTypeFormComponent } from './researchType/research-type-form/research-type-form.component';
import { ScientificDisciplineComponent } from './scientificDiscipline/scientific-discipline/scientific-discipline.component';
import { ScientificDisciplineFormComponent } from './scientificDiscipline/scientific-discipline-form/scientific-discipline-form.component';


@NgModule({
  declarations: [LineResearchComponent, LineResearchFormComponent, MenuComponent, ResearchGroupComponent, ResearchGroupFormComponent, ResearchTypeComponent,
    ResearchTypeFormComponent, ScientificDisciplineComponent, ScientificDisciplineFormComponent],
  imports: [CommonModule, ImportsModule, ResearchRoutingModule]
})
export class ResearchModule { }