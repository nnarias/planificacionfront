import { AuthGuard } from '../../security/guard/auth.guard';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { ProjectDeliverableComponent } from './project-deliverable/project-deliverable.component';
import { ProjectDeliverableFormComponent } from './project-deliverable-form/project-deliverable-form.component';
import { ProjectDeliverableInvoiceComponent } from './project-deliverable-invoice/project-deliverable-invoice.component';
import { ProjectInvoiceComponent } from './project-invoice/project-invoice.component';
import { ProjectInvoiceFormComponent } from './project-invoice-form/project-invoice-form.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectDeliverableInvoice', component: ProjectDeliverableInvoiceComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectDeliverable/:id', component: ProjectDeliverableComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectDeliverable/form/:id/:id2', component: ProjectDeliverableFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectInvoice/:id', component: ProjectInvoiceComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'projectInvoice/form/:id/:id2', component: ProjectInvoiceFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliverableInvoiceRoutingModule { }
