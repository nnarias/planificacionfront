import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GoalActivity } from 'src/app/Pages/registry/project/model/goalActivity';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import swal from 'sweetalert2';
import { GoalActivityService } from '../service/goal-activity.service';
@Component({
  selector: 'app-project-deliverable-form',
  templateUrl: './project-deliverable-form.component.html',
  styleUrls: ['./project-deliverable-form.component.scss']
})
export class ProjectDeliverableFormComponent implements OnInit {
  private idProject: number;
  private _project: Project = new Project();

  private idGoalActivity: number;
  private _showSpiner: Boolean = true;
  private _form: FormGroup = this.formBuilder.group({
    file: ['', Validators.required],
    description: ['']
  });

  private _goalActivity: GoalActivity = new GoalActivity();

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private goalActivityService: GoalActivityService, private router: Router, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.idProject = params['id'];
      this.idGoalActivity = params['id2'];
      this.loadProject(this.idProject);
      this.loadProjectDeliverable(this.idGoalActivity);
    })
  }

  loadProjectDeliverable(id: number) {
    this.goalActivityService.findPending(id).subscribe((goalActivity) => {
      this._goalActivity = goalActivity;
    })
  }

  loadProject(id: number): void {
    this.projectService.findByProjectDirector(id).subscribe((project) => {
      this._project = project;
      this._showSpiner = false;
    });
  }

  public save(): void {
    const formData = new FormData();
    formData.append('id', this.idGoalActivity.toString());
    formData.append('file', this._form.get('file').value);
    formData.append('description', this._form.get('description').value);
    this.goalActivityService.saveDeliverable(formData).subscribe(() => {
      this.router.navigate(['app/tracking/deliverableInvoice/projectDeliverableInvoice']);
      swal.fire('Operación Completa', `Entregable guardado con éxito`, 'success');
    });
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get goalActivity(): GoalActivity {
    return this._goalActivity;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get file() { return this._form.get('file'); }

  public get description() { return this._form.get('description'); }

  public get project(): Project {
    return this._project;
  }
}
