import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { DeliverableInvoiceRoutingModule } from './deliverable-invoice-routing.module';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { ProjectDeliverableComponent } from './project-deliverable/project-deliverable.component';
import { ProjectDeliverableFormComponent } from './project-deliverable-form/project-deliverable-form.component';
import { ProjectDeliverableInvoiceComponent } from './project-deliverable-invoice/project-deliverable-invoice.component';
import { ProjectInvoiceComponent } from './project-invoice/project-invoice.component';
import { ProjectInvoiceFormComponent } from './project-invoice-form/project-invoice-form.component';

@NgModule({
  declarations: [MenuComponent, ProjectDeliverableInvoiceComponent, ProjectDeliverableComponent, ProjectDeliverableFormComponent, ProjectInvoiceComponent, ProjectInvoiceFormComponent],
  imports: [CommonModule, DeliverableInvoiceRoutingModule, ImportsModule]
})
export class DeliverableInvoiceModule { }
