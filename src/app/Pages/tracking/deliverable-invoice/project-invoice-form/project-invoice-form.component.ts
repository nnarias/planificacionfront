import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityResource } from 'src/app/Pages/registry/project/model/activityResource';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import { ActivityResourceService } from '../service/activity-resource.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-project-invoice-form',
  templateUrl: './project-invoice-form.component.html',
  styleUrls: ['./project-invoice-form.component.scss']
})
export class ProjectInvoiceFormComponent implements OnInit {
  private _project: Project = new Project();
  private idActivityResource: number;
  private idProject: number;
  private _showSpiner: Boolean = true;
  private _form: FormGroup = this.formBuilder.group({
    file: ['', Validators.required],
    investmentBudget: ['', Validators.required],
    currentBudget: ['', Validators.required],
    description: ['']
  });

  private _activityResource: ActivityResource = new ActivityResource();

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, private activityResourceService: ActivityResourceService, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.idProject = params['id'];
      this.idActivityResource = params['id2'];
      this.loadProjectInvoice(this.idActivityResource);
      this.loadProject(this.idProject);
    })
  }

  loadProjectInvoice(id: number) {
    this.activityResourceService.findPending(id).subscribe((activityResource) => {
      this._activityResource = activityResource;
    })
  }

  loadProject(id: number): void {
    this.projectService.findByProjectDirector(id).subscribe((project) => {
      this._project = project;
      this._showSpiner = false;
    });
  }

  save(): void {
    const formData = new FormData();
    formData.append('id', this.idActivityResource.toString());
    formData.append('file', this._form.get('file').value);
    formData.append('description', this._form.get('description').value);
    formData.append('investmentBudget', this._form.get('investmentBudget').value);
    formData.append('currentBudget', this._form.get('currentBudget').value);
    this.activityResourceService.saveInvoice(formData).subscribe(() => {
      this.router.navigate(['app/tracking/deliverableInvoice/projectDeliverableInvoice']);
      swal.fire('Operación Completa', `Entregable guardado con éxito`, 'success');
    });
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get activityResource(): ActivityResource {
    return this._activityResource;
  }
  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
  public get file() { return this._form.get('file'); }

  public get description() { return this._form.get('description'); }

  public get investmentBudget() { return this._form.get('investmentBudget'); }

  public get currentBudget() { return this._form.get('currentBudget'); }

  public get project(): Project {
    return this._project;
  }

}
