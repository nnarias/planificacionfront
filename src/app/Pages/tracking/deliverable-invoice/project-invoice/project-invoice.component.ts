import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { ActivityResource } from 'src/app/Pages/registry/project/model/activityResource';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import { ActivityResourceService } from '../service/activity-resource.service';

@Component({
  selector: 'app-project-invoice',
  templateUrl: './project-invoice.component.html',
  styleUrls: ['./project-invoice.component.scss']
})
export class ProjectInvoiceComponent implements OnInit {

  private _project: Project = new Project();
  private _dataSource = new MatTableDataSource<ActivityResource>();
  private _displayedColumns: string[] = ['goalActivity', 'resource', 'investmentBudget', 'currentBudget', 'actions'];
  private _paginator: MatPaginator;
  private _activityResourceList: ActivityResource[] = [];
  private _search: string;
  private _show: Boolean = true;
  private _sort: MatSort;
  private _id: number;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this._paginator = paginator;
    if (this._paginator) {
      this._dataSource.paginator = this._paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this._sort = sort;
    if (this._sort) {
      this._dataSource.sort = this._sort;
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private activityResourceService: ActivityResourceService, private projectService: ProjectService) { }

  ngOnInit(): void {
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'resource': return item.resource.name;
        case 'goalActivity': return item.goalActivity.name;
        default: return item[property];
      }
    }

    this.activatedRoute.params.subscribe(params => {
      this._id = params['id'];
      if (this._id) {
        this.loadActivityResourceList(this._id);
        this.loadProject(this._id);
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadActivityResourceList(id: number): void {
    this.activityResourceService.findAllPending(id).subscribe(activityResourceList => {
      console.log(activityResourceList);
      this._dataSource.data = activityResourceList;
      this._activityResourceList = activityResourceList;
      this._show = false;
    });
  }

  loadProject(id: number): void {
    this.projectService.findByProjectDirector(id).subscribe((project) => {
      this._project = project;
      console.log(project);
    });
  }

  public get dataSource(): MatTableDataSource<ActivityResource> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get activityResourceList(): ActivityResource[] {
    return this._activityResourceList;
  }

  public get showSpiner(): Boolean {
    return this._show;
  }

  public get id(): Number {
    return this._id;
  }

  public get project(): Project {
    return this._project;
  }

  public set setSearch(search: string) {
    this._search = search;
  }

}
