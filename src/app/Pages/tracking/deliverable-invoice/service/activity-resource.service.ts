import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivityResource } from 'src/app/Pages/registry/project/model/activityResource';
import { environment } from 'src/environments/environment';
import { ActivityResourceApprovalDTO } from '../../financial-tracking/model/activityResourceApprovalDTO';

@Injectable({
  providedIn: 'root'
})
export class ActivityResourceService {

  private urlEndPoint: string = `${environment.apiUrl}/activityResource`;

  constructor(private http: HttpClient) { }

  findAllPending(idProject: number): Observable<ActivityResource[]> {
    return this.http.get<ActivityResource[]>(`${this.urlEndPoint}/allPending/${idProject}`);
  }

  findAllTracking(idProject: number): Observable<ActivityResource[]> {
    return this.http.get<ActivityResource[]>(`${this.urlEndPoint}/allTracking/${idProject}`);
  }

  findPending(id: number): Observable<ActivityResource> {
    return this.http.get<ActivityResource>(`${this.urlEndPoint}/pending/${id}`);
  }

  findTracking(id: number): Observable<ActivityResource> {
    return this.http.get<ActivityResource>(`${this.urlEndPoint}/tracking/${id}`);
  }
  saveInvoice(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/invoice`, formData);
  }
  invoiceApproval(activityResourceApprovalDTO: ActivityResourceApprovalDTO): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/invoiceApproval`, activityResourceApprovalDTO);
  }
}
