import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GoalActivity } from 'src/app/Pages/registry/project/model/goalActivity';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GoalActivityService {
  private urlEndPoint: string = `${environment.apiUrl}/goalActivity`;

  constructor(private http: HttpClient) { }

  deliverableApproval(goalActivityApproval: GoalActivity): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/deliverableApproval`, goalActivityApproval);
  }

  findAllPending(idProject: number): Observable<GoalActivity[]> {
    return this.http.get<GoalActivity[]>(`${this.urlEndPoint}/allPending/${idProject}`);
  }

  findAllTracking(idProject: number): Observable<GoalActivity[]> {
    return this.http.get<GoalActivity[]>(`${this.urlEndPoint}/allTracking/${idProject}`);
  }

  findPending(id: number): Observable<GoalActivity> {
    return this.http.get<GoalActivity>(`${this.urlEndPoint}/pending/${id}`);
  }

  findTracking(id: number): Observable<GoalActivity> {
    return this.http.get<GoalActivity>(`${this.urlEndPoint}/tracking/${id}`);
  }

  saveDeliverable(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/deliverable`, formData);
  }
}