import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDeliverableInvoiceComponent } from './project-deliverable-invoice.component';

describe('ProjectDeliverableInvoiceComponent', () => {
  let component: ProjectDeliverableInvoiceComponent;
  let fixture: ComponentFixture<ProjectDeliverableInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectDeliverableInvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDeliverableInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
