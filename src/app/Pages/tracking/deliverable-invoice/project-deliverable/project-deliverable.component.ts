import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { GoalActivity } from 'src/app/Pages/registry/project/model/goalActivity';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import { GoalActivityService } from '../service/goal-activity.service';

@Component({
  selector: 'app-project-deliverable',
  templateUrl: './project-deliverable.component.html',
  styleUrls: ['./project-deliverable.component.scss']
})
export class ProjectDeliverableComponent implements OnInit {

  private _project: Project = new Project();
  private _dataSource = new MatTableDataSource<GoalActivity>();
  private _displayedColumns: string[] = ['name', 'actions'];
  private _paginator: MatPaginator;
  private _goalActivityList: GoalActivity[] = [];
  private _search: string;
  private _showSpiner: Boolean = true;
  private _sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this._paginator = paginator;
    if (this._paginator) {
      this._dataSource.paginator = this._paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this._sort = sort;
    if (this._sort) {
      this._dataSource.sort = this._sort;
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private goalActivityService: GoalActivityService, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.loadGoalActivityList(id);
        this.loadProject(id);
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadGoalActivityList(id: number): void {
    this.goalActivityService.findAllPending(id).subscribe(goalActivityList => {
      this._dataSource.data = goalActivityList;
      this._goalActivityList = goalActivityList;
      this._showSpiner = false;
    });
  }

  loadProject(id: number): void {
    this.projectService.findByProjectDirector(id).subscribe((project) => {
      this._project = project;
    });
  }

  public get dataSource(): MatTableDataSource<GoalActivity> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get goalActivityList(): GoalActivity[] {
    return this._goalActivityList;
  }

  public get project(): Project {
    return this._project;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}