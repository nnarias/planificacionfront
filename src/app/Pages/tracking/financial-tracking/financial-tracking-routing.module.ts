import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../security/guard/auth.guard';
import { RoleGuard } from '../../security/guard/role.guard';
import { MenuComponent } from './menu/menu.component';
import { ProjectInvoiceFormComponent } from './project-invoice-form/project-invoice-form.component';
import { ProjectInvoiceComponent } from './project-invoice/project-invoice.component';
import { ProjectComponent } from './project/project.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_FINANCIAL'] } },
{ path: 'project', component: ProjectComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_FINANCIAL'] } },
{ path: 'projectInvoice/:id', component: ProjectInvoiceComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_FINANCIAL'] } },
{ path: 'projectInvoice/form/:id/:id2', component: ProjectInvoiceFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_FINANCIAL'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialTrackingRoutingModule { }
