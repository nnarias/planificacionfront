import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityResource } from 'src/app/Pages/registry/project/model/activityResource';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import { ActivityResourceService } from '../../deliverable-invoice/service/activity-resource.service';
import swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { ActivityResourceApprovalEnum } from '../model/activityResourceApprovalEnum';
import { ActivityResourceApprovalDTO } from '../model/activityResourceApprovalDTO';

interface ActivityResourceApprovalStatus {
  value: ActivityResourceApprovalEnum;
  viewValue: string;
}

@Component({
  selector: 'app-project-invoice-form',
  templateUrl: './project-invoice-form.component.html',
  styleUrls: ['./project-invoice-form.component.scss']
})
export class ProjectInvoiceFormComponent implements OnInit {

  private _project: Project = new Project();
  private idActivityResource: number;
  private idProject: number;
  private _showSpiner: Boolean = true;
  private _form: FormGroup = this.formBuilder.group({
    activityResourceApprovalEnum: ['', Validators.required],
    comment: ['']
  });

  private _activityResourceApprovalStatus: ActivityResourceApprovalStatus[] = [
    { value: ActivityResourceApprovalEnum.Approved, viewValue: "Aprobar factura" },
    { value: ActivityResourceApprovalEnum.Rejected, viewValue: "Rechazar factura" },
  ];

  private _activityResourceApprovalDTO: ActivityResourceApprovalDTO = new ActivityResourceApprovalDTO();

  private _urlEndPoint: string = `${environment.apiUrl}/activityResource/invoiceFile`;

  private _activityResource: ActivityResource = new ActivityResource();

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, private activityResourceService: ActivityResourceService, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.idProject = params['id'];
      this.idActivityResource = params['id2'];
      this.loadProjectInvoice(this.idActivityResource);
      this.loadProject(this.idProject);
    })
  }

  loadProjectInvoice(id: number) {
    this.activityResourceService.findTracking(id).subscribe((activityResource) => {
      this._activityResource = activityResource;
    })
  }

  loadProject(id: number): void {
    this.projectService.findByFinancialSupervisor(id).subscribe((project) => {
      this._project = project;
      this._showSpiner = false;
    });
  }

  invoiceApproval(): void {
    this._activityResourceApprovalDTO = this.form.value;
    this._activityResourceApprovalDTO.id = this.idActivityResource;
    this.activityResourceService.invoiceApproval(this._activityResourceApprovalDTO).subscribe(() => {
      this.router.navigate(['app/tracking/financialTracking']);
      swal.fire('Operación Completa', `Evaluación guardada con éxito`, 'success');
    });
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get activityResource(): ActivityResource {
    return this._activityResource;
  }
  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
  public get comment() { return this._form.get('comment'); }

  public get activityResourceApprovalEnum() { return this._form.get('activityResourceApprovalEnum'); }

  public get project(): Project {
    return this._project;
  }

  public get urlEndpoint(): string {
    return this._urlEndPoint;
  }

  public get activityResourceApprovalStatus(): ActivityResourceApprovalStatus[] {
    return this._activityResourceApprovalStatus;
  }
}
