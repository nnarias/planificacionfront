import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinancialTrackingRoutingModule } from './financial-tracking-routing.module';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { ProjectInvoiceComponent } from './project-invoice/project-invoice.component';
import { ProjectComponent } from './project/project.component';
import { ProjectInvoiceFormComponent } from './project-invoice-form/project-invoice-form.component';

@NgModule({
  declarations: [MenuComponent, ProjectComponent, ProjectInvoiceComponent, ProjectInvoiceFormComponent],
  imports: [CommonModule, FinancialTrackingRoutingModule, ImportsModule]
})
export class FinancialTrackingModule { }
