import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Project>();
  private _displayedColumns: string[] = ['name', 'callName', 'actions'];
  private _paginator: MatPaginator;
  private _projectList: Project[] = [];
  private _search: string;
  private _show: Boolean = true;
  private _sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this._paginator = paginator;
    if (this._paginator) {
      this._dataSource.paginator = this._paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this._sort = sort;
    if (this._sort) {
      this._dataSource.sort = this._sort;
    }
  };

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'callName': return item.call.name;
        default: return item[property];
      }
    }
    this.loadProjectList()
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadProjectList(): void {
    this.projectService.findAllByFinancialSupervisor().subscribe(projectList => {
      this._dataSource.data = projectList;
      this._projectList = projectList;
      this._show = false;
    });
  }

  public get dataSource(): MatTableDataSource<Project> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get projectList(): Project[] {
    return this._projectList;
  }

  public get showSpiner(): Boolean {
    return this._show;
  }

  public set setSearch(search: string) {
    this._search = search;
  }
}
