export enum ActivityResourceApprovalEnum {
  Approved = "APPROVED",
  Rejected = "REJECTED"
}
