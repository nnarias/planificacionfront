import { ActivityResourceApprovalEnum } from "./activityResourceApprovalEnum";

export class ActivityResourceApprovalDTO {
  id: number;
  comment: string;
  activityResourceApprovalEnum: ActivityResourceApprovalEnum;
}
