export enum GoalActivityApprovalEnum {
  Approved = "APPROVED",
  Rejected = "REJECTED"
}
