import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { ProjectDeliverableComponent } from './project-deliverable/project-deliverable.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { TechnicalTrackingRoutingModule } from './technical-tracking-routing.module';
import { ProjectComponent } from './project/project.component';
import { ProjectDeliverableFormComponent } from './project-deliverable-form/project-deliverable-form.component';

@NgModule({
  declarations: [MenuComponent, ProjectComponent, ProjectDeliverableComponent, ProjectDeliverableFormComponent],
  imports: [CommonModule, TechnicalTrackingRoutingModule, ImportsModule]
})
export class TechnicalTrackingModule { }
