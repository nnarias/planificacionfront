import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { GoalActivity } from 'src/app/Pages/registry/project/model/goalActivity';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import { GoalActivityService } from '../../deliverable-invoice/service/goal-activity.service';

@Component({
  selector: 'app-project-deliverable',
  templateUrl: './project-deliverable.component.html',
  styleUrls: ['./project-deliverable.component.scss']
})
export class ProjectDeliverableComponent implements OnInit {

  private _project: Project = new Project();
  private _dataSource = new MatTableDataSource<GoalActivity>();
  private _displayedColumns: string[] = ['name', 'actions'];
  private _paginator: MatPaginator;
  private _goalActivityList: GoalActivity[] = [];
  private _search: string;
  private _show: Boolean = true;
  private _sort: MatSort;
  private _id: number;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this._paginator = paginator;
    if (this._paginator) {
      this._dataSource.paginator = this._paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this._sort = sort;
    if (this._sort) {
      this._dataSource.sort = this._sort;
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private goalActivityService: GoalActivityService, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this._id = params['id'];
      if (this.id) {
        this.loadGoalActivityList(this._id);
        this.loadProject(this._id);
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadGoalActivityList(id: number): void {
    this.goalActivityService.findAllTracking(id).subscribe(goalActivityList => {
      this._dataSource.data = goalActivityList;
      this._goalActivityList = goalActivityList;
      this._show = false;
    });
  }

  loadProject(id: number): void {
    this.projectService.findByTechnicalSupervisor(id).subscribe((project) => {
      this._project = project;
    });
  }

  public get dataSource(): MatTableDataSource<GoalActivity> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get goalActivityList(): GoalActivity[] {
    return this._goalActivityList;
  }

  public get showSpiner(): Boolean {
    return this._show;
  }

  public set setSearch(search: string) {
    this._search = search;
  }

  public get project(): Project {
    return this._project;
  }

  public get id(): Number {
    return this._id;
  }
}
