import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../security/guard/auth.guard';
import { RoleGuard } from '../../security/guard/role.guard';
import { MenuComponent } from './menu/menu.component';
import { ProjectDeliverableFormComponent } from './project-deliverable-form/project-deliverable-form.component';
import { ProjectDeliverableComponent } from './project-deliverable/project-deliverable.component';
import { ProjectComponent } from './project/project.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_TECHNICAL'] } },
{ path: 'project', component: ProjectComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_TECHNICAL'] } },
{ path: 'projectDeliverable/:id', component: ProjectDeliverableComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_TECHNICAL'] } },
{ path: 'projectDeliverable/form/:id/:id2', component: ProjectDeliverableFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_TECHNICAL'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TechnicalTrackingRoutingModule { }
