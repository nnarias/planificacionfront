import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GoalActivity } from 'src/app/Pages/registry/project/model/goalActivity';
import { GoalActivityApprovalEnum } from '../model/goalActivityApprovalEnum';
import { GoalActivityService } from '../../deliverable-invoice/service/goal-activity.service';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import swal from 'sweetalert2';

interface GoalActivityApprovalStatus {
  value: GoalActivityApprovalEnum;
  viewValue: string;
}

@Component({
  selector: 'app-project-deliverable-form',
  templateUrl: './project-deliverable-form.component.html',
  styleUrls: ['./project-deliverable-form.component.scss']
})
export class ProjectDeliverableFormComponent implements OnInit {
  private _form: FormGroup = this.formBuilder.group({
    goalActivityApprovalEnum: ['', Validators.required],
    comment: ['', Validators.required],
  });
  private _goalActivity: GoalActivity = new GoalActivity();
  private _goalActivityApprovalStatus: GoalActivityApprovalStatus[] = [
    { value: GoalActivityApprovalEnum.Approved, viewValue: "Aprobar entregable" },
    { value: GoalActivityApprovalEnum.Rejected, viewValue: "Rechazar entregable" },
  ];
  private goalActivityId: number;
  private idProject: number;
  private _project: Project = new Project();
  private _showSpiner: Boolean = true;
  private _urlEndPoint: string = `${environment.apiUrl}/goalActivity/deliverableFile`

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private goalActivityService: GoalActivityService, private router: Router, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.idProject = params['id'];
      this.goalActivityId = params['id2'];
      this.loadProject(this.idProject);
      this.loadProjectDeliverable(this.goalActivityId);
    })
  }

  loadProjectDeliverable(id: number) {
    this.goalActivityService.findTracking(id).subscribe((goalActivity) => {
      this._goalActivity = goalActivity;
    })
  }

  loadProject(id: number): void {
    this.projectService.findByTechnicalSupervisor(id).subscribe((project) => {
      this._project = project;
      this._showSpiner = false;
    });
  }

  public deliverableApproval(): void {
    let goalActivityApproval = this.form.value;
    goalActivityApproval.id = this.goalActivityId;
    this.goalActivityService.deliverableApproval(goalActivityApproval).subscribe(() => {
      this.router.navigate(['app/tracking/technicalTracking']);
      swal.fire('Operación Completa', `Evaluación guardada con éxito`, 'success');
    });
  }

  public get comment() { return this._form.get('comment'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get goalActivity(): GoalActivity {
    return this._goalActivity;
  }

  public get goalActivityApprovalEnum() { return this._form.get('goalActivityApprovalEnum'); }

  public get goalActivityApprovalStatus(): GoalActivityApprovalStatus[] {
    return this._goalActivityApprovalStatus;
  }

  public get project(): Project {
    return this._project;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get urlEndpoint(): string {
    return this._urlEndPoint;
  }
}