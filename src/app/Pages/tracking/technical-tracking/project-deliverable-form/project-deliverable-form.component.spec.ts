import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDeliverableFormComponent } from './project-deliverable-form.component';

describe('ProjectDeliverableFormComponent', () => {
  let component: ProjectDeliverableFormComponent;
  let fixture: ComponentFixture<ProjectDeliverableFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectDeliverableFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDeliverableFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
