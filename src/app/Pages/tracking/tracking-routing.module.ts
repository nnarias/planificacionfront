import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: 'deliverableInvoice', loadChildren: () => import('./deliverable-invoice/deliverable-invoice.module').then(m => m.DeliverableInvoiceModule) },
{ path: 'technicalTracking', loadChildren: () => import('./technical-tracking/technical-tracking.module').then(m => m.TechnicalTrackingModule) },
{ path: 'financialTracking', loadChildren: () => import('./financial-tracking/financial-tracking.module').then(m => m.FinancialTrackingModule) }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrackingRoutingModule { }
