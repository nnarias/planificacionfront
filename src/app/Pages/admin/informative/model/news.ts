export class News {
  body: string;
  endDate: string;
  fileName: string;
  fileUUID: string;
  id: number;
  startDate: string;
  subtitle: string;
  title: string;
  update: boolean;
  constructor() {
    this.update = false;
  }
}
