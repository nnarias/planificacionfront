export class Notification {
  date: string;
  description: string;
  hour: string;
  id: number;
  recentState: boolean;
  title: string;
  user: string;
}
