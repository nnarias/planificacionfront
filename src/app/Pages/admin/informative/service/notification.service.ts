import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Notification } from '../model/notification';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private urlEndPoint: string = `${environment.apiUrl}`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Notification> {
    return this.http.delete<Notification>(`${this.urlEndPoint}/notification/${id}`);
  }

  find(id: number): Observable<Notification> {
    return this.http.get<Notification>(`${this.urlEndPoint}/notification/${id}`);
  }

  findAll(): Observable<Notification[]> {
    return this.http.get<Notification[]>(`${this.urlEndPoint}/notification`)
  }

  findAllByUser(): Observable<Notification[]> {
    return this.http.get<Notification[]>(`${this.urlEndPoint}/notification/user`);
  }

  findByUser(id: number): Observable<Notification> {
    return this.http.get<Notification>(`${this.urlEndPoint}/notification/user/${id}`);
  }

  findByUserRecentNotification(): Observable<Notification[]> {
    return this.http.get<Notification[]>(`${this.urlEndPoint}/recentNotification/user`);
  }

  save(notification: Notification): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/notification`, notification);
  }
}