import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { News } from '../model/news';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private urlEndPoint: string = `${environment.apiUrl}/news`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<News> {
    return this.http.delete<News>(`${this.urlEndPoint}/${id}`);
  }

  find(id: number): Observable<News> {
    return this.http.get<News>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<News[]> {
    return this.http.get<News[]>(this.urlEndPoint);
  }

  save(file: File, news: News): Observable<News> {
    let formData = new FormData();
    formData.append('file', file);
    formData.append('news', new Blob([JSON.stringify(news)], { type: 'application/json' }));
    return this.http.post<News>(this.urlEndPoint, formData);
  }
}