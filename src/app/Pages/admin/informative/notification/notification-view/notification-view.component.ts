import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../service/notification.service';
import { UserService } from 'src/app/Pages/security/user-admin/service/user.service';
import { UserGeneral } from 'src/app/Pages/security/user-admin/model/userGeneral';

@Component({
  selector: 'app-notification-view',
  templateUrl: './notification-view.component.html',
  styleUrls: ['./notification-view.component.scss']
})
export class NotificationViewComponent implements OnInit {
  private _notification: Notification = new Notification();
  private _showSpiner: boolean = true;
  private _userGeneral: UserGeneral = new UserGeneral();

  constructor(private activatedRoute: ActivatedRoute, private notificationService: NotificationService, private userService: UserService) { }

  ngOnInit(): void {
    this.loadNotification();
  }

  loadNotification(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      let notificationDate;
      if (id) {
        this.notificationService.find(id).subscribe(notification => {
          notificationDate = notification.date.split(' ');
          notification.date = notificationDate[0];
          notification.hour = notificationDate[1];
          this._notification = notification;
          this.userService.findUserGeneralByUsername(notification.user).subscribe(userGeneral => {
            this._userGeneral = userGeneral;
            this._showSpiner = false;
          });
        });
      }
    });
  }

  public get notification(): Notification {
    return this._notification;
  }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public get userGeneral(): UserGeneral {
    return this._userGeneral;
  }
}
