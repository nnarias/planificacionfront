import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../service/notification.service';

@Component({
  selector: 'app-user-notification-view',
  templateUrl: './user-notification-view.component.html',
  styleUrls: ['./user-notification-view.component.scss']
})
export class UserNotificationViewComponent implements OnInit {
  private notification: Notification = new Notification();

  constructor(private activatedRoute: ActivatedRoute, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.loadNotification();
  }

  loadNotification(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      let notificationDate;
      if (id) {
        this.notificationService.findByUser(id).subscribe(notification => {
          notificationDate = notification.date.split(' ');
          notification.date = notificationDate[0];
          notification.hour = notificationDate[1];
          this.notification = notification;
        });
      }
    });
  }

  public get getNotification(): Notification {
    return this.notification;
  }
}