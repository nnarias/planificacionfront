import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNotificationViewComponent } from './user-notification-view.component';

describe('UserNotificationViewComponent', () => {
  let component: UserNotificationViewComponent;
  let fixture: ComponentFixture<UserNotificationViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserNotificationViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNotificationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
