import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../service/notification.service';

@Component({
  selector: 'app-user-notification',
  templateUrl: './user-notification.component.html',
  styleUrls: ['./user-notification.component.scss']
})
export class UserNotificationComponent implements OnInit {
  private dataSource = new MatTableDataSource<Notification>();
  private displayedColumns: string[] = ['title', 'recentState', 'date', 'hour', 'actions'];
  private notificationList: Notification[] = [];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.loadNotificationListData();
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadNotificationListData(): void {
    let notificationDate;
    this.notificationService.findAllByUser().subscribe(notificationList => {
      this.notificationList.length = 0;
      for (let notification of notificationList) {
        notificationDate = notification.date.split(' ');
        notification.date = notificationDate[0];
        notification.hour = notificationDate[1];
        this.notificationList.push(notification);
      }
      this.dataSource.data = this.notificationList;
    });
  }

  public get getDataSource(): MatTableDataSource<Notification> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getNotificationList(): Notification[] {
    return this.notificationList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}