import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../service/notification.service';
import swal from 'sweetalert2';
import { UserService } from 'src/app/Pages/security/user-admin/service/user.service';


@Component({
  selector: 'app-notification-form',
  templateUrl: './notification-form.component.html',
  styleUrls: ['./notification-form.component.scss']
})
export class NotificationFormComponent implements OnInit {
  private _form: FormGroup = this.formBuilder.group({
    description: ['', Validators.required],
    id: [''],
    name: [''],
    title: ['', Validators.required],
    user: ['', Validators.required],
  });
  private notification: Notification = new Notification();
  private _showSpiner: boolean = true;
  private _title: string;
  private _username: FormControl = new FormControl('', Validators.required);

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private notificationService: NotificationService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    this.loadNotification();
  }

  loadNotification(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.notificationService.find(id).subscribe(notification => {
          this._form.patchValue({
            description: notification.description,
            id: notification.id,
            title: notification.title,
            user: notification.user
          });
          this.userService.findUserGeneralByUsername(notification.user).subscribe(userGeneral => {
            this._form.patchValue({
              name: userGeneral.name
            });
            this._showSpiner = false;
          });
        });
        this._title = 'Actualizar Notificación';
      } else {
        this._title = 'Agregar Notificación';
        this._showSpiner = false;
      }
    });
  }

  loadUserGeneral(): void {
    this.userService.findUserGeneralByUsername(this._username.value).subscribe(userGeneral => {
      const swallBootstrap = swal.mixin({
        buttonsStyling: false,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success'
        }
      });
      swallBootstrap.fire({
        cancelButtonText: 'No, cancelar',
        confirmButtonText: 'Sí, guardar',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        text: `Seguro que desea agregar el usuario: ${userGeneral.name}`,
        title: '¿Está seguro?'
      }).then((result) => {
        if (result.value) {
          this._form.patchValue({
            name: userGeneral.name,
            user: userGeneral.username
          });
        }
      });
    })
  }

  save(): void {
    this.notification = this._form.value;
    this.notificationService.save(this.notification).subscribe(() => {
      this.router.navigate(['app/admin/informative/notification']);
      swal.fire(this.notification.id ? 'Notificación Actualizada' : 'Nueva Notificación',
        `La Notificación ${this.notification.title}, se ha guardado con éxito`, 'success');
    });
  }

  public get description() { return this._form.get('description'); }

  public get form(): FormGroup {
    return this._form;
  }

  public get name() { return this._form.get('name'); }
  public get notificationTitle() { return this._form.get('title'); }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public get title(): string {
    return this._title;
  }

  public get user() { return this._form.get('user'); }

  public get username(): FormControl {
    return this._username;
  }
}