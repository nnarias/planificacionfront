import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../service/notification.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Notification>();
  private _displayedColumns: string[] = ['title', 'recentState', 'date', 'hour', 'actions'];
  private _notificationList: Notification[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.loadNotificationDataList();
  }

  delete(notification: Notification): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar La Notificación: ${notification.title}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.notificationService.delete(notification.id).subscribe(() => {
          this._notificationList = this._notificationList.filter(result => result !== notification);
          this._dataSource.data = this._notificationList;
          swal.fire('Notificación Eliminada', `${notification.title}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadNotificationDataList(): void {
    let notificationDate;
    this.notificationService.findAll().subscribe(notificationList => {
      this._notificationList.length = 0;
      for (let notification of notificationList) {
        notificationDate = notification.date.split(' ');
        notification.date = notificationDate[0];
        notification.hour = notificationDate[1];
        this._notificationList.push(notification);
      }
      this._dataSource.data = this._notificationList;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<Notification> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get notificationList(): Notification[] {
    return this._notificationList;
  }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}