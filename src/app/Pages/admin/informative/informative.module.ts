import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { InformativeRoutingModule } from './informative-routing.module';
import { MenuComponent } from './menu/menu.component';
import { NewsComponent } from './news/news/news.component';
import { NewsFormComponent } from './news/news-form/news-form.component';
import { NewsViewComponent } from './news/news-view/news-view.component';
import { NgModule } from '@angular/core';
import { NotificationComponent } from './notification/notification/notification.component';
import { NotificationFormComponent } from './notification/notification-form/notification-form.component';
import { NotificationViewComponent } from './notification/notification-view/notification-view.component';
import { UserNotificationComponent } from './notification/user-notification/user-notification.component';
import { UserNotificationViewComponent } from './notification/user-notification-view/user-notification-view.component';

@NgModule({
  declarations: [MenuComponent, NewsComponent, NewsFormComponent, NewsViewComponent, NotificationComponent, NotificationFormComponent, NotificationViewComponent, 
    UserNotificationComponent, UserNotificationViewComponent],
  imports: [CommonModule, ImportsModule, InformativeRoutingModule]
})
export class InformativeModule { }