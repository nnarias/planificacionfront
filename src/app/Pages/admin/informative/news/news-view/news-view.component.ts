import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { News } from '../../model/news';
import { NewsService } from '../../service/news.service';

@Component({
  selector: 'app-news-view',
  templateUrl: './news-view.component.html',
  styleUrls: ['./news-view.component.scss']
})
export class NewsViewComponent implements OnInit {
  private _news: News = new News();
  private _showSpiner: boolean = true;
  private _urlEndPoint: string = `${environment.apiUrl}/news/file`;

  constructor(private activatedRoute: ActivatedRoute, private newsService: NewsService) { }

  ngOnInit(): void {
    this.loadNews();
  }

  loadNews(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.newsService.find(id).subscribe(news => {
          this._news = news;
          this._showSpiner = false;
        });
      }
    });
  }

  public get news(): News {
    return this._news;
  }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public get urlEndpoint(): string {
    return this._urlEndPoint;
  }
}