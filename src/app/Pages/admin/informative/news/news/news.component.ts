import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { News } from '../../model/news';
import { NewsService } from '../../service/news.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  private _dataSource = new MatTableDataSource<News>();
  private _displayedColumns: string[] = ['title', 'subtitle', 'startDate', 'endDate', 'actions'];
  private _newsList: News[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.newsService.findAll().subscribe(newsList => {
      this._dataSource.data = newsList;
      this._newsList = newsList;
      this._showSpiner = false;
    })
  }

  delete(news: News): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar La Noticia: ${news.title}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.newsService.delete(news.id).subscribe(() => {
          this._newsList = this._newsList.filter(result => result !== news);
          this._dataSource.data = this._newsList;
          swal.fire('Noticia Eliminada', `${news.title}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  public get dataSource(): MatTableDataSource<News> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get newsList(): News[] {
    return this._newsList;
  }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}