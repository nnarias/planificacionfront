import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NewsService } from '../../service/news.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.scss']
})
export class NewsFormComponent implements OnInit {
  private file: File;
  private _fileName: string = 'Selecionar Archivo ...';
  private _form: FormGroup = this.formBuilder.group({
    body: ['', Validators.required],
    endDate: ['', Validators.required],
    id: [''],
    fileName: [''],
    startDate: ['', Validators.required],
    subtitle: ['', Validators.required],
    title: ['', Validators.required],
    update: [false]
  });
  private _imgUrl: string | ArrayBuffer = '';
  private _isImageSaved: boolean;
  private _showSpiner: boolean = true;
  private _title: string;
  private _urlEndPoint: string = `${environment.apiUrl}/news/file`;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private newsService: NewsService, private router: Router) { }

  ngOnInit(): void {
    this.loadNews();

  }

  loadNews(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.newsService.find(id).subscribe(news => {
          this._form.patchValue({
            body: news.body,
            endDate: news.endDate,
            id: news.id,
            fileName: news.fileName,
            startDate: news.startDate,
            subtitle: news.subtitle,
            title: news.title,
            update: false
          });
          this._showSpiner = false;
        });
        this._title = 'Actualizar Noticia';
      } else {
        this._title = 'Agregar Noticia';
        this._showSpiner = false;
      }
    });
  }

  removeImage() {
    this._imgUrl = '';
    this.file = null;
    this._isImageSaved = false;
    this._fileName = 'Selecionar Archivo ...';
  }

  save(): void {
    let news = this._form.value;
    this.newsService.save(this.file, news).subscribe(() => {
      this.router.navigate(['app/admin/informative/news']);
      swal.fire(news.id ? 'Noticia actualizada' : 'Nueva Noticia', `${news.title}`, 'success');
    });

  }

  selectImage(event): void {
    if (event.target.files[0]) {
      this.file = event.target.files[0];
      if (this.file.type.indexOf('image') != -1) {
        this._fileName = this.file.name;
        this._isImageSaved = true;
        const reader = new FileReader();
        reader.readAsDataURL(this.file);
        reader.onload = () => {
          this._imgUrl = reader.result;
        }
      }
      else {
        swal.fire('Error', 'El archivo debe ser del tipo imagen', 'error');
        this.removeImage();
      }
    }
  }

  public get body() { return this._form.get('body'); }
  public get endDate() { return this._form.get('endDate'); }

  public get fileName(): string {
    return this._fileName;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get imageUrl(): string | ArrayBuffer {
    return this._imgUrl;
  }

  public get isImageSaved(): boolean {
    return this._isImageSaved;
  }

  public get newsTitle() { return this._form.get('title'); }

  public get showSpiner(): boolean {
    return this._showSpiner;
  }

  public get startDate() { return this._form.get('startDate'); }
  public get subtitle() { return this._form.get('subtitle'); }

  public get title(): string {
    return this._title;
  }

  public get update() { return this._form.get('update'); }

  public get urlEndpoint(): string {
    return this._urlEndPoint;
  }
}
