import { AuthGuard } from '../../security/guard/auth.guard';
import { MenuComponent } from './menu/menu.component';
import { NewsComponent } from './news/news/news.component';
import { NewsFormComponent } from './news/news-form/news-form.component';
import { NewsViewComponent } from './news/news-view/news-view.component';
import { NgModule } from '@angular/core';
import { NotificationComponent } from './notification/notification/notification.component';
import { NotificationFormComponent } from './notification/notification-form/notification-form.component';
import { NotificationViewComponent } from './notification/notification-view/notification-view.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';
import { UserNotificationComponent } from './notification/user-notification/user-notification.component';
import { UserNotificationViewComponent } from './notification/user-notification-view/user-notification-view.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'news', component: NewsComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'news/form', component: NewsFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'news/form/:id', component: NewsFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'newsView/:id', component: NewsViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'notification', component: NotificationComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'notification/form', component: NotificationFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'notification/form/:id', component: NotificationFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'notificationView/:id', component: NotificationViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'userNotification', component: UserNotificationComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER'] } },
{ path: 'userNotificationView/:id', component: UserNotificationViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformativeRoutingModule { }