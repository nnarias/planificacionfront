import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: 'informative', loadChildren: () => import('./informative/informative.module').then(m => m.InformativeModule) },
{ path: 'projectNotification', loadChildren: () => import('./project-notification/project-notification.module').then(m => m.ProjectNotificationModule) }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }