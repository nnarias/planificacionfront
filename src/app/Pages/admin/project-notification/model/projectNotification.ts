import { Project } from "../../../registry/project/model/project";

export class ProjectNotification {
  commentary: string;
  creationDate: string;
  id: number;
  project: Project = new Project();
  title: string;
  user: string;
}