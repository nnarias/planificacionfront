import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ProjectNotification } from 'src/app/Pages/admin/project-notification/model/projectNotification';
import { ProjectNotificationService } from '../service/project-notification.service';

@Component({
  selector: 'app-user-notification',
  templateUrl: './user-notification.component.html',
  styleUrls: ['./user-notification.component.scss']
})
export class UserNotificationComponent implements OnInit {
  private _dataSource = new MatTableDataSource<ProjectNotification>();
  private _displayedColumns: string[] = ['title', 'projectName', 'commentary', 'creationDate'];
  private _paginator: MatPaginator;
  private _projectNotificationList: ProjectNotification[] = [];
  private _search: string;
  private _showSpiner: Boolean = true;
  private _sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this._paginator = paginator;
    if (this._paginator) {
      this._dataSource.paginator = this._paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this._sort = sort;
    if (this._sort) {
      this._dataSource.sort = this._sort;
    }
  };

  constructor(private projectNotificationService: ProjectNotificationService) { }

  ngOnInit(): void {
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'projectName': return item.project.nameSpanish;
        default: return item[property];
      }
    }
    this.loadProjectNotificationList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadProjectNotificationList(): void {
    this.projectNotificationService.findByUser().subscribe(projectNotification => {
      this._dataSource.data = projectNotification;
      this._projectNotificationList = projectNotification;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<ProjectNotification> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get projectNotificationList(): ProjectNotification[] {
    return this._projectNotificationList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}