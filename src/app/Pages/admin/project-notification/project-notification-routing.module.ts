import { AuthGuard } from '../../security/guard/auth.guard';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';
import { UserNotificationComponent } from './user-notification/user-notification.component';

const routes: Routes = [{ path: 'userNotification', component: UserNotificationComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_EVALUER', 'ROLE_DEPARTMENT_EVALUER', 'ROLE_TECHNICAL', 'ROLE_FINANCIAL'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectNotificationRoutingModule { }