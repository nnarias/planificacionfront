import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { NgModule } from '@angular/core';
import { ProjectNotificationRoutingModule } from './project-notification-routing.module';
import { UserNotificationComponent } from './user-notification/user-notification.component';

@NgModule({
  declarations: [UserNotificationComponent],
  imports: [CommonModule, ImportsModule, ProjectNotificationRoutingModule]
})
export class ProjectNotificationModule { }