import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectNotification } from '../model/projectNotification';

@Injectable({
  providedIn: 'root'
})
export class ProjectNotificationService {

  private urlEndPoint: string = `${environment.apiUrl}/projectNotification`;

  constructor(private http: HttpClient) { }

  find(id: number): Observable<ProjectNotification[]> {
    return this.http.get<ProjectNotification[]>(`${this.urlEndPoint}/project/${id}`);
  }

  findByUser(): Observable<ProjectNotification[]> {
    return this.http.get<ProjectNotification[]>(`${this.urlEndPoint}/user`);
  }
}