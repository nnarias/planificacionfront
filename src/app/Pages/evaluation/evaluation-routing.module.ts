import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
{ path: 'evaluation', loadChildren: () => import('./evaluation/evaluation.module').then(m => m.EvaluationModule) },
{ path: 'projectApproval', loadChildren: () => import('./project-approval/project-approval.module').then(m => m.ProjectApprovalModule) },
{ path: 'projectManagement', loadChildren: () => import('./project-management/project-management.module').then(m => m.ProjectManagementModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluationRoutingModule { }