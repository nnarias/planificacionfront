import { AuthGuard } from '../../security/guard/auth.guard';
import { EvaluationComponent } from './evaluation/evaluation/evaluation.component';
import { EvaluationFormComponent } from './evaluation/evaluation-form/evaluation-form.component';
import { EvaluationViewComponent } from '../admin/evaluation/evaluation-view/evaluation-view.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'evaluation', component: EvaluationComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'evaluation/form', component: EvaluationFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'evaluation/form/:id', component: EvaluationFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'evaluation/view/:id', component: EvaluationViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }