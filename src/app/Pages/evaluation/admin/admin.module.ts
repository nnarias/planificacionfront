import { AdminRoutingModule } from './admin-routing.module';
import { CommonModule } from '@angular/common';
import { EvaluationComponent } from './evaluation/evaluation/evaluation.component';
import { EvaluationFormComponent } from './evaluation/evaluation-form/evaluation-form.component';
import { EvaluationViewComponent } from './evaluation/evaluation-view/evaluation-view.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [EvaluationComponent, EvaluationFormComponent, EvaluationViewComponent, MenuComponent],
  imports: [AdminRoutingModule, CommonModule, ImportsModule]
})
export class AdminModule { }