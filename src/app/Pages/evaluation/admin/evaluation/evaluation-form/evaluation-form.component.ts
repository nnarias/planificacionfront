import { ActivatedRoute, Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Criteria } from '../../../model/criteria';
import { CriteriaService } from '../../../service/criteria.service';
import { Evaluation } from '../../../model/evaluation';
import { EvaluationCriteria } from '../../../model/evaluationCriteria';
import { EvaluationCriteriaSubcriteria } from '../../../model/evaluationCriteriaSubcriteria';
import { EvaluationService } from '../../../service/evaluation.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Subcriteria } from '../../../model/subcriteria';
import { SubcriteriaService } from '../../../service/subcriteria.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-evaluation-form',
  templateUrl: './evaluation-form.component.html',
  styleUrls: ['./evaluation-form.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {
      displayDefaultIndicatorType: false
    }
  }],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class EvaluationFormComponent implements OnInit {
  private _criteriaForm: FormGroup = this.formBuilder.group({
    criteriaGrade: [''],
    criteriaId: [''],
    criteriaName: ['', Validators.required]
  });
  private _criteriaList: Criteria[] = [];
  private _displayedColumns: string[] = ['name', 'grade', 'actions'];
  private _evaluation: Evaluation = new Evaluation();
  private _evaluationCriteria: EvaluationCriteria;
  private _evaluationCriteriaDataSource = new MatTableDataSource<EvaluationCriteria>();
  private evaluationCriteriaList: EvaluationCriteria[] = [];
  private _evaluationCriteriaSubcriteria: EvaluationCriteriaSubcriteria;
  private evaluationCriteriaSubcriteriaList: EvaluationCriteriaSubcriteria[] = [];
  private _evaluationForm: FormGroup = this.formBuilder.group({
    evaluationGrade: ['', Validators.required],
    evaluationId: [''],
    evaluationName: ['', Validators.required]
  });
  private _expandedElement: EvaluationCriteriaSubcriteria[] = [];
  private _filteredCriteriaList: Observable<Criteria[]>;
  private _filteredSubcriteriaList: Observable<Subcriteria[]>;
  private loading = {
    criteria: false,
    evaluation: false,
    subcriteria: false
  }
  private seletedCriterias: Criteria[] = [];
  private seletedSubcriterias: Subcriteria[] = [];
  private _showSpiner: Boolean = true;
  private _stepCriteria: boolean = true;
  private _stepSubcriteria: boolean = false;
  private _subcriteriaForm: FormGroup = this.formBuilder.group({
    subcriteriaGrade: ['', Validators.required],
    subcriteriaId: [''],
    subcriteriaName: ['', Validators.required]
  });
  private _subcriteriaList: Subcriteria[] = [];

  constructor(private activatedRoute: ActivatedRoute, private criteriaService: CriteriaService, private evaluationService: EvaluationService, private formBuilder: FormBuilder,
    private router: Router, private subcriteriaService: SubcriteriaService) { }

  ngOnInit(): void {
    this._evaluation.evaluationCriteriaList = [];
    this.loadCriteriaList();
    this.loadEvaluation();
    this.loadSubcriteriaList();
  }

  addCriteria(stepper: MatStepper): void {
    let criteriaName = this._criteriaForm.value.criteriaName;
    let criteriaNameToLowerCase = this._criteriaForm.value.criteriaName.toLowerCase();
    let evaluationCriteria = new EvaluationCriteria();
    evaluationCriteria.criteria = new Criteria();
    evaluationCriteria.evaluationCriteriaSubcriteriaList = [];
    let criteriaList = this._criteriaList.filter(criteria => criteria.name.toLowerCase().indexOf(criteriaNameToLowerCase) === 0);
    if (criteriaList.length) {
      evaluationCriteria.criteria = criteriaList[0];
    } else {
      evaluationCriteria.criteria.name = criteriaName;
    }
    let criteriaExists = this.seletedCriterias.filter(criteria => criteria.name.toLowerCase().indexOf(criteriaNameToLowerCase) === 0);
    if (criteriaExists.length) {
      swal.fire('Error', 'El crierio ya existe', 'error');
    } else {
      this.seletedCriterias.push(evaluationCriteria.criteria);
      this.evaluationCriteriaList.push(evaluationCriteria);
      this._evaluationCriteriaDataSource.data = this.evaluationCriteriaList;
      this._evaluation.evaluationCriteriaList = this.evaluationCriteriaList;
      this._criteriaForm.patchValue({
        criteriaGrade: '',
        criteriaId: '',
        criteriaName: ''
      });
      swal.fire('Criterio añadido con éxito', `${criteriaName}`, 'success');
      stepper.previous();
    }
  }

  addSubcriteria(stepper: MatStepper): void {
    let subcriteriaName = this._subcriteriaForm.value.subcriteriaName;
    let subcriteriaNameLowerCase = this._subcriteriaForm.value.subcriteriaName.toLowerCase();
    let evaluationCriteriaSubcriteria = new EvaluationCriteriaSubcriteria();
    evaluationCriteriaSubcriteria.subcriteria = new Subcriteria();
    let subcriteriaList = this._subcriteriaList.filter(subcriteria => subcriteria.name.toLowerCase().indexOf(subcriteriaNameLowerCase) === 0);
    if (subcriteriaList.length) {
      evaluationCriteriaSubcriteria.subcriteria = subcriteriaList[0];
    } else {
      evaluationCriteriaSubcriteria.subcriteria.name = subcriteriaName;
    }
    let subcriteriaExists = this.seletedSubcriterias.filter(subcriteria => subcriteria.name.toLowerCase().indexOf(subcriteriaNameLowerCase) === 0);
    if (subcriteriaExists.length) {
      swal.fire('Error', 'El subcrierio ya existe', 'error');
    } else {
      evaluationCriteriaSubcriteria.grade = this._subcriteriaForm.value.subcriteriaGrade;
      if (this._evaluation.grade) {
        this._evaluation.grade += evaluationCriteriaSubcriteria.grade;
      } else {
        this._evaluation.grade = evaluationCriteriaSubcriteria.grade;
      }
      this._evaluationForm.patchValue({ evaluationGrade: this._evaluation.grade });
      if (this._evaluationCriteria.grade) {
        this._evaluationCriteria.grade += evaluationCriteriaSubcriteria.grade;
      } else {
        this._evaluationCriteria.grade = evaluationCriteriaSubcriteria.grade;
      }
      this.seletedSubcriterias.push(evaluationCriteriaSubcriteria.subcriteria);
      this.evaluationCriteriaSubcriteriaList.push(evaluationCriteriaSubcriteria);
      this._evaluationCriteria.evaluationCriteriaSubcriteriaList = this.evaluationCriteriaSubcriteriaList;
      this._subcriteriaForm.patchValue({
        subcriteriaGrade: '',
        subcriteriaId: '',
        subcriteriaName: ''
      });
      this._evaluationCriteria = null;
      swal.fire('Subcriterio añadido con éxito', `${subcriteriaName}`, 'success');
      this._stepSubcriteria = false;
      this._stepCriteria = true;
      stepper.steps.first;
    }
  }

  cancelCriteria(stepper: MatStepper): void {
    this._evaluationCriteria = null;
    this._criteriaForm.patchValue({
      criteriaGrade: '',
      criteriaId: '',
      criteriaName: ''
    });
    stepper.previous();
  }

  cancelSubcriteria(stepper: MatStepper): void {
    this._evaluationCriteriaSubcriteria = null;
    this._subcriteriaForm.patchValue({
      subcriteriaGrade: '',
      subcriteriaId: '',
      subcriteriaName: ''
    });
    stepper.previous();
  }

  check(): void {
    let charged = true;
    for (let value in this.loading) {
      if (!this.loading[value]) {
        charged = false;
        return;
      }
    }
    if (charged) {
      this._showSpiner = false;
    }
  }

  deleteCriteria(evaluationCriteria: EvaluationCriteria) {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el criterio: ${evaluationCriteria.criteria.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.evaluationCriteriaList = this.evaluationCriteriaList.filter(result => result !== evaluationCriteria);
        this._evaluationCriteriaDataSource.data = this.evaluationCriteriaList;
        this._evaluation.evaluationCriteriaList = this.evaluationCriteriaList;
        this._evaluation.grade -= evaluationCriteria.grade;
        this._evaluationForm.patchValue({ evaluationGrade: this._evaluation.grade });
        swal.fire('Criterio Eliminado', `${evaluationCriteria.criteria.name}, eliminado con éxito`, 'success');
      }
    });
  }

  deleteSubcriteria(evaluationCriteria: EvaluationCriteria, evaluationCriteriaSubcriteria: EvaluationCriteriaSubcriteria) {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el subcriterio: ${evaluationCriteriaSubcriteria.subcriteria.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        let index = this.evaluationCriteriaList.indexOf(evaluationCriteria);
        evaluationCriteria.evaluationCriteriaSubcriteriaList = evaluationCriteria.evaluationCriteriaSubcriteriaList.filter(result => result !== evaluationCriteriaSubcriteria);
        this._evaluation.grade -= evaluationCriteriaSubcriteria.grade;
        evaluationCriteria.grade -= evaluationCriteriaSubcriteria.grade;
        this.evaluationCriteriaList[index] = evaluationCriteria;
        this._evaluationCriteriaDataSource.data = this.evaluationCriteriaList;
        this._evaluation.evaluationCriteriaList = this.evaluationCriteriaList;
        this._evaluationForm.patchValue({ evaluationGrade: this._evaluation.grade });
        swal.fire('Subriterio Eliminado', `${evaluationCriteriaSubcriteria.subcriteria.name}, eliminado con éxito`, 'success');
      }
    });
  }

  private filterCriteria(criteriaList: Criteria[], criteriaName: string): Criteria[] {
    const filterValue = criteriaName.toLowerCase();
    let criteriasMatchingName = criteriaList.filter(criteria => criteria.name.toLowerCase().indexOf(filterValue) === 0);
    if (criteriasMatchingName.length) {
      return criteriasMatchingName;
    } else {
      return null;
    }
  }

  private filterCriteriaOnValueChange(criteriaName: string): Criteria[] {
    let criteriaLessSelected = this._criteriaList.filter(criteria => this.seletedCriterias.indexOf(criteria) < 0);
    if (criteriaName) {
      return this.filterCriteria(criteriaLessSelected, criteriaName);
    } else {
      return criteriaLessSelected;
    }
  }

  private filterSubcriteria(subcriteriaList: Subcriteria[], subcriteriaName: string): Subcriteria[] {
    const filterValue = subcriteriaName.toLowerCase();
    let subcriteriasMatchingName = subcriteriaList.filter(subcriteria => subcriteria.name.toLowerCase().indexOf(filterValue) === 0);
    if (subcriteriasMatchingName.length) {
      return subcriteriasMatchingName;
    } else {
      return null;
    }
  }

  private filterSubcriteriaOnValueChange(subcriteriaName: string): Subcriteria[] {
    let subcriteriaLessSelected = this._subcriteriaList.filter(subcriteria => this.seletedSubcriterias.indexOf(subcriteria) < 0);
    if (subcriteriaName) {
      return this.filterSubcriteria(subcriteriaLessSelected, subcriteriaName);
    } else {
      return subcriteriaLessSelected;
    }
  }

  loadCriteriaList(): void {
    this.criteriaService.findAll().subscribe(criteriaList => {
      this._criteriaList = criteriaList;
      this._filteredCriteriaList = this.criteriaName.valueChanges.pipe(
        startWith(''),
        map(criteriaName => this.filterCriteriaOnValueChange(criteriaName))
      );
      this.loading.criteria = true;
      this.check();
    });
  }

  loadEvaluation(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.evaluationService.find(id).subscribe(evaluation => {
          this._evaluation = evaluation;
          this.evaluationCriteriaList = evaluation.evaluationCriteriaList;
          this._evaluationCriteriaDataSource.data = evaluation.evaluationCriteriaList;
          this.evaluationForm.patchValue({
            evaluationGrade: evaluation.grade,
            evaluationId: evaluation.id,
            evaluationName: evaluation.name
          });
          this.loading.evaluation = true;
          this.check();
        })
      }
    })
  }

  loadSubcriteriaList(): void {
    this.subcriteriaService.findAll().subscribe(subcriteriaList => {
      this._subcriteriaList = subcriteriaList;
      this._filteredSubcriteriaList = this.subcriteriaName.valueChanges.pipe(
        startWith(''),
        map(subcriteriaName => this.filterSubcriteriaOnValueChange(subcriteriaName))
      );
      this.loading.subcriteria = true;
      this.check();
      this._subcriteriaForm.patchValue({
        subcriteriaGrade: '',
        subcriteriaId: '',
        subcriteriaName: ''
      });
    });
  }

  save() {
    this.evaluationService.save(this.evaluation).subscribe(() => {
      this.router.navigate(['app/evaluation/admin/evaluation']);
      swal.fire(this.evaluation.id ? 'Evaluación Actualizada' : 'Nueva Evaluación', `${this.evaluation.name}`, 'success');
    });
  }

  setCriteria(evaluationCriteria: EvaluationCriteria, stepper: MatStepper): void {
    this._evaluationCriteria = evaluationCriteria;
    this.criteriaName.setValue(evaluationCriteria.criteria.name);
    stepper.next();
  }

  setCriteriaSubcriteria(evaluationCriteria: EvaluationCriteria, stepper: MatStepper): void {
    this._stepCriteria = false;
    this._stepSubcriteria = true;
    this._evaluationCriteria = evaluationCriteria;
    if (evaluationCriteria.evaluationCriteriaSubcriteriaList) {
      this.evaluationCriteriaSubcriteriaList = evaluationCriteria.evaluationCriteriaSubcriteriaList;
    } else {
      this.evaluationCriteriaSubcriteriaList = [];
    }
    stepper.steps.last;
  }

  setSubcriteria(evaluationCriteria: EvaluationCriteria, evaluationCriteriaSubcriteria: EvaluationCriteriaSubcriteria, stepper: MatStepper): void {
    this._evaluationCriteriaSubcriteria = evaluationCriteriaSubcriteria;
    this._stepCriteria = false;
    this._stepSubcriteria = true;
    this._evaluationCriteria = evaluationCriteria;
    this.subcriteriaName.setValue(evaluationCriteriaSubcriteria.subcriteria.name);
    this.subcriteriaGrade.setValue(evaluationCriteriaSubcriteria.grade);
    this.evaluationCriteriaSubcriteriaList = evaluationCriteria.evaluationCriteriaSubcriteriaList;
    stepper.steps.last;
  }

  updateCriteria(stepper: MatStepper): void {
    let criteriaName = this._criteriaForm.value.criteriaName;
    let criteriaNameToLowerCase = this._criteriaForm.value.criteriaName.toLowerCase();
    let evaluationCriteria = new EvaluationCriteria();
    evaluationCriteria.criteria = new Criteria();
    evaluationCriteria.evaluationCriteriaSubcriteriaList = [];
    let criteriaList = this._criteriaList.filter(criteria => criteria.name.toLowerCase().indexOf(criteriaNameToLowerCase) === 0);
    if (criteriaList.length) {
      evaluationCriteria.criteria = criteriaList[0];
    } else {
      evaluationCriteria.criteria.name = criteriaName;
    }
    let criteriaExists = this.seletedCriterias.filter(criteria => criteria.name.toLowerCase().indexOf(criteriaNameToLowerCase) === 0);
    if (criteriaExists.length && criteriaExists[0] != this._evaluationCriteria.criteria) {
      swal.fire('Error', 'El crierio ya existe', 'error');
    } else {
      this.seletedCriterias[this.seletedCriterias.indexOf(this._evaluationCriteria.criteria)] = evaluationCriteria.criteria;
      this.evaluationCriteriaList[this.evaluationCriteriaList.indexOf(this.evaluationCriteria)] = evaluationCriteria;
      this._evaluationCriteriaDataSource.data = this.evaluationCriteriaList;
      this._evaluation.evaluationCriteriaList = this.evaluationCriteriaList;
      this._criteriaForm.patchValue({
        criteriaGrade: '',
        criteriaId: '',
        criteriaName: ''
      });
      swal.fire('Criterio actualizado con éxito', `${criteriaName}`, 'success');
      this._evaluationCriteria = null;
      stepper.previous();
    }
  }

  updateSubcriteria(stepper: MatStepper): void {
    let subcriteriaName = this._subcriteriaForm.value.subcriteriaName;
    let subcriteriaNameLowerCase = this._subcriteriaForm.value.subcriteriaName.toLowerCase();
    let evaluationCriteriaSubcriteria = new EvaluationCriteriaSubcriteria();
    evaluationCriteriaSubcriteria.subcriteria = new Subcriteria();
    let subcriteriaList = this._subcriteriaList.filter(subcriteria => subcriteria.name.toLowerCase().indexOf(subcriteriaNameLowerCase) === 0);
    if (subcriteriaList.length) {
      evaluationCriteriaSubcriteria.subcriteria = subcriteriaList[0];
    } else {
      evaluationCriteriaSubcriteria.subcriteria.name = subcriteriaName;
    }
    let subcriteriaExists = this.seletedSubcriterias.filter(subcriteria => subcriteria.name.toLowerCase().indexOf(subcriteriaNameLowerCase) === 0);
    if (subcriteriaExists.length && subcriteriaExists[0] != this._evaluationCriteriaSubcriteria.subcriteria) {
      swal.fire('Error', 'El subcrierio ya existe', 'error');
    } else {
      evaluationCriteriaSubcriteria.grade = this._subcriteriaForm.value.subcriteriaGrade;
      this._evaluation.grade -= this._evaluationCriteriaSubcriteria.grade;
      this._evaluation.grade += evaluationCriteriaSubcriteria.grade;
      this._evaluationForm.patchValue({ evaluationGrade: this._evaluation.grade });
      this._evaluationCriteria.grade -= this._evaluationCriteriaSubcriteria.grade;
      this._evaluationCriteria.grade += evaluationCriteriaSubcriteria.grade;
      this.seletedSubcriterias[this.seletedSubcriterias.indexOf(this.evaluationCriteriaSubcriteria.subcriteria)] = evaluationCriteriaSubcriteria.subcriteria;
      this.evaluationCriteriaSubcriteriaList[this.evaluationCriteriaSubcriteriaList.indexOf(this._evaluationCriteriaSubcriteria)] = evaluationCriteriaSubcriteria;
      this._evaluationCriteria.evaluationCriteriaSubcriteriaList = this.evaluationCriteriaSubcriteriaList;
      this._subcriteriaForm.patchValue({
        subcriteriaGrade: '',
        subcriteriaId: '',
        subcriteriaName: ''
      });
      this._evaluationCriteria = null;
      swal.fire('Subcriterio añadido con éxito', `${subcriteriaName}`, 'success');
      this._stepSubcriteria = false;
      this._stepCriteria = true;
      stepper.steps.first;
    }
  }

  public get criteriaForm(): FormGroup {
    return this._criteriaForm;
  }

  public get criteriaGrade() { return this._criteriaForm.get('criteriaGrade'); }

  public get criteriaList(): Criteria[] {
    return this._criteriaList;
  }

  public get criteriaName() { return this._criteriaForm.get('criteriaName'); }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get evaluation(): Evaluation {
    return this._evaluation;
  }

  public get evaluationCriteria(): EvaluationCriteria {
    return this._evaluationCriteria;
  }

  public get evaluationCriteriaDataSource(): MatTableDataSource<EvaluationCriteria> {
    return this._evaluationCriteriaDataSource;
  }

  public get evaluationCriteriaSubcriteria(): EvaluationCriteriaSubcriteria {
    return this._evaluationCriteriaSubcriteria;
  }

  public get evaluationForm(): FormGroup {
    return this._evaluationForm;
  }

  public get evaluationName() { return this._evaluationForm.get('evaluationName'); }

  public get expandedElement(): EvaluationCriteriaSubcriteria[] {
    return this._expandedElement;
  }

  public get filteredCriteriaList(): Observable<Criteria[]> {
    return this._filteredCriteriaList;
  }

  public get filteredSubcriteriaList(): Observable<Subcriteria[]> {
    return this._filteredSubcriteriaList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get stepCriteria(): Boolean {
    return this._stepCriteria;
  }

  public get stepSubcriteria(): Boolean {
    return this._stepSubcriteria;
  }

  public get subcriteriaForm(): FormGroup {
    return this._subcriteriaForm;
  }

  public get subcriteriaGrade() { return this._subcriteriaForm.get('subcriteriaGrade'); }
  public get subcriteriaName() { return this._subcriteriaForm.get('subcriteriaName'); }

  public get subcriteriaList(): Subcriteria[] {
    return this._subcriteriaList;
  }

  public set expandedElement(_expandedElement: EvaluationCriteriaSubcriteria[]) {
    this._expandedElement = _expandedElement;
  }
}