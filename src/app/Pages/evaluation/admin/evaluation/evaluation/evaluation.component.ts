import { Component, OnInit, ViewChild } from '@angular/core';
import { Evaluation } from '../../../model/evaluation';
import { EvaluationService } from '../../../service/evaluation.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';

@Component({
  selector: 'app-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss']
})
export class EvaluationComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Evaluation>();
  private _displayedColumns: string[] = ['name', 'grade', 'actions'];
  private _evaluationList: Evaluation[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private evaluationService: EvaluationService) {
  }

  ngOnInit(): void {
    this.loadCallDataList();
  }

  delete(evaluation: Evaluation): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Si, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar La Evaluación: ${evaluation.name}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.evaluationService.delete(evaluation.id).subscribe(() => {
          this._evaluationList = this._evaluationList.filter(result => result !== evaluation);
          this._dataSource.data = this._evaluationList;
          swal.fire('Evaluación Eliminada', `${evaluation.name}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadCallDataList(): void {
    this.evaluationService.findAll().subscribe(evaluationData => {
      this._dataSource.data = evaluationData;
      this._evaluationList = evaluationData;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<Evaluation> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get evaluationList(): Evaluation[] {
    return this._evaluationList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}
