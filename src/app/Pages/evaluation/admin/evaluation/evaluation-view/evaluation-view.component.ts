import { ActivatedRoute } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Evaluation } from '../../../model/evaluation';
import { EvaluationService } from '../../../service/evaluation.service';

@Component({
  selector: 'app-evaluation-view',
  templateUrl: './evaluation-view.component.html',
  styleUrls: ['./evaluation-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class EvaluationViewComponent implements OnInit {
  private _displayedColumns: string[] = ['name', 'grade'];
  private _evaluation = new Evaluation();
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private evaluationService: EvaluationService) {
  }

  ngOnInit(): void {
    this.loadEvaluation();
  }

  loadEvaluation(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadEvaluationData(this.id);
      }
    });
  }

  loadEvaluationData(id: number): void {
    this.evaluationService.find(id).subscribe(evaluation => {
      this._evaluation = evaluation;
      this._showSpiner = false;
    });
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get evaluation() {
    return this._evaluation;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}