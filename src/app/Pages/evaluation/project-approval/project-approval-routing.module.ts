import { AuthGuard } from '../../security/guard/auth.guard';
import { MenuComponent } from '../../evaluation/project-approval/menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectApprovalComponent } from './projectApproval/project-approval/project-approval.component';
import { ProjectApprovalFormComponent } from './projectApproval/project-approval-form/project-approval-form.component';
import { RoleGuard } from '../../security/guard/role.guard';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_DEPARTMENT_EVALUATOR'] } },
{ path: 'projectApproval', component: ProjectApprovalComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_DEPARTMENT_EVALUATOR'] } },
{ path: 'projectApproval/form/:id', component: ProjectApprovalFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_DEPARTMENT_EVALUATOR'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectApprovalRoutingModule { }
