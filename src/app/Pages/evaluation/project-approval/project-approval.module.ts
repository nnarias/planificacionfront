import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectApprovalComponent } from './projectApproval/project-approval/project-approval.component';
import { ProjectApprovalFormComponent } from './projectApproval/project-approval-form/project-approval-form.component';
import { ProjectApprovalRoutingModule } from './project-approval-routing.module';

@NgModule({
  declarations: [MenuComponent, ProjectApprovalComponent, ProjectApprovalFormComponent],
  imports: [CommonModule, ImportsModule, ProjectApprovalRoutingModule]
})
export class ProjectApprovalModule { }
