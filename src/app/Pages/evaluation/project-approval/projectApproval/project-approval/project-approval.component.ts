import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';

@Component({
  selector: 'app-project-approval',
  templateUrl: './project-approval.component.html',
  styleUrls: ['./project-approval.component.scss']
})
export class ProjectApprovalComponent implements OnInit {
  private dataSource = new MatTableDataSource<Project>();
  private displayedColumns: string[] = ['name', 'callName', 'actions'];
  private paginator: MatPaginator;
  private projectList: Project[] = [];
  private search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'callName': return item.call.name;
        default: return item[property];
      }
    }
    this.loadProjectList();
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  loadProjectList(): void {
    this.projectService.findAllByDepartmentalEvaluator().subscribe(projectList => {
      this.dataSource.data = projectList;
      this.projectList = projectList;
      this._showSpiner = false;
    });
  }

  public get getDataSource(): MatTableDataSource<Project> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getProjectList(): Project[] {
    return this.projectList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}
