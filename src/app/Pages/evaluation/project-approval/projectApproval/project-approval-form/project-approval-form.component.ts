import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectApprovalEvaluationDTO } from '../../../model/projectApprovalEvaluationDTO';
import { ProjectApprovalEvaluationEnum } from '../../../model/projectApprovalEvaluationEnum';
import { ProjectApprovalService } from '../../../service/project-approval.service';
import swal from 'sweetalert2';

interface ProjectApprovalEvaluationStatus {
  value: ProjectApprovalEvaluationEnum;
  viewValue: string;
}

@Component({
  selector: 'app-project-approval-form',
  templateUrl: './project-approval-form.component.html',
  styleUrls: ['./project-approval-form.component.scss']
})
export class ProjectApprovalFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    projectApprovalEvaluationEnum: ['', Validators.required],
    comment: ['', Validators.required]
  });
  private projectId: number;
  private projectApprovalEvaluationDTO: ProjectApprovalEvaluationDTO = new ProjectApprovalEvaluationDTO();
  private projectApprovalEvaluationStatus: ProjectApprovalEvaluationStatus[] = [
    { value: ProjectApprovalEvaluationEnum.Approved, viewValue: "Aprobar Proyecto" },
    { value: ProjectApprovalEvaluationEnum.Rejected, viewValue: "Rechazar Proyecto" },
  ];
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private projectApprovalService: ProjectApprovalService, private router: Router) { }

  ngOnInit(): void {
    this.loadProjectApproval();
  }

  save(): void {
    this.projectApprovalEvaluationDTO = this.form.value;
    this.projectApprovalEvaluationDTO.projectId = this.projectId;
    this.projectApprovalService.save(this.projectApprovalEvaluationDTO).subscribe(() => {
      this.router.navigate(['app/evaluation/projectApproval/projectApproval']);
      swal.fire('Evaluación del Proyecto Actualizada', `Estado: ${this.projectApprovalEvaluationDTO.projectApprovalEvaluationEnum}`, 'success');
    })
  }

  loadProjectApproval(): void {
    this.activatedRoute.params.subscribe(params => {
      this.projectId = params['id'];
      this._showSpiner = false;
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getProjectApprovalEvaluationStatus(): ProjectApprovalEvaluationStatus[] {
    return this.projectApprovalEvaluationStatus;
  }

  public get projectApprovalEvaluationEnum() { return this.form.get('projectApprovalEvaluationEnum'); }
  public get comment() { return this.form.get('comment'); }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}