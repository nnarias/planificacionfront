import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectApprovalFormComponent } from './project-approval-form.component';

describe('ProjectApprovalFormComponent', () => {
  let component: ProjectApprovalFormComponent;
  let fixture: ComponentFixture<ProjectApprovalFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectApprovalFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectApprovalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
