import { EvaluationCriteria } from './evaluationCriteria';

export class Evaluation {
    evaluationCriteriaList: EvaluationCriteria[] = [];
    grade: number;
    id: number;
    name: string;
    score: number;
}