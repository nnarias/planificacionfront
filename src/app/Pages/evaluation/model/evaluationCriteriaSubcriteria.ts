import { EvaluationCriteria } from './evaluationCriteria';
import { Subcriteria } from './subcriteria';

export class EvaluationCriteriaSubcriteria {
    evaluationCriteria: EvaluationCriteria;
    grade: number;
    id: number;
    score: number;
    subcriteria: Subcriteria;
}