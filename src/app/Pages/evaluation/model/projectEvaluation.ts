import { EvaluationCriteriaSubcriteria } from "./evaluationCriteriaSubcriteria";
import { Project } from "../../registry/project/model/project";

export class ProjectEvaluation {
    comment: string;
    evaluationCriteriaSubcriteria: EvaluationCriteriaSubcriteria;
    grade: number;
    id: number;
    name: string;
    project: Project;
    score: number;
}