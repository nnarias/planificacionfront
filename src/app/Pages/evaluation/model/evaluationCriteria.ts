import { Criteria } from './criteria';
import { Evaluation } from './evaluation';
import { EvaluationCriteriaSubcriteria } from './evaluationCriteriaSubcriteria';

export class EvaluationCriteria {
    criteria: Criteria;
    evaluation: Evaluation;
    evaluationCriteriaSubcriteriaList: EvaluationCriteriaSubcriteria[] = [];
    grade: number;
    id: number;
    name: string;
    score: number;
}