import { ProjectApprovalEvaluationEnum } from "./projectApprovalEvaluationEnum";

export class ProjectApprovalEvaluationDTO {
  projectId: number;
  comment: string;
  projectApprovalEvaluationEnum: ProjectApprovalEvaluationEnum;
}
