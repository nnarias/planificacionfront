export enum ProjectApprovalEvaluationEnum {
  Approved = "APPROVED",
  Rejected = "REJECTED"
}
