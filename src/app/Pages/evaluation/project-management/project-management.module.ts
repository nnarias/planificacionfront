import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { ProjectManagementRoutingModule } from './project-management-routing.module';

@NgModule({
  declarations: [MenuComponent],
  imports: [CommonModule, ImportsModule, ProjectManagementRoutingModule]
})
export class ProjectManagementModule { }