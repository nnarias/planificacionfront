import { AssignmentComponent } from './assignment/assignment.component';
import { CallComponent } from './call/call.component';
import { CommonModule } from '@angular/common';
import { EvaluatorAssignmentRoutingModule } from './evaluator-assignment-routing.module';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { NgModule } from '@angular/core';
import { ProjectComponent } from './project/project.component';

@NgModule({
  declarations: [AssignmentComponent, CallComponent, ProjectComponent],
  imports: [CommonModule, EvaluatorAssignmentRoutingModule, ImportsModule]
})
export class EvaluatorAssignmentModule { }