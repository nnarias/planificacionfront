import { AssignmentComponent } from './assignment/assignment.component';
import { AuthGuard } from 'src/app/Pages/security/guard/auth.guard';
import { CallComponent } from './call/call.component';
import { NgModule } from '@angular/core';
import { ProjectComponent } from './project/project.component';
import { RoleGuard } from 'src/app/Pages/security/guard/role.guard';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: '', component: CallComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'assignment/:id', component: AssignmentComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } },
{ path: 'project/:id', component: ProjectComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluatorAssignmentRoutingModule { }