import { Call } from 'src/app/Pages/registry/call/model/call';
import { CallService } from 'src/app/Pages/registry/call/service/call.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})
export class CallComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Call>();
  private _displayedColumns: string[] = ['name', 'startDate', 'endDate', 'evaluationDate', 'actions'];
  private _callList: Call[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private callService: CallService) { }

  ngOnInit(): void {
    this.loadCallDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadCallDataList(): void {
    this.callService.findAllByEvaluationActive().subscribe(callList => {
      this._callList = callList;
      this._dataSource.data = callList;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<Call> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get callList(): Call[] {
    return this._callList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}