import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectAssignmentService } from '../../../service/project-assignment.service';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import swal from 'sweetalert2';
import { UserService } from 'src/app/Pages/security/user-admin/service/user.service';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.scss']
})
export class AssignmentComponent implements OnInit {
  private _departmentalEvaluator: FormControl = new FormControl('', Validators.required);
  private _evaluator: FormControl = new FormControl('', Validators.required);
  private _form: FormGroup = this.formBuilder.group({
    evaluator: ['', Validators.required],
    evaluatorName: [''],
    departamentalEvaluator: ['', Validators.required],
    departamentalEvaluatorName: [''],
    project: ['', Validators.required]
  });
  private _project: Project = new Project();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private projectAssignmentService: ProjectAssignmentService,
    private projectService: ProjectService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    this.loadProject();
  }

  loadDepartmentalEvaluator(): void {
    this.userService.findUserGeneralByUsername(this.departamentalEvaluator.value).subscribe(userGeneral => {
      const swallBootstrap = swal.mixin({
        buttonsStyling: false,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success'
        }
      });
      swallBootstrap.fire({
        cancelButtonText: 'No, cancelar',
        confirmButtonText: 'Sí, guardar',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        text: `Seguro que desea agregar el evaluador departamental: ${userGeneral.name}`,
        title: '¿Está seguro?'
      }).then((result) => {
        if (result.value) {
          this._form.patchValue({
            departamentalEvaluator: userGeneral.username,
            departamentalEvaluatorName: userGeneral.name
          });
        }
      });
    });
  }

  loadEvaluator(): void {
    this.userService.findUserGeneralByUsername(this.evaluator.value).subscribe(userGeneral => {
      const swallBootstrap = swal.mixin({
        buttonsStyling: false,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success'
        }
      });
      swallBootstrap.fire({
        cancelButtonText: 'No, cancelar',
        confirmButtonText: 'Sí, guardar',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        text: `Seguro que desea agregar el evaluador: ${userGeneral.name}`,
        title: '¿Está seguro?'
      }).then((result) => {
        if (result.value) {
          this._form.patchValue({
            evaluator: userGeneral.username,
            evaluatorName: userGeneral.name
          });
        }
      });
    });
  }

  loadProject(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.projectService.findByEvaluatorAssigning(id).subscribe(project => {
          this._project = project;
          this._form.patchValue({
            project: project
          });
          this._showSpiner = false;
        });
      }
    });
  }

  save(): void {
    this.projectAssignmentService.evaluatorAssignment(this._form.value).subscribe(() => {
      this.router.navigate(['app/evaluation/evaluatorAssignment/' + this.project.call.id]);
      swal.fire('Asignación Completa', 'Los evaluadores se ha asignado con éxito al proyecto', 'success');
    });
  }

  public get departamentalEvaluator(): FormControl {
    return this._departmentalEvaluator;
  }

  public get evaluator(): FormControl {
    return this._evaluator;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get project(): Project {
    return this._project;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }
}