import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectApprovalEvaluationDTO } from '../model/projectApprovalEvaluationDTO';

@Injectable({
  providedIn: 'root'
})
export class ProjectApprovalService {
  private urlEndPoint: string = `${environment.apiUrl}/projectApproval`;

  constructor(private http: HttpClient) { }

  save(projectApprovalEvaluationDTO: ProjectApprovalEvaluationDTO): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, projectApprovalEvaluationDTO);
  }
}