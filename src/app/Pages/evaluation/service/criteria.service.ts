import { Criteria } from '../model/criteria';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CriteriaService {
  private urlEndPoint: string = `${environment.apiUrl}/criteria`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Criteria> {
    return this.http.patch<Criteria>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<Criteria> {
    return this.http.get<Criteria>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Criteria[]> {
    return this.http.get<Criteria[]>(this.urlEndPoint);
  }

  save(criteria: Criteria): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, criteria);
  }
}
