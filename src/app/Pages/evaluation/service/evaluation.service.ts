import { environment } from 'src/environments/environment';
import { Evaluation } from '../model/evaluation';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../../registry/project/model/project';

@Injectable({
  providedIn: 'root'
})
export class EvaluationService {
  private urlEndPoint: string = `${environment.apiUrl}/evaluation`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Evaluation> {
    return this.http.patch<Evaluation>(`${this.urlEndPoint}/${id}`, null);
  }

  evaluate(project: Project) {
    return this.http.post<any>(`${environment.apiUrl}/evaluate`, project);
  }

  find(id: number): Observable<Evaluation> {
    return this.http.get<Evaluation>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Evaluation[]> {
    return this.http.get<Evaluation[]>(this.urlEndPoint);
  }

  save(evaluation: Evaluation): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, evaluation);
  }
}