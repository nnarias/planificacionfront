import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subcriteria } from '../model/subcriteria';

@Injectable({
  providedIn: 'root'
})
export class SubcriteriaService {
  private urlEndPoint: string = `${environment.apiUrl}/subcriteria`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<Subcriteria> {
    return this.http.patch<Subcriteria>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<Subcriteria> {
    return this.http.get<Subcriteria>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Subcriteria[]> {
    return this.http.get<Subcriteria[]>(this.urlEndPoint);
  }

  save(subcriteria: Subcriteria): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, subcriteria);
  }
}