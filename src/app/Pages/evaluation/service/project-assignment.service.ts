import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectAssignmentService {
  private urlEndPoint: string = `${environment.apiUrl}/evaluatorAssignmet`;

  constructor(private http: HttpClient) { }

  evaluatorAssignment(evaluatorAssignmentDTO): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, evaluatorAssignmentDTO);
  }
}