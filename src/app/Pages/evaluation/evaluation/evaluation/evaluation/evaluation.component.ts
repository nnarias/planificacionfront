import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';

@Component({
  selector: 'app-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss']
})
export class EvaluationComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Project>();
  private _displayedColumns: string[] = ['name', 'callName', 'actions'];
  private paginator: MatPaginator;
  private _projectList: Project[] = [];
  private search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this._dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'callName': return item.call.name;
        default: return item[property];
      }
    }
    this.loadProjectList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this.search.trim().toLowerCase();
  }

  loadProjectList(): void {
    this.projectService.findAllEvaluating().subscribe(projectList => {
      this._dataSource.data = projectList;
      this._projectList = projectList;
      this._showSpiner = false;
    });
  }

  public get dataSource(): MatTableDataSource<Project> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get projectList(): Project[] {
    return this._projectList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}