import { ActivatedRoute, Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/operators';
import { Evaluation } from '../../../model/evaluation';
import { EvaluationCriteriaSubcriteria } from '../../../model/evaluationCriteriaSubcriteria';
import { EvaluationService } from '../../../service/evaluation.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from 'src/app/Pages/registry/project/model/project';
import { ProjectEvaluation } from '../../../model/projectEvaluation';
import { ProjectService } from 'src/app/Pages/registry/project/service/project.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-evaluation-form',
  templateUrl: './evaluation-form.component.html',
  styleUrls: ['./evaluation-form.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class EvaluationFormComponent implements OnInit {
  private _displayedColumns: string[] = ['name', 'grade', 'score'];
  private _displayedEvaluationColumns: string[] = ['name', 'grade', 'score', 'comment'];
  private _evaluation = new Evaluation();
  private _expandedElement: EvaluationCriteriaSubcriteria[] = [];
  private _form: FormGroup = this.formBuilder.group({});
  private project = new Project();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, public evaluationService: EvaluationService, private formBuilder: FormBuilder, private projectService: ProjectService,
    private router: Router) { }

  ngOnInit(): void {
    this.loadProject();
  }

  getTotalCriteria(): void {
    Object.keys(this._form.controls).forEach(item => {
      this._form.controls[item].valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
        var regex = /(\d+)/g;
        var id = parseInt(item.match(regex).toString());
        if (item.indexOf('grade') !== -1) {
          this._evaluation.score = 0;
          this._evaluation.evaluationCriteriaList.forEach(evaluationCriteria => {
            evaluationCriteria.score = 0;
            evaluationCriteria.evaluationCriteriaSubcriteriaList.forEach(evaluationCriteriaSubcriteria => {
              if (evaluationCriteriaSubcriteria.id == id) {
                evaluationCriteriaSubcriteria.score = value;
                evaluationCriteria.score += value;
                this.project.projectEvaluationList.forEach(projectEvaluation => {
                  if (projectEvaluation.evaluationCriteriaSubcriteria == evaluationCriteriaSubcriteria) {
                    projectEvaluation.grade = value;
                  }
                });
              } else {
                evaluationCriteria.score += evaluationCriteriaSubcriteria.score;
              }
            });
            this.evaluation.score += evaluationCriteria.score;
          });
        } else {
          this._evaluation.evaluationCriteriaList.forEach(evaluationCriteria => {
            evaluationCriteria.evaluationCriteriaSubcriteriaList.forEach(evaluationCriteriaSubcriteria => {
              if (evaluationCriteriaSubcriteria.id == id) {
                this.project.projectEvaluationList.forEach(projectEvaluation => {
                  if (projectEvaluation.evaluationCriteriaSubcriteria == evaluationCriteriaSubcriteria) {
                    projectEvaluation.comment = value;
                  }
                });
              }
            });
          });
        }
      });
    });
  }

  loadProject(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.projectService.findByProjectEvaluator(id).subscribe(project => {
          this._evaluation = project.call.evaluation;
          this._evaluation.score = 0;
          this.project = project;
          this.project.callId = project.call.id;
          this.project.projectEvaluationList = [];
          this._evaluation.evaluationCriteriaList.forEach(evaluationCriteria => {
            evaluationCriteria.score = 0;
            evaluationCriteria.evaluationCriteriaSubcriteriaList.forEach(evaluationCriteriaSubcriteria => {
              let projectEvaluation: ProjectEvaluation = new ProjectEvaluation();
              projectEvaluation.evaluationCriteriaSubcriteria = new EvaluationCriteriaSubcriteria();
              evaluationCriteriaSubcriteria.score = 0;
              projectEvaluation.evaluationCriteriaSubcriteria = evaluationCriteriaSubcriteria;
              this._form.addControl("grade" + evaluationCriteriaSubcriteria.id, new FormControl('',
                Validators.compose([Validators.min(1), Validators.max(evaluationCriteriaSubcriteria.grade), Validators.required])));
              this._form.addControl("comment" + evaluationCriteriaSubcriteria.id, new FormControl(''));
              this.project.projectEvaluationList.push(projectEvaluation);
            });
          });
          this._showSpiner = false;
          this.getTotalCriteria();
        });
      }
    });
  }

  save(): void {
    this.project.evaluation = this._evaluation.score;
    this.evaluationService.evaluate(this.project).subscribe(() => {
      this.router.navigate(['app/evaluation/evaluation/evaluation']);
      swal.fire('Evaluación del Proyecto efectuada', `Puntuación final del proyecto: ${this.project.evaluation}`, 'success');
    });
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get displayedEvaluationColumns(): string[] {
    return this._displayedEvaluationColumns;
  }

  public get evaluation(): Evaluation {
    return this._evaluation;
  }

  public get expandedElement(): EvaluationCriteriaSubcriteria[] {
    return this._expandedElement;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set expandedElement(_expandedElement: EvaluationCriteriaSubcriteria[]) {
    this._expandedElement = _expandedElement;
  }
}