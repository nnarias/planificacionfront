import { CommonModule } from '@angular/common';
import { EvaluationComponent } from './evaluation/evaluation/evaluation.component';
import { EvaluationFormComponent } from './evaluation/evaluation-form/evaluation-form.component';
import { EvaluationRoutingModule } from './evaluation-routing.module';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [EvaluationComponent, EvaluationFormComponent, MenuComponent],
  imports: [CommonModule, ImportsModule, EvaluationRoutingModule]
})
export class EvaluationModule { }