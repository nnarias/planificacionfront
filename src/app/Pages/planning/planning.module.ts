import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningRoutingModule } from './planning-routing.module';


@NgModule({
  declarations: [
  ],
  imports: [PlanningRoutingModule, CommonModule]
})
export class PlanningModule { }
