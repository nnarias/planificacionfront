import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../security/guard/auth.guard';
import { RoleGuard } from '../../security/guard/role.guard';
import { MenuComponent } from './menu/menu.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ActivitiesViewComponent } from './activities/activities-view/activities-view.component';
import { ActivitiesFormComponent } from './activities/activities-form/activities-form.component';
import { DegreesComponent } from './degrees/degrees.component';
import { DegreesFormComponent } from './degrees/degrees-form/degrees-form.component';
import { DegreesViewComponent } from './degrees/degrees-view/degrees-view.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'activities', component: ActivitiesComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'activities/form', component: ActivitiesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'activities/form/:id', component: ActivitiesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'activitiesView/:id', component: ActivitiesViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'degrees', component: DegreesComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'degrees/form', component: DegreesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'degrees/form/:id', component: DegreesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'degreesView/:id', component: DegreesViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
