import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Degrees } from '../../../model/degrees'; 
import { DegreesService } from '../../../service/degrees.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-degrees-form',
  templateUrl: './degrees-form.component.html',
  styleUrls: ['./degrees-form.component.scss']
})
export class DegreesFormComponent implements OnInit {

  private _degrees: Degrees = new Degrees();

  private _form: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    id: ['']
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private degreesService: DegreesService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadDegrees();
  }

  loadDegrees(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadDegreesData();
        this._title = 'Modificar Grado Acádemico';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Grado Acádemico';
        this._showSpiner = false;
      }
    });
  }

  loadDegreesData(): void {
    this.degreesService.findById(this.id).subscribe(degrees => {
      this._form.patchValue({
        name: degrees.name,
        id: degrees.id,
      });
    });
  }

  save(): void {
    this._degrees = this.form.value;
    this.degreesService.save(this._degrees).subscribe(() => {
      this.router.navigate(['app/planning/administration/degrees']);
      swal.fire(this._degrees.id ? 'Grado Acádemico Actualizado' : 'Nuevo Grado Acádemico', `${this.degrees.id}`, 'success');
    });
  }

  public get degrees(): Degrees {
    return this._degrees;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }


  public get name() { return this._form.get('name'); }
  
}
