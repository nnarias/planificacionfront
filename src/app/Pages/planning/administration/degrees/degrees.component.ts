import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Degrees } from '../../model/degrees'; 
import { DegreesService } from '../../service/degrees.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-degrees',
  templateUrl: './degrees.component.html',
  styleUrls: ['./degrees.component.scss']
})
export class DegreesComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Degrees>();
  private _displayedColumns: string[] = ['id', 'name','actions'];
  private _degreesList: Degrees[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private degreesService: DegreesService) { }

  ngOnInit(): void {
    this.loadDegreesDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadDegreesDataList(): void {
    this.degreesService.findAll().subscribe(degreesList => {
      this._degreesList = degreesList;
      this._dataSource.data = degreesList;
      this._showSpiner = false;
    });
  }

  delete(degrees: Degrees): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Grado Acádemico: ${degrees.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.degreesService.delete(degrees.id).subscribe(() => {
          this._degreesList = this._degreesList.filter(result => result !== degrees);
          this._dataSource.data = this._degreesList;
          swal.fire('Grado Acádemico ', `${degrees.id}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Degrees> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get degreesList(): Degrees[] {
    return this._degreesList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }

}
