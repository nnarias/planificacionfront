import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Degrees } from '../../../model/degrees';
import { DegreesService } from '../../../service/degrees.service'; 


@Component({
  selector: 'app-degrees-view',
  templateUrl: './degrees-view.component.html',
  styleUrls: ['./degrees-view.component.scss']
})
export class DegreesViewComponent implements OnInit {

  private _degrees: Degrees = new Degrees();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private degreesService: DegreesService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.degreesService.findById(id).subscribe(degrees => {
          this._degrees = degrees;
          this._showSpiner = false;
        });
      }
    });
  }

  public get degrees(): Degrees {
    return this._degrees;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }


}
