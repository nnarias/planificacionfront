import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DegreesViewComponent } from './degrees-view.component';

describe('DegreesViewComponent', () => {
  let component: DegreesViewComponent;
  let fixture: ComponentFixture<DegreesViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DegreesViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DegreesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
