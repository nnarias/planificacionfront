import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Activities } from '../../model/activities'; 
import { ActivitiesService } from '../../service/activities.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Activities>();
  private _displayedColumns: string[] = ['id', 'name', 'duration', 'maxDuration','actions'];
  private _activitiesList: Activities[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private activitiesService: ActivitiesService) { }

  ngOnInit(): void {
    this.loadActivitiesDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadActivitiesDataList(): void {
    this.activitiesService.findAll().subscribe(activitiesList => {
      this._activitiesList = activitiesList;
      this._dataSource.data = activitiesList;
      this._showSpiner = false;
    });
  }

  delete(activities: Activities): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Actividad: ${activities.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.activitiesService.delete(activities.id).subscribe(() => {
          this._activitiesList = this._activitiesList.filter(result => result !== activities);
          this._dataSource.data = this._activitiesList;
          swal.fire('Actividad ', `${activities.id}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Activities> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get activitiesList(): Activities[] {
    return this._activitiesList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }

}
