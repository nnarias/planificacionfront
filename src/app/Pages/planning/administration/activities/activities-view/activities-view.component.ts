import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Activities } from '../../../model/activities';
import { ActivitiesService } from '../../../service/activities.service'; 


@Component({
  selector: 'app-activities-view',
  templateUrl: './activities-view.component.html',
  styleUrls: ['./activities-view.component.scss']
})
export class ActivitiesViewComponent implements OnInit {

  private _activities: Activities = new Activities();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private activitiesService: ActivitiesService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.activitiesService.findById(id).subscribe(activities => {
          this._activities = activities;
          this._showSpiner = false;
        });
      }
    });
  }

  public get activities(): Activities {
    return this._activities;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }


}
