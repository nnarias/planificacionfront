import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Activities } from '../../../model/activities'; 
import { ActivitiesService } from '../../../service/activities.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-activities-form',
  templateUrl: './activities-form.component.html',
  styleUrls: ['./activities-form.component.scss']
})
export class ActivitiesFormComponent implements OnInit {

  private _activities: Activities = new Activities();

  private _form: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    duration: ['', Validators.required],
    maxDuration: ['', Validators.required],
    id: ['']
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private activitiesService: ActivitiesService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadActivities();
  }

  loadActivities(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadActivitiesData();
        this._title = 'Modificar Actividad';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Actividad';
        this._showSpiner = false;
      }
    });
  }

  loadActivitiesData(): void {
    this.activitiesService.findById(this.id).subscribe(activities => {
      this._form.patchValue({
        name: activities.name,
        duration: activities.duration,
        maxDuration: activities.maxDuration,
        id: activities.id,
      });
    });
  }

  save(): void {
    this._activities = this.form.value;
    this.activitiesService.save(this._activities).subscribe(() => {
      this.router.navigate(['app/planning/administration/activities']);
      swal.fire(this._activities.id ? 'Actividad Actualizada' : 'Nueva Actividad', `${this.activities.id}`, 'success');
    });
  }

  public get activities(): Activities {
    return this._activities;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }


  public get name() { return this._form.get('name'); }

  public get duration() { return this._form.get('duration'); }

  public get maxDuration() { return this._form.get('maxDuration'); }

}
