import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { AdministrationRoutingModule } from './administration-routing.module';
import { MenuComponent } from './menu/menu.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ActivitiesViewComponent } from './activities/activities-view/activities-view.component';
import { ActivitiesFormComponent } from './activities/activities-form/activities-form.component';
import { DegreesComponent } from './degrees/degrees.component';
import { DegreesFormComponent } from './degrees/degrees-form/degrees-form.component';
import { DegreesViewComponent } from './degrees/degrees-view/degrees-view.component';


@NgModule({
  declarations: [
    MenuComponent,
    ActivitiesComponent,
    ActivitiesViewComponent,
    ActivitiesFormComponent,
    DegreesComponent,
    DegreesFormComponent,
    DegreesViewComponent
  ],
  imports: [
    CommonModule,
    ImportsModule,
    AdministrationRoutingModule
  ]
})
export class AdministrationModule { }
