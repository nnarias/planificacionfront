export class Activities {
    id: number;
    name: string;
    degree: string;
    duration: number;
    maxDuration: number;
    complete: boolean;
    aprobation: string;
}