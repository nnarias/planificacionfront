import { Planning } from "./planning";
export class Books {
    id: number;
    planning: Planning;
    codIES: string;
    publicationType: string;
    codPUB: string;
    bookTitle: string;
    codISB: string;
    publicationDate: string;
    detailField: string;
    peerReviewed: string;
    filiation: boolean;
    competitor: string;
    duration: number;
    aprobation: string;
    observations: string;
  }