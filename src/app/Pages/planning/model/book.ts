export class Book {
  id: number;
  codies: number;
  publicationType: string;
  publicationCode: string;
  publicationDate: string;
}
