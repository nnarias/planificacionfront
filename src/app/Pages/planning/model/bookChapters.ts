import { Planning } from "./planning";
export class BookChapters {
    id: number;
    planning: Planning;
    codIES: string;
    publicationType: string;
    chapterCode: string;
    codPUB: string;
    capTitle: string;
    bookTitle: string;
    codISB: string;
    editor: string;
    numberPages: number;
    publicationDate: string;
    detailField: string;
    filiation: boolean;
    competitor: string;
    duration: number;
    aprobation: string;
    observations: string;
  }