import { Planning } from "./planning";
export class Publications {
    id: number;
    planning: Planning;
    codIES: string;
    type: string;
    articleType: string;
    codPUB: string;
    title: string;
    indexBase: string;
    codeISS: string;
    magazineName: string;
    quartile: string;
    magazineNumber: string;
    sjr: string;
    publicationDate: string;
    detailField: string;
    condition: string;
    publicationLink: string;
    magazineLink: string;
    filiation: boolean;
    competitor: string;
    observation: string;
    duration: number;
    aprobation: string;
    observations: string;
  }