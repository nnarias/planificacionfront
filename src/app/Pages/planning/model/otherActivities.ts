import { Planning } from "./planning";
export class OtherActivities {
    id: number;
    planning: Planning;
    name: string;
    duration: number;
    link: string;
    aprobation: string;
    observations: string;
  }