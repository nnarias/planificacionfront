export class Planning{
    id: number;
    users: null;
    maxDuration: number;
    aprobation: boolean;
    responsibleId: string
    registrationDate: string;
    complete: boolean;
    removed: boolean;
    observations: string;
    evaluatorId: string;
    degree: string;
  }