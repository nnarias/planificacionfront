import { Planning } from "./planning";
export class Congress {
    id: number;
    planning: Planning;
    codIES: string;
    type: string;
    articleType: string;
    codPUB: string;
    presentationName: string;
    eventName: string;
    eventEdition: number;
    eventOrganizer: string;
    organizingCommite: string;
    country: string;
    city: string;
    publicationDate: string;
    detailField: string;
    competitor: string;
    duration: number;
    aprobation: string;
    observations: string;
  }