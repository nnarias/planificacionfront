import { Component, OnInit } from '@angular/core';
import { Planning } from '../../model/planning';
import { PlanningService } from '../../service/planning.service';
import { Activities } from '../../model/activities'; 

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  private _planningList: Planning[] = [];
  _listActivities: Activities[] = [];
  _numberPublications: number=0;
  _numberBook: number=0;
  _numberChapterBook: number=0;
  _numberCongress: number=0;
  _numberOtherActivities: number=0;

  constructor(private planningService: PlanningService) { }

  ngOnInit(): void {
    this.loadPlanningDataList();
  }

  loadPlanningDataList(): void {
    this.planningService.findAll().subscribe(planningList => {
      this._planningList = planningList;
      for(let i=0;i<this._planningList.length;i++){
        this.loadActivities(this._planningList[i].id);
      }
    });
  }

  loadActivities(id: number): void{
    this.planningService.findAllActivities(id).subscribe(activities => {
      this._listActivities =[];
      this._listActivities = activities;
      for(let i=0;i<this._listActivities.length;i++){
        switch(this._listActivities[i].name){
          case 'Publicaciones científicas':
              this._numberPublications++;
              break;
          case 'Congresos':
              this._numberCongress++;
              break;
          case 'Capítulo de Libro':
              this._numberChapterBook++;
              break;
          case 'Libros':
              this._numberBook++;
              break;
          default:
              this._numberOtherActivities++; 
              break;
        }
      }
    });
  }

}
