import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../../../model/book'; 
import { BookService } from '../../../service/book.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {
  private _book: Book = new Book();

  private _form: FormGroup = this.formBuilder.group({
    codies: [''],
    publicationType: ['', Validators.required],
    publicationCode: ['', Validators.required],
    publicationDate: ['', Validators.required],
    id: ['']
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private bookService: BookService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadBook();
  }

  loadBook(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadBookData();
        this._title = 'Modificar Libro';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Libro';
        this._showSpiner = false;
      }
    });
  }

  loadBookData(): void {
    this.bookService.findById(this.id).subscribe(book => {
      this._form.patchValue({
        codies: book.codies,
        publicationType: book.publicationType,
        publicationCode: book.publicationCode,
        publicationDate: book.publicationDate,
        id: book.id,
      });
    });
  }

  save(): void {
    this._book = this.form.value;
    this.bookService.save(this._book).subscribe(() => {
      this.router.navigate(['app/planning/planning/book']);
      swal.fire(this._book.id ? 'Libro Actualizado' : 'Nuevo Libro', `${this.book.codies}`, 'success');
    });
  }

  public get book(): Book {
    return this._book;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get codies() { return this._form.get('codies'); }

  public get publicationType() { return this._form.get('publicationType'); }

  public get publicationCode() { return this._form.get('publicationCode'); }

  public get publicationDate() { return this._form.get('publicationDate'); }

}
