import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Book } from '../../model/book';
import { BookService } from '../../service/book.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Book>();
  private _displayedColumns: string[] = ['codies', 'publicationType', 'publicationDate', 'actions'];
  private _bookList: Book[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.loadBookDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadBookDataList(): void {
    this.bookService.findAll().subscribe(bookList => {
      this._bookList = bookList;
      this._dataSource.data = bookList;
      this._showSpiner = false;
    });
  }

  delete(book: Book): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el libro: ${book.codies}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.bookService.delete(book.id).subscribe(() => {
          this._bookList = this._bookList.filter(result => result !== book);
          this._dataSource.data = this._bookList;
          swal.fire('Libro Eliminado', `${book.codies}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Book> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get bookList(): Book[] {
    return this._bookList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}
