import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from '../../../model/book';
import { BookService } from '../../../service/book.service'; 

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.scss']
})
export class BookViewComponent implements OnInit {
  private _book: Book = new Book();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private bookService: BookService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.bookService.findById(id).subscribe(book => {
          this._book = book;
          this._showSpiner = false;
        });
      }
    });
  }

  public get book(): Book {
    return this._book;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

}
