import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CongressViewComponent } from './congress-view.component';

describe('CongressViewComponent', () => {
  let component: CongressViewComponent;
  let fixture: ComponentFixture<CongressViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CongressViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CongressViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
