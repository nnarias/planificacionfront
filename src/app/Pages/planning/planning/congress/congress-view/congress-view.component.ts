import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Congress } from '../../../model/congress';
import { CongressService } from '../../../service/congress.service';

@Component({
  selector: 'app-congress-view',
  templateUrl: './congress-view.component.html',
  styleUrls: ['./congress-view.component.scss']
})
export class CongressViewComponent implements OnInit {

  private _congress: Congress = new Congress();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private congressService: CongressService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.congressService.findById(id).subscribe(congress => {
          this._congress = congress;
          this._showSpiner = false;
        });
      }
    });
  }

  public get congress(): Congress {
    return this._congress;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

}
