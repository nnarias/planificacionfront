import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Congress } from '../../../model/congress'; 
import { CongressService } from '../../../service/congress.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-congress-form',
  templateUrl: './congress-form.component.html',
  styleUrls: ['./congress-form.component.scss']
})
export class CongressFormComponent implements OnInit {

  private _congress: Congress = new Congress();

  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    /* planning: [''], */
    codIES: [''],
    type: [''],
    articleType: [''],
    codPUB: [''],
    presentationName: [''],
    eventName: [''],
    eventEdition: [''],
    eventOrganizer: [''],
    organizingCommite: [''],
    country: [''],
    city: [''],
    publicationDate: [''],
    detailField: [''],
    competitor: [''],
    duration: [''],
    aprobation: [''],
    observations: [''],
    
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private congressService: CongressService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadCongress();
  }

  loadCongress(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadCongressData();
        this._title = 'Modificar Congreso';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Congreso';
        this._showSpiner = false;
      }
    });
  }

  loadCongressData(): void {
    this.congressService.findById(this.id).subscribe(congress => {
      this._form.patchValue({
        id: congress.id,
        /* planning: congress.planning.id, */
        codIES: congress.codIES,
        type:congress.type,
        articleType: congress.articleType,
        codPUB: congress.codPUB,
        presentationName: congress.presentationName,
        eventName: congress.eventName,
        eventEdition: congress.eventEdition,
        eventOrganizer: congress.eventOrganizer,
        organizingCommite: congress.organizingCommite,
        country: congress.country,
        city: congress.city,
        publicationDate: congress.publicationDate,
        detailField: congress.detailField,
        competitor: congress.competitor,
        duration: congress.duration,
        aprobation: congress.aprobation,
        observations: congress.observations,
      });
    });
  }

  save(): void {
    this._congress = this.form.value;
    console.log('Congreso: ',this._congress);

    this.congressService.save(this._congress).subscribe(() => {
      this.router.navigate(['app/planning/planning/congress']);
      swal.fire(this._congress.id ? 'Congreso Actualizado' : 'Nuevo Congreso', `${this.congress.id}`, 'success');
    });
  }

  public get congress(): Congress {
    return this._congress;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

 /*  public get planning() { return this._form.get('planning'); } */

  public get codIES() { return this._form.get('codIES'); }

  public get type() { return this._form.get('type'); }

  public get articleType() { return this._form.get('articleType'); }

  public get codPUB() { return this._form.get('codPUB'); }

  public get presentationName() { return this._form.get('presentationName'); }

  public get eventName() { return this._form.get('eventName'); }

  public get eventEdition() { return this._form.get('eventEdition'); }

  public get eventOrganizer() { return this._form.get('eventOrganizer'); }

  public get organizingCommite() { return this._form.get('organizingCommite'); }

  public get country() { return this._form.get('country'); }

  public get city() { return this._form.get('city'); }

  public get publicationDate() { return this._form.get('publicationDate'); }

  public get detailField() { return this._form.get('detailField'); }

  public get competitor() { return this._form.get('competitor'); }

  public get duration() { return this._form.get('duration'); }

  public get aprobation() { return this._form.get('aprobation'); }

  public get observations() { return this._form.get('observations'); }

}
