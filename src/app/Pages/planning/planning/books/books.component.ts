import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Books } from '../../model/books';
import { BooksService } from '../../service/books.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Books>();
  private _displayedColumns: string[] = ['id', 'planning', 'codIES', 'publicationType', 'codPUB', 'bookTitle', 'codISB', 'publicationDate', 'detailField', 'peerReviewed', 'filiation', 'competitor',  'duration', 'complete', 'aprobation', 'observations' ,'actions'];
  private _booksList: Books[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {
    this.loadBooksDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadBooksDataList(): void {
    this.booksService.findAll().subscribe(booksList => {
      this._booksList = booksList;
      this._dataSource.data = booksList;
      this._showSpiner = false;
    });
  }

  delete(books: Books): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el libro: ${books.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.booksService.delete(books.id).subscribe(() => {
          this._booksList = this._booksList.filter(result => result !== books);
          this._dataSource.data = this._booksList;
          swal.fire('Libro Eliminado', `${books.id}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Books> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get booksList(): Books[] {
    return this._booksList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}
