import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Books } from '../../../model/books';
import { BooksService } from '../../../service/books.service';

@Component({
  selector: 'app-books-view',
  templateUrl: './books-view.component.html',
  styleUrls: ['./books-view.component.scss']
})
export class BooksViewComponent implements OnInit {

  private _books: Books = new Books();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private booksService: BooksService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.booksService.findById(id).subscribe(books => {
          this._books = books;
          this._showSpiner = false;
        });
      }
    });
  }

  public get books(): Books {
    return this._books;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

}
