import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../security/guard/auth.guard';
import { RoleGuard } from '../../security/guard/role.guard';
import { BookFormComponent } from './book/book-form/book-form.component';
import { PlanningComponent} from './planning/planning.component';
import { PlanningFormComponent } from './planning/planning-form/planning-form.component';
import { PlanningViewComponent } from './planning/planning-view/planning-view.component';
import { BookViewComponent } from './book/book-view/book-view.component';
import { BookComponent } from './book/book.component';
import { MenuComponent } from './menu/menu.component';
import { BooksComponent } from './books/books.component';
import { BooksFormComponent } from './books/books-form/books-form.component';
import { BooksViewComponent } from './books/books-view/books-view.component'; 
import { CongressComponent } from './congress/congress.component';
import { CongressFormComponent } from './congress/congress-form/congress-form.component';
import { CongressViewComponent } from './congress/congress-view/congress-view.component'; 
import { BookChaptersComponent } from './book-chapters/book-chapters.component';
import { BookChaptersFormComponent } from './book-chapters/book-chapters-form/book-chapters-form.component'; 
import { BookChaptersViewComponent } from './book-chapters/book-chapters-view/book-chapters-view.component'; 
import { PublicationsComponent } from './publications/publications.component'; 
import { PublicationsFormComponent } from './publications/publications-form/publications-form.component'; 
import { PublicationsViewComponent } from './publications/publications-view/publications-view.component'; 
import { OtherActivitiesComponent } from './other-activities/other-activities.component'; 
import { OtherActivitiesFormComponent } from './other-activities/other-activities-form/other-activities-form.component';
import { OtherActivitiesViewComponent } from './other-activities/other-activities-view/other-activities-view.component'; 


const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'books', component: BooksComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'books/form', component: BooksFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'books/form/:id', component: BooksFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'booksView/:id', component: BooksViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'congress', component: CongressComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'congress/form', component: CongressFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'congress/form/:id', component: CongressFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'congressView/:id', component: CongressViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'bookChapters', component: BookChaptersComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'bookChapters/form', component: BookChaptersFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'bookChapters/form/:id', component: BookChaptersFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'bookChaptersView/:id', component: BookChaptersViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'publications', component: PublicationsComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'publications/form', component: PublicationsFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'publications/form/:id', component: PublicationsFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'publicationsView/:id', component: PublicationsViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'otherActivities', component: OtherActivitiesComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'otherActivities/form', component: OtherActivitiesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'otherActivities/form/:id', component: OtherActivitiesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'otherActivitiesView/:id', component: OtherActivitiesViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'planning', component: PlanningComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'planning/form', component: PlanningFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'planning/form/:id', component: PlanningFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'planningView/:id', component: PlanningViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'book', component: BookComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'book/form', component: BookFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'book/form/:id', component: BookFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'bookView/:id', component: BookViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningRoutingModule { }
