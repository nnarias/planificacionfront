import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookChaptersFormComponent } from './book-chapters-form.component';

describe('BookChaptersFormComponent', () => {
  let component: BookChaptersFormComponent;
  let fixture: ComponentFixture<BookChaptersFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookChaptersFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookChaptersFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
