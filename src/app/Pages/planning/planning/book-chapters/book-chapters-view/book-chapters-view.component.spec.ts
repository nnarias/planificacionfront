import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookChaptersViewComponent } from './book-chapters-view.component';

describe('BookChaptersViewComponent', () => {
  let component: BookChaptersViewComponent;
  let fixture: ComponentFixture<BookChaptersViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookChaptersViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookChaptersViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
