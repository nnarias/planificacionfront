import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookChapters } from '../../../model/bookChapters';
import { BookChaptersService } from '../../../service/book-chapters.service';

@Component({
  selector: 'app-book-chapters-view',
  templateUrl: './book-chapters-view.component.html',
  styleUrls: ['./book-chapters-view.component.scss']
})
export class BookChaptersViewComponent implements OnInit {

  private _bookChapters: BookChapters = new BookChapters();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private bookChaptersService: BookChaptersService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.bookChaptersService.findById(id).subscribe(bookChapters => {
          this._bookChapters = bookChapters;
          this._showSpiner = false;
        });
      }
    });
  }

  public get bookChapters(): BookChapters {
    return this._bookChapters;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

}
