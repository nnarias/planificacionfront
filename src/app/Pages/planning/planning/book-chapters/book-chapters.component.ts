
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BookChapters } from '../../model/bookChapters';
import { BookChaptersService } from '../../service/book-chapters.service'; 
import swal from 'sweetalert2';


@Component({
  selector: 'app-book-chapters',
  templateUrl: './book-chapters.component.html',
  styleUrls: ['./book-chapters.component.scss']
})
export class BookChaptersComponent implements OnInit {

  private _dataSource = new MatTableDataSource<BookChapters>();
  private _displayedColumns: string[] = ['id','planning', 'codIES', 'publicationType', 'chapterCode', 'codPUB', 'capTitle', 'bookTitle', 'codISB', 'editor', 'numberPages', 'publicationDate', 'detailField', 'filiation', 'competitor',  'duration', 'complete', 'aprobation', 'observations' , 'actions'];
  private _bookChaptersList: BookChapters[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private bookChaptersService: BookChaptersService) { }

  ngOnInit(): void {
    this.loadBookChaptersDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadBookChaptersDataList(): void {
    this.bookChaptersService.findAll().subscribe(bookChaptersList => {
      this._bookChaptersList = bookChaptersList;
      this._dataSource.data = bookChaptersList;
      this._showSpiner = false;
    });
  }

  delete(bookChapters: BookChapters): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el capítulo de libro: ${bookChapters.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.bookChaptersService.delete(bookChapters.id).subscribe(() => {
          this._bookChaptersList = this._bookChaptersList.filter(result => result !== bookChapters);
          this._dataSource.data = this._bookChaptersList;
          swal.fire('Capítulo de libro Eliminado', `${bookChapters.id}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<BookChapters> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get bookChaptersList(): BookChapters[] {
    return this._bookChaptersList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }

}
