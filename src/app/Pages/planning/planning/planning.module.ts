import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { BookComponent } from './book/book.component';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { PlanningRoutingModule } from './planning-routing.module';
import { BookFormComponent } from './book/book-form/book-form.component';
import { BookViewComponent } from './book/book-view/book-view.component';
import { PlanningComponent } from './planning/planning.component';
import { PlanningFormComponent } from './planning/planning-form/planning-form.component';
import { PlanningViewComponent } from './planning/planning-view/planning-view.component'; 
import { PublicationsComponent } from './publications/publications.component';
import { OtherActivitiesComponent } from './other-activities/other-activities.component';
import { CongressComponent } from './congress/congress.component';
import { BooksComponent } from './books/books.component';
import { BookChaptersComponent } from './book-chapters/book-chapters.component';
import { BooksFormComponent } from './books/books-form/books-form.component';
import { BooksViewComponent } from './books/books-view/books-view.component';
import { BookChaptersFormComponent } from './book-chapters/book-chapters-form/book-chapters-form.component';
import { BookChaptersViewComponent } from './book-chapters/book-chapters-view/book-chapters-view.component';
import { CongressFormComponent } from './congress/congress-form/congress-form.component';
import { CongressViewComponent } from './congress/congress-view/congress-view.component';
import { OtherActivitiesFormComponent } from './other-activities/other-activities-form/other-activities-form.component';
import { OtherActivitiesViewComponent } from './other-activities/other-activities-view/other-activities-view.component';
import { PublicationsViewComponent } from './publications/publications-view/publications-view.component';
import { PublicationsFormComponent } from './publications/publications-form/publications-form.component';

@NgModule({
  declarations: [
    MenuComponent,
    BookComponent,
    BookFormComponent,
    BookViewComponent,
    PlanningComponent,
    PlanningFormComponent,
    PlanningViewComponent,
    PublicationsComponent,
    OtherActivitiesComponent,
    CongressComponent,
    BooksComponent,
    BookChaptersComponent,
    BooksFormComponent, 
    BooksViewComponent, BookChaptersFormComponent, BookChaptersViewComponent, CongressFormComponent, CongressViewComponent, OtherActivitiesFormComponent, OtherActivitiesViewComponent, PublicationsViewComponent, PublicationsFormComponent
  ],
  imports: [CommonModule, ImportsModule, PlanningRoutingModule]
})
export class PlanningModule { }
