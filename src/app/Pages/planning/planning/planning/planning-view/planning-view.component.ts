import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Activities } from '../../../model/activities';
import { Planning } from '../../../model/planning';
import { PlanningService } from '../../../service/planning.service';


@Component({
  selector: 'app-planning-view',
  templateUrl: './planning-view.component.html',
  styleUrls: ['./planning-view.component.scss']
})
export class PlanningViewComponent implements OnInit {

  private _planning: Planning = new Planning();
  private _dataSource = new MatTableDataSource<Activities>();
  private _displayedColumns: string[] = ['name', 'duration'];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;


  _listActivities: Activities[] = [];

  @ViewChild(MatPaginator, { static: false }) set matPaginator(
    paginator: MatPaginator
  ) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  }
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute, 
    private planningService: PlanningService) { }

  

  ngOnInit(): void {
    this.loadCall();
    
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadCall(): any {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.planningService.findById(id).subscribe(planning => {
          this._planning = planning;
          this._showSpiner = false;
          this.loadActivities(id);
        });
      }
    });
  }


  loadActivities(id: number): void{
    this.planningService.findAllActivities(id).subscribe(activities => {
      this._listActivities = activities;
      this._dataSource.data = this._listActivities;
      this._showSpiner = false;
    });
  }

  public get planning(): Planning {
    return this._planning;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get dataSource(): MatTableDataSource<Activities> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get listActivities(): Activities[] {
    return this._listActivities;
  }

}
