import { Component, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Planning } from '../../../model/planning';
import { PlanningService } from '../../../service/planning.service'; 
import { Books } from '../../../model/books';
import { BooksService } from '../../../service/books.service'; 
import { BookChapters } from '../../../model/bookChapters';
import { BookChaptersService } from '../../../service/book-chapters.service'; 
import { Publications } from '../../../model/publications';
import { PublicationsService } from '../../../service/publications.service'; 
import { OtherActivities } from '../../../model/otherActivities';
import { OtherActivitiesService } from '../../../service/other-activities.service'; 
import { Congress } from '../../../model/congress';
import { CongressService } from '../../../service/congress.service';
import { ActivitiesService } from '../../../service/activities.service'; 
import { DegreesService } from '../../../service/degrees.service';
import * as moment from 'moment';
import { Degrees } from '../../../model/degrees';
import { Activities } from '../../../model/activities';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';
import { Duration } from '../../../model/durations';

@Component({
  selector: 'app-planning-form',
  templateUrl: './planning-form.component.html',
  styleUrls: ['./planning-form.component.scss'],
})
export class PlanningFormComponent implements OnInit {
  private _dataSource = new MatTableDataSource<Activities>();
  private _displayedColumns: string[] = ['name', 'duration', 'actions'];
  private paginator: MatPaginator;
  private sort: MatSort;
  private _search: string;
  private _planning: Planning = new Planning();
  private _planningListTemp: Planning[] = [];
  private _pTemp: Planning;
  private _booksTemp: Books;
  private _bookChaptersTemp: BookChapters;
  private _congressTemp: Congress;
  private _publicationsTemp: Publications;
  private _otherActivitiesTemp: OtherActivities;
  private _index: number;
  private today = moment(new Date()).format('DD-MM-YYYY');
  isReadonly: boolean;
  _flagMode: string;
  _listActivities: Activities[] = [];
  _listNewActivities: Activities[] = [];
  _listDeletedActivities: Activities[] = [];
  _flag: string;
  _totalHours: number;
  _sameHours: number;
  _dataDuration: number;
  _dataDegree: string;
  _selectedActivitiy: Activities;
  _selectedDuration: number;
  _selectedDegree: Degrees;
  _academicDs: Degrees[] = [];
  _activities: Activities[] = [];
  _activitiesTemp: Activities[] = [];
  _optionMaster: Duration[] = [{ value: 6 }, { value: 12 }, { value: 24 }];
  _optionPHD: Duration[] = [{ value: 12 }, { value: 24 }, { value: 31 }];

  @ViewChild(MatPaginator, { static: false }) set matPaginator(
    paginator: MatPaginator
  ) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  }
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  }

  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    registrationDate: [this.today],
    maxDuration: [''],
    duration: [''],
    aprobation: ['Pendiente de Aprobación'],
    selectedActivitiy: [''],
    selectedDegree: [''],
    activity: this._listActivities,
    academicDegree: this._academicDs,
    evaluatorId: ['', Validators.required],
    observations: [''],
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private planningService: PlanningService,
    private publicationsService: PublicationsService,
    private otherActivitiesService: OtherActivitiesService,
    private congressService: CongressService,
    private booksService: BooksService,
    private activitiesService: ActivitiesService,
    private degressService: DegreesService,
    private bookChaptersService: BookChaptersService,
    private router: Router
  ) {
    this._selectedDegree = new Degrees();
    this._selectedActivitiy = new Activities();
  }
  private _title: string;

  ngOnInit(): void {
    this.loadPlanningDataList();
    this.loadDegreesDataList();
    this._flag = 'N/A';
    this.loadOtherActivitiesDataList(0);
    this.loadPlanning();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadPlanningDataList(): void {
    this.planningService.findAllIR().subscribe((planningList) => {
      this._planningListTemp = planningList;
    });
  }

  loadDegreesDataList(): void {
    this.degressService.findAll().subscribe((degreesList) => {
      this._academicDs = degreesList;
    });
  }

  loadActivitiesDataList(maxDuration: number): void {
    this.activitiesService
      .findMaxDuration(maxDuration)
      .subscribe((activitiesList) => {
        this._activities = activitiesList;
        for (let i = 0; i < this._activities.length; i++) {
          this._activities[i].id = -1;
          this._activitiesTemp[i].complete = false;
        }
        this._activities = this._activities.concat(this._activitiesTemp);
      });
  }

  loadOtherActivitiesDataList(maxDuration: number): void {
    this.activitiesService
      .findMaxDuration(maxDuration)
      .subscribe((activitiesList) => {
        this._activitiesTemp = activitiesList;
        for (let i = 0; i < this._activitiesTemp.length; i++) {
          this._activitiesTemp[i].id = -1;
          
          this._activitiesTemp[i].complete = false;
        }
      });
  }

  loadPlanning(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.id = params['id'];
      if (this.id) {
        this._title = 'Modificar Planificación';
        this._flagMode = 'Modificar';
        this.isReadonly = true;
        this.loadPlanningData();
        this._showSpiner = false;
      } else {
        this._title = 'Crear Planificación';
        this._flagMode = 'Crear';
        this.isReadonly = false;
        this._showSpiner = false;
      }
    });
  }

  loadPlanningData(): void {
    this.planningService.findById(this.id).subscribe((planning) => {
      this._form.patchValue({
        registrationDate: planning.registrationDate,
        aprobation: planning.aprobation,
        evaluatorId: planning.evaluatorId,
        observations: planning.observations,
        id: planning.id,
      });
      this._dataDegree = planning.degree;
      for (let i = 0; i < this._academicDs.length; i++) {
        if (this._academicDs[i].name === this._dataDegree) {
          this._selectedDegree = this._academicDs[i];
        }
      }
      this.durationOptions();
      this._selectedDuration = planning.maxDuration;
      this.activitiesOptions();
      this.loadActivities(planning.id);
    });
  }

  loadActivities(id: number): void {
    this.planningService.findAllActivities(id).subscribe((activities) => {
      this._listActivities = activities;
      this._dataSource.data = this._listActivities;
      this._showSpiner = false;
    });
  }

  async save() {
    try {
      this._planning = this.form.value;
      this._planning.responsibleId = 'Director Carrera';
      if(this._flagMode==='Modificar'){
        this._planning.observations = ('Se efectua un cambio en la planificación el dia: ' + this.today + ", por efecto esta queda en espera de aprobación");
      }
      else{
        this._planning.registrationDate = moment(new Date()).format('YYYY-MM-DD');
        this._planning.observations= 'Ninguna';
      }
      this._planning.maxDuration = this._selectedDuration;
      this._planning.aprobation = false;
      this._planning.degree = this._selectedDegree.name;
      this.planningService.save(this._planning).subscribe(() => {
        this.router.navigate(['app/planning/planning/planning']);
        swal.fire('Operación Exitosa!!!', 'success');

        //await new Promise((resolve) => setTimeout(resolve, 2000));
        this._planningListTemp.sort((a, b) => a.id - b.id);
        this._pTemp = this._planningListTemp.pop();
        if (this._flagMode === 'Modificar') {
          this._index = this.id;
        } else {
          if (this._pTemp === undefined) {
            this._index = 1;
          } else {
            this._index = this._pTemp.id + 1;
          }
        }
        if (!(this._listActivities === undefined)) {
          for (let i = 0; i < this._listActivities.length; i++) {
            switch (this._listActivities[i].name) {
              case 'Publicaciones científicas':
                if(this._listActivities[i].id === -1){
                  this._publicationsTemp = new Publications();
                  this._publicationsTemp.duration =
                    this._listActivities[i].duration;
                  this._publicationsTemp.planning = this._planning;
                  this._publicationsTemp.planning.id = this._index;
                  this._publicationsTemp.aprobation = 'Pendiente';
                  this.publicationsService
                    .save(this._publicationsTemp)
                    .subscribe(() => {});
                }
                break;
              case 'Congresos':
                if(this._listActivities[i].id === -1){
                  this._congressTemp = new Congress();
                  this._congressTemp.duration = this._listActivities[i].duration;
                  this._congressTemp.planning = this._planning;
                  this._congressTemp.planning.id = this._index;
                  this._congressTemp.aprobation = 'Pendiente';
                  this.congressService
                    .save(this._congressTemp)
                    .subscribe(() => {});
                }
                break;
              case 'Capítulo de Libro':
                if(this._listActivities[i].id === -1){
                  this._bookChaptersTemp = new BookChapters();
                  this._bookChaptersTemp.duration =
                    this._listActivities[i].duration;
                  this._bookChaptersTemp.planning = this._planning;
                  this._bookChaptersTemp.planning.id = this._index;
                  this._bookChaptersTemp.aprobation = 'Pendiente';
                  this.bookChaptersService
                    .save(this._bookChaptersTemp)
                    .subscribe(() => {});
                }
                break;
              case 'Libros':
                if(this._listActivities[i].id === -1){
                  this._booksTemp = new Books();
                  this._booksTemp.duration = this._listActivities[i].duration;
                  this._booksTemp.planning = this._planning;
                  this._booksTemp.planning.id = this._index;
                  this._booksTemp.aprobation = 'Pendiente';
                  this.booksService.save(this._booksTemp).subscribe(() => {});
                }
                break;
              default:
                if(this._listActivities[i].id === -1){
                  this._otherActivitiesTemp = new OtherActivities();
                  this._otherActivitiesTemp.name = this._listActivities[i].name;
                  this._otherActivitiesTemp.duration =
                    this._listActivities[i].duration;
                  this._otherActivitiesTemp.planning = this._planning;
                  this._otherActivitiesTemp.planning.id = this._index;
                  this._otherActivitiesTemp.aprobation = 'Pendiente';
                  this.otherActivitiesService
                    .save(this._otherActivitiesTemp)
                    .subscribe(() => {});
                }
                break;
            }
          }
        }
        //borrado de elemntos de base
        for(let i=0;i<this._listDeletedActivities.length;i++){
          switch (this._listDeletedActivities[i].name){
            case 'Publicaciones científicas':
              this.publicationsService.delete(this._listDeletedActivities[i].id).subscribe(() => {});
              break;
              break;
            case 'Congresos':
              this.congressService.delete(this._listDeletedActivities[i].id).subscribe(() => {});
              break;
            case 'Capítulo de Libro':
              this.bookChaptersService.delete(this._listDeletedActivities[i].id).subscribe(() => {});
              break;
            case 'Libros':
              this.booksService.delete(this._listDeletedActivities[i].id).subscribe(() => {});
              break;
            default:
              this.otherActivitiesService.delete(this._listDeletedActivities[i].id).subscribe(() => {});
              break;
          }
        }
      });
    } catch (error) {
      console.error(error);
    }
  }

  addListActivities(activit: Activities): void {
    this._listActivities.push(activit);
    this._dataSource.data = this._listActivities;
    this._showSpiner = false;
  }

  saveListActivities(activit: Activities): void {
    this._totalHours = 0;
    this._sameHours = 0;
    if (!(activit.name === undefined)) {
      for (let i = 0; i < this._listActivities.length; i++) {
        this._totalHours += this._listActivities[i].duration;
        if (activit.name === this._listActivities[i].name) {
          this._sameHours += this._listActivities[i].duration;
        }
      }
      this._totalHours += activit.duration;
      this._sameHours += activit.duration;
      if (this._totalHours <= this._selectedDuration) {
        if (
          activit.name === 'Publicaciones científicas' ||
          activit.name === 'Congresos' ||
          activit.name === 'Capítulo de Libro' ||
          activit.name === 'Libros' ||
          activit.name === 'Participación proyectos de investigación' ||
          activit.name === 'Dirección de proyectos de investigación'
        ) {
          switch (this._selectedDuration) {
            case 6:
              if (this._sameHours <= 4) {
                this.addListActivities(activit);
              } else {
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'No puede acumular mas de 4 horas de una misma actividad !!!',
                });
              }
              break;
            case 12:
              if (this._sameHours <= 8) {
                this.addListActivities(activit);
              } else {
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'No puede acumular mas de 8 horas de una misma actividad !!!',
                });
              }
              break;
            case 24:
              switch (activit.name) {
                case 'Participación proyectos de investigación':
                  if (this._sameHours <= 8) {
                    this.addListActivities(activit);
                  } else {
                    swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No puede acumular mas de 8 horas para esta actividad !!!',
                    });
                  }
                  break;
                case 'Dirección de proyectos de investigación':
                  if (this._sameHours <= 16) {
                    this.addListActivities(activit);
                  } else {
                    swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No puede acumular mas de 16 horas para esta actividad !!!',
                    });
                  }
                  break;
                default:
                  if (this._sameHours <= 12) {
                    this.addListActivities(activit);
                  } else {
                    swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No puede acumular mas de 12 horas de una misma actividad !!!',
                    });
                  }
                  break;
              }
              break;
            case 31:
              switch (activit.name) {
                case 'Participación proyectos de investigación':
                  if (this._sameHours <= 8) {
                    this.addListActivities(activit);
                  } else {
                    swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No puede acumular mas de 8 horas para esta actividad !!!',
                    });
                  }
                  break;
                case 'Dirección de proyectos de investigación':
                  if (this._sameHours <= 16) {
                    this.addListActivities(activit);
                  } else {
                    swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No puede acumular mas de 16 horas para esta actividad !!!',
                    });
                  }
                  break;
                default:
                  if (this._sameHours <= 20) {
                    this.addListActivities(activit);
                  } else {
                    swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No puede acumular mas de 20 horas de una misma actividad !!!',
                    });
                  }
                  break;
              }
              break;
          }
        } else if (
          !(
            activit.name === 'Estudios doctorales semipresenciales - 12H' ||
            activit.name === 'Estudios doctorales semipresenciales - 13H' ||
            activit.name === 'Estudios doctorales semipresenciales - 14H' ||
            activit.name === 'Estudios doctorales semipresenciales - 15H' ||
            activit.name === 'Estudios doctorales semipresenciales - 16H' ||
            activit.name === 'Dirección de centros de investigación'
          )
        ) {
          if (this._sameHours <= 4) {
            this.addListActivities(activit);
          } else {
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'No puede acumular mas de 4 horas de esta misma actividad !!!',
            });
          }
        } else {
          this.addListActivities(activit);
        }
      } else {
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Ha alcanzado el número de horas máxima para su planificación !!!',
        });
      }
    }
  }

  deletedListActivities(index: number): void {
    if (
      this._flagMode === 'Modificar' &&
      !(this._listActivities[index].id === -1)
    ) {
      this._listDeletedActivities.push(this._listActivities[index]);
    }
    this._listActivities.splice(index, 1);
    this._dataSource.data = this._listActivities;
    this._showSpiner = false;
  }

  cleanListActivities(): void {
    this._listActivities = [];
    this._dataSource.data = this._listActivities;
  }

  durationOptions(): void {
    this.cleanListActivities();
    if (this._selectedDegree.name === 'Master') {
      this._flag = 'Master';
    } else if (this._selectedDegree.name === 'PHD') {
      this._flag = 'PHD';
    }
  }

  activitiesOptions(): void {
    this.cleanListActivities();
    this._selectedActivitiy = new Activities();
    this.loadActivitiesDataList(this._selectedDuration);
  }

  public get planning(): Planning {
    return this._planning;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get dataSource(): MatTableDataSource<Activities> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get listActivities(): Activities[] {
    return this._listActivities;
  }

  public get maxDuration() {
    return this._form.get('maxDuration');
  }

  public get duration() {
    return this._form.get('duration');
  }

  public get selectedActivitiy() {
    return this._form.get('selectedActivitiy');
  }

  public get selectedDegree() {
    return this._form.get('selectedDegree');
  }

  public get activity() {
    return this._form.get('sactivity');
  }

  public get academicDegree() {
    return this._form.get('academicDegree');
  }

  public get evaluatorId() {
    return this._form.get('evaluatorId');
  }

  public get observations() {
    return this._form.get('observations');
  }
}
