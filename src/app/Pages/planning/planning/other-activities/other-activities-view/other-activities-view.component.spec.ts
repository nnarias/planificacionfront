import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherActivitiesViewComponent } from './other-activities-view.component';

describe('OtherActivitiesViewComponent', () => {
  let component: OtherActivitiesViewComponent;
  let fixture: ComponentFixture<OtherActivitiesViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherActivitiesViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherActivitiesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
