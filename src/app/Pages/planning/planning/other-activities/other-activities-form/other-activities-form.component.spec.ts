import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherActivitiesFormComponent } from './other-activities-form.component';

describe('OtherActivitiesFormComponent', () => {
  let component: OtherActivitiesFormComponent;
  let fixture: ComponentFixture<OtherActivitiesFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherActivitiesFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherActivitiesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
