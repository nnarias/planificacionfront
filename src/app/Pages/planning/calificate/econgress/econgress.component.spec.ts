import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcongressComponent } from './econgress.component';

describe('EcongressComponent', () => {
  let component: EcongressComponent;
  let fixture: ComponentFixture<EcongressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcongressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EcongressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
