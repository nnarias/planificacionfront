import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Congress } from '../../model/congress';
import { CongressService } from '../../service/congress.service'; 
import swal from 'sweetalert2';

@Component({
  selector: 'app-econgress',
  templateUrl: './econgress.component.html',
  styleUrls: ['./econgress.component.scss']
})
export class EcongressComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Congress>();
  private _displayedColumns: string[] = ['id', 'planning', 'codIES', 'type', 'articleType', 'codPUB', 'presentationName', 'eventName', 'eventEdition', 'eventOrganizer', 'organizingCommite', 'country', 'city',  'publicationDate', 'detailField', 'competitor',  'duration', 'complete', 'aprobation', 'observations' ,'actions'];
  private _congressList: Congress[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private congressService: CongressService) { }

  ngOnInit(): void {
    this.loadCongressDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadCongressDataList(): void {
    this.congressService.findAll().subscribe(congressList => {
      this._congressList = congressList;
      this._dataSource.data = congressList;
      this._showSpiner = false;
    });
  }

  delete(congress: Congress): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar el Congreso: ${congress.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.congressService.delete(congress.id).subscribe(() => {
          this._congressList = this._congressList.filter(result => result !== congress);
          this._dataSource.data = this._congressList;
          swal.fire('Congreso Eliminado', `${congress.id}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Congress> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get congressList(): Congress[] {
    return this._congressList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }

}
