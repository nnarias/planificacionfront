import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcongressFormComponent } from './econgress-form.component';

describe('EcongressFormComponent', () => {
  let component: EcongressFormComponent;
  let fixture: ComponentFixture<EcongressFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcongressFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EcongressFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
