import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcongressViewComponent } from './econgress-view.component';

describe('EcongressViewComponent', () => {
  let component: EcongressViewComponent;
  let fixture: ComponentFixture<EcongressViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcongressViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EcongressViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
