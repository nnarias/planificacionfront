import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../security/guard/auth.guard';
import { RoleGuard } from '../../security/guard/role.guard';
import { MenuComponent } from './menu/menu.component';
import { EplanningComponent } from './eplanning/eplanning.component';
import { EplanningFormComponent } from './eplanning/eplanning-form/eplanning-form.component';
import { EplanningViewComponent } from './eplanning/eplanning-view/eplanning-view.component';
import { EbookChaptersComponent } from './ebook-chapters/ebook-chapters.component';
import { EbookChaptersFormComponent } from './ebook-chapters/ebook-chapters-form/ebook-chapters-form.component';
import { EbookChaptersViewComponent } from './ebook-chapters/ebook-chapters-view/ebook-chapters-view.component';
import { EbooksComponent } from './ebooks/ebooks.component';
import { EbooksViewComponent } from './ebooks/ebooks-view/ebooks-view.component';
import { EbooksFormComponent } from './ebooks/ebooks-form/ebooks-form.component';
import { EcongressComponent } from './econgress/econgress.component';
import { EcongressViewComponent } from './econgress/econgress-view/econgress-view.component';
import { EcongressFormComponent } from './econgress/econgress-form/econgress-form.component';
import { EpublicationsComponent } from './epublications/epublications.component';
import { EpublicationsFormComponent } from './epublications/epublications-form/epublications-form.component';
import { EpublicationsViewComponent } from './epublications/epublications-view/epublications-view.component';
import { EotherActivitiesComponent } from './eother-activities/eother-activities.component';
import { EotherActivitiesFormComponent } from './eother-activities/eother-activities-form/eother-activities-form.component';
import { EotherActivitiesViewComponent } from './eother-activities/eother-activities-view/eother-activities-view.component';

const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebooks', component: EbooksComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebooks/form', component: EbooksFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebooks/form/:id', component: EbooksFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebooksView/:id', component: EbooksViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'econgress', component: EcongressComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'econgress/form', component: EcongressFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'econgress/form/:id', component: EcongressFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'econgressView/:id', component: EcongressViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebookChapters', component: EbookChaptersComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebookChapters/form', component: EbookChaptersFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebookChapters/form/:id', component: EbookChaptersFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'ebookChaptersView/:id', component: EbookChaptersViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'epublications', component: EpublicationsComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'epublications/form', component: EpublicationsFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'epublications/form/:id', component: EpublicationsFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'epublicationsView/:id', component: EpublicationsViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eotherActivities', component: EotherActivitiesComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eotherActivities/form', component: EotherActivitiesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eotherActivities/form/:id', component: EotherActivitiesFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eotherActivitiesView/:id', component: EotherActivitiesViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eplanning', component: EplanningComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eplanning/form', component: EplanningFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eplanning/form/:id', component: EplanningFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'eplanningView/:id', component: EplanningViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalificateRoutingModule { }
