import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Publications } from '../../model/publications';
import { PublicationsService } from '../../service/publications.service'; 
import swal from 'sweetalert2';

@Component({
  selector: 'app-epublications',
  templateUrl: './epublications.component.html',
  styleUrls: ['./epublications.component.scss']
})
export class EpublicationsComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Publications>();
  private _displayedColumns: string[] = ['id', 'planning', 'codIES', 'type', 'articleType', 'codPUB', 'title', 'indexBase', 'codeISS', 'magazineName', 'quartile', 'magazineNumber', 'sjr', 'publicationDate', 'detailField', 'condition', 'publicationLink', 'magazineLink', 'filiation', 'competitor', 'observation', 'duration', 'complete', 'aprobation', 'observations' , 'actions'];
  private _publicationsList: Publications[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private publicationsService: PublicationsService) { }

  ngOnInit(): void {
    this.loadPublicationsDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadPublicationsDataList(): void {
    this.publicationsService.findAll().subscribe(publicationsList => {
      this._publicationsList = publicationsList;
      this._dataSource.data = publicationsList;
      this._showSpiner = false;
    });
  }

  delete(publications: Publications): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la publicación: ${publications.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.publicationsService.delete(publications.id).subscribe(() => {
          this._publicationsList = this._publicationsList.filter(result => result !== publications);
          this._dataSource.data = this._publicationsList;
          swal.fire('Publicación Eliminada', `${publications.id}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Publications> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get publicationsList(): Publications[] {
    return this._publicationsList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }


}
