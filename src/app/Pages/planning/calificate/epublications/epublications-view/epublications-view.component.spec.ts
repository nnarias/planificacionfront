import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpublicationsViewComponent } from './epublications-view.component';

describe('EpublicationsViewComponent', () => {
  let component: EpublicationsViewComponent;
  let fixture: ComponentFixture<EpublicationsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpublicationsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EpublicationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
