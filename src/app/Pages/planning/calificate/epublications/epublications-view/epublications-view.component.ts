import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Publications } from '../../../model/publications';
import { PublicationsService } from '../../../service/publications.service'; 

@Component({
  selector: 'app-epublications-view',
  templateUrl: './epublications-view.component.html',
  styleUrls: ['./epublications-view.component.scss']
})
export class EpublicationsViewComponent implements OnInit {

  private _publications: Publications = new Publications();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private publicationsService: PublicationsService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.publicationsService.findById(id).subscribe(publications => {
          this._publications = publications;
          this._showSpiner = false;
        });
      }
    });
  }

  public get publications(): Publications {
    return this._publications;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

}
