import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpublicationsFormComponent } from './epublications-form.component';

describe('EpublicationsFormComponent', () => {
  let component: EpublicationsFormComponent;
  let fixture: ComponentFixture<EpublicationsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpublicationsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EpublicationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
