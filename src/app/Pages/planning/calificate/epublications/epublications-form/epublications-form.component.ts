import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Publications } from '../../../model/publications'; 
import { PublicationsService } from '../../../service/publications.service'; 
import swal from 'sweetalert2';

@Component({
  selector: 'app-epublications-form',
  templateUrl: './epublications-form.component.html',
  styleUrls: ['./epublications-form.component.scss']
})
export class EpublicationsFormComponent implements OnInit {

  private _publications: Publications = new Publications();
  _aprobation: string;
  _filiation: string='';

  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    /* planning: [''], */
    codIES: [''],
    type: [''],
    articleType: [''],
    codPUB: [''],
    title: [''],
    indexBase: [''],
    codeISS: [''],
    magazineName: [''],
    quartile: [''],
    magazineNumber: [''],
    sjr: [''],
    publicationDate: [''],
    detailField: [''],
    condition: [''],
    publicationLink: [''],
    magazineLink: [''],
    filiation: [''],
    competitor: [''],
    observation: [''],
    duration: [''],
    aprobation: [''],
    observations: [''],
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private publicationsService: PublicationsService, private router: Router) { }
  private _titles: string;

  ngOnInit(): void {
    this.loadPublications();
  }

  loadPublications(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadPublicationsData();
        this._titles = 'Modificar Publicación';
        this._showSpiner = false;
      } else {
        this._titles = 'Crear Publicación';
        this._showSpiner = false;
      }
    });
  }

  loadPublicationsData(): void {
    this.publicationsService.findById(this.id).subscribe(publications => {
      this._form.patchValue({
        id: publications.id,
        /* planning: publications.planning.id, */
        codIES: publications.codIES,
        type: publications.type,
        articleType: publications.articleType,
        codPUB: publications.codPUB,
        title: publications.title,
        indexBase: publications.indexBase,
        codeISS: publications.codeISS,
        magazineName: publications.magazineName,
        quartile: publications.quartile,
        magazineNumber: publications.magazineNumber,
        sjr: publications.sjr,
        publicationDate: publications.publicationDate,
        detailField: publications.detailField,
        condition: publications.condition,
        publicationLink: publications.publicationLink,
        magazineLink: publications.magazineLink,
        filiation: publications.filiation,
        competitor: publications.competitor,
        observation: publications.observation,
        duration: publications.duration,
        aprobation: publications.aprobation,
        observations: publications.observations,
      });
      this._aprobation=publications.aprobation;
      if(publications.filiation)
        this._filiation='Si';
      else
        this._filiation='No';
    });
  }

  save(): void {
    this._publications = this.form.value;
    this._publications.aprobation = this._aprobation;
    this.publicationsService.save(this._publications).subscribe(() => {
      this.router.navigate(['app/planning/planning/publications']);
      swal.fire(this._publications.id ? 'Publicación Actualizada' : 'Nuevo Publicación', `${this.publications.id}`, 'success');
    });
  }

  public get publications(): Publications {
    return this._publications;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get titles(): string {
    return this._titles;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

 /*  public get planning() { return this._form.get('planning'); } */

  public get codIES() { return this._form.get('codIES'); }

  public get type() { return this._form.get('type'); }

  public get articleType() { return this._form.get('articleType'); }

  public get codPUB() { return this._form.get('codPUB'); }

  public get title() { return this._form.get('title'); }

  public get indexBase() { return this._form.get('indexBase'); }

  public get codeISS() { return this._form.get('codeISS'); }

  public get magazineName() { return this._form.get('magazineName'); }

  public get quartile() { return this._form.get('quartile'); }

  public get magazineNumber() { return this._form.get('magazineNumber'); }

  public get sjr() { return this._form.get('sjr'); }

  public get publicationDate() { return this._form.get('publicationDate'); }

  public get detailField() { return this._form.get('detailField'); }

  public get condition() { return this._form.get('condition'); }

  public get publicationLink() { return this._form.get('publicationLink'); }

  public get magazineLink() { return this._form.get('magazineLink'); }

  public get filiation() { return this._form.get('filiation'); }

  public get competitor() { return this._form.get('competitor'); }

  public get observation() { return this._form.get('observation'); }

  public get duration() { return this._form.get('duration'); }

  public get aprobation() { return this._form.get('aprobation'); }

  public get observations() { return this._form.get('observations'); }

}
