import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpublicationsComponent } from './epublications.component';

describe('EpublicationsComponent', () => {
  let component: EpublicationsComponent;
  let fixture: ComponentFixture<EpublicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpublicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EpublicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
