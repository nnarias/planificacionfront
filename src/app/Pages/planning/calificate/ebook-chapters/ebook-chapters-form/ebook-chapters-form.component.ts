import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookChapters } from '../../../model/bookChapters'; 
import { BookChaptersService } from '../../../service/book-chapters.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-ebook-chapters-form',
  templateUrl: './ebook-chapters-form.component.html',
  styleUrls: ['./ebook-chapters-form.component.scss']
})
export class EbookChaptersFormComponent implements OnInit {

  private _bookChapters: BookChapters = new BookChapters();
  _aprobation: string;
  _filiation: string='';

  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    /* planning: [''], */
    codIES: [''],
    publicationType: [''],
    chapterCode: [''],
    codPUB: [''],
    capTitle: [''],
    bookTitle: [''],
    codISB: [''],
    editor: [''],
    numberPages: [''],
    publicationDate: [''],
    detailField: [''],
    //filiation: this._filiation,
    filiation: [''],
    competitor: [''],
    duration: [''],
    //aprobation: [''],
    observations: ['',Validators.required],
    
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private bookChaptersService: BookChaptersService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadBookChapters();
  }

  loadBookChapters(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadBookChaptersData();
        this._title = 'Evaluar Capítulo de Libro';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Capítulo de Libro';
        this._showSpiner = false;
      }
    });
  }

  loadBookChaptersData(): void {
    this.bookChaptersService.findById(this.id).subscribe(bookChapters => {
      this._form.patchValue({
        
        id: bookChapters.id,
        /* planning: bookChapters.planning.id, */
        codIES: bookChapters.codIES,
        publicationType: bookChapters.publicationType,
        chapterCode: bookChapters.chapterCode,
        codPUB: bookChapters.codPUB,
        capTitle: bookChapters.capTitle,
        bookTitle: bookChapters.bookTitle,
        codISB: bookChapters.codISB,
        editor: bookChapters.editor,
        numberPages: bookChapters.numberPages,
        publicationDate: bookChapters.publicationDate,
        detailField: bookChapters.detailField,
        filiation: bookChapters.filiation,
        competitor: bookChapters.competitor,
        duration: bookChapters.duration,
        observations: bookChapters.observations,

      });
      this._aprobation=bookChapters.aprobation;
      if(bookChapters.filiation)
        this._filiation='Si';
      else
        this._filiation='No';
    });
  }

  save(): void {
    this._bookChapters = this.form.value;
    this._bookChapters.aprobation = this._aprobation;
    this.bookChaptersService.save(this._bookChapters).subscribe(() => {
      this.router.navigate(['app/planning/calificate/ebookChapters']);
      swal.fire(this._bookChapters.id ? 'Capítulo de Libro Actualizado' : 'Nuevo Capítulo de Libro', `${this.bookChapters.id}`, 'success');
    });
  }

  public get bookChapters(): BookChapters {
    return this._bookChapters;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

 /*  public get planning() { return this._form.get('planning'); } */

  public get codIES() { return this._form.get('codIES'); }

  public get publicationType() { return this._form.get('publicationType'); }

  public get chapterCode() { return this._form.get('chapterCode'); }

  public get codPUB() { return this._form.get('codPUB'); }

  public get capTitle() { return this._form.get('capTitle'); }

  public get bookTitle() { return this._form.get('bookTitle'); }

  public get codISB() { return this._form.get('codISB'); }

  public get editor() { return this._form.get('editor'); }

  public get numberPages() { return this._form.get('numberPages'); }

  public get publicationDate() { return this._form.get('publicationDate'); }

  public get detailField() { return this._form.get('detailField'); }

  public get filiation() { return this._form.get('filiation'); }

  public get competitor() { return this._form.get('competitor'); }

  public get duration() { return this._form.get('duration'); }

  public get aprobation() { return this._form.get('aprobation'); }

  public get observations() { return this._form.get('observations'); }

}
