import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookChaptersFormComponent } from './ebook-chapters-form.component';

describe('EbookChaptersFormComponent', () => {
  let component: EbookChaptersFormComponent;
  let fixture: ComponentFixture<EbookChaptersFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EbookChaptersFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookChaptersFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
