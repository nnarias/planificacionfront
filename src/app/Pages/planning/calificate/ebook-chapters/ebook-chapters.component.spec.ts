import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookChaptersComponent } from './ebook-chapters.component';

describe('EbookChaptersComponent', () => {
  let component: EbookChaptersComponent;
  let fixture: ComponentFixture<EbookChaptersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EbookChaptersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookChaptersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
