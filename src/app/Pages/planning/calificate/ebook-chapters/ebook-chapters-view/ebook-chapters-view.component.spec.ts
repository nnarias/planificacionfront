import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookChaptersViewComponent } from './ebook-chapters-view.component';

describe('EbookChaptersViewComponent', () => {
  let component: EbookChaptersViewComponent;
  let fixture: ComponentFixture<EbookChaptersViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EbookChaptersViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookChaptersViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
