import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EbooksFormComponent } from './ebooks-form.component';

describe('EbooksFormComponent', () => {
  let component: EbooksFormComponent;
  let fixture: ComponentFixture<EbooksFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EbooksFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EbooksFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
