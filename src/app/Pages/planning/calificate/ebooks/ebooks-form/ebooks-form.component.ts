import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Books } from '../../../model/books'; 
import { BooksService } from '../../../service/books.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-ebooks-form',
  templateUrl: './ebooks-form.component.html',
  styleUrls: ['./ebooks-form.component.scss']
})
export class EbooksFormComponent implements OnInit {

  private _books: Books = new Books();
  _aprobation: string;
  _filiation: string='';

  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    /* planning: [''], */
    codIES: [''],
    publicationType: [''],
    codPUB: [''],
    bookTitle: [''],
    codISB: [''],
    publicationDate: [''],
    detailField: [''],
    peerReviewed: [''],
    filiation: [''],
    competitor: [''],
    duration: [''],
    aprobation: [''],
    observations: ['',Validators.required],
    
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private booksService: BooksService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadBooks();
  }

  loadBooks(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadBooksData();
        this._title = 'Evaluar Libro';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Libro';
        this._showSpiner = false;
      }
    });
  }

  loadBooksData(): void {
    this.booksService.findById(this.id).subscribe(books => {
      this._form.patchValue({
        id: books.id,
        /* planning: books.planning.id, */
        codIES: books.codIES,
        publicationType: books.publicationType,
        codPUB: books.codPUB,
        bookTitle: books.bookTitle,
        codISB: books.codISB,
        publicationDate: books.publicationDate,
        detailField: books.detailField,
        peerReviewed: books.peerReviewed,
        filiation: books.filiation,
        competitor: books.competitor,
        duration: books.duration,
        aprobation: books.aprobation,
        observations: books.observations,
      });
      this._aprobation=books.aprobation;
      if(books.filiation)
        this._filiation='Si';
      else
        this._filiation='No';
    });
  }

  save(): void {
    this._books = this.form.value;
    this._books.aprobation = this._aprobation;
    this.booksService.save(this._books).subscribe(() => {
      this.router.navigate(['app/planning/calificate/ebooks']);
      swal.fire(this._books.id ? 'Libro Actualizado' : 'Nuevo Libro', `${this.books.id}`, 'success');
    });
  }

  public get books(): Books {
    return this._books;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

 /*  public get planning() { return this._form.get('planning'); } */

  public get codIES() { return this._form.get('codIES'); }

  public get publicationType() { return this._form.get('publicationType'); }

  public get codPUB() { return this._form.get('codPUB'); }

  public get bookTitle() { return this._form.get('bookTitle'); }

  public get codISB() { return this._form.get('codISB'); }

  public get publicationDate() { return this._form.get('publicationDate'); }

  public get detailField() { return this._form.get('detailField'); }

  public get peerReviewed() { return this._form.get('peerReviewed'); }

  public get filiation() { return this._form.get('filiation'); }

  public get competitor() { return this._form.get('competitor'); }

  public get duration() { return this._form.get('duration'); }

  public get aprobation() { return this._form.get('aprobation'); }

  public get observations() { return this._form.get('observations'); }
}
