import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EbooksViewComponent } from './ebooks-view.component';

describe('EbooksViewComponent', () => {
  let component: EbooksViewComponent;
  let fixture: ComponentFixture<EbooksViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EbooksViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EbooksViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
