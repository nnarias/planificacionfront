import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { CalificateRoutingModule } from './calificate-routing.module';
import { MenuComponent } from './menu/menu.component';
import { EplanningComponent } from './eplanning/eplanning.component';
import { EotherActivitiesComponent } from './eother-activities/eother-activities.component';
import { EbookChaptersComponent } from './ebook-chapters/ebook-chapters.component';
import { EbooksComponent } from './ebooks/ebooks.component';
import { EcongressComponent } from './econgress/econgress.component';
import { EpublicationsComponent } from './epublications/epublications.component';
import { EplanningFormComponent } from './eplanning/eplanning-form/eplanning-form.component';
import { EplanningViewComponent } from './eplanning/eplanning-view/eplanning-view.component';
import { EbooksViewComponent } from './ebooks/ebooks-view/ebooks-view.component';
import { EbooksFormComponent } from './ebooks/ebooks-form/ebooks-form.component';
import { EbookChaptersFormComponent } from './ebook-chapters/ebook-chapters-form/ebook-chapters-form.component';
import { EbookChaptersViewComponent } from './ebook-chapters/ebook-chapters-view/ebook-chapters-view.component';
import { EcongressViewComponent } from './econgress/econgress-view/econgress-view.component';
import { EcongressFormComponent } from './econgress/econgress-form/econgress-form.component';
import { EpublicationsFormComponent } from './epublications/epublications-form/epublications-form.component';
import { EpublicationsViewComponent } from './epublications/epublications-view/epublications-view.component';
import { EotherActivitiesViewComponent } from './eother-activities/eother-activities-view/eother-activities-view.component';
import { EotherActivitiesFormComponent } from './eother-activities/eother-activities-form/eother-activities-form.component';


@NgModule({
  declarations: [
    MenuComponent,
    EplanningComponent,
    EotherActivitiesComponent,
    EbookChaptersComponent,
    EbooksComponent,
    EcongressComponent,
    EpublicationsComponent,
    EplanningFormComponent,
    EplanningViewComponent,
    EbooksViewComponent,
    EbooksFormComponent,
    EbookChaptersFormComponent,
    EbookChaptersViewComponent,
    EcongressViewComponent,
    EcongressFormComponent,
    EpublicationsFormComponent,
    EpublicationsViewComponent,
    EotherActivitiesViewComponent,
    EotherActivitiesFormComponent
  ],
  imports: [
    CommonModule,
    ImportsModule,
    CalificateRoutingModule
  ]
})
export class CalificateModule { }
