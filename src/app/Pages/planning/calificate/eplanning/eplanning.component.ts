import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Planning } from '../../model/planning';
import { PlanningService } from '../../service/planning.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-eplanning',
  templateUrl: './eplanning.component.html',
  styleUrls: ['./eplanning.component.scss']
})
export class EplanningComponent implements OnInit {

  private _dataSource = new MatTableDataSource<Planning>();
  private _displayedColumns: string[] = ['id', 'maxDuration', 'registrationDate', 'responsibleId', 'aprobation', 'complete', 'evaluatorId','observations' ,'actions'];
  private _planningList: Planning[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private planningService: PlanningService) { }

  ngOnInit(): void {
    this.loadPlanningDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadPlanningDataList(): void {
    this.planningService.findAll().subscribe(planningList => {
      this._planningList = planningList;
      this._dataSource.data = planningList;
      this._showSpiner = false;
    });
  }

  delete(planning: Planning): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la planificación: ${planning.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.planningService.delete(planning.id).subscribe(() => {
          this._planningList = this._planningList.filter(result => result !== planning);
          this._dataSource.data = this._planningList;
          swal.fire('Planificación Eliminado', `${planning.id}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<Planning> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get planningList(): Planning[] {
    return this._planningList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }
}
