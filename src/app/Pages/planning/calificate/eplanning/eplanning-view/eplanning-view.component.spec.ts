import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EplanningViewComponent } from './eplanning-view.component';

describe('EplanningViewComponent', () => {
  let component: EplanningViewComponent;
  let fixture: ComponentFixture<EplanningViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EplanningViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EplanningViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
