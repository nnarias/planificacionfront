import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EplanningFormComponent } from './eplanning-form.component';

describe('EplanningFormComponent', () => {
  let component: EplanningFormComponent;
  let fixture: ComponentFixture<EplanningFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EplanningFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EplanningFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
