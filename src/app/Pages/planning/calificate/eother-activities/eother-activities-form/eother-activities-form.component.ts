import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OtherActivities } from '../../../model/otherActivities'; 
import { OtherActivitiesService } from '../../../service/other-activities.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-eother-activities-form',
  templateUrl: './eother-activities-form.component.html',
  styleUrls: ['./eother-activities-form.component.scss']
})
export class EotherActivitiesFormComponent implements OnInit {

  private _otherActivities: OtherActivities = new OtherActivities();
  _aprobation: string;

  private _form: FormGroup = this.formBuilder.group({
    id: [''],
    /* planning: [''], */
    name: [''],
    duration: [''],
    link: [''],
    aprobation: [''],
    observations: [''],
    
  });
  private id: number;
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private otherActivitiesService: OtherActivitiesService, private router: Router) { }
  private _title: string;

  ngOnInit(): void {
    this.loadOtherActivities();
  }

  loadOtherActivities(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.loadOtherActivitiesData();
        this._title = 'Evaluar Actividad';
        this._showSpiner = false;
      } else {
        this._title = 'Crear Actividad';
        this._showSpiner = false;
      }
    });
  }

  loadOtherActivitiesData(): void {
    this.otherActivitiesService.findById(this.id).subscribe(otherActivities => {
      this._form.patchValue({
        id: otherActivities.id,
        /* planning: otherActivities.planning.id, */
        name: otherActivities.name,
        duration: otherActivities.duration,
        link: otherActivities.link,
        aprobation: otherActivities.aprobation,
        observations: otherActivities.observations,
      });
      this._aprobation=otherActivities.aprobation;
    });
  }

  save(): void {
    this._otherActivities = this.form.value;
    this._otherActivities.aprobation = this._aprobation;
    this.otherActivitiesService.save(this._otherActivities).subscribe(() => {
      this.router.navigate(['app/planning/calificate/eotherActivities']);
      swal.fire(this._otherActivities.id ? 'Actividad Actualizada' : 'Nueva Actividad', `${this.otherActivities.id}`, 'success');
    });
  }

  public get otherActivities(): OtherActivities {
    return this._otherActivities;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get title(): string {
    return this._title;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

 /*  public get planning() { return this._form.get('planning'); } */

  public get name() { return this._form.get('name'); }

  public get duration() { return this._form.get('duration'); }

  public get link() { return this._form.get('link'); }

  public get aprobation() { return this._form.get('aprobation'); }

  public get observations() { return this._form.get('observations'); }
}
