import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EotherActivitiesFormComponent } from './eother-activities-form.component';

describe('EotherActivitiesFormComponent', () => {
  let component: EotherActivitiesFormComponent;
  let fixture: ComponentFixture<EotherActivitiesFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EotherActivitiesFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EotherActivitiesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
