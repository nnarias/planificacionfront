import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EotherActivitiesComponent } from './eother-activities.component';

describe('EotherActivitiesComponent', () => {
  let component: EotherActivitiesComponent;
  let fixture: ComponentFixture<EotherActivitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EotherActivitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EotherActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
