import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EotherActivitiesViewComponent } from './eother-activities-view.component';

describe('EotherActivitiesViewComponent', () => {
  let component: EotherActivitiesViewComponent;
  let fixture: ComponentFixture<EotherActivitiesViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EotherActivitiesViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EotherActivitiesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
