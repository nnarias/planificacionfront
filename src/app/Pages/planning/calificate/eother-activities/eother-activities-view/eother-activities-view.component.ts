import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OtherActivities } from '../../../model/otherActivities';
import { OtherActivitiesService } from '../../../service/other-activities.service';

@Component({
  selector: 'app-eother-activities-view',
  templateUrl: './eother-activities-view.component.html',
  styleUrls: ['./eother-activities-view.component.scss']
})
export class EotherActivitiesViewComponent implements OnInit {

  private _otherActivities: OtherActivities = new OtherActivities();
  private _showSpiner: Boolean = true;

  constructor(private activatedRoute: ActivatedRoute, private otherActivitiesService: OtherActivitiesService) { }

  ngOnInit(): void {
    this.loadCall();
  }

  loadCall(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.otherActivitiesService.findById(id).subscribe(otherActivities => {
          this._otherActivities = otherActivities;
          this._showSpiner = false;
        });
      }
    });
  }

  public get otherActivities(): OtherActivities {
    return this._otherActivities;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

}
