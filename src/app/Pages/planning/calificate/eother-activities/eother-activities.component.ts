import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OtherActivities } from '../../model/otherActivities';
import { OtherActivitiesService } from '../../service/other-activities.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-eother-activities',
  templateUrl: './eother-activities.component.html',
  styleUrls: ['./eother-activities.component.scss']
})
export class EotherActivitiesComponent implements OnInit {

  private _dataSource = new MatTableDataSource<OtherActivities>();
  private _displayedColumns: string[] = ['id', 'planning','name', 'duration', 'link','complete','aprobation', 'observations' ,'actions'];
  private _otherActivitiesList: OtherActivities[] = [];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  };

  constructor(private otherActivitiesService: OtherActivitiesService) { }

  ngOnInit(): void {
    this.loadOtherActivitiesDataList();
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }

  loadOtherActivitiesDataList(): void {
    this.otherActivitiesService.findAll().subscribe(otherActivitiesList => {
      this._otherActivitiesList = otherActivitiesList;
      this._dataSource.data = otherActivitiesList;
      this._showSpiner = false;
    });
  }

  delete(otherActivities: OtherActivities): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: true,
      showCancelButton: true,
      text: `Seguro que desea eliminar la Actividad: ${otherActivities.id}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.otherActivitiesService.delete(otherActivities.id).subscribe(() => {
          this._otherActivitiesList = this._otherActivitiesList.filter(result => result !== otherActivities);
          this._dataSource.data = this._otherActivitiesList;
          swal.fire('Actividad Eliminada', `${otherActivities.id}, eliminada con éxito`, 'success');
        });
      }
    });
  }

  public get dataSource(): MatTableDataSource<OtherActivities> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get otherActivitiesList(): OtherActivities[] {
    return this._otherActivitiesList;
  }

  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public set search(search: string) {
    this._search = search;
  }

}
