import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Congress } from '../model/congress';
import { Planning } from '../model/planning';

@Injectable({
  providedIn: 'root'
})
export class CongressService {
  private urlEndPoint: string = `${environment.apiUrl}/congress`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  complete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/complete/${id}`, null);
  }

  findById(id: number): Observable<Congress> {
    return this.http.get<Congress>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Congress[]> {
    return this.http.get<Congress[]>(this.urlEndPoint);
  }

  findAllByPlanning(planning: Planning): Observable<Congress[]> {
    return this.http.get<Congress[]>(`${this.urlEndPoint}/planning/${planning}`);
  }
  save(congress: Congress): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, congress);
  }
}
