import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BookChapters } from '../model/bookChapters';
import { Planning } from '../model/planning';

@Injectable({
  providedIn: 'root'
})
export class BookChaptersService {
  private urlEndPoint: string = `${environment.apiUrl}/bookchapters`;

  constructor(private http: HttpClient) {}

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  complete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/complete/${id}`, null);
  }

  findById(id: number): Observable<BookChapters> {
    return this.http.get<BookChapters>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<BookChapters[]> {
    return this.http.get<BookChapters[]>(this.urlEndPoint);
  }

  findAllByPlanning(planning: Planning): Observable<BookChapters[]> {
    return this.http.get<BookChapters[]>(`${this.urlEndPoint}/planning/${planning}`);
  }
  save(bookChapters: BookChapters): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, bookChapters);
  }
}
