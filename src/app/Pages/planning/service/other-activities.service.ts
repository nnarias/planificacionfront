import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { OtherActivities } from '../model/otherActivities';
import { Planning } from '../model/planning';

@Injectable({
  providedIn: 'root'
})
export class OtherActivitiesService {

  private urlEndPoint: string = `${environment.apiUrl}/otheractivities`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  complete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/complete/${id}`, null);
  }

  findById(id: number): Observable<OtherActivities> {
    return this.http.get<OtherActivities>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<OtherActivities[]> {
    return this.http.get<OtherActivities[]>(this.urlEndPoint);
  }

  findAllByPlanning(id: number): Observable<OtherActivities[]> {
    return this.http.get<OtherActivities[]>(`${this.urlEndPoint}/planning/${id}`);
  }
  save(otheractivities: OtherActivities): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, otheractivities);
  }
}
