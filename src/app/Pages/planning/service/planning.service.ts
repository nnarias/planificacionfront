import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Activities } from '../model/activities';
import { Planning } from '../model/planning';

@Injectable({
  providedIn: 'root',
})
export class PlanningService {
  private urlEndPoint: string = `${environment.apiUrl}/planning`;

  constructor(private http: HttpClient) {}

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  complete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/complete/${id}`, null);
  }

  aprobation(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/aprobation/${id}`, null);
  }

  findById(id: number): Observable<Planning> {
    return this.http.get<Planning>(`${this.urlEndPoint}/${id}`);
  }

  findAllIR(): Observable<Planning[]> {
    return this.http.get<Planning[]>(`${this.urlEndPoint}IR`);
  }

  findAll(): Observable<Planning[]> {
    return this.http.get<Planning[]>(this.urlEndPoint);
  }

  findAllActivities(id: number): Observable<Activities[]> {
    return this.http.get<Activities[]>(`${this.urlEndPoint}/activities/${id}`);
  }
  save(planning: Planning): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, planning);
  }
}
