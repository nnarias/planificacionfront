import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Degrees } from '../model/degrees'; 


@Injectable({
  providedIn: 'root'
})
export class DegreesService {
  private urlEndPoint: string = `${environment.apiUrl}/degrees`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  findById(id: number): Observable<Degrees> {
    return this.http.get<Degrees>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Degrees[]> {
    return this.http.get<Degrees[]>(this.urlEndPoint);
  }
  
  save(degrees: Degrees): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, degrees);
  }
}
