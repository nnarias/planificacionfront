import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Books } from '../model/books';
import { Planning } from '../model/planning';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private urlEndPoint: string = `${environment.apiUrl}/books`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  complete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/complete/${id}`, null);
  }

  findById(id: number): Observable<Books> {
    return this.http.get<Books>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Books[]> {
    return this.http.get<Books[]>(this.urlEndPoint);
  }

  findAllByPlanning(planning: Planning): Observable<Books[]> {
    return this.http.get<Books[]>(`${this.urlEndPoint}/planning/${planning}`);
  }
  save(books: Books): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, books);
  }
}
