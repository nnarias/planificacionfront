import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Planning } from '../model/planning';
import { Publications } from '../model/publications';

@Injectable({
  providedIn: 'root'
})
export class PublicationsService {

  private urlEndPoint: string = `${environment.apiUrl}/publications`;

  constructor(private http: HttpClient) {}

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  complete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/complete/${id}`, null);
  }

  findById(id: number): Observable<Publications> {
    return this.http.get<Publications>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Publications[]> {
    return this.http.get<Publications[]>(this.urlEndPoint);
  }

  findAllByPlanning(planning: Planning): Observable<Publications[]> {
    return this.http.get<Publications[]>(`${this.urlEndPoint}/planning/${planning}`);
  }
  save(publications: Publications): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, publications);
  }
}
