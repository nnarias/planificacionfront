import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Activities } from '../model/activities'; 

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  private urlEndPoint: string = `${environment.apiUrl}/activities`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.patch<any>(`${this.urlEndPoint}/${id}`, null);
  }

  findById(id: number): Observable<Activities> {
    return this.http.get<Activities>(`${this.urlEndPoint}/${id}`);
  }

  findAll(): Observable<Activities[]> {
    return this.http.get<Activities[]>(this.urlEndPoint);
  }
  findMaxDuration(maxDuration: number): Observable<Activities[]> {
    return this.http.get<Activities[]>(`${this.urlEndPoint}/max/${maxDuration}`);
  }

  save(activities: Activities): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, activities);
  }
}
