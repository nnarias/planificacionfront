import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: 'planning', loadChildren: () => import('./planning/planning.module').then(m => m.PlanningModule) },
{ path: 'calificate', loadChildren:() => import('./calificate/calificate.module').then(m => m.CalificateModule) },
{ path: 'administration', loadChildren:() => import('./administration/administration.module').then(m => m.AdministrationModule) },
{ path: 'reports', loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule) },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningRoutingModule { }
