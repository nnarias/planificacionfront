import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { ActivatedRoute } from '@angular/router';
import { Activities } from '../../../model/activities';
import { Planning } from '../../../model/planning';
import { PlanningService } from '../../../service/planning.service';

@Component({
  selector: 'app-report-view',
  templateUrl: './report-view.component.html',
  styleUrls: ['./report-view.component.scss']
})
export class ReportViewComponent implements OnInit {
  private _planning: Planning = new Planning();
  private _dataSource = new MatTableDataSource<Activities>();
  private _displayedColumns: string[] = ['name', 'duration', 'complete', 'aprobation'];
  private paginator: MatPaginator;
  private _search: string;
  private _showSpiner: Boolean = true;
  private sort: MatSort;


  _listActivities: Activities[] = [];
  _numberActivities: number=0;
  _numberComplete: number=0;
  _numberIncomplete: number=0;
  _numberAS: number=0;
  _numberAIS: number=0;
  _numberNA: number=0;
  _numberHoursComplete: number=0;
  _numberHoursIncomplete: number=0;


  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = [ 'Aprobadas' , 'Pendientes' , 'No Aprobadas', 'Completas' , 'Incompletas'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)', 'rgba(0,100,140,0.3)' , 'rgba(165,122,74,0.6)'],
    },
  ];

  @ViewChild(MatPaginator, { static: false }) set matPaginator(
    paginator: MatPaginator
  ) {
    this.paginator = paginator;
    if (this.paginator) {
      this._dataSource.paginator = this.paginator;
    }
  }
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this._dataSource.sort = this.sort;
    }
  }


  constructor(
    private activatedRoute: ActivatedRoute,
    private planningService: PlanningService) { }

  

  ngOnInit(): void {
    this.loadCall();
    
  }

  filterDataTable(): void {
    this._dataSource.filter = this._search.trim().toLowerCase();
  }


  loadCall(): any {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.planningService.findById(id).subscribe(planning => {
          this._planning = planning;
          this.loadActivities(id);
        });
      }
    });
  }


  loadActivities(id: number): void{
    this.planningService.findAllActivities(id).subscribe(activities => {
      this._listActivities = activities;
      this._numberActivities=this._listActivities.length;
          console.log(this._numberActivities);
          for(let i=0;i<this._listActivities.length;i++){
            if(this._listActivities[i].complete){
              this._numberComplete++;
              this._numberHoursComplete+=this._listActivities[i].duration;
            }
            else{
              this._numberIncomplete++;
              this._numberHoursIncomplete+=this._listActivities[i].duration;
            }
            switch(this._listActivities[i].aprobation){
              case 'Aprobada':
                this._numberAS++;
                break;
              case 'Pendiente':
                this._numberAIS++;
                break;
              case 'No Aprobado':
                this._numberNA++;
                break
            } 
          }   
          this.pieChartData.push(this._numberAS);
          this.pieChartData.push(this._numberAIS);
          this.pieChartData.push(this._numberNA);
          this.pieChartData.push(this._numberComplete);
          this.pieChartData.push(this._numberIncomplete); 
          this._dataSource.data = this._listActivities;
          this._showSpiner = false;
          console.log('Elo: ',this._listActivities)
    });
  }

  public get planning(): Planning {
    return this._planning;
  }

  public get listActivities(): Activities[] {
    return this._listActivities;
  }


  public get showSpiner(): Boolean {
    return this._showSpiner;
  }

  public get dataSource(): MatTableDataSource<Activities> {
    return this._dataSource;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }


}
