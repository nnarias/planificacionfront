import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../security/guard/auth.guard';
import { RoleGuard } from '../../security/guard/role.guard';
import { MenuComponent } from './menu/menu.component';
import { ReportComponent } from './report/report.component';
import { ReportViewComponent } from './report/report-view/report-view.component';


const routes: Routes = [{ path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'reports', component: ReportComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } },
{ path: 'reportsView/:id', component: ReportViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_PROJECT_ADMIN', 'ROLE_PROJECT_DIRECTOR'] } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
