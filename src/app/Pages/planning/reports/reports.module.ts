import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { ReportsRoutingModule } from './reports-routing.module';
import { MenuComponent } from './menu/menu.component';
import { ReportComponent } from './report/report.component';
import { ChartsModule } from 'ng2-charts';
import { ReportViewComponent } from './report/report-view/report-view.component'; 


@NgModule({
  declarations: [
    MenuComponent,
    ReportComponent,
    ReportViewComponent
  ],
  imports: [
    CommonModule,
    ImportsModule,
    ChartsModule,
    ReportsRoutingModule
  ]
})
export class ReportsModule { }
