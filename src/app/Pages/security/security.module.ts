import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { NgModule } from '@angular/core';
import { SecurityRoutingModule } from './security-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ImportsModule, SecurityRoutingModule]
})
export class SecurityModule { }