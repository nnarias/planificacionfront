import { AuthService } from "../../login/service/auth.service";
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (this.authService.isAuthenticated()) {
            if (this.isTokenExpired()) {
                this.authService.logout();
                this.router.navigate(['']);
                return false;
            }
            return true;
        }
        else {
            this.router.navigate(['']);
            swal.fire('Acceso denegado', 'Por favor inicia sesión antes!', 'warning');
        }
    }

    isTokenExpired(): boolean {
        let token = this.authService.getToken;
        let payload = this.authService.getTokenData(token);
        let now = new Date().getTime() / 1000;
        if (payload.exp < now) {
            return true;
        } else {
            return false;
        }
    }
}