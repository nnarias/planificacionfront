import { ActivatedRouteSnapshot, CanActivate, Router, UrlTree } from '@angular/router';
import { AuthService } from "../../login/service/auth.service";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let role = next.data['role'];
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['']);
      swal.fire('Acceso denegado', 'Por favor inicia sesión antes', 'warning');
      return false;
    }
    if (this.authService.hasRole(role)) {
      return true;
    }
    swal.fire('Acceso denegado', `Hola ${this.authService.getUserLogin.username}, no tienes acceso a este recurso`, 'warning');
    this.router.navigate(['app']);
    return false;
  }
}
