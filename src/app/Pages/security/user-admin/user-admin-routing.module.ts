import { AuthGuard } from '../guard/auth.guard';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleComponent } from './role/role.component';
import { RoleGuard } from '../guard/role.guard';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user/user.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserViewComponent } from './user/user-view/user-view.component';

const routes: Routes = [
  { path: '', component: MenuComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'changePassword', component: ChangePasswordComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER'] } },
  { path: 'roles/:id', component: RoleComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'users', component: UserComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'user/form', component: UserFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'user/form/:id', component: UserFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'userView', component: UserViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER'] } },
  { path: 'userView/:id', component: UserViewComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER', 'ROLE_PROJECT_ADMIN'] } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAdminRoutingModule { }