import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Role } from '../model/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private urlEndPoint: string = `${environment.apiUrl}/role`;

  constructor(private http: HttpClient) { }

  find(): Observable<Role[]> {
    return this.http.get<Role[]>(this.urlEndPoint);
  }

  findByUser(id: number): Observable<any> {
    return this.http.get<Role[]>(`${this.urlEndPoint}/user/${id}`);
  }

  roleAssign(roleList: number[], id: number): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/user/${id}`, roleList);
  }
}