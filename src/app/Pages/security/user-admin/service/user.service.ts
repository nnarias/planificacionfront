import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { UserGeneral } from '../model/userGeneral';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlEndPoint: string = `${environment.apiUrl}/user`;

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<User> {
    return this.http.patch<User>(`${this.urlEndPoint}/${id}`, null);
  }

  find(id: number): Observable<User> {
    return this.http.get<User>(`${this.urlEndPoint}/${id}`);
  }

  findByUser(): Observable<User[]> {
    return this.http.get<User[]>(this.urlEndPoint);
  }

  findUserGeneralByUsername(username: String): Observable<UserGeneral> {
    return this.http.get<UserGeneral>(`${this.urlEndPoint}/username/${username}`);
  }

  resetPassword(id: number): Observable<User> {
    return this.http.get<User>(`${this.urlEndPoint}/passwordReset/${id}`);
  }

  save(user: User): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, user);
  }
}
