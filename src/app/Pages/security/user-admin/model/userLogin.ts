import { Role } from "./role";

export class UserLogin {
  username: string;
  name: string;
  roleList: Role[];
  internalUSer: boolean;
}
