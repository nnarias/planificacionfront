import { Role } from './role';

export class User {
    birthDate: string;
    creationDate: string;
    email: string;
    id: number;
    lastName: string;
    name: string;
    password: string;
    roleList: Role[];
    username: string;
}