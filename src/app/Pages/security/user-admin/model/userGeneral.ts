import { Role } from "./role";

export class UserGeneral {
  username: string;
  name: string;
  email: string;
  roleList: Role[];
  internalUSer: boolean;
}
