import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Role } from '../model/role';
import { RoleService } from '../service/role.service';
import swal from 'sweetalert2';
import { User } from '../model/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  private id: number;
  private myselectedRoles = new Array();
  private roleForm: FormControl = new FormControl();
  private roleList: Role[] = [];
  private user: User = new User();

  constructor(private activatedRoute: ActivatedRoute, private roleService: RoleService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      this.id = id;
      if (id) {
        this.loadRoles();
        this.loadUserData();
      }
    });
  }

  loadRoles(): void {
    this.roleService.find().subscribe(roleList => {
      this.roleList = roleList;
    });
  }

  loadUserData(): void {
    this.userService.find(this.id).subscribe(user => {
      this.user = user;
      user.roleList.forEach(role => this.myselectedRoles.push(role.id));
      this.roleForm.patchValue(this.myselectedRoles);
    });
  }

  roleAssign(): void {
    this.roleService.roleAssign(this.roleForm.value, this.id).subscribe(() => {
      this.router.navigate(['app/security/user-admin/users']);
      swal.fire('Roles Actualizados', `Los roles fueron actualizados, correctamente para el usuario: ${this.user.username}`, 'success');
    });
  }

  public get getRoleForm(): FormControl {
    return this.roleForm;
  }

  public get getRoleList(): Role[] {
    return this.roleList;
  }

  public get getUser(): User {
    return this.user;
  }
}