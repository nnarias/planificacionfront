import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/Pages/login/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Role } from '../../model/role';
import { User } from '../../model/user';
import { UserService } from '../../service/user.service';
import { UserGeneral } from '../../model/userGeneral';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {
  private userGeneral: UserGeneral = new UserGeneral();

  constructor(private activatedRoute: ActivatedRoute, private authService: AuthService, private userService: UserService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.loadUser(id);
      } else {
        this.loadUser(this.authService.getUserLogin.username);
      }
    });
  }

  loadUser(username: string): void {
    this.userService.findUserGeneralByUsername(username).subscribe(userGeneral => {
      this.userGeneral = userGeneral;
    });
  }

  public get getRoleList(): Role[] {
    return this.userGeneral.roleList;
  }

  public get getUserGeneral(): UserGeneral {
    return this.userGeneral;
  }
}
