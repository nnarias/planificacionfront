import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';
import { User } from '../../model/user';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  private dataSource = new MatTableDataSource<User>();
  private displayedColumns: string[] = ['username', 'name', 'email', 'actions'];
  private paginator: MatPaginator;
  private search: string;
  private sort: MatSort;
  private userList: User[] = [];

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  };
  @ViewChild(MatSort, { static: false }) set matSort(sort: MatSort) {
    this.sort = sort;
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  };

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.findByUser().subscribe(userList => {
      this.userList = userList;
      this.dataSource.data = userList;
    });
  }

  delete(user: User): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      },
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, eliminar',
      icon: 'warning',
      reverseButtons: false,
      showCancelButton: true,
      text: `Seguro que desea eliminar al usuario: ${user.name} ${user.lastName}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.userService.delete(user.id).subscribe(() => {
          this.userList = this.userList.filter(result => result !== user);
          this.dataSource.data = this.userList;
          swal.fire('Usuario Eliminado', `Usuario: ${user.name} ${user.lastName}, eliminado con éxito`, 'success');
        });
      }
    });
  }

  filterDataTable(): void {
    this.dataSource.filter = this.search.trim().toLowerCase();
  }

  resetPassword(user: User): void {
    const swallBootstrap = swal.mixin({
      buttonsStyling: false,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success'
      }
    });
    swallBootstrap.fire({
      cancelButtonText: 'No, cancelar',
      confirmButtonText: 'Sí, reestablecer',
      icon: 'warning',
      reverseButtons: false,
      showCancelButton: true,
      text: `Seguro que desea reestablecer la contraseña del usuario: ${user.name} ${user.lastName}`,
      title: '¿Está seguro?'
    }).then((result) => {
      if (result.value) {
        this.userService.resetPassword(user.id).subscribe(() => {
          swal.fire('La contraseña fue reestablecida con éxito',
            `Se ha reestablecido la contraseña al usuario: ${user.name} ${user.lastName}, esta es la fecha de nacimiento del usuario en formato dd/mm/aa`, 'success');
        });
      }
    });
  }

  public get getDataSource(): MatTableDataSource<User> {
    return this.dataSource;
  }

  public get getDisplayedColumns(): string[] {
    return this.displayedColumns;
  }

  public get getUserList(): User[] {
    return this.userList;
  }

  public set setSearch(search: string) {
    this.search = search;
  }
}