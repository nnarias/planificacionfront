import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { customEmail } from 'src/app/validators/validation.directive';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { showNotification } from 'src/assets/utils/notification-intl';
import swal from 'sweetalert2';
import { User } from '../../model/user';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    birthDate: ['', Validators.required],
    email: ['', Validators.compose([Validators.email, customEmail(), Validators.required])],
    id: [''],
    lastName: ['', Validators.required],
    name: ['', Validators.required],
    username: ['']
  });
  private title: String;
  private user: User = new User();

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private router: Router, private userService: UserService) { }


  ngOnInit(): void {
    this.loadUser();
  }

  alertMessages(): void {
    showNotification('El nombre de usuario es generado automáticamente con la primera letra del nombre, seguido del apellido completo', 'top', 'right', 'info', 1000, 'info');
    showNotification('Si el nombre de usuario ya está siendo utilizado, se le asignará un número', 'top', 'right', 'info', 1000, 'info');
    showNotification('La contraseña es generada automáticamente con la fecha de nacimiento del usuario, en formato dd/mm/aa', 'top', 'right', 'info', 1000, 'info');
  }

  loadUser(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.userService.find(id).subscribe(user => {
          this.form.patchValue({
            birthDate: user.birthDate,
            email: user.email,
            id: user.id,
            lastName: user.lastName,
            name: user.name,
            username: user.username
          });
          this.title = 'Modificar Usuario';
        });
      } else {
        this.alertMessages();
        this.title = 'Crear Usuario';
      }
    });
  }

  save(): void {
    this.user = this.form.value;
    this.userService.save(this.user).subscribe(() => {
      swal.fire(this.user.id ? 'Usuario Actualizado' : 'Nuevo Usuario', `Usuario guardado con éxito ${this.user.name} ${this.user.lastName}`, 'success');
      this.router.navigate(['app/security/user-admin/users']);
    });
  }

  public get birthDate() { return this.form.get('birthDate'); }
  public get email() { return this.form.get('email'); }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getTitle(): String {
    return this.title;
  }

  public get getUser(): User {
    return this.user;
  }

  public get lastName() { return this.form.get('email'); }
  public get name() { return this.form.get('name'); }
}