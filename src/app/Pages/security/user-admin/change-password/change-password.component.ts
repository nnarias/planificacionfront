import { AuthService } from 'src/app/Pages/login/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Password } from '../model/password';
import { password, passwords } from 'src/app/validators/validation.directive';
import { Router } from '@angular/router'
import { showNotification } from 'src/assets/utils/notification-intl';
import swal from 'sweetalert2';
;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    newPassword: ['', Validators.compose([Validators.minLength(8), password(), Validators.required])],
    newPassword2: ['', Validators.compose([Validators.minLength(8), password(), Validators.required])],
    oldPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])]
  }, {
    validators: [passwords()]
  });
  private hideNewPassword: boolean = true;
  private hideNewPassword2: boolean = true;
  private hideOldPassword: boolean = true;
  private password: Password = new Password();

  constructor(private authService: AuthService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.alertMessage();
  }

  alertMessage(): void {
    showNotification('La contraseña debe contener un caracter especial y un número', 'top', 'right', 'info', 1000, 'info');
    showNotification('La contraseña debe contener al menos una letra minúscula y una letra mayúscula', 'top', 'right', 'info', 1000, 'info');
    showNotification('La contraseña debe contener al menos 8 caracteres, no se permiten espacios en blanco', 'top', 'right', 'info', 1000, 'info');
  }

  changePassword(): void {
    this.password = this.form.value;
    this.authService.changePassword(this.password).subscribe(() => {
      this.password = null;
      this.router.navigate(['app']);
      swal.fire('Acción Completada', 'Contraseña actualizada', 'success');
    })
  }

  proob(): void {
    console.log(this.form.controls['oldPassword'].errors)
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get hideNewPwd(): boolean {
    return this.hideNewPassword;
  }

  public get hideNewPwd2(): boolean {
    return this.hideNewPassword2;
  }

  public get hideOldPwd(): boolean {
    return this.hideOldPassword;
  }

  public set hideOldPwd(hide: boolean) {
    this.hideOldPassword = hide;
  }

  public set hideNewPwd(hide: boolean) {
    this.hideNewPassword = hide;
  }

  public set hideNewPwd2(hide: boolean) {
    this.hideNewPassword2 = hide;
  }

  public get newPassword() { return this.form.get('newPassword'); }
  public get newPassword2() { return this.form.get('newPassword2'); }
  public get oldPassword() { return this.form.get('oldPassword'); }
}
