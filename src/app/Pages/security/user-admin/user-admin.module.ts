import { ChangePasswordComponent } from './change-password/change-password.component';
import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { RoleComponent } from './role/role.component';
import { UserAdminRoutingModule } from './user-admin-routing.module';
import { UserComponent } from './user/user/user.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserViewComponent } from './user/user-view/user-view.component';


@NgModule({
  declarations: [ChangePasswordComponent, MenuComponent, RoleComponent, UserComponent, UserFormComponent, UserViewComponent],
  imports: [CommonModule, ImportsModule, UserAdminRoutingModule]
})
export class UserAdminModule { }