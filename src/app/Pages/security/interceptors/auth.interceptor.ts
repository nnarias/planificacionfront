import { AuthService } from '../../login/service/auth.service';
import { catchError } from 'rxjs/operators';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import swal from 'sweetalert2';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(e => {

      //HttpStatus BadRequest
      if (e.status == 400) {
        if (e.error) {
          if (e.error.apierror) {
            swal.fire(e.error.apierror.message, e.error.apierror.debugMessage, 'warning');
          }
        }
        else {
          swal.fire("Error en el proceso", "problemas en la entrada de datos", 'warning');
        }
      }

      //HttpStatus Unauthorized
      if (e.status == 401) {
        if (this.authService.isAuthenticated()) {
          this.authService.logout();
        }
        swal.fire('Error Acceso denegado', `No tienes acceso a este recurso`, 'error');
        this.router.navigate(['']);
      }

      //HttpStatus Forbidden
      if (e.status == 403) {
        swal.fire('Error en los Roles y Permisos del Usuario', `No tienes acceso a este recurso`, 'warning');
        this.router.navigate(['app']);
      }

      //HttpStatus Not Found
      if (e.status == 404) {
        if (e.error.apierror) {
          swal.fire(e.error.apierror.message, e.error.apierror.debugMessage, 'warning');
        }
      }

      //HttpStatus Conflict
      if (e.status == 409) {
        if (e.error.apierror) {
          swal.fire(e.error.apierror.message, e.error.apierror.debugMessage, 'error');
        }

      }

      //HttpStatus 422 Unprocessable Entity
      if (e.status == 422) {
        if (e.error.apierror.subErrors) {
          let subErrorList = [];
          subErrorList = e.error.apierror.subErrors;
          let message = subErrorList.map(function (x) {
            return " " + x.message;
          })
          swal.fire(e.error.apierror.message, message.toString(), 'error');
        }
      }

      //HttpStatus Upgrade Required
      if (e.status == 426) {
        if (this.authService.isAuthenticated()) {
          this.authService.logout();
        }
        swal.fire(e.error.apierror.message, e.error.apierror.debugMessage, 'error');
        this.router.navigate(['']);
      }

      //HttpStatus Internal Server Error
      if (e.status == 500) {
        swal.fire('Error en el servidor', 'Disculpe las molestias el servidor está presentando problemas, por favor inténtelo más tarde o comuníquese con nosotros');

      }

      return throwError(e);
    }));
  }
}
