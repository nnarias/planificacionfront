import { CommonModule } from '@angular/common';
import { ImportsModule } from 'src/assets/utils/imports.module';
import { LoginComponent } from './login/login.component';
import { LoginRoutingModule } from './login-routing.module';
import { NewsLoginComponent } from './news-login/news-login.component';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  declarations: [LoginComponent, NewsLoginComponent],
  imports: [CommonModule, ImportsModule, LoginRoutingModule, SlickCarouselModule]
})
export class LoginModule { }