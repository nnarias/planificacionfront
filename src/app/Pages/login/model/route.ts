export class Route {
    icon: string;
    id: number;
    name: string;
    routeClass: string;
    title: string;
}