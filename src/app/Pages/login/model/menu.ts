import { Route } from "./route";

export class Menu {
    icon: string;
    id: number;
    identifier: string;
    name: string;
    routeList: Route[] = [];
}