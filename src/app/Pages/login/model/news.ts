export class News {
    body: string;
    endDate: string;
    id: number;
    image: string;
    startDate: string;
    subtitle: string;
    title: string;
    update: boolean;
    constructor() {
        this.update = false;
    }
}