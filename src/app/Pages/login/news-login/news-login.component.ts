import { AuthService } from '../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { News } from '../../admin/informative/model/news';

@Component({
  selector: 'app-news-login',
  templateUrl: './news-login.component.html',
  styleUrls: ['./news-login.component.scss']
})
export class NewsLoginComponent implements OnInit {
  private newsList: News[] = [];
  private slideConfig: any = { 'autoplay': true, 'autoplaySpeed': 6000, 'dots': true, 'slidesToScroll': 1, 'slidesToShow': 1 };
  private urlEndPoint: string = `${environment.apiUrl}/news/file`;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.newsList().subscribe(response => { this.newsList = response });
  }

  public get getConfig(): any {
    return this.slideConfig;
  }

  public get getNewsList(): News[] {
    return this.newsList;
  }

  public get getUrlEndpoint(): string {
    return this.urlEndPoint;
  }
}