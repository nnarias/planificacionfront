import { AuthService } from '../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { User } from '../../security/user-admin/model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private form: FormGroup = this.formBuilder.group({
    password: ['', Validators.required],
    username: ['', Validators.required]
  });
  private hide: boolean = true;
  private user: User;

  constructor(private authService: AuthService, private formBuilder: FormBuilder, private router: Router) {
    this.user = new User();
  }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      swal.fire('Login', `Hola ${this.authService.getUserLogin.name}, ya estás autentificado`, 'info');
      this.router.navigate(['app']);
    }
  }

  login(): void {
    this.user = this.form.value;
    this.authService.login(this.user).subscribe(response => {
      this.authService.saveUser(response.access_token);
      this.authService.saveToken(response.access_token);
      let user = this.authService.getUserLogin;
      this.router.navigate(['app']);
      swal.fire('Login', `Hola ${user.name}, has iniciado sesión con éxito`, 'success');
    }, error => {
      if (error.status == 400) {
        swal.fire('Error login', 'Usuario o contraseña incorrectas', 'error');
      } else if (error.status == 500) {
        swal.fire('Error login', 'Problemas con el servidor', 'error');
      }
    });
  }

  public get getForm(): FormGroup {
    return this.form;
  }

  public get getHide(): boolean {
    return this.hide;
  }

  public get password() { return this.form.get('password'); }
  public get username() { return this.form.get('username'); }

  public set setHide(hide: boolean) {
    this.hide = hide;
  }
}
