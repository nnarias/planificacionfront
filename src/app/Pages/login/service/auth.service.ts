import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { News } from '../../admin/informative/model/news';
import { Observable } from 'rxjs';
import { Password } from '../../security/user-admin/model/password';
import { User } from '../../security/user-admin/model/user';
import { UserLogin } from '../../security/user-admin/model/userLogin';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private credentials = btoa(environment.ClientUsername + ':' + environment.ClientPassword);
  private httpHeaders = new HttpHeaders({
    'Authorization': 'Basic ' + this.credentials,
    'Content-Type': 'application/x-www-form-urlencoded'
  });
  private token: string;
  private urlEndPoint: string = `${environment.apiUrl}`;
  private urlOauth: string = `${environment.oauthUrl}`;
  private userLogin: UserLogin;

  constructor(private http: HttpClient) { }

  changePassword(password: Password): Observable<any> {
    return this.http.post<any>(`${this.urlEndPoint}/user/changePassword`, password);
  }

  getTokenData(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(decodeURIComponent(escape(window.atob(accessToken.split('.')[1]))));
    }
    return null;
  }

  hasRole(roles: []): boolean {
    for (var role of roles) {
      if (this.getUserLogin.roleList.includes(role)) {
        return true;
      }
    }
    return false;
  }

  isAuthenticated(): boolean {
    let payload = this.getTokenData(this.getToken);
    if (payload != null && payload.username && payload.username.length > 0) {
      return true;
    }
    else {
      return false;
    }
  }

  login(user: User): Observable<any> {
    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', user.username);
    params.set('password', user.password);
    return this.http.post<any>(`${this.urlOauth}/token`, params.toString(), { headers: this.httpHeaders });
  }

  logout(): void {
    this.token = null;
    this.userLogin = null;
    localStorage.clear();
  }

  newsList(): Observable<News[]> {
    return this.http.get<News[]>(`${this.urlEndPoint}/news/current`, { headers: this.httpHeaders });
  }

  saveToken(accessToken: string): void {
    this.token = accessToken;
    localStorage.setItem('token', accessToken);
  }

  saveUser(accessToken: string): void {
    let payload = this.getTokenData(accessToken);
    this.userLogin = new UserLogin();
    this.userLogin.username = payload.username;
    this.userLogin.name = payload.name;
    this.userLogin.roleList = payload.authorities;
    this.userLogin.internalUSer = payload.internalUSer;
    localStorage.setItem('userLogin', JSON.stringify(this.userLogin));
  }


  public get getToken(): string {
    if (this.token != null) {
      return this.token;
    }
    this.token = localStorage.getItem('token');
    if (this.token != null) {
      return this.token;
    }
    return null;
  }

  public get getUserLogin(): UserLogin {
    if (this.userLogin != null) {
      return this.userLogin;
    }
    this.userLogin = JSON.parse(localStorage.getItem('userLogin')) as UserLogin;
    if (this.userLogin != null) {
      return this.userLogin;
    }
    return new UserLogin();
  }
}