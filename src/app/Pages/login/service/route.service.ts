import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Menu } from '../model/menu';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouteService {
  private urlEndPoint: string = `${environment.apiUrl}/route`;

  constructor(private http: HttpClient) { }

  getUserRoutes(): Observable<Menu[]> {
    return this.http.get<Menu[]>(`${this.urlEndPoint}/user`);
  }
}
