import { AuthService } from '../../login/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Menu } from '../../login/model/menu';
import { Notification } from '../../admin/informative/model/notification';
import { NotificationService } from '../../admin/informative/service/notification.service';
import { Router } from '@angular/router';
import { RouteService } from '../../login/service/route.service';
import swal from 'sweetalert2';
import { User } from '../../security/user-admin/model/user';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  private menu: Menu[];
  private notificationList: Notification[] = [];
  private user: User;

  constructor(private authService: AuthService, private notificationService: NotificationService, private router: Router, private routeService: RouteService) { }

  ngOnInit(): void {
    this.getUserNotifications();
    this.defineRoutes();
  }

  changePassword(): void {
    this.router.navigate(['app/security/user-admin/changePassword']);
  }

  defineRoutes(): void {
    //this.user = this.authService.getUser;
    this.routeService.getUserRoutes().subscribe(menu => {
      this.menu = menu;
    });
  }

  getUserNotifications(): void {
    this.notificationService.findByUserRecentNotification().subscribe(notifications => {
      this.notificationList = notifications;
    });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  logout(): void {
    this.router.navigate(['']);
    swal.fire('Logout', `Hola ${this.authService.getUserLogin.name} has cerrado sesión exitosamente`, 'success');
    this.authService.logout();
  }

  userNotificationView(notification: Notification): void {
    this.notificationList = this.notificationList.filter(notifications => notifications !== notification);
    this.router.navigate(['app/admin/informative/userNotificationView', notification.id]);
  }

  userNotificationViewList(): void {
    this.router.navigate(['app/admin/informative/userNotification']);
  }

  viewUser(): void {
    this.router.navigate(['app/security/user-admin/userView']);
  }

  public get getAuthService(): AuthService {
    return this.authService;
  }

  public get getMenu(): Menu[] {
    return this.menu;
  }

  public get getNotificationList(): Notification[] {
    return this.notificationList;
  }
}
