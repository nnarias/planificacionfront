import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
  declarations: [FooterComponent, LayoutComponent, NavbarComponent, SidebarComponent],
  imports: [CommonModule, LayoutRoutingModule]
})
export class LayoutModule { }
