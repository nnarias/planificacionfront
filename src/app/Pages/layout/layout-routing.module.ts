import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: 'admin', loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule) },
{ path: 'registry', loadChildren: () => import('../registry/registry.module').then(m => m.RegistryModule) },
{ path: 'security', loadChildren: () => import('../security/security.module').then(m => m.SecurityModule) },
{ path: 'evaluation', loadChildren: () => import('../evaluation/evaluation.module').then(m => m.EvaluationModule) },
{ path: 'tracking', loadChildren: () => import('../tracking/tracking.module').then(m => m.TrackingModule) },
{ path: 'planning', loadChildren: () => import('../planning/planning.module').then(m => m.PlanningModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
