import { Directive } from '@angular/core';
import { AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[validation]',
  providers: [{ multi: true, provide: NG_VALIDATORS, useExisting: ValidationDirective }]
})
export class ValidationDirective {

  constructor() { }

  validateAddress(control: AbstractControl): ValidationErrors {
    let address = control.value;
    let addressParts = address.split(',');
    if (addressParts.length < 4) {
      return { 'address': { 'message': 'Ingresar todos los campos requeridos (calle principal, numeración, calle secundaria y ciudad)' } };
    }
    let subErrors = addressParts.map(addressPart => {
      if (addressPart == '') {
        return 1;
      }
      if (addressPart.match(/^(?!.*[a-z0-9])^.*$/)) {
        return 2;
      }
      return 0;
    });
    if (subErrors.includes(1)) {
      return { 'address': { 'message': 'Cada parte de la dirección debe contener caracteres' } };
    }
    if (subErrors.includes(2)) {
      return { 'address': { 'message': 'Cada parte de la dirección debe contener al menos un caracter alfanumérico' } };
    }
    return null;
  }

  validateEmail(control: AbstractControl): ValidationErrors {
    let email: String = control.value;

    //Valor mínimo permitido para email dado por email de ejemplo aa@aa.aa mínimo válido
    if (email.length < 8) {
      return { 'cemail': { 'message': 'El email debe contener al menos 8 caracteres' } };
    }

    //Se requiere un @ y un . para un email, se comprueba mediante la función lncludes que busca una cadena específica dentro de una cadena, en este caso @ y .
    if (!email.includes('@')) {
      return { 'cemail': { 'message': 'El email debe contener un (@)' } };
    }

    //Split separa en dos cadenas un email válido separándolo por el arroba aa@aa.aa lo separa en aa y aa.aa
    let emailparts = email.split('@');

    //Como ya se comprueba previamente que existe al menos un símbolo @, ahora se verifica si no existen más de uno, de ser el caso la cadena se separara en un array de más de 3 dígitos
    if (emailparts.length > 2) {
      return { 'cemail': { 'message': 'El email solo puede contener un (@)' } };
    }

    if (!emailparts[1].includes('.')) {
      return { 'cemail': { 'message': 'El email debe contener al menos un (.) luego del (@)' } };
    }

    if (email.endsWith('.')) {
      return { 'cemail': { 'message': 'El email no puede terminar en (.)' } };
    }

    //Se comprueba que exitan al menos 2 caracteres antes del @, cumpliendo con el mínimo permitido aa@aa.aa, como se separó el email en 2 partes, la primera corresponde a aa antes del @
    //Por lo tanto se verifica su longitud, debe ser mayor o igual a 2
    if (emailparts[0].length < 2) {
      return { 'cemail': { 'message': 'Deben existir al menos 2 caracteres antes del (@)' } };
    }

    //Ahora se verifican los dos datos después del @, por lo que se utiliza otro split para separar nuevamente la cadena antes y después del punto, y se comprueba su longitud
    if (emailparts[1].split('.')[0].length < 2) {
      return { 'cemail': { 'message': 'Deben existir al menos 2 caracteres despés del (@) y antes del (.)' } };
    }


    //Ahora se verifican los dos datos después del ., por lo que se utiliza otro split para separar nuevamente la cadena antes y después del punto, y se comprueba ahora la longitud de la cadena luego del punto
    if (emailparts[1].split('.')[1].length < 2) {
      return { 'cemail': { 'message': 'Deben existir al menos 2 caracteres despés del primer (.) luego del (@)' } };
    }

    if (emailparts[0].match(/^(?=.*([.])\1{1}).+$/)) {
      return { 'cemail': { 'message': 'No deben existir puntos consecutivos' } };
    }

    if (emailparts[1].match(/^(?=.*([.])\1{1}).+$/)) {
      return { 'cemail': { 'message': 'No deben existir puntos consecutivos' } };
    }

    if (email.match(/^(?=.*[A-Z])^.*$/)) {
      return { 'cemail': { 'message': 'El email no debe contener mayúsculas' } };
    }

    //Primer dígito del email debe ser un valor alfanumético se comrpueba con un pattern haciendo un substrisg del primer elemento, esto se puede reemplazar con charAt que extrae un elemento específico
    //de la cadena
    if (email.substr(0, 1).match(/^(?!.*[a-z0-9])^.*$/)) {
      return { 'cemail': { 'message': 'El email debe comenzar con un valor alfanumérico' } };
    }

    if (emailparts[0].substr(1).match(/^(?!.*[a-z0-9._\-])^.*$/)) {
      return { 'cemail': { 'message': 'El email solo puede contener guiones o puntos' } };
    }

    if (!emailparts[1].includes('.')) {
      return { 'cemail': { 'message': 'Falta el símbolo (.) El email debe manejar la estructura (algo@example.com)' } };
    }

    if (emailparts[1].match(/^(?!.*[a-z0-9._\-])^.*$/)) {
      return { 'cemail': { 'message': 'El email solo puede contener guiones o puntos' } };
    }

    return null;
  }

  validateFax(control: FormGroup): ValidationErrors {
    let fax = control.value;
    if (fax.length < 6) {
      return { 'fax': { 'message': 'El número de fax debe contener al menos 6 dígitos' } };
    }
    if (fax.match(/[^0-9a-zA-Z\s()+\-]/)) {
      return { 'fax': { 'message': 'El número de fax no puede contener caracteres especiales' } };
    }
    if (!fax.match(/^(\(\+[0-9]{1,3}\)|\+[0-9]{1,3})? ?-?[0-9]{1,3} ?-?[0-9]{2,5} ?-?[0-9]{2,4}$/)) {
      return { 'fax': { 'message': 'El número de fax está en un formato no válido' } };
    }
    return null;
  }

  validatePassword(control: AbstractControl): ValidationErrors {
    let password: String = control.value;

    if (password.match(/^(?!.*[A-Z])^.*$/)) {
      return { 'password': { 'message': 'La contraseña debe contener al menos una mayúscula' } };
    }

    if (password.match(/^(?!.*[a-z])^.*$/)) {
      return { 'password': { 'message': 'La contraseña debe contener al menos una minúscula' } };
    }

    if (password.match(/^(?!.*[0-9])^.*$/)) {
      return { 'password': { 'message': 'La contraseña debe contener al menos un dígito' } };
    }

    if (password.match(/^(?!.*[*+ºª|!"@·#$~%€&¬/()=?¿¡çÇ}^{\',;.:\-\[\]])^.*$/)) {
      return { 'password': { 'message': 'La contraseña debe contener al menos un caracter especial' } };
    }

    if (password.match(/^(?=.*[\´\¨])^.*/)) {
      return { 'password': { 'message': 'Evita usar los caracteres ´ y ¨ por si solos pueden causar confusión' } };
    }
    return null;
  }

  validatePasswords(control: FormGroup): ValidationErrors {
    let oldPassword = control.get('oldPassword').value;
    let newPassword = control.get('newPassword').value;
    let newPassword2 = control.get('newPassword2').value;
    if (newPassword != newPassword2) {
      return { 'passwords': { 'message': 'Las contraseñas no coinciden' } };
    }
    if (oldPassword == newPassword) {
      return { 'passwords': { 'message': 'No puedes utilizar la misma contraseña' } };
    }
    return null;
  }

  validatePhone(control: FormGroup): ValidationErrors {
    let phone = control.value;
    if (phone.length < 7) {
      return { 'phone': { 'message': 'El número telefónico debe contener al menos 7 dígitos' } };
    }
    if (phone.match(/[^0-9a-zA-Z\s()+\-]/)) {
      return { 'phone': { 'message': 'El número telefónico no puede contener caracteres especiales' } };
    }
    if (!phone.match(/^(\(\+[0-9]{1,3}\)|\+[0-9]{1,3})? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{3,4}( ?-?[0-9]{1,3}|[a-z]{3,10} ?[0-9]{1,6})?$/)) {
      return { 'phone': { 'message': 'El número telefónico está en un formato no válido' } };
    }
    return null;
  }

  validateUrl(control: AbstractControl): ValidationErrors {
    let url = control.value;
    let urlParts = url.split('.');
    if (url.length < 10) {
      return { 'url': { 'message': 'La página web debe contener al menos 10 caracteres' } };
    }
    if (url.substr(0, 1).match(/^(?!.*[a-z0-9])^.*$/)) {
      return { 'url': { 'message': 'La pagina web debe comenzar con un valor alfanumérico' } };
    }
    if (url.match(/^(?=.*[*+ºª|!"·#$~%€&¬()=?¿¡çÇ}^{\',;\[\]\´\¨])^.*$/)) {
      return { 'url': { 'message': 'La pagina web tiene caracteres no permitidos' } };
    }
    if (url.endsWith('.')) {
      return { 'url': { 'message': 'La página web no puede terminar en (.)' } };
    }
    let subErrors = urlParts.map(part => {
      if (part.substr(0, 1).match(/^(?!.*[a-z0-9])^.*$/)) {
        return 1;
      }
      if (part.substr(part.length - 1, part.length).match(/^(?!.*[a-z0-9])^.*$/)) {
        return 2;
      }
      return 0;
    });
    if (subErrors.includes(1)) {
      return { 'url': { 'message': 'Cada parte de la pagina web debe comenzar con un valor alfanumérico' } };
    }
    if (subErrors.includes(2)) {
      return { 'url': { 'message': 'Cada parte de la pagina web debe terminar con un valor alfanumérico' } };
    }
    if (url.match(/^(?=.*([_.\-])\1{1}).+$/)) {
      return { 'url': { 'message': 'No deben existir puntos o guiones consecutivos' } };
    }
    if (!url.match(/^((http:\/\/|https:\/\/)?)?(www.)?[0-9a-z_\-]{3,}\.[0-9a-z_\-]{2,}/)) {
      return { 'url': { 'message': 'La página web está en un formato no válido' } };
    }
    return null;
  }
}

export function address(): ValidatorFn {
  return (control: AbstractControl) => {
    let validation = new ValidationDirective();
    return validation.validateAddress(control);
  }
}

export function customEmail(): ValidatorFn {
  return (control: AbstractControl) => {
    let validation = new ValidationDirective();
    return validation.validateEmail(control);
  }
}

export function fax(): ValidatorFn {
  return (control: FormGroup) => {
    let validation = new ValidationDirective();
    return validation.validateFax(control);
  }
}

export function password(): ValidatorFn {
  return (control: AbstractControl) => {
    let validation = new ValidationDirective();
    return validation.validatePassword(control);
  }
}

export function passwords(): ValidatorFn {
  return (control: FormGroup) => {
    let validation = new ValidationDirective();
    return validation.validatePasswords(control);
  }
}

export function phone(): ValidatorFn {
  return (control: FormGroup) => {
    let validation = new ValidationDirective();
    return validation.validatePhone(control);
  }
}

export function url(): ValidatorFn {
  return (control: FormGroup) => {
    let validation = new ValidationDirective();
    return validation.validateUrl(control);
  }
}