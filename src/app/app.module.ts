import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthInterceptor } from './Pages/security/interceptors/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { getSpanishPaginatorIntl } from 'src/assets/utils/spanish-paginator-intl';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MomentUtcDateAdapter } from 'src/assets/utils/MomentUtcDateAdapter';
import { NgModule } from '@angular/core';
import { TokenInterceptor } from './Pages/security/interceptors/token.interceptor';
import { ValidationDirective } from './validators/validation.directive';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [AppComponent, ValidationDirective],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MatSelectModule,
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentUtcDateAdapter },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() },
  ],
})
export class AppModule {}
